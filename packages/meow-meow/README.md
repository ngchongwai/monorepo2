# Gamegeon Slot Games Engine 

## Requirements
- node 8 (LTS)
- webpack 4

## Development
- `npm install`
- `npm run start`

## Compile
- `npm run build`

## Author
Ray (ray.low@majagames.com)
