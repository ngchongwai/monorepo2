const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = (env) => {

    return {
        entry: './src/index.js',
        output: {
            filename: 'bundle.[chunkhash].js',
            path: path.resolve(__dirname, 'dist')
        },
        devServer: {
            contentBase: './dist'
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    loader: "file-loader?name=[name].[ext]"
                },
                {
                    test: /\.tsx?$/,
                    loaders: ['babel-loader', 'ts-loader'],
                    exclude: /node_modules/
                },
                {
                    'test': /\.(jsx?)$/,
                    loaders: ['babel-loader'],
                    exclude: /node_modules/
                },
            ]
        },
        resolve: {
            extensions: ['.ts', '.js'],
            alias: {
                environment: path.resolve(__dirname, `src/environments/environment.${env.environment}.ts`),
            }
        },
        plugins: [
            new CleanWebpackPlugin(['dist']),
            new HtmlWebPackPlugin({
                template: "./src/index.html",
                filename: "./index.html"
            }),
            new CopyWebpackPlugin([
                {from: 'src/core/assets/image', to: 'assets/core'},
                {from: 'src/core/assets/audio', to: 'assets/sounds', force: true},
                {from: 'src/core/assets/particles', to: 'assets/particles', force: true},
                {from: 'src/assets', to: 'assets', force: true},
                {from: 'src/core/languages', to: 'assets', force: true},
            ]),
        ]
    };
};
