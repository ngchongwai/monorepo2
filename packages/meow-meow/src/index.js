import app, { checkCanInit } from './core/app';
import game from './game';
import utils from './core/utils';
import loading from './core/loading';
const module = require('node_modules');

PIXI.utils.sayHello('Gamegeon: Meow Meow');

let isMobile = app.isMobile();
let loader = app.getLoader();

let lang = (utils.getQueryParam('lang') === 'en' || utils.getQueryParam('lang') === 'cn') ? utils.getQueryParam('lang') : 'en';
loader.add('gameLanguageFile', `./assets/languages/${lang}/game_text.json`);

loader
    .add('slotBackground', './assets/Image_background_behine_slot.png')

    .add('winGlow1', './assets/win_animation_frame_glow01.png')
    .add('winGlow2', './assets/win_animation_frame_glow02.png')
    .add('winGlow3', './assets/win_animation_frame_glow03.png')
    .add('winGlow4', './assets/win_animation_frame_glow04.png')

    //! Totems folder (totems)
    .add('totemA', './assets/spine/totems/totem_a.json')
    .add('totemB', './assets/spine/totems/totem_b.json')
    .add('totemC', './assets/spine/totems/totem_c.json')
    .add('totemD', './assets/spine/totems/totem_d.json')
    .add('totemE', './assets/spine/totems/totem_e.json')
    .add('totemF', './assets/spine/totems/totem_f.json')
    .add('totemG', './assets/spine/totems/totem_g.json')
    .add('totemH', './assets/spine/totems/totem_h.json')

    //! character
    .add('characterAtlas', 'assets/spine/fm_anime.atlas')
    .add('characterJson', 'assets/spine/fm_anime.json')

    // frame
    .add('slotFrame', './assets/frame.png')

if (isMobile) {
    loader
    //! Image ssset
        .add('bg', './assets/spine/background_p.json')

        //!language
        //! logo
        .add('logo', `./assets/languages/${lang}/title_p.png`)

        .add('infoPayout', `./assets/languages/${lang}/paytable_payout_p.png`)
}
else if (!isMobile) {
    loader
    //! Icons folder (icons)
        .add('bg', './assets/spine/background_l.json')

        //!language
        //! logo
        .add('logo', `./assets/languages/${lang}/title_l.png`)

        .add('infoPayout', `./assets/languages/${lang}/paytable_payout_l.png`)
}

loader
    .on('start', function () {
        loading.start({
            loadingRectColor: 0xFFBE33,
        });
    });

// start the assets and init game when load completed.
loader.on('complete', function () {
    if (checkCanInit()) {
        game.initSound();
        game.init();
    }
});
