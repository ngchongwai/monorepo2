export default {
    environmentName: 'localhost',
    gameName: 'meow-meow',
    debug: true,
    allowCheatMode: true,
    googleAnalyticCode: 'UA-124821115-1',
}
