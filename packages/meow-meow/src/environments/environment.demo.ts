export default {
    environmentName: 'demo',
    gameName: 'meow-meow',
    debug: false,
    allowCheatMode: false,
    googleAnalyticCode: 'UA-124821115-2',
}
