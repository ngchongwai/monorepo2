export default {
    environmentName: 'production',
    gameName: 'meow-meow',
    debug: false,
    allowCheatMode: false,
    googleAnalyticCode: 'UA-124821115-3',
}
