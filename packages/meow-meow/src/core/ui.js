import * as PIXI from 'pixi.js';
import 'gsap';
import { each, find } from 'lodash';

import app, { isMobile } from './app';
import sound from './sound';
import state from "./state";
import utils from './utils';

let loaded = PIXI.loader.resources;

// todo: document for options
// texture, clickedTexture, hoverTexture, onPointerUp, onPointerDown
function createToggleButton(options) {

    let toggleCondition = false;

    let button = new PIXI.Sprite(options.textureOff);

    button.interactive = true;
    button.buttonMode = true;

    button.on('pointerup', function () {
        selecttarget();
    });

    button.on('pointerdown', function (event) {
        if (event.buttons === 2) {
            return;
        }

        toggleCondition ? (options.textureDownOn ? (this.texture = options.textureDownOn) : (this.alpha = 0.5)) : (options.textureDownOn ? (this.texture = options.textureDownOff) : (this.alpha = 0.5));

        if (options.sound) {
            options.sound.play();
        } else {
            sound.play('buttonClickSound');
        }

        if (options.onPointerDown) {
            options.onPointerDown();
        }
    });

    button.on('pointerover', function () {
        if (options.textureHover && this.texture !== options.textureOn && !toggleCondition) {
            this.texture = options.textureHover;
        }
        if (options.textureHover2 && this.texture == options.textureOn && toggleCondition) {
            this.texture = options.textureHover2;
        }
    });

    button.on('pointerout', function () {
        if (this.texture === options.textureHover && !toggleCondition) {
            this.texture = options.textureOff;
        }
        if (this.texture === options.textureHover2 && toggleCondition) {
            this.texture = options.textureOn;
        }
        this.alpha = 1;
    })

    button.on('pointerupoutside', function () {
        selecttarget();
        //     if (!toggleCondition) {
        //         this.texture = options.textureOff;
        //     }
        //     if (toggleCondition) {
        //         this.texture = options.textureOn;
        //     }
        //     this.alpha = 1;
    });

    if (options.centerAnchor) {
        button.anchor.set(0.5);
    }

    if (options.text) {
        const newOptions = Object.assign({
            fontFamily: 'Arial',
            fontSize: 30,
            fontWeight: 'normal',
            fill: '#ffffff',
            TextX: button.getBounds().width / 2,
            TextY: button.getBounds().height / 2,
        }, options);

        const text = createText({
            fontFamily: newOptions.fontFamily,
            fontSize: newOptions.fontSize,
            fontWeight: newOptions.fontWeight,
            fill: newOptions.fill,
        }, options.text);
        text.x = newOptions.TextX;
        text.y = newOptions.TextY;
        button.addChild(text);
    }

    function selecttarget() {
        if (!toggleCondition) {
            button.texture = options.textureOn;
        } else {
            button.texture = options.textureOff;
        }

        if (options.soundRelease) {
            options.soundRelease.play();
        } else {
            sound.play('buttonClickReleaseSound');
        }

        if (options.onPointerUp) {
            toggleCondition = !toggleCondition;
            options.onPointerUp();
        }
        button.alpha = 1;
    }

    return button;
}

function createButton(options) {
    let clickedDown = false;

    if (!options.texture) {
        throw new Error('texture is required.');
    }

    let button = new PIXI.Sprite(options.texture);

    button.interactive = true;
    button.buttonMode = true;

    if (options.hitArea) {
        button.hitArea = new PIXI.Rectangle(
            (-button.getBounds().width / 2) + options.hitArea.leftOffset,
            (-button.getBounds().height / 2) + options.hitArea.topOffset,
            options.hitArea.width,
            options.hitArea.height
        );
    }

    if (app.isMobile()) {
        button.on('pointertap', function () {
            clickedDown = false;
            selecttarget();
        });
    }
    else {
        button.on('click', function () {
            clickedDown = false;
            selecttarget();
        });
    }

    button.on('pointerupoutside', function () {
        if (clickedDown) {
            selecttarget();
        }

        if (!options.clickedTexture) {
            this.alpha = 1;
        }
        // this.texture = options.texture;
        //
        // if (options.onPointerOutside) {
        //     options.onPointerOutside();
        // }
    });

    button.on('pointerdown', function (event) {
        if (event.buttons === 2) {
            return;
        }

        clickedDown = true;
        if (options.clickedTexture) {
            this.texture = options.clickedTexture;
        } else {
            this.alpha = 0.5;
        }
        if (options.sound) {
            options.sound.play();
        } else {
            sound.play('buttonClickSound');
        }
        if (options.onPointerDown) {
            options.onPointerDown();
        }
    });

    button.on('pointerover', function () {
        // Setup hover texture if it's provided in options
        if (options.textureHover) {
            this.texture = options.textureHover;
        }

        if (options.onPointerOver) {
            options.onPointerOver();
        }
    });

    button.on('pointerout', function () {
        if (options.textureHover) {
            this.texture = options.texture;
        }

        if (options.onPointerOut) {
            options.onPointerOut();
        }
    });

    button.on('pointerupoutside', function () {
        if (options.onPointerUpOutside) {
            options.onPointerUpOutside();
        }
    });

    if (options.centerAnchor) {
        button.anchor.set(0.5);
    }

    // todo: fixed the position
    if (options.text) {
        const newOptions = Object.assign({
            fontFamily: 'Arial',
            fontSize: 30,
            fontWeight: 'normal',
            fill: '#ffffff',
            wordWrap: false,
            wordWrapWidth: 1000,
            breakWords: false,
            breakWordsWidth: 1000,
            TextX: button.getBounds().width / 2,
            TextY: button.getBounds().height / 2,
            textAnchorX: 0.5,
            textAnchorY: 0.5,
        }, options);

        const text = createText({
            fontFamily: newOptions.fontFamily,
            fontSize: newOptions.fontSize,
            fontWeight: newOptions.fontWeight,
            fill: newOptions.fill,
            wordWrap: newOptions.wordWrap,
            wordWrapWidth: newOptions.wordWrapWidth,
            breakWords: newOptions.breakWords,
            breakWordsWidth: newOptions.breakWordsWidth,
            textAnchorX: newOptions.textAnchorX,
            textAnchorY: newOptions.textAnchorY,
        }, options.text);
        text.x = newOptions.TextX;
        text.y = newOptions.TextY;
        button.addChild(text);
    }

    function selecttarget() {
        button.alpha = 1;

        // we should not use back normal texture after button clicked if the hover texture provided
        if (!app.isMobile() && options.textureHover) {
            button.texture = options.textureHover;
        } else {
            button.texture = options.texture;
        }

        if (options.soundRelease) {
            options.soundRelease.play();
        } else {
            sound.play('buttonClickReleaseSound');
        }

        if (options.onPointerUp) {
            options.onPointerUp();
        }
    }

    return button;
}

function createTextButton(options, string = '') {
    let newOptions = Object.assign({
        fontFamily: 'Arial',
        fontSize: 36,
        fill: 0xffffff,
        align: 'center',
        fontWeight: 'normal',
    }, options);

    const button = new PIXI.Text(string, newOptions);

    button.interactive = true;
    button.buttonMode = true;

    if (app.isMobile()) {
        button.on('pointertap', function () {
            selecttarget();
        });
    }
    else {
        button.on('click', function () {
            selecttarget();
        });
    }

    button.on('pointerdown', function (event) {
        if (event.buttons === 2) {
            return;
        }

        if (options.onPointerDown) {
            options.onPointerDown();
        }
        if (options.sound) {
            options.sound.play();
        } else {
            sound.play('buttonClickSound');
        }
    });

    button.on('pointerupoutside', function () {
        selecttarget();
    });

    if (options.centerAnchor) {
        button.anchor.set(0.5);
    }

    function selecttarget() {
        if (options.soundRelease) {
            options.soundRelease.play();
        } else {
            sound.play('buttonClickReleaseSound');
        }

        if (options.onPointerUp) {
            options.onPointerUp();
        }
    }

    return button;
}

function createSlider(options) {

    const newOptions = Object.assign({
        min: 0,
        max: 100,
        step: 10,
        buttonHitBox: isMobile() ? 30 : 20,
    }, options);

    let valueOptions = [];
    if (newOptions.options) {
        valueOptions = newOptions.options;
    } else {
        const range = newOptions.max - newOptions.min;
        const everyStepValue = range / newOptions.step;
        for (let i = 0; i < newOptions.step; i++) {
            valueOptions.push(newOptions.min + ((i + 1) * everyStepValue));
        }
    }

    const slider = new PIXI.Sprite(PIXI.loader.resources.scrollBar.texture);
    const sliderHighlight = new PIXI.Sprite(PIXI.loader.resources.scrollBarHighlight.texture);
    slider.anchor.set(0, 0.5);
    sliderHighlight.anchor.set(0, 0.5);
    sliderHighlight.width = 0;

    const sliderButton = new PIXI.Sprite(PIXI.loader.resources.scrollButton.texture);
    sliderButton.anchor.set(0.5);
    sliderButton.buttonMode = true;
    sliderButton.interactive = true;
    if (newOptions.buttonHitBox){
        sliderButton.hitArea = new PIXI.Rectangle(-newOptions.buttonHitBox, -newOptions.buttonHitBox, 2*newOptions.buttonHitBox, 2*newOptions.buttonHitBox);
    }

    slider.addChild(sliderHighlight);
    slider.addChild(sliderButton);

    sliderButton.on('pointerdown', (event) => {
        if (event.buttons === 2) {
            return;
        }

        sliderButton.dragging = true;
        if (options.sound) {
            options.sound.play();
        } else {
            sound.play('buttonClickSound');
        }
    });

    sliderButton.on('pointerup', () => {
        sliderButton.dragging = false;
        if (options.soundRelease) {
            options.soundRelease.play();
        } else {
            sound.play('buttonClickReleaseSound');
        }
    });

    sliderButton.on('pointerupoutside', () => {
        sliderButton.dragging = false;
    });

    sliderButton.on('pointermove', (event) => {

        if (!sliderButton.dragging) {
            return;
        }

        let newX = event.data.getLocalPosition(slider).x;
        const maxWidth = slider.width;

        // make sure button only slide in slider range
        if (newX <= 0) {
            return;
        }

        // make sure newX hit the max width
        if (newX > maxWidth) {
            newX = maxWidth;
        }

        // note: if 5 value options available, then we should allow 5 steps
        const everyStepWidth = maxWidth / (valueOptions.length - 1);
        const currentStep = Math.floor(newX / everyStepWidth);
        newX = everyStepWidth * currentStep;
        const value = valueOptions[currentStep];

        sliderButton.x = newX;
        sliderHighlight.width = newX;
        slider.emit('valueUpdated', value);

    });

    // helpers
    slider.setToMax = () => { // todo this function not complete need revise
        slider.children[0].x = slider.width;
    };

    slider.setToMin = () => {
        each(slider.children, (child) => {
            child.x = 0;
        })

        sliderHighlight.width = 0;
    };

    slider.setToIndex = (index) => {
        if (index > valueOptions.length - 1) {
            sliderButton.x = slider.width;
            sliderHighlight.width = slider.width;
            const value = valueOptions[valueOptions.length - 1];
            slider.emit('valueUpdated', value);
            return;
        }
        else if (index < 0) {
            slider.children[0].x = 0;
            const value = valueOptions[0];
            slider.emit('valueUpdated', value);
            return;
        }

        const everyStepWidth = slider.width / (valueOptions.length - 1);
        const currentStep = index;
        const newX = everyStepWidth * currentStep;
        const value = valueOptions[currentStep];
        sliderButton.x = newX;
        sliderHighlight.width = newX;
        slider.emit('valueUpdated', value);
    };

    slider.setToValue = (value) => {
        if (!valueOptions.includes(value)) {
            throw new Error('The slider does not contain the value that you want to set it to');
        }

        const currentStep = valueOptions.indexOf(value);
        const everyStepWidth = slider.width / (valueOptions.length - 1);
        const newX = everyStepWidth * currentStep;
        sliderButton.x = newX;
        sliderHighlight.width = newX;
        slider.emit('valueUpdated', value);
    };

    slider.getValue = () => {
        return slider;
    };

    slider.setRange = (range) => {
        valueOptions = range;
    }

    return slider;
}

function createRadioGroup(options) {
    const newOptions = Object.assign({
        offset: 10,
        options: [],
        fontSize: 14,
    }, options);

    const container = new PIXI.Container();

    each(newOptions.options, (option, index) => {
        const button = new PIXI.Sprite(newOptions.offTexture);
        button.buttonMode = true;
        button.interactive = true;
        button.value = option.value;
        button.x = (index * (button.width + newOptions.offset));
        button.y = 0;
        button.anchor.set(0.5);
        button.selected = false;
        button.on('pointerup', () => {
            selecttarget();
        });

        button.on('pointerdown', (event) => {
            if (event.buttons === 2) {
                return;
            }

            //! button Press sound
            if (options.sound) {
                options.sound.play();
            } else {
                sound.play('buttonClickSound');
            }
        });

        button.on('pointerupoutside', () => {
            selecttarget();
        });

        function selecttarget() {
            // set all other button grey out
            each(container.children, (otherButton) => {
                otherButton.texture = newOptions.offTexture;
                if (otherButton.selected && otherButton !== button) {
                    otherButton.selected = false;
                }
                // otherButton.texture = PIXI.Texture.EMPTY;
            });

            //! button Release sound
            if (options.soundRelease) {
                options.soundRelease.play();
            } else {
                sound.play('buttonClickSound');
            }

            button.selected = !button.selected;
            button.texture = button.selected ? newOptions.onTexture : newOptions.offTexture;

            let selectedButton = find(container.children, (tempButton) => {
                return tempButton.selected;
            });

            container.emit('valueChanged', selectedButton ? selectedButton.value : 0);
        }

        const text = createText({
            fontSize: newOptions.fontSize,
            fontWeight: 'lighter',
            fill: '#ffffff'
        }, option.label);

        button.addChild(text);
        container.addChild(button);
    });

    return container;
}

function createText(options, string = '') {
    let newOptions = Object.assign({
        // Font
        fontFamily: 'Arial',
        fontSize: 36,
        fontStyle: 'normal',
        fontVariant: 'normal',
        fontWeight: 'normal',

        // Fill
        fill: 0xffffff,
        fillGradientType: 0,
        fillGradientStops: [],

        // Stroke
        stroke: '#ffffff',
        strokeThickness: 0,
        lineJoin: 'miter',
        miterLimit: 10,

        //Layout
        letterSpacing: 0,
        textBaseline: 'alphabetical',

        // Drop Shadow
        dropShadow: false,
        dropShadowColor: '#ffffff',
        dropShadowAlpha: 1,
        dropShadowAngle: 0.5235987755982988,
        dropShadowBlur: 0,
        dropShadowDistance: 5,

        // Word Wrap
        wordWrap: false,
        breakWords: false,
        align: 'center',
        whiteSpace: 'pre',
        wordWrapWidth: 100,
        lineHeight: 0,

        // Padding
        padding: 0,
        trim: false,

        // anchor
        textAnchorX: 0.5,
        textAnchorY: 0.5,
    }, options);

    let sizeMultiplier = 1.75;
    newOptions.fontSize = newOptions.fontSize * sizeMultiplier;

    const text = new PIXI.Text(string, newOptions);
    text.anchor.set(newOptions.textAnchorX, newOptions.textAnchorY);
    text.scale.set(1 / sizeMultiplier);

    // Helpers

    // This will allow us to update the text size during run time
    text.updateFontSize = (fontSize) => {
        text.style.fontSize = fontSize * sizeMultiplier;
        text.scale.set(1 / sizeMultiplier);
    };

    return text;
}

function createTextBox(options) {

    const newOptions = Object.assign({
        fontSize: 30,
        fontWeight: 'normal',
        fill: '#ffffff',
    }, options);

    if (!options.texture) {
        throw new Error('texture is required.');
    }

    let display = new PIXI.Sprite(options.texture);

    if (options.centerAnchor) {
        display.anchor.set(0.5);
    }

    if (options.text) {
        const text = createText({
            fontSize: newOptions.fontSize,
            fontWeight: newOptions.fontWeight,
            fill: newOptions.fill,
        }, options.text);
        text.x = display.x + 90;
        text.y = display.y + 28;
        display.addChild(text);
    }

    display.text = (string) => {
        display.children[0].text = string;
    };

    return display;
}

function createSpriteNumbers(option, numberTextures, string, needBacking = true, backingPositioningY = null) {
    if (!string) {
        throw new Error('createSpritedText requires a string');
    }
    if (!numberTextures) {
        throw new Error('createSpritesText requires the textures for the string');
    }

    let numberContainer = new PIXI.Container();
    let charactersArray = string.split('');

    //! scaling need avoid odd number && after scaling smaller than 0.40 will cause backing pixel blur for the border pixel
    let options = Object.assign({
        scaling: app.isMobile() ? 0.35 : 0.55,
        offset: 153.8
    }, option);

    let scaling = options.scaling;
    let offset = options.offset;

    if (needBacking) {
        let numberStartSpacing = new PIXI.Sprite(loaded.numberBackStart.texture);
        numberStartSpacing.x = 0;
        numberStartSpacing.y = backingPositioningY ? backingPositioningY : 0;
        numberStartSpacing.scale.set(scaling, scaling);
        numberStartSpacing.name = 'start space sprite';
        numberContainer.addChild(numberStartSpacing);
    }

    each(charactersArray, (character, index) => {
        //init backing for number sprite
        if (needBacking) {
            let backing = new PIXI.Sprite(loaded.numberBackBody.texture);
            backing.x = offset * (index + 1) * scaling;
            backing.y = backingPositioningY ? backingPositioningY : 0;
            backing.scale.set(scaling, scaling);
            numberContainer.addChild(backing);
        }

        //init number sprite
        let currentTexture = numberTextures[character];
        let currentSprite = new PIXI.Sprite(currentTexture);
        currentSprite.x = offset * (index + (needBacking ? 1 : 0)) * scaling + (needBacking ? 11 : 0);
        currentSprite.y = needBacking ? 15 : 0;
        currentSprite.scale.set(scaling - 0.08, scaling - 0.08);
        numberContainer.addChild(currentSprite);
    });

    if (needBacking) {
        let numberEndSpacing = new PIXI.Sprite(loaded.numberBackEnd.texture);
        numberEndSpacing.x = offset * (charactersArray.length + 1) * scaling;
        numberEndSpacing.y = backingPositioningY ? backingPositioningY : 0;
        numberEndSpacing.scale.set(scaling, scaling);
        numberEndSpacing.name = 'end space sprite';
        numberContainer.addChild(numberEndSpacing);
    }

    numberContainer.interactiveChildren = false;
    numberContainer.cacheAsBitmap = true;

    return numberContainer;
}

//! flashing a target to the passed color
function flash(target, color, flashTimes) {
    for (let i = 0; i < flashTimes; i++) {
        TweenMax.delayedCall((0.2 * i), function () {
            target.style.fill = color
        });
        TweenMax.delayedCall((0.2 * i) + 0.1, function () {
            target.style.fill = 0xFFFFFF
        });
    }
}

function drawLine(points, color = 0xff0000) {

    let line = new PIXI.Graphics();
    line.lineStyle(3, color, 1);

    // move the cursor to initial position
    line.moveTo(points[0].x, points[0].y);

    // draw the line based on points provided
    for (let i = 1; i < points.length; i++) {
        line.lineTo(points[i].x, points[i].y);
    }

    return line;
}

function drawParticleLine(points, isSpine, offset, texture) {
    let particlesContainer = new PIXI.Container();

    //Init First particle
    let currentPosition = points[0];
    let particle = isSpine ? new PIXI.spine.Spine(texture) : new PIXI.Sprite(texture);
    particle.x = currentPosition.x;
    particle.y = currentPosition.y;
    particle.anchor.set(0.5);
    particlesContainer.addChild(particle);

    // draw the line based on points provided
    for (let i = 1; i < points.length; i++) {
        let differentNumber = {
            x: points[i].x - points[i - 1].x,
            y: points[i].y - points[i - 1].y
        };
        let direction = utils.normalize2DVector(differentNumber);
        // let counter = 1;
        // while(currentPosition.x !== points[i].x) {
        for (let i = 0; i < 68; i++) {
            currentPosition.x += direction.x * (offset * i);
            currentPosition.y += direction.y * (offset * i);

            let particle = isSpine ? new PIXI.spine.Spine(texture) : new PIXI.Sprite(texture);
            particle.x = currentPosition.x;
            particle.y = currentPosition.y;
            particle.anchor.set(0.5);
            particlesContainer.addChild(particle);
        }

        // }
    }

    return particlesContainer;
}

/**
 * Bring sprite to front layer, inside particular container
 * Params: _sprite: sprite you want to bring in front
 *         -parent: The container where the sprite is located
 * */
function bringToFront(_sprite, _parent) {
    const sprite = (typeof(_sprite) !== "undefined") ? _sprite.target || _sprite : this;
    const parent = _parent || sprite.parent || {"children": false};
    if (parent.children) {
        for (let keyIndex in sprite.parent.children) {
            if (sprite.parent.children[keyIndex] === sprite) {
                sprite.parent.children.splice(keyIndex, 1);
                break;
            }
        }
        parent.children.push(sprite);
    }
}

function bringBehindDrawer(sprite) {
    if (!sprite)
        throw new Error('requires a sprite to be shifted');

    let drawerIndex = 0;
    each(app.stage.children, (child, index) => {
        if (!app.isMobile()) return;

        if (child.name === 'Drawer Container') {
            drawerIndex = index;
        }
    });

    bringToIndex(sprite, drawerIndex);
}

function bringToIndex(sprite, index) {
    if (!sprite)
        throw new Error('requires a sprite to be shifted');

    if (index < 0)
        throw new Error('requires an index to be specified');

    const parent = sprite.parent || {"children": false};
    if (parent.children) {
        for (let keyIndex in sprite.parent.children) {
            if (sprite.parent.children[keyIndex] === sprite) {
                sprite.parent.children.splice(keyIndex, 1);
                break;
            }
        }
        parent.children.splice(index, 0, sprite);
    }
}

function createWarning(options) {

    const newOptions = Object.assign({
        descriptionFontSize: isMobile() ? 25 : 18,
        descriptionLineHeight: isMobile() ? 30 : 22,
        descriptionPadding: isMobile() ? 25 : 18,
        descriptionWordWrapWidth: isMobile() ? 450 : 400,
        titleFontSize: isMobile() ? 35 : 20,
        titleLineHeight: 20,
        titlePadding: 20,
        titleWordWrapWidth: 300,
        buttonText: isMobile() ? 25 : 16,
        backContainerColor: 0xffffff,
        frontContainerColor: 0x000000,
        spacingForBox: isMobile() ? 70 : 70,
        boxWidth: 5,
        gotSecondButton: false,
    }, options);

    let warningOpened = false;

    const warningContainer = new PIXI.Container();
    warningContainer.name = 'Warning Container';

    const warningBoxContainer = new PIXI.Container();
    warningBoxContainer.name = 'Warning Box Container';

    let warningBackground = new PIXI.Sprite(PIXI.Texture.WHITE);
    warningBackground.name = 'Info Background';
    warningBackground.tint = 0x000000; // Tint Black
    warningBackground.width = app.getResolutionX();
    warningBackground.height = app.getResolutionY();
    warningBackground.alpha = 0.5;
    warningBackground.interactive = true;

    const descriptionStyle = new PIXI.TextStyle({
        align: "center",
        fill: "white",
        fontSize: newOptions.descriptionFontSize,
        lineHeight: newOptions.descriptionLineHeight,
        padding: newOptions.descriptionPadding,
        wordWrap: true,
        wordWrapWidth: newOptions.descriptionWordWrapWidth,
    });

    const descriptionText = new PIXI.Text(newOptions.warningInfo.text, descriptionStyle);
    descriptionText.anchor.set(0.5);
    descriptionText.y = !newOptions.warningInfo.title ? newOptions.spacingForBox / 10 : newOptions.spacingForBox / 4;

    const titleStyle = new PIXI.TextStyle({
        align: "center",
        fill: "white",
        fontWeight: "bold",
        fontSize: newOptions.titleFontSize,
        lineHeight: newOptions.titleLineHeight,
        padding: newOptions.titlePadding,
        wordWrap: true,
        wordWrapWidth: newOptions.titleWordWrapWidth,
    });

    const titleText = new PIXI.Text(newOptions.warningInfo.title, titleStyle);
    titleText.anchor.set(0.5, 0);
    titleText.x = descriptionText.x;
    titleText.y = descriptionText.y - (descriptionText.height / 2) - titleText.height - (newOptions.titlePadding / 2);

    let buttonOffsetY = (newOptions.gotSecondButton && app.isMobile()) ? !newOptions.warningInfo.title ? newOptions.spacingForBox / 10 * 8.5 : newOptions.spacingForBox / 4 * 3 : 72;

    let exitWarningButton = createButton({
        texture: newOptions.warningInfo.buttonTexture,
        text: newOptions.warningInfo.buttonText,
        fontSize: newOptions.buttonText,
        fill: '#FFFFFF',
        onPointerUp: newOptions.onPointerUp ? () => {
            newOptions.onPointerUp();
        } : () => {
            closedOpenWarning();
        }
    });
    exitWarningButton.x = descriptionText.x - (newOptions.gotSecondButton ?  -5 : exitWarningButton.width / 2);
    exitWarningButton.y = app.isMobile() ? descriptionText.y + descriptionText.height / 2 + buttonOffsetY - exitWarningButton.height / 2 : descriptionText.y + descriptionText.height / 2 + 45 - exitWarningButton.height / 2;

    // second Button
    let secondButton;
    if(newOptions.gotSecondButton) {
        secondButton = createButton({
            texture: newOptions.warningInfo.buttonTexture,
            text: loaded.languageFile.data.repeat,
            fontSize: newOptions.buttonText,
            fill: '#FFFFFF',
            onPointerUp: newOptions.onPointerUp2 ? () => {
                newOptions.onPointerUp2();
            } : () => {
                closedOpenWarning();
            }
        });
        secondButton.x = descriptionText.x - (exitWarningButton.width + 10);
        secondButton.y = app.isMobile() ? descriptionText.y + descriptionText.height / 2 + buttonOffsetY - secondButton.height / 2 : descriptionText.y + descriptionText.height / 2 + 45 - secondButton.height / 2;
    }

    const backContainer = new PIXI.Graphics();
    backContainer.beginFill(newOptions.backContainerColor);
    backContainer.drawRoundedRect(
        (newOptions.gotSecondButton && app.isMobile()) ? -200 / 2 : 0,
        !newOptions.warningInfo.title ? 0 : -titleText.height - newOptions.titlePadding / 2,
        descriptionText.width + newOptions.spacingForBox / 2 + 2 * newOptions.boxWidth + ((newOptions.gotSecondButton && app.isMobile()) ? 200 : 0),
        app.isMobile() ?
            !newOptions.warningInfo.title ? descriptionText.height + newOptions.spacingForBox + 2 * newOptions.boxWidth + exitWarningButton.height + newOptions.titleLineHeight : descriptionText.height + titleText.height + newOptions.spacingForBox + 2 * newOptions.boxWidth + newOptions.titlePadding / 2 + exitWarningButton.height + 2 * newOptions.titleLineHeight
            :
            !newOptions.warningInfo.title ? descriptionText.height + newOptions.spacingForBox + 2 * newOptions.boxWidth + exitWarningButton.height + newOptions.titleLineHeight : descriptionText.height + titleText.height + newOptions.spacingForBox + 2 * newOptions.boxWidth + newOptions.titlePadding / 2 + exitWarningButton.height + newOptions.titleLineHeight,
        20);
    backContainer.endFill();
    backContainer.x = -(descriptionText.width / 2) - 25;
    backContainer.y = -(descriptionText.height / 2) - 25;

    const frontContainer = new PIXI.Graphics();
    frontContainer.beginFill(newOptions.frontContainerColor);
    frontContainer.drawRoundedRect(
        (newOptions.gotSecondButton && app.isMobile()) ? -200 / 2 : 0,
        !newOptions.warningInfo.title ? 0 : -titleText.height - newOptions.titlePadding / 2,
        descriptionText.width + newOptions.spacingForBox / 2 + ((newOptions.gotSecondButton && app.isMobile()) ? 200 : 0),
        app.isMobile() ?
            !newOptions.warningInfo.title ? descriptionText.height + +newOptions.spacingForBox + exitWarningButton.height + newOptions.titleLineHeight : descriptionText.height + titleText.height + newOptions.spacingForBox + newOptions.titlePadding / 2 + exitWarningButton.height + 2 * newOptions.titleLineHeight
            :
            !newOptions.warningInfo.title ? descriptionText.height + +newOptions.spacingForBox + exitWarningButton.height + newOptions.titleLineHeight : descriptionText.height + titleText.height + newOptions.spacingForBox + newOptions.titlePadding / 2 + exitWarningButton.height + newOptions.titleLineHeight,
        20);
    frontContainer.endFill();
    frontContainer.x = -(descriptionText.width / 2) - 20;
    frontContainer.y = -(descriptionText.height / 2) - 20;

    warningBoxContainer.x = (app.getResolutionX() / 2);
    warningBoxContainer.y = (app.getResolutionY() / 2);
    //dont use visible. need alpha for tween.
    warningBoxContainer.alpha = 0;
    warningBackground.alpha = 0;

    warningContainer.addChild(warningBackground);
    warningBoxContainer.addChild(backContainer);
    warningBoxContainer.addChild(frontContainer);
    warningBoxContainer.addChild(descriptionText);
    warningBoxContainer.addChild(titleText);
    warningBoxContainer.addChild(exitWarningButton);
    if(newOptions.gotSecondButton) {
        warningBoxContainer.addChild(secondButton);
    }
    warningContainer.addChild(warningBoxContainer);
    app.addComponent(warningContainer);

    warningContainer.closedOpenWarning = closedOpenWarning;

    return warningContainer;

    function closedOpenWarning() {
        warningBoxContainer.x = (app.getResolutionX() / 2);
        warningBoxContainer.y = (app.getResolutionY() / 2);
        if (warningOpened) {
            TweenMax.fromTo(warningBoxContainer.scale, 0.2, {x: 1, y: 1}, {x: 0, y: 0});
            TweenMax.fromTo(warningBackground, 0.2, {alpha: 0.8}, {
                alpha: 0,
                onComplete: () => {
                    state.set('gotWarning', false);
                    warningBoxContainer.destroy();
                }
            })
            warningBackground.interactive = false;
            exitWarningButton.interactive = false;
            exitWarningButton.buttonMode = false;

            if(newOptions.gotSecondButton) {
                secondButton.interactive = false;
                secondButton.buttonMode = false;
            }
        }
        else {
            warningBoxContainer.alpha = 1;
            TweenMax.fromTo(warningBoxContainer.scale, 0.2, {x: 0, y: 0}, {x: 1, y: 1});
            TweenMax.fromTo(warningBackground, 0.2, {alpha: 0}, {alpha: 0.8});
            warningBackground.interactive = true;
            exitWarningButton.interactive = true;
            exitWarningButton.buttonMode = true;
            if(newOptions.gotSecondButton) {
                secondButton.interactive = true;
                secondButton.buttonMode = true;
            }
            bringToFront(warningContainer);
        }
        warningOpened = !warningOpened;
    }
}

function showDialog(content, title, buttonText = loaded.languageFile.data.okay, options = null) {
    if (!state.get('gotWarning')) {
        state.set('gotWarning', true);
        const newOptions = Object.assign({
            warning: {
                title: title,
                text: content,
                buttonText: buttonText,
                buttonTexture: PIXI.loader.resources.autoSpinPlayButton.texture
            },
            gotSecondButton: false,
            addCloseFunctionTo2Button: false,
            onPointerUp2: null
        }, options);

        if(newOptions.gotSecondButton && app.isMobile()) {
            newOptions.warning.buttonTexture = loaded.radioButtonBackgroundAnyOn.texture;
        }

        let warningContainer = createWarning({
            warningInfo: newOptions.warning,
            onPointerUp: newOptions.onPointerUp ? () => {
                newOptions.onPointerUp();
            } : () => {
                warningContainer.closedOpenWarning();
            },
            onPointerUp2: newOptions.onPointerUp2 ? () => {
                newOptions.onPointerUp2();
                console.log('in');
                if(newOptions.addCloseFunctionTo2Button) {
                    warningContainer.closedOpenWarning();
                }
            } : () => {
                warningContainer.closedOpenWarning();
            },
            gotSecondButton: newOptions.gotSecondButton
        });

        warningContainer.closedOpenWarning();
        return warningContainer;
    }

}

function shakeItem(displayObject, speed) {

    return new Promise((resolve) => {
        let shakePositionX = displayObject.x;
        let shakePositionY = displayObject.y;

        TweenMax.fromTo(displayObject, 0.20 / speed, {
            x: shakePositionX,
            y: shakePositionY,
        }, {
            repeat: 2,
            x: shakePositionX + (Math.random() * 4 - 2),
            y: shakePositionY + (Math.random() * 4 - 2),
            ease: Expo.easeInOut,
            onComplete: () => {

                TweenMax.to(displayObject, 0.10 / speed, {
                    x: shakePositionX,
                    y: shakePositionY,
                    ease: Expo.easeInOut,
                    onComplete: resolve
                })

            }
        });
    });
}

export default {
    createToggleButton,
    createButton,
    createTextButton,
    createText,
    createSlider,
    createRadioGroup,
    createTextBox,
    createSpriteNumbers,
    flash,
    drawLine,
    bringToFront,
    bringBehindDrawer,
    bringToIndex,
    createWarning,
    showDialog,
    shakeItem,
    drawParticleLine,
}
