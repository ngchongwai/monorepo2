import { each } from 'lodash';

let sounds = {};
let muted = false;
let soundMuted = false;
let musicMuted = false;

function addSound(key, sound, options) {
    options.class = 'sound';
    register(key, sound, options);
}

function addMusic(key, sound, options) {
    options.class = 'music';
    register(key, sound, options);
}

function register(key, sound, options) {
    const soundOptions = Object.assign({
        loop: false,
        volume: 1,
        muted: false,
        class: undefined
    }, options);

    sounds[key] = sound;
    sounds[key].loop = soundOptions.loop;
    sounds[key].volume = soundOptions.volume;
    sounds[key].muted = soundOptions.muted;
    sounds[key].class = soundOptions.class;
}

function set(key, options) {
    if (!key)
        throw new Error('sound.set() requires key');

    const soundOptions = Object.assign({
        loop: sounds[key].loop,
        volume: sounds[key].volume,
        muted: sounds[key].muted,
        class: sounds[key].class,
    }, options);

    sounds[key].loop = soundOptions.loop;
    sounds[key].volume = soundOptions.volume;
    sounds[key].muted = soundOptions.muted;
    sounds[key].class = soundOptions.class;
}

function get(key) {
    return sounds[key];
}

function play(key, options) {
    const newOptions = Object.assign({
        volume: sounds[key].volume,
        loop: sounds[key].loop,
    }, options);

    sounds[key].volume = newOptions.volume;
    sounds[key].play();
}

function stop(key) {
    sounds[key].stop();
}

function muteSound() {
    soundMuted = true;
    each(sounds, (sound) => {
        if (sound.class === 'sound')
            sound.muted = true;
    });
}

function unmuteSound() {
    soundMuted = false;
    each(sounds, (sound) => {
        if (sound.class === 'sound')
            sound.muted = false;
    });
}

function muteMusic() {
    musicMuted = true;
    each(sounds, (sound) => {
        if (sound.class === 'music')
            sound.muted = true;
    });
}

function unmuteMusic() {
    musicMuted = false;
    each(sounds, (sound) => {
        if (sound.class === 'music')
            sound.muted = false;
    });
}

function mute() {
    muted = true;
    each(sounds, (sound) => {
        sound.muted = true;
    });
}

function unmute() {
    muted = false;
    each(sounds, (sound) => {
        sound.muted = false;
    });
}

function isSoundMuted() {
    return soundMuted;
}

// todo music muted naming and its function work in opposite way need check and fix
function isMusicMuted() {
    return musicMuted;
}

function isMuted() {
    return muted;
}

let hidden, visibilityChange;
if (typeof document.hidden !== 'undefined') {
    hidden = 'hidden';
    visibilityChange = 'visibilitychange';
} else if (typeof document.mozHidden !== 'undefined') {
    hidden = 'mozHidden';
    visibilityChange = 'mozvisibilitychange';
} else if (typeof document.msHidden !== 'undefined') {
    hidden = 'msHidden';
    visibilityChange = 'msvisibilitychange';
} else if (typeof document.webkitHidden !== 'undefined') {
    hidden = 'webkitHidden';
    visibilityChange = 'webkitvisibilitychange';
}

document.addEventListener(visibilityChange, handleVisibilityChange, false);

function handleVisibilityChange() {
    if (document[hidden]) mute();
    else {
        if (!soundMuted)
            unmuteSound();
        if (!musicMuted)
            unmuteMusic();
    }
}

function playArray(keyArray, index = null) {
    stopArray(keyArray);
    let playKey;

    let randomizer;
    if (index === null) {
        randomizer = Math.floor((Math.random() * keyArray.length));
        playKey = keyArray[randomizer];
        index = randomizer;
    }
    else {
        let arrayException = keyArray.slice();
        arrayException.splice(index, 1);
        randomizer = Math.floor((Math.random() * arrayException.length));
        playKey = arrayException[randomizer];
        let randomArray = [];
        each(keyArray, (key, index) => {
            randomArray.push(index);
        });
        randomArray.splice(index, 1);
        index = randomArray[randomizer];
    }

    if (sounds[playKey].loop) {
        sounds[playKey].loop = false;
    }

    sounds[playKey].play({
        complete: () => {
            sounds[playKey].stop();
            playArray(keyArray, index);
        }
    });
}

function stopArray(keyArray) {
    each(keyArray, (soundName) => {
        sounds[soundName].stop();
    });
}

export default {
    addSound,
    addMusic,
    register,
    get,
    set,
    play,
    stop,
    muteSound,
    muteMusic,
    mute,
    unmuteSound,
    unmuteMusic,
    unmute,
    isSoundMuted,
    isMusicMuted,
    isMuted,
    playArray,
    stopArray
}
