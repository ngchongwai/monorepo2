// Determine browser and version
import * as Bowser from 'bowser';
import { each } from 'lodash';
import axios from 'axios';

let browserSupported = false;
let browserName = Bowser.name;
let browserVersion = Bowser.version;
let gameBrowserRequirement = [
    {'name': 'Chrome', 'version': 41},
    {'name': 'Firefox', 'version': 60},
    {'name': 'Opera', 'version': 53},
    {'name': 'Safari', 'version': 6},
    {'name': 'Microsoft Edge', 'version': 16},
    {'name': 'Internet Explorer', 'version': 6},
    {'name': 'Samsung Internet for Android', 'version': 6.2},
    {'name': 'UC Browser', 'version': 10.5},
];

// Loop through browser requirement list
each(gameBrowserRequirement, (browser) => {
    // If correct browser and version has been found, set browserSupported to true
    if (browser.name === browserName && browser.version <= parseFloat(browserVersion)) {
        browserSupported = true;
    }
});

// If no browsers from the browserRequirement list have set browserSupported to true, stops the game
if (!browserSupported) {

    let canvas = document.getElementsByTagName('canvas')[0];
    let target = document.getElementsByTagName('body')[0];
    target.removeChild(canvas);
    const newdom = document.createElement('h1');
    const chrome = document.createElement('img');
    const edge = document.createElement('img');
    const firefox = document.createElement('img');
    const safari = document.createElement('img');
    const newdom3 = document.createElement('h4');
    const newdom4 = document.createElement('h4');

    axios.get(`./assets/${lang}.json`).then((response) => {
        newdom.innerHTML = response.data.unsupported_browser;

        // styles
        newdom.style.color = 'white';
        newdom.style.fontSize = '2.0rem';
        newdom.style.fontFamily = 'sans-serif';
        newdom.style.position = 'absolute';
        newdom.style.textAlign = 'center';
        newdom.style.top = isMobile() ? '35%' : "30%";

        //! Browser image
        chrome.src = './assets/core/google.png';
        chrome.style.position = 'absolute';
        chrome.style.top = '60%';
        chrome.style.left = '32%';
        chrome.onclick = function () {
            let browserDownloadLocation = 'https://www.google.com/chrome/';
            window.open(browserDownloadLocation, "_self");
        };

        edge.src = './assets/core/edge.png';
        edge.style.position = 'absolute';
        edge.style.top = '60%';
        edge.style.left = '42%';
        edge.onclick = function () {
            let browserDownloadLocation = 'https://www.microsoft.com/en-my/windows/microsoft-edge';
            window.open(browserDownloadLocation, "_self");
        };

        firefox.src = './assets/core/firefox.png';
        firefox.style.position = 'absolute';
        firefox.style.top = '60%';
        firefox.style.left = '52%';
        firefox.onclick = function () {
            let browserDownloadLocation = 'https://www.mozilla.org/en-US/firefox/new/';
            window.open(browserDownloadLocation, "_self");
        };

        safari.src = './assets/core/safari.png';
        safari.style.position = 'absolute';
        safari.style.top = '60%';
        safari.style.left = '62%';
        safari.onclick = function () {
            let browserDownloadLocation = 'https://support.apple.com/downloads/safari';
            window.open(browserDownloadLocation, "_self");
        };

        newdom3.innerHTML = response.data.unsupported_browser_content1;
        newdom3.style.color = 'white';
        newdom3.style.fontSize = isMobile() ? '2.5rem' : '1.5rem';
        newdom3.style.fontFamily = 'sans-serif';
        newdom3.style.position = 'absolute';
        newdom3.style.textAlign = 'center';
        newdom3.style.top = '40%';

        newdom4.innerHTML = response.data.unsupported_browser_content2;
        newdom4.style.color = 'white';
        newdom4.style.fontSize = isMobile() ? '2.5rem' : '1.5rem';
        newdom4.style.fontFamily = 'sans-serif';
        newdom4.style.position = 'absolute';
        newdom4.style.textAlign = 'center';
        newdom4.style.top = '45%';

        target.appendChild(newdom);
        target.appendChild(chrome);
        target.appendChild(edge);
        target.appendChild(firefox);
        target.appendChild(safari);
        target.appendChild(newdom3);
        target.appendChild(newdom4);
    });
}

function isMobile() {
    return Bowser.mobile;
}

function isBrowserSupported() {
    return browserSupported;
}

export default {
    isMobile,
    isBrowserSupported,
}
