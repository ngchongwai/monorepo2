import * as crosstab from 'crosstab';

// to check which page need to be blank up when multiple game open in once
// let needBlank = true;
// let gamegeonLocation = (environment.environmentName === 'staging') ? 'https://staging.majagames.com/' : 'https://majagames.com/';

// Single Game Instance
// if (!isMobile()) {
//
//     let tabCount = 0;
//     each(crosstab.util.tabs, () => {
//         tabCount++;
//     });
//
//     crosstab.on('emptyPage', function () {
//
//         if (needBlank) {
//             window.open(gamegeonLocation, '_self');
//             return;
//         }
//
//         setTimeout(() => { // to make sure other page load the gamegeon site only refresh the page
//             window.location.reload(true);
//             needBlank = true;
//         }, 1000);
//     });
//
//     //! check is there more than one gamegeon game opened   ** tabCount > 2 is because added master tab
//     if (tabCount > 2) {
//         //!Stop init game and sound
//         canInit = false;
//         console.log(lang);
//         // wait the asset finish loaded only show infomation
//         setTimeout(() => {
//             let canvas = document.getElementsByTagName('canvas')[0];
//
//             let target = document.getElementsByTagName('body')[0];
//             target.removeChild(canvas);
//
//             //! declare element needed for show the message
//             const newdom = document.createElement('h1');
//             const newdom1 = document.createElement('h4');
//             const newdom2 = document.createElement('img');
//             const domReturnButton = document.createElement('BUTTON');
//             const domReturnButtonText = document.createTextNode(loaded.languageFile.data.return_lobby);
//             domReturnButton.appendChild(domReturnButtonText);
//             const domResumeButton = document.createElement('BUTTON');
//             const domResumeButtonText = document.createTextNode(loaded.languageFile.data.resume_game);
//             domResumeButton.appendChild(domResumeButtonText);
//
//             newdom.innerHTML = loaded.languageFile.data.caution;
//
//             // init element
//             newdom.style.color = 'white';
//             newdom.style.fontSize = '1.5rem';
//             newdom.style.fontFamily = 'sans-serif';
//             newdom.style.position = 'absolute';
//             newdom.style.textAlign = 'center';
//             newdom.style.transformOrigin = '50% 50%';
//             newdom.style.top = isMobile() ? '42%' : '40%';
//
//             newdom1.innerHTML = loaded.languageFile.data.duplicated_game;
//             newdom1.style.color = 'white';
//             newdom1.style.fontSize = isMobile() ? '2.5rem' : '1.5rem';
//             newdom1.style.fontFamily = 'sans-serif';
//             newdom1.style.position = 'absolute';
//             newdom1.style.textAlign = 'center';
//             newdom1.style.transformOrigin = '50% 50%';
//             newdom1.style.top = isMobile() ? '47%' : '45%';
//             newdom1.style.textAlign = 'center';
//
//             newdom2.src = "./assets/core/caution.png";
//             newdom2.style.position = 'absolute';
//             newdom2.style.textAlign = 'center';
//             newdom2.style.transformOrigin = '50% 50%';
//             newdom2.style.top = isMobile() ? '30%' : '25%';
//
//             //! init 'return lobby' Button
//             domReturnButton.style.backgroundColor = 'red';
//             domReturnButton.style.border = 'none';
//             domReturnButton.style.color = 'white';
//             domReturnButton.style.padding = '10px';
//             domReturnButton.style.textAlign = 'center';
//             domReturnButton.style.textDecoration = 'none';
//             domReturnButton.style.display = 'inline-block   ';
//             domReturnButton.style.cursor = 'pointer';
//             domReturnButton.style.position = 'absolute';
//             domReturnButton.style.borderRadius = '12px';
//             domReturnButton.style.transformOrigin = '50% 50%';
//             domReturnButton.style.top = isMobile() ? '62%' : '60%';
//             domReturnButton.style.left = isMobile() ? '35%' : (lang === 'cn') ? '40%' : '38%';
//             domReturnButton.onclick = function () {
//                 window.open(gamegeonLocation, "_self");
//             };
//
//             //! init Resume Game Button
//             domResumeButton.style.backgroundColor = 'red';
//             domResumeButton.style.border = 'none';
//             domResumeButton.style.color = 'white';
//             domResumeButton.style.padding = '10px';
//             domResumeButton.style.textAlign = 'center';
//             domResumeButton.style.textDecoration = 'none';
//             domResumeButton.style.display = 'inline-block   ';
//             domResumeButton.style.cursor = 'pointer';
//             domResumeButton.style.position = 'absolute';
//             domResumeButton.style.borderRadius = '12px';
//             domResumeButton.style.transformOrigin = '50% 50%';
//             domResumeButton.style.top = isMobile() ? '62%' : '60%';
//             domResumeButton.style.left = isMobile() ? '58%' : '55%';
//             domResumeButton.onclick = function () {
//                 needBlank = false;
//                 crosstab.broadcast('emptyPage');
//             };
//
//             target.appendChild(newdom);
//             target.appendChild(newdom1);
//             target.appendChild(newdom2);
//             target.appendChild(domReturnButton);
//             target.appendChild(domResumeButton);
//         }, 1500);
//     }
// }
