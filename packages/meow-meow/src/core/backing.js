import * as PIXI from 'pixi.js';
import app from './app';
import 'gsap';
import { each } from 'lodash';

let backing;

function init(backingColor = 0x000000) {
    // create semi transparent backing
    backing = new PIXI.Graphics();
    backing.beginFill(backingColor);
    backing.drawRect(0, 0, app.getResolutionX(), app.getResolutionY());
    backing.endFill();
    backing.alpha = 0;
    backing.visible = false;

    app.addComponent(backing);
}

function setAndOpen(frontObjectName = 'Panel Container') { // todo delete it when no one use it
    open(frontObjectName);
}

function open(frontObjectName = 'Panel Container', backingTargetAlpha = 0.5) {
    if(!backing) {
        init();
    }

    TweenMax.killTweensOf(backing); // stop all backing tween in case backing in tween
    backing.alpha = 0;

    backing.visible = true;
    let foundFrontObject = false;
    app.stage.removeChild(backing);
    // finding layer
    each(app.stage.children, (child, index) => {
        if (child.name === frontObjectName) {
            // console.log('here name: -', child);
            foundFrontObject = true;
            app.stage.addChildAt(backing, index - 1);
            return;
        }
    });

    // if invalid input layer then use default layer which is behind Panel Container
    if(!foundFrontObject) {
        console.error('Invalid Layer');

        each(app.stage.children, (child, index) => {
            if (child.name === 'Panel Container') {
                foundFrontObject = true;
                app.stage.addChildAt(backing, index - 1);
                return;
            }
        });
    }

    TweenMax.to(backing, 0.5, {alpha: backingTargetAlpha});
}

function close() {
    if(!backing) {
        init();
    }

    TweenMax.to(backing, 0.5, {alpha: 0}, {
        onComplete: () => {
            backing.visible = false;
        }
    });
}

export default {
    init,
    setAndOpen,
    open,
    close
}