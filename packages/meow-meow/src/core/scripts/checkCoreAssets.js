const fs = require('fs');

const fileList = [
    'button_auto_spin_base_l.png',
    'button_auto_spin_base_l_down.png',
    'button_auto_spin_base_p.png',
    'button_auto_spin_base_p_down.png',
    'button_close.png',
    'button_auto_spin_l.png',
    'button_auto_spin_l_down.png',
    'button_auto_spin_l_on.png',
    'button_auto_spin_l_on_down.png',
    'button_auto_spin_p.png',
    'button_auto_spin_p_down.png',
    'button_auto_spin_p_on.png',
    'button_auto_spin_p_on_down.png',
    'button_auto_spin_play_l.png',
    'button_auto_spin_play_l_down.png',
    'button_auto_spin_start_p.png',
    'button_auto_spin_stop_l.png',
    'button_auto_spin_stop_l_down.png',
    'button_auto_spin_stop_p.png',
    'button_auto_spin_stop_p_down.png',
    'button_bet_max_l.png',
    'button_bet_max_l_on.png',
    'button_bet_max_p.png',
    'button_content_drop_arrow_p.png',
    'button_decrease_l.png',
    'button_decrease_l_down.png',
    'button_drawer_header_drop_arrow_p.png',
    'button_fx_l.png',
    'button_fx_l_off.png',
    'button_fx_p.png',
    'button_fx_p_off.png',
    'button_increase_l.png',
    'button_increase_l_down.png',
    'button_info_l.png',
    'button_info_l_down.png',
    'button_info_p.png',
    'button_info_p_down.png',
    'button_music_l.png',
    'button_music_l_off.png',
    'button_music_p.png',
    'button_music_p_off.png',
    'button_select_any_l.png',
    'button_select_auto_spin_l.png',
    'button_sound_l.png',
    'button_sound_l_off.png',
    'button_sound_p.png',
    'button_sound_p_off.png',
    'button_spin_base_l.png',
    'button_spin_base_l_down.png',
    'button_spin_base_p.png',
    'button_spin_base_p_down.png',
    'button_spin_l.png',
    'button_spin_l_down.png',
    'button_spin_p.png',
    'button_spin_p_down.png',
    'button_turbo_l.png',
    'button_turbo_l_down.png',
    'button_turbo_l_on.png',
    'button_turbo_l_on_down.png',
    'button_turbo_p.png',
    'button_turbo_p_down.png',
    'button_turbo_p_on.png',
    'button_turbo_p_on_down.png',
    'button_wallet_coin.png',
    'button_wallet_coin_down.png',
    'display_amount_l.png',
    'display_amount_p.png',
    'label_any_win_l.png',
    'label_auto_play_l.png',
    'label_auto_spin_p.png',
    'label_bet_amount_l.png',
    'label_bet_amount_p.png',
    'label_bet_lines_l.png',
    'label_bet_lines_p.png',
    'label_bet_max_l.png',
    'label_bet_max_p.png',
    'label_number_of_spins_l.png',
    'label_stop_autoplay_l.png',
    'label_stop_autoplay_p.png',
    'label_stop_if_cash_decreases_l.png',
    'label_stop_if_cash_decreases_p.png',
    'label_stop_if_cash_increases_l.png',
    'label_stop_if_cash_increases_p.png',
    'label_stop_if_single_win_exceeds_l.png',
    'label_stop_if_single_win_exceeds_p.png',
    'label_total_bet_l.png',
    'label_total_bet_p.png',
    'label_winning_bonus_l.png',
    'panel_auto_spin_l.png',
    'panel_auto_spin_p.png',
    'panel_bet_amount_p.png',
    'panel_layout_bottom_l.png',
    'panel_layout_bottom_p.png',
    'panel_layout_drawer_body_p.png',
    'panel_layout_drawer_header_p.png',
    'panel_wallet_bar_l.png',
    'panel_wallet_bar_p.png',
    'scroll_bar_l.png',
    'scroll_bar_p.png',
    'scroll_button_l.png',
    'scroll_button_p.png',
    'sprite_number_0.png',
    'sprite_number_1.png',
    'sprite_number_2.png',
    'sprite_number_3.png',
    'sprite_number_4.png',
    'sprite_number_5.png',
    'sprite_number_6.png',
    'sprite_number_7.png',
    'sprite_number_8.png',
    'sprite_number_9.png',
    'sprite_number_comma.png',
    'sprite_number_dollar_sign.png',
    'sprite_number_dot.png',
];

let foundFiles = [];
let notFoundFiles = [];

for (let i = 0; i < fileList.length; i++) {
    const fileName = fileList[i];
    if (fs.existsSync(`src/assets/core/${fileName}`)) {
        foundFiles.push(fileName);
    } else {
        notFoundFiles.push(fileName);
    }
}

console.log('\x1b[31m', 'Not found:');
for (let i = 0; i < notFoundFiles.length; i++) {
    console.log(notFoundFiles[i]);
}

console.log('');

console.log('\x1b[32m', 'Found:');
for (let i = 0; i < foundFiles.length; i++) {
    console.log(foundFiles[i]);
}

console.log('\x1b[0m');
