import * as PIXI from 'pixi.js';
import { each, find, map, minBy, maxBy, sortBy } from 'lodash';
import { Back, TweenMax } from 'gsap';
import SkeletonData = PIXI.spine.core.SkeletonData;

import { AnimatedSpriteItem } from '../reel-item/AnimatedSpriteItem';
import { ExplodableSpineItem } from '../reel-item/ExplodableSpineItem';
import { SpineItem } from '../reel-item/SpineItem';
import { CoverableItem } from '../reel-item/CoverableItem';
import { ReelItem } from '../reel-item/ReelItem';
import app from '../../app';
import utils from '../../utils';
import sound from '../../sound';
import ui from '../../ui';

export class Reel {

    protected container: PIXI.Container = new PIXI.Container();
    protected options: any;

    protected totemBank: any;
    protected totemBankWithException: any;
    protected reelItems: Array<ReelItem> = [];

    protected isSpinning: boolean = false;
    protected ticker;
    protected currentSpeed = 0;

    constructor(options: any, totemBank: any) {

        // init options
        this.options = Object.assign({
            direction: 'vertical',
            rows: 3,
            x: 0,
            y: 0,
            offset: 180,
            initialSpeed: -15,
            maxSpeed: 25,
            accelerationSpeed: 1,
            scrollingEase: Back.easeOut.config(1),
            spinStopTweenTime: 1,
            animationSpeed: 0.30,
            virtualRows: true, // prepend and append row for smooth spin animation
            isSpine: false,
            needCover: false,
            totemBg: null,
            totemBgIsSpine: false,
            totemYReset: 0, // todo: need check
            scale: 1, // todo: need check
            loop: false, // todo: need check
            inOut: 'upDown', // todo: need check
            nearMissOffset: 85, // todo: need check
            haveIdleSpine: false, // todo: need check
            totemKey: null, // todo: need check
            totemForLong: null, // todo: need check
            totemLong: null, // todo: need check
            totemMask: null, // todo: need check
            totemWinGlow: null,
            winGlowAnimation: 'state_1',
            coverObject: null,
            exception: [],
            idleAnimation: 'idle_1', // todo: need check
            coverObjectOffsetX: 0,
            coverObjectOffsetY: 0,
            spinStopDelay: 0,
        }, options);

        // setup current reel container
        this.container.name = 'Reel Container';
        this.container.x = this.options.x;
        this.container.y = this.options.y;
        this.container.interactiveChildren = false;

        // setup totem bank
        this.totemBank = map(totemBank, (totem, index) => {
            return {
                key: index,
                asset: totem,
            }
        });

        if (this.options.exception.length !== 0) {
            this.totemBankWithException = this.totemBank.slice();
            each(this.options.exception, (exceptionTotem) => {
                for (let i = 0; i < this.totemBank.length; i++) {
                    if (this.totemBankWithException[i].key === exceptionTotem) {
                        this.totemBankWithException.splice(i, 1)
                    }
                }
            });
        }
        // inOut options Checking
        if (this.options.inOut !== 'upDown' && this.options.inOut !== 'downUp') {
            throw new Error('Invalid input of Reel.ts inOut option');
        }

        // setup reel items
        const totalItemsIncludingVirtual = (this.options.rows + (this.hasVirtualRows() ? 2 : 0));
        for (let i = 0; i < totalItemsIncludingVirtual; i++) {

            const totem = this.getRandomTotem();
            let reelItem: ReelItem = this.generateNewReelItem(totem);

            // if the slot is vertical, then we need to just the y, else will be the x
            const directionTo = this.options.direction === 'vertical' ? 'y' : 'x';

            // we don't know the exact item length at this moment because slots haven't finish init.
            // thus we need calculate our self.
            const itemsLength = this.options.rows + (this.hasVirtualRows() ? 2 : 0);
            const centerIndex = Math.floor(itemsLength / 2);

            // position the reel item to correct position
            reelItem.container[directionTo] = (i - centerIndex) * this.options.offset;

            if (this.options.totemMask) {
                reelItem.setMasking(this.options.totemMask);
            }
            this.reelItems.push(reelItem);
            this.container.addChild(reelItem.container);
        }
    }

    getContainer() {
        return this.container;
    }

    getItems() {
        return this.reelItems;
    }

    getItemsWithoutVirtualRows() {

        // if current reel doesn't support virtual rows, then we should return the raw items.
        if (!this.hasVirtualRows()) {
            return this.reelItems;
        }

        const notVirtualRow = [];
        //! if virtual rows more than 2 modification needed for the for loop
        for (let i = 1; i <= this.options.rows; i++) {
            notVirtualRow.push(this.reelItems[i]);
        }
        return notVirtualRow;
    }

    getHighestPoint() {
        let getStartPosFn = this.isUpDown() ? minBy : maxBy;
        const highestItem = getStartPosFn(this.reelItems, (item) => {
            return this.isVertical() ? item.container.y : item.container.x;
        });
        return this.isVertical() ? highestItem.container.y : highestItem.container.x;
    }

    hasVirtualRows() {
        return !!this.options.virtualRows;
    }

    isVertical() {
        return this.options.direction === 'vertical';
    }

    protected getRandomTotem() {
        const totemBankUsed = this.options.exception.length !== 0 ? this.totemBankWithException : this.totemBank
        const randNum = Math.floor(Math.random() * (totemBankUsed.length));
        return totemBankUsed[randNum];
    }

    protected generateNewReelItem(totem) {

        let newReelItem;
        if (this.options.isSpine && this.options.explodable) {
            newReelItem = new ExplodableSpineItem(totem.asset, totem.key, this.options.explodable);
        } else if (this.options.isSpine) {
            newReelItem = new SpineItem(totem.asset, totem.key, this.options.idleAnimation);
        } else if (this.options.needCover) {
            newReelItem = new CoverableItem(totem.asset, totem.key, this.options.idleAnimation, this.options.coverObject);
        } else {
            newReelItem = new AnimatedSpriteItem(totem.asset, totem.key);
            newReelItem.setAnimationSpeed(this.options.animationSpeed);
        }

        newReelItem.setScale(this.options.scale);

        // set totem background if provided
        if (this.options.totemBg) {
            newReelItem.setBackground(this.options.totemBg, this.options.totemBgIsSpine);
        }

        // set totem win glow if provided
        if (this.options.totemWinGlow) {
            let isSpine = this.options.totemWinGlow instanceof SkeletonData;
            newReelItem.setWinGlow(this.options.totemWinGlow, isSpine, this.options.winGlowAnimation);
        }

        // set totem mask if provided
        if (this.options.totemMask) {
            newReelItem.setMasking(this.options.totemMask);
        }

        return newReelItem;
    }

    removeReelItem(index) {
        const target = this.reelItems[index];
        target.container.parent.removeChild(target.container);
        this.reelItems[index] = null;
    }

    getTotem(key: string) {

        if (key === 'z') {
            return this.getRandomTotem();
        }

        const totem = find(this.totemBank, (totem) => {
            return totem.key === key;
        });
        if (!totem) {
            throw new Error(`Can't find totem. key: ${key}`);
        }

        return totem;
    }

    // reelItems is contain additional 2 row when virtualRows is enabled.
    // thus we need to deduct it.
    // this function SHOULD stand on the perspective of developer.
    public getCenterIndex() {
        const itemLength = this.hasVirtualRows() ? this.reelItems.length - 2 : this.reelItems.length;
        return Math.floor((itemLength / 2));
    }

    start() {
        // this.tempTotemToFindLong = [];
        // this.offset = 2;

        this.currentSpeed = this.options.initialSpeed;
        this.isSpinning = true;
        this.ticker = new PIXI.ticker.Ticker();
        this.ticker.add(() => {
            this.spin();
        });

        this.ticker.start();
    }

    stop(key: any, audio: boolean = true): Promise<any> {
        this.isSpinning = false;
        let isNearMiss = false;
        let defaultKey = key;

        // add in key that needed for slot to stop
        if (key) {
            if (typeof key === 'string') {
                const targetTotem = this.getTotem(key);
                this.reelItems[this.getCenterIndex()].changeItem(targetTotem.asset);

                // for now only 3x1 has near miss
                // isNearMiss = key === 'z';

                // make sure key is an array.
                key = [key];

            } else {
                for (let i = 0; i < key.length; i++) {
                    if (key[i] === 'z') {
                        isNearMiss = true;
                    }

                    const targetTotem = this.getTotem(key[i]);

                    // notes: note sure why + 1, need to figure out and write down comment
                    const targetIndex = i + (this.hasVirtualRows() ? 1 : 0);
                    this.reelItems[targetIndex].changeItem(targetTotem.asset);
                }
            }
        }

        // add in random key for virtual slot
        if (this.hasVirtualRows()) {
            key.unshift(this.getRandomTotem().key);
            key.push(this.getRandomTotem().key);
        }

        // const totalRows = this.options.rows + (this.hasVirtualRows() ? this.options.rows * 2 : 0);
        const totalRows = this.reelItems.length;
        const centerIndex = Math.floor(totalRows / 2);
        const highestPoint = this.getHighestPoint();
        const directionTo = this.options.direction === 'vertical' ? 'y' : 'x';

        // reverse the key as the front array item will be put first on the lower point
        key.reverse();

        // insert target reelItems into beginning of array.
        for (let i = 0; i < key.length; i++) {

            const targetTotem = this.getTotem(key[i]);

            let newReelItem: ReelItem = this.generateNewReelItem(targetTotem);

            // to set the direction of new totems to go
            if (this.isUpDown()) {
                newReelItem.container[directionTo] = highestPoint - ((i + 1) * this.options.offset);
                this.reelItems.unshift(newReelItem);
            } else if (!this.isUpDown()) {
                newReelItem.container[directionTo] = highestPoint + ((i + 1) * this.options.offset);
                this.reelItems.push(newReelItem);
            }

            this.container.addChild(newReelItem.container);
        }


        // make sure reelItems are sorted by their position
        this.reelItems = sortBy(this.reelItems, (item) => {
            return this.isVertical() ? item.container.y : item.container.x;
        });


        // hard code for totemLong
        if (this.options.totemLong) {
            // find key in reel
            let longTotemLocation = utils.checkKeyInArray(defaultKey, this.options.totemForLong);
            let longTotemArray = [];
            let longTotemPosition;

            // put all key into an array
            for (let i = 0; i < longTotemLocation.length; i++) {
                longTotemArray.push(longTotemLocation[i].row);
                longTotemPosition = this.reelItems[2].container.y;
            }

            // if reel have 'x', position it and alpha the others
            if (longTotemArray.length === 3) {
                this.reelItems[1].container.renderable = false;
                this.reelItems[2].changeItem(this.options.totemLong);
                this.reelItems[3].container.renderable = false;
            }
            else if (longTotemArray.length === 2) {
                if (longTotemArray[0] === 1) {
                    this.reelItems[4].container.renderable = false;
                    this.reelItems[3].changeItem(this.options.totemLong);
                    this.reelItems[2].container.renderable = false;
                    key[0] = this.options.totemForLong;
                }
                else {
                    this.reelItems[0].container.renderable = false;
                    this.reelItems[1].changeItem(this.options.totemLong);
                    this.reelItems[2].container.renderable = false;
                    key[4] = this.options.totemForLong;
                }
            }
            else if (longTotemArray.length === 1) {
                if (longTotemArray[0] === 1) {
                    this.reelItems[4].changeItem(this.options.totemLong);
                    this.reelItems[3].container.renderable = false;
                    key[0] = this.options.totemForLong;
                }
                else {
                    this.reelItems[0].changeItem(this.options.totemLong);
                    this.reelItems[1].container.renderable = false;
                    key[4] = this.options.totemForLong;
                }
            }
        }

        let stopPromises = [];
        each(this.reelItems, (item, index) => {

            let newPosition = null;

            // to get the stop position by compare with old totems index
            if (this.isUpDown()) {
                newPosition = (index - centerIndex) * this.options.offset;
            } else if (!this.isUpDown()) {
                newPosition = ((index - totalRows) - centerIndex) * this.options.offset;
            }

            // if is near miss, minus the offset
            newPosition = !isNearMiss ? newPosition : newPosition - this.options.nearMissOffset;

            stopPromises.push(new Promise((resolve) => {

                let finalPositionOption: any = {
                    ease: this.options.scrollingEase,
                    onComplete: resolve
                };

                if (this.isVertical()) {
                    finalPositionOption.y = newPosition;
                } else {
                    finalPositionOption.x = newPosition;
                }

                // slowly move to final position
                TweenMax.to(item.container, this.options.spinStopTweenTime / this.options.accelerationSpeed, finalPositionOption);
            }));
        });

        if (audio) {
            setTimeout(() => {
                sound.play('slotSpinningStopSound');
            }, (this.isUpDown() ? 500 : 200) / this.options.accelerationSpeed);
        }


        this.ticker.stop();
        this.currentSpeed = this.options.initialSpeed;

        // removing slot item that no longer needed when new slot totem is placed.
        return Promise.all(stopPromises).then(() => {

            if (this.isUpDown()) {
                for (let i = totalRows; i < this.reelItems.length; i++) {
                    this.container.removeChild(this.reelItems[i].container);
                }

                this.reelItems = this.reelItems.splice(0, totalRows);
            } else if (!this.isUpDown()) {
                for (let i = 0; i < totalRows; i++) {
                    this.container.removeChild(this.reelItems[i].container);
                }

                this.reelItems = this.reelItems.splice(totalRows);

            }

        });
    }

    private spin() {

        if (!this.isSpinning) {
            return;
        }

        if (this.currentSpeed < this.options.maxSpeed) {
            this.currentSpeed += this.options.accelerationSpeed;
        }
        if (this.currentSpeed >= this.options.maxSpeed) {
            this.currentSpeed = this.options.maxSpeed;
        }

        const xOrY = this.isVertical() ? 'y' : 'x';
        let highestPoint = this.getHighestPoint();

        // move every single item position forward by current speed
        each(this.reelItems, (item) => {

            let changed = false;
            item.setScale(this.options.scale);
            // moving slot
            if (this.isUpDown()) {

                item.container[xOrY] += this.currentSpeed * this.options.accelerationSpeed;

                if (item.getBounds().top >= (app.screen.height + this.options.totemYReset)) {
                    item.container[xOrY] = highestPoint - this.options.offset;
                    highestPoint = item.container[xOrY];
                    changed = true;
                }
            }
            else if (!this.isUpDown()) {

                item.container[xOrY] -= this.currentSpeed * this.options.accelerationSpeed;

                if (item.getBounds().bottom <= this.options.totemYReset) {
                    item.container[xOrY] = highestPoint + this.options.offset;
                    highestPoint = item.container[xOrY];
                    changed = true;
                }
            }

            // randomize totem if they going out the screen
            if (changed) {
                const totem = this.getRandomTotem();
                item.container.renderable = true;
                item.changeItem(totem.asset);
                item.container.alpha = 1;

                // todo. if x is detected, place or change the totem then place it y + this.options.offset. then alpha the rest
            }
        })

    }

    set(key: any) {
        let isNearMiss = false;
        let defaultKey = key;

        // add in key that needed for slot to stop
        if (key) {
            if (typeof key === 'string') {
                const targetTotem = this.getTotem(key);
                this.reelItems[this.getCenterIndex()].changeItem(targetTotem.asset);
                this.reelItems[this.getCenterIndex()].setScale(this.options.scale);
                // make sure key is an array.
                key = [key];

            } else {
                for (let i = 0; i < key.length; i++) {
                    if (key[i] === 'z') {
                        isNearMiss = true;
                    }

                    const targetTotem = this.getTotem(key[i]);

                    const targetIndex = i + (this.hasVirtualRows() ? 1 : 0);
                    this.reelItems[targetIndex].changeItem(targetTotem.asset);
                    this.reelItems[targetIndex].setScale(this.options.scale);
                }
            }
        }

        const totalRows = this.reelItems.length;
        const centerIndex = Math.floor(totalRows / 2);

        // support for totemLong
        if (this.options.totemLong) {
            // find key in reel
            let longTotemLocation = utils.checkKeyInArray(defaultKey, this.options.totemForLong);
            let longTotemArray = [];
            let longTotemPosition;

            // put all key into an array
            for (let i = 0; i < longTotemLocation.length; i++) {
                longTotemArray.push(longTotemLocation[i].row);
                longTotemPosition = this.reelItems[2].container.y;
            }

            // if reel have 'x', position it and alpha the others
            if (longTotemArray.length === 3) {
                this.reelItems[1].container.renderable = false;
                this.reelItems[2].changeItem(this.options.totemLong);
                this.reelItems[2].setScale(this.options.scale);
                this.reelItems[3].container.renderable = false;
            }
            else if (longTotemArray.length === 2) {
                if (longTotemArray[0] === 1) {
                    this.reelItems[4].container.renderable = false;
                    this.reelItems[3].changeItem(this.options.totemLong);
                    this.reelItems[3].setScale(this.options.scale);
                    this.reelItems[2].container.renderable = false;
                    key[0] = this.options.totemForLong;
                }
                else {
                    this.reelItems[0].container.renderable = false;
                    this.reelItems[1].changeItem(this.options.totemLong);
                    this.reelItems[1].setScale(this.options.scale);
                    this.reelItems[2].container.renderable = false;
                    key[4] = this.options.totemForLong;
                }
            }
            else if (longTotemArray.length === 1) {
                if (longTotemArray[0] === 2) {
                    this.reelItems[4].changeItem(this.options.totemLong);
                    this.reelItems[4].setScale(this.options.scale);
                    this.reelItems[3].container.renderable = false;
                    key[0] = this.options.totemForLong;
                }
                else {
                    this.reelItems[0].changeItem(this.options.totemLong);
                    this.reelItems[0].setScale(this.options.scale);
                    this.reelItems[1].container.renderable = false;
                    key[4] = this.options.totemForLong;
                }
            }
        }

        // support for near miss
        each(this.reelItems, (item, index) => {
            let newPosition = null;

            // repositioning
            newPosition = (index - centerIndex) * this.options.offset;

            // if is near miss, minus the offset
            newPosition = !isNearMiss ? newPosition : newPosition - this.options.nearMissOffset;

            if (this.isVertical()) {
                item.container.y = newPosition;
            } else {
                item.container.x = newPosition;
            }
        });
    }

    setAccelerationSpeed(speed: number = 1) {
        this.options.accelerationSpeed = speed;
    }

    createReelsItem(key: any) {
        const locationAboveReel = this.options.offset * -(this.reelItems.length - 1);
        const totalReelLength = this.reelItems.length;
        if (key) {
            if (this.hasVirtualRows()) {
                if (typeof key === 'string') {
                    const targetTotem = this.getTotem(key);
                    this.reelItems[0].changeItem(targetTotem.asset);
                }
                else {
                    if (key[0]) {
                        const targetTotem = this.getTotem(key[0]);
                        this.reelItems[0].changeItem(targetTotem.asset);
                    }
                    for (let i = 1; i < key.length; i++) {
                        if (!key[i]) {
                            continue;
                        }
                        const targetTotem = this.getTotem(key[i]);
                        if (targetTotem) {
                            let reelItem: ReelItem = this.generateNewReelItem(targetTotem);

                            // if the slot is vertical, then we need to just the y, else will be the x
                            const directionTo = this.options.direction === 'vertical' ? 'y' : 'x';

                            // position the reel item to correct position
                            reelItem.container[directionTo] = locationAboveReel - (this.options.offset * i);

                            this.reelItems.splice(0, 0, reelItem);
                            this.container.addChild(reelItem.container);
                        }
                    }
                }
                for (let i = 0; i < this.reelItems.length; i++) {
                    if (!this.reelItems[i]) {
                        this.reelItems.splice(i, 1);
                        i--;
                    }
                }

                if (totalReelLength > this.reelItems.length) {
                    const totem = this.getRandomTotem();
                    let virtualReelItem: ReelItem = this.generateNewReelItem(totem);
                    const directionTo = this.options.direction === 'vertical' ? 'y' : 'x';

                    // position the reel item to correct position
                    virtualReelItem.container[directionTo] = locationAboveReel - (this.options.offset * (key.length + 1));

                    this.reelItems.splice(0, 0, virtualReelItem);
                    this.container.addChild(virtualReelItem.container);
                }

            }
            else {
                if (typeof key === 'string') {
                    const targetTotem = this.getTotem(key);

                    let reelItem: ReelItem = this.generateNewReelItem(targetTotem);

                    // if the slot is vertical, then we need to just the y, else will be the x
                    const directionTo = this.options.direction === 'vertical' ? 'y' : 'x';

                    // position the reel item to correct position
                    reelItem.container[directionTo] = locationAboveReel;

                    this.reelItems.splice(0, 0, reelItem);
                    this.container.addChild(reelItem.container);
                }
                else {
                    for (let i = 0; i < key.length; i++) {

                        if (!key[i]) {
                            continue;
                        }

                        const targetTotem = this.getTotem(key[i]);
                        if (targetTotem) {
                            let reelItem: ReelItem = this.generateNewReelItem(targetTotem);

                            // if the slot is vertical, then we need to just the y, else will be the x
                            const directionTo = this.options.direction === 'vertical' ? 'y' : 'x';

                            // position the reel item to correct position
                            reelItem.container[directionTo] = locationAboveReel - (this.options.offset * i);

                            this.reelItems.splice(0, 0, reelItem);
                            this.container.addChild(reelItem.container);
                        }
                    }
                }
                for (let i = 0; i < this.reelItems.length; i++) {
                    if (!this.reelItems[i]) {
                        this.reelItems.splice(i, 1);
                        i--;
                    }
                }
            }
        }
    }

    setMasking(mask: PIXI.Sprite | PIXI.Graphics) {
        this.container.mask = mask;
        app.stage.addChild(mask);
    }

    isUpDown() { // with the inOut Option checking in constructor this function work
        return (this.options.inOut === 'upDown');
    }

    moveToOriginalPosition(duration) {
        return new Promise((resolve) => {
            for (let i = 0; i < this.reelItems.length; i++) {
                if (this.reelItems[i]) {
                    if (this.reelItems[i].container.y !== (-this.options.offset + this.options.offset * i)) {
                        TweenMax.to(this.reelItems[i].container, duration, {
                            y: (this.hasVirtualRows() ? -(2 * this.options.offset) : -this.options.offset) + this.options.offset * i,
                            onComplete: () => {
                                ui.shakeItem(this.reelItems[i].container).then(resolve);
                            }
                        })
                    }
                }

            }
        })
    }
}
