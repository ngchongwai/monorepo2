import { TweenMax, Bounce, Back } from 'gsap';
import { each } from 'lodash';
import app from '../../app';

import { Reel } from './Reel';
import utils from '../../utils';

export class ReelTurn extends Reel {

    private itemMapping;
    private originLocationArray;

    constructor(options: any, totemBank: any) {

        if (options.virtualRows) {
            throw new Error('option virtualRows is not available in this ReelOutIn');
        }

        super(options, totemBank);

        this.itemMapping = [];
        this.originLocationArray = [];
    }

    start() {
        this.originLocationArray = [];
        each(this.getItems(), (item) => {
            let itemLocation = {x : this.getItems()[0].container.parent.x, y : this.getItems()[0].container.parent.y};

            this.originLocationArray.push(Object.assign({}, itemLocation));
            TweenMax.fromTo(item.container.parent, 0.25,{x : itemLocation.x, y : itemLocation.y},{x : app.screen.width/2, y : app.screen.height/2});
            TweenMax.fromTo(item.container.scale, 0.25, {x: 1, y: 1}, {x: 0, y: 0, ease: Back.easeIn.config(5)});
        });
    }

    stop(key) {
        each(this.getItems(), (item, index) => {
            if (key) {
                if (typeof key === 'string') {
                    const targetTotem = this.getTotem(key);
                    item.changeItem(targetTotem.asset);

                    // make sure key is an array.
                    key = [key];

                } else {
                    const temp = key[index];
                    const targetTotem = this.getTotem(temp);
                    item.changeItem(targetTotem.asset);
                }
            }

            TweenMax.to(item.container.parent, 0.25,{x : this.originLocationArray[index].x, y : this.originLocationArray[index].y});
            TweenMax.fromTo(item.container.scale, 0.25, {x: 0, y: 0}, {x: 1, y: 1, ease: Back.easeOut.config(5)});
        });
        return Promise.resolve();
    }
}
