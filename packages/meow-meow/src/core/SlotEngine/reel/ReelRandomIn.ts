import {getResolutionX, getResolutionY} from '../../app';
import { each } from 'lodash';

import { Reel } from './Reel';
import utils from '../../utils';
import * as PIXI from "pixi.js";
import app from '../../app';
import { TweenMax } from 'gsap';

/* currently this reel only support 3x1 without virtual row*/

export class ReelRandomIn extends Reel {

    private itemsDirection: any;

    constructor(options: any, totemBank: any) {
        if (options.virtualRows) {
            throw new Error('option virtualRows is not available in this ReelRandomIn');
        }

        super(options, totemBank);

        this.isSpinning = false;

        this.itemsDirection = [];
        each(this.getItems(), (item) => {
            let tempDir = {x: 0 , y: 1};
            this.itemsDirection.push(tempDir);
        });

    }

    start() {
        this.isSpinning = true;

        this.ticker = new PIXI.ticker.Ticker();
        this.ticker.add(() => {
            this.spinning();
        });

        this.ticker.start();
    }

    stop(key) {
        if(!key) {
            throw new Error('invalid input');
        }

        this.isSpinning = false;

        this.ticker.stop();
        this.ticker.destroy();

        return new Promise((resolve) => {

            this.ticker = new PIXI.ticker.Ticker();
            this.ticker.add(() => {
                if (this.currentSpeed < this.options.maxSpeed) {
                    this.currentSpeed += this.options.accelerationSpeed;
                }
                if (this.currentSpeed >= this.options.maxSpeed) {
                    this.currentSpeed = this.options.maxSpeed;
                }

                let highestPoint = this.getHighestPoint();

                // move every single item position forward by current speed
                each(this.reelItems, (item, index) => {
                    item.setScale(this.options.scale);
                    item.container.renderable = true;
                    // moving slot
                    item.container.x += this.itemsDirection[index].x * this.currentSpeed * this.options.accelerationSpeed;
                    item.container.y += this.itemsDirection[index].y * this.currentSpeed * this.options.accelerationSpeed;

                    if (item.getBounds().top >= (app.screen.height + this.options.totemYReset)) {
                        this.ticker.stop();
                        item.container.x = utils.randomRange(-100, 300);
                        item.container.y = highestPoint - this.options.offset;

                        if (typeof key === 'string') {
                            const targetTotem = this.getTotem(key);
                            this.reelItems[this.getCenterIndex()].changeItem(targetTotem.asset);

                            // make sure key is an array.
                            key = [key];

                        }

                        TweenMax.to(item.container, this.options.spinStopTweenTime / this.options.accelerationSpeed, {x: 0, y: 0,
                            onComplete: () => {
                                resolve(0);
                            }
                        });
                    }
                });
            });

            this.ticker.start();
        });

    }

    spinning() {
        if(!this.isSpinning) {
            return;
        }

        if (this.currentSpeed < this.options.maxSpeed) {
            this.currentSpeed += this.options.accelerationSpeed;
        }
        if (this.currentSpeed >= this.options.maxSpeed) {
            this.currentSpeed = this.options.maxSpeed;
        }

        let highestPoint = this.getHighestPoint();

        // move every single item position forward by current speed
        each(this.reelItems, (item, index) => {
            let changed = false;
            item.setScale(this.options.scale);
            item.container.renderable = true;
            // moving slot
            item.container.x += this.itemsDirection[index].x * this.currentSpeed * this.options.accelerationSpeed;
            item.container.y += this.itemsDirection[index].y * this.currentSpeed * this.options.accelerationSpeed;

            if (item.getBounds().top >= (app.screen.height + this.options.totemYReset)) {
                item.container.x = utils.randomRange(-100, 300);
                item.container.y = highestPoint - this.options.offset;
                highestPoint = item.container.y;
                this.itemsDirection[index] = this.getDirection({x: item.container.x, y: item.container.y}, {x: 0, y: 0});
                changed = true;
            }

            // randomize totem if they going out the screen
            if (changed) {
                const totem = this.getRandomTotem();
                item.changeItem(totem.asset);
                item.container.alpha = 1;
            }
        })
    }

    getDirection(origin = {x: 0, y: 0}, target = {x: 0, y: 0}) {
        let different = {x: target.x - origin.x, y: target.y - origin.y};
        let mag = Math.sqrt((different.x * different.x) + (different.y * different.y));
        let direction = {x: different.x / mag, y: different.y / mag};

        return direction;
    }
}