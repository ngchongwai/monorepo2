import {TweenMax} from 'gsap';
import {each} from 'lodash';

import {Reel} from './Reel';
import sound from '../../sound';
import utils from '../../utils';
import {CoverableItem} from "../reel-item/CoverableItem";

import environment from 'environment';

export class ReelFade extends Reel {
    // private gameName = environment.gameName;
    private itemMapping;
    private promiseForStopping;
    private spinningPromises;

    constructor(options: any, totemBank: any) {

        if (options.virtualRows) {
            throw new Error('option virtualRows is not available in this ReelOutIn');
        }

        super(options, totemBank);
    }

    start() {
        each(this.getItems(), (item, index) => {
            setTimeout(() => {
                // special handling
                if (environment.gameName === 'happy-road') {
                    // @ts-ignore
                    if (item.item.state.tracks[0].animation.name != 'win_2') {
                        item.playAnimation({animationName: 'spin_1'});
                        item.addAnimation('spin_2');
                    }
                }
                else {
                    item.playAnimation({animationName: 'spin_1'});
                    item.addAnimation('spin_2');
                }

            }, (this.options.accelerationSpeed < 3 ? 500 : 120) * index);
        });
    }

    stop(key, audio = true) {

        const allStopPromises = [];

        each(this.getItems(), (item, index) => {

            // add in key that needed for slot to stop
            let targetTotem = null;
            if (key) {
                if (typeof key === 'string') {
                    targetTotem = this.getTotem(key);
                } else {
                    targetTotem = this.getTotem(key[index]);
                }
            }

            allStopPromises.push(new Promise((resolve) => {
                setTimeout(() => {
                    item.container.alpha = 1;
                    // special handling
                    if (environment.gameName === 'happy-road') {
                        // @ts-ignore
                        if (item.item.state.tracks[0].animation.name != 'win_2') {
                            item.playAnimation({animationName: 'spin_3'});
                        } else {
                            TweenMax.to(item.container, 0.5 / this.options.accelerationSpeed, {
                                alpha: 0,
                            });
                        }
                    }else{
                        item.playAnimation({animationName: 'spin_3'});
                    }

                    TweenMax.to(item.container, 0.25, {
                        delay: 0.15,
                        alpha: 0,
                        onComplete: () => {
                            let totem = targetTotem;
                            item.changeItem(totem.asset);
                            item.playAnimation({animationName: 'spin_4'});
                            if (audio) {
                                sound.play('slotSpinningStopSound');
                                sound.stop('slotSpinningSound');
                            }
                            item.addAnimation('idle');
                            TweenMax.fromTo(item.container, 0.25, {
                                alpha: 0
                            }, {
                                alpha: 1,
                                onComplete: () => {
                                    resolve();
                                }
                            })
                        }
                    });
                }, (this.options.accelerationSpeed < 3 ? 500 : 120) * index);
            }));

        });

        return Promise.all(allStopPromises);
    }
}
