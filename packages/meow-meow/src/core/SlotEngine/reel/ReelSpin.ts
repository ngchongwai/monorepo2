import { TweenMax, Bounce } from 'gsap';
import { each } from 'lodash';

import { Reel } from './Reel';
import utils from '../../utils';
import * as PIXI from "pixi.js";

export class ReelSpin extends Reel {

    private promiseForStopping;
    private itemsStatus;
    private stopping;

    constructor(options: any, totemBank: any) {

        if (options.virtualRows) {
            throw new Error('option virtualRows is not available in this ReelSpin');
        }

        super(options, totemBank);

    }

    start() {
        // set promise to null
        this.promiseForStopping = null;
        this.stopping = false;
        this.itemsStatus = [];
        for (let i = 0; i < this.reelItems.length; i++) {
            let status = {
                speed: -0.2 * this.options.accelerationSpeed,
                stopped: false
            };

            this.itemsStatus.push(status);
        }

        this.isSpinning = true;
        this.ticker = new PIXI.ticker.Ticker();
        this.ticker.add((deltaTime) => {
            this.spinning(deltaTime);
        });

        this.ticker.start();
    }

    spinning(deltaTime) {

        // let all item spinning
        each(this.getItems(), (item, index) => {
            if (this.itemsStatus.stopped) {
                return;
            }

            item.container.scale.x += this.itemsStatus[index].speed * deltaTime;

            // bounce
            if (!this.stopping) {

                if (item.container.scale.x >= 1) {
                    let totem = this.getRandomTotem();
                    item.changeItem(totem.asset);

                    item.container.scale.x = 1;
                    this.itemsStatus[index].speed *= -1;
                }
                else if (item.container.scale.x <= -1) {
                    item.container.scale.x = -1;
                    this.itemsStatus[index].speed *= -1;
                }

            }
            else {

                if (item.container.scale.x >= 1) {
                    item.container.scale.x = 1;
                    this.itemsStatus[index].stopped = true;
                }
                else if (item.container.scale.x <= -1) {
                    item.container.scale.x = -1;
                    this.itemsStatus[index].speed *= -1;
                }

            }

        });

        // checking all reel stop and resolve promise
        let allStop = true;
        each(this.itemsStatus, (item) => {
            if (!item.stopped) {
                allStop = false;
            }
        });

        if (allStop) {
            // resolve defer promise
            this.ticker.stop();
            this.promiseForStopping.resolve();
        }

    }

    stop(key) {
        this.stopping = true;

        each(this.getItems(), (item, index) => {
            let totem = this.getTotem(key[index]);
            item.changeItem(totem.asset);
        });


        this.promiseForStopping = utils.defer();

        return this.promiseForStopping;
    }
}
