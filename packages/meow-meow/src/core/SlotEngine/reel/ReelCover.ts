import { TweenMax, Bounce, Back } from 'gsap';
import { each } from 'lodash';
import app from '../../app';

import { Reel } from './Reel';
import utils from '../../utils';
import * as PIXI from "pixi.js";

export class ReelCover extends Reel {

    private itemMapping;
    private originLocationArray;
    private coverObjects;

    constructor(options: any, totemBank: any) {

        if (options.virtualRows) {
            throw new Error('option virtualRows is not available in this ReelCover');
        }

        super(options, totemBank);
        this.itemMapping = [];
        this.originLocationArray = [];
        this.coverObjects = [];
    }

    start() {
        this.originLocationArray = [];
        this.coverObjects = [];
        each(this.getItems(), (item, index) => {
            setTimeout(() => {
                let itemLocation = {x: this.getItems()[0].container.parent.x, y: this.getItems()[0].container.parent.y};
                let spinCover = new PIXI.spine.Spine(this.options.coverObject);
                spinCover.autoUpdate = true;
                spinCover.x = 0 + this.options.coverObjectOffsetX;
                spinCover.y = -this.options.offset + this.options.offset * index + this.options.coverObjectOffsetY;
                spinCover.state.setAnimation(0, (this.options.accelerationSpeed < 3 ? 'idle_1' : 'idle_2'), true);
                spinCover.name = 'Spin Cover';
                this.container.addChild(spinCover);
                this.coverObjects.push(spinCover);

                this.originLocationArray.push(Object.assign({}, itemLocation));
                // TweenMax.fromTo(item.container.parent, 0.25,{x : itemLocation.x, y : itemLocation.y},{x : app.screen.width/2, y : app.screen.height/2});
                TweenMax.fromTo(spinCover.scale, 0.25, {x: 0, y: 0}, {x: 0.5, y: 0.5, ease: Back.easeIn.config(5)});
                TweenMax.fromTo(item.container.scale, 0.25, {x: 1, y: 1}, {x: 0, y: 0, ease: Back.easeIn.config(5)});
            }, this.getItems().length === 1 ? 0 : (Math.random() * 500))
        });
    }

    stop(key) {
        const allStopPromises = [];
        each(this.getItems(), (item, index) => {
            allStopPromises.push(new Promise((resolve) => {
                setTimeout(() => {
                    if (key) {
                        if (typeof key === 'string') {
                            const targetTotem = this.getTotem(key);
                            item.changeItem(targetTotem.asset);

                            // make sure key is an array.
                            key = [key];

                        } else {
                            const temp = key[index];
                            const targetTotem = this.getTotem(temp);
                            item.changeItem(targetTotem.asset);
                        }
                    }

                    TweenMax.fromTo(item.container.scale, 0.25, {x: 0, y: 0}, {
                        x: 1,
                        y: 1,
                        ease: Back.easeOut.config(5)
                    });
                    TweenMax.fromTo(this.coverObjects[index].scale, 0.25, {x: 0.5, y: 0.5}, {
                        x: 0, y: 0, ease: Back.easeIn.config(5),
                        onComplete: () => {
                            setTimeout(() => {
                                resolve();
                            }, 100);
                            this.coverObjects[index].destroy()
                        }
                    });
                }, this.getItems().length === 1 ? this.options.spinStopDelay : (Math.random() * 500) + this.options.spinStopDelay);
            }))
        });
        return Promise.all(allStopPromises);
    }
}
