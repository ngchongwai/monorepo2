import { Power0, TweenMax } from 'gsap';
import { each, sortBy } from 'lodash';

import { Reel } from './Reel';
import { ReelItem } from '../reel-item/ReelItem';
import ui from '../../ui';
import sound from '../../sound';

export class ReelOutIn extends Reel {

    constructor(options: any, totemBank: any) {

        if (options.virtualRows) {
            throw new Error('option virtualRows is not available in this ReelOutIn');
        }

        super(options, totemBank);
    }

    start() {
        each(this.getItems(), (item) => {
            TweenMax.to(item.container, 2 / this.options.accelerationSpeed, {
                y: 2000, onComplete: () => {
                    item.container.y = -1000
                }
            });
        });
    }

    stop(key, audio = true) {
        let stopPromises = [];

        key.reverse();

        const highestPoint = this.getHighestPoint();
        const directionTo = this.options.direction === 'vertical' ? 'y' : 'x';

        for (let i = 0; i < key.length; i++) {

            const targetTotem = this.getTotem(key[i]);

            let newReelItem: ReelItem = this.generateNewReelItem(targetTotem);
            newReelItem.container[directionTo] = highestPoint - ((i + 1) * this.options.offset);

            this.reelItems.unshift(newReelItem);
            this.container.addChild(newReelItem.container);
        }

        const readyToDelete = this.reelItems.splice(3);
        each(readyToDelete, (reelItem) => {
            this.container.removeChild(reelItem.container);
        });

        this.reelItems = sortBy(this.reelItems, (item) => {
            return this.isVertical() ? item.container.y : item.container.x;
        });

        each(this.getItems(), (item, index) => {
            let newPosition = (index - 1) * this.options.offset;
            let initialPosition;

            stopPromises.push(new Promise((resolve) => {

                let finalPositionOption: any = {
                    ease: Power0.easeNone,
                    onComplete: () => {
                        if (audio) {
                            sound.play('slotSpinningStopSound');
                        }

                        ui.shakeItem(item.container, this.options.accelerationSpeed).then(resolve);
                    }
                };

                if (this.isVertical()) {
                    finalPositionOption.y = newPosition;
                } else {
                    finalPositionOption.x = newPosition;
                }
                initialPosition = newPosition - 1000;

                item.container.scale.set(this.options.scale);

                // slowly move to final position
                TweenMax.fromTo(item.container, 1 / this.options.accelerationSpeed, {y: initialPosition}, finalPositionOption);
            }));
        });

        return Promise.all(stopPromises);
    }

    moveToOriginalPosition(duration) {
        return new Promise((resolve) => {
            for (let i = 0; i < this.reelItems.length; i++) {
                if (this.reelItems[i].container.y !== (-this.options.offset + this.options.offset * i)) {
                    TweenMax.to(this.reelItems[i].container, duration, {
                        y: -this.options.offset + this.options.offset * i,
                        onComplete: () => {
                            sound.play('slotSpinningStopSound');
                            ui.shakeItem(this.reelItems[i].container, this.options.accelerationSpeed).then(resolve);
                        }
                    })
                }
            }
        })
    }
}
