import * as PIXI from 'pixi.js';
import { TweenMax, Power4 } from 'gsap';
import { each, map } from 'lodash';

import { Reel } from '../reel/Reel';
import { RowAndColumn } from '../RowAndColumn';
import app from '../../app';
import utils from '../../utils';
import ui from '../../ui';
import state from '../../state';
import Point = PIXI.Point;

export class ReelGroup {

    container: PIXI.Container;
    reels: Array<Reel>;

    // bet lines
    patterns: Array<any>;
    lineColors: Array<any>;
    lineTexture: Array<any>;
    betLines: Array<any>;
    shownMaxLine: boolean = false; // to cater fix bet line game 
    betLinesContainer: PIXI.Container;

    constructor(reels, betLineOptions = null) {

        this.reels = reels;

        // setup container
        this.container = new PIXI.Container();
        this.container.name = 'Reel Group';
        this.container.interactiveChildren = false;
        this.reels.forEach((reel) => {
            this.container.addChild(reel.getContainer());
        });

        if (betLineOptions) {
            this.patterns = betLineOptions.patterns;

            this.betLinesContainer = new PIXI.Container();
            this.betLinesContainer.interactiveChildren = false;
            this.betLinesContainer.name = 'Bet Lines Container';
            this.betLines = [];
            const slots = this.reels[0].hasVirtualRows() ? this.getReelsWithoutVirtualRows() : this.getReelItems();

            if (betLineOptions.lineColors) {
                this.lineColors = betLineOptions.lineColors;

                /** Bet Lines **/
                // simple validation to check patterns and lineColors.
                if (this.patterns.length !== this.lineColors.length) {
                    throw new Error('Patterns and lineColors not tally.');
                }

                // init bet lines.


                each(this.patterns, (pattern, index) => {
                    const path = pattern.path;
                    const points = utils.codeToPosition(path, slots);

                    const betLine = ui.drawLine(points, this.lineColors[index]); // ui.drawParticleLine(points, false, 0.1, betLineOptions.texture);
                    betLine.alpha = 0;

                    this.betLines.push(betLine);
                    this.betLinesContainer.addChild(betLine);
                });
            }
            else if (betLineOptions.texture) {
                this.lineTexture = betLineOptions.texture;

                /** Bet Lines **/
                // simple validation to check patterns and lineTexture.
                if (this.patterns.length !== this.lineTexture.length) {
                    throw new Error('Patterns and lineTexture not tally.');
                }

                // init bet lines.
                each(this.lineTexture, (texture, index) => {

                    const betLine = new PIXI.Sprite(texture);
                    betLine.name = 'bet line';
                    betLine.alpha = 0;
                    betLine.anchor.set(0.5, 0.5);
                    if (betLineOptions.textureScaling) {
                        betLine.scale.set(betLineOptions.textureScaling);
                    }
                    if (betLineOptions.textureScalingX) {
                        betLine.scale.x = betLineOptions.textureScalingX;
                    }
                    if (betLineOptions.textureScalingY) {
                        betLine.scale.y = betLineOptions.textureScalingY;
                    }
                    betLine.x = this.getCenterPosition().x;
                    betLine.y = this.getCenterPosition().y;
                    this.betLines.push(betLine);
                    this.betLinesContainer.addChild(betLine);
                });
            }
        }
    }

    getContainer() {
        return this.container;
    }

    getBetLinesContainer() {
        return this.betLinesContainer;
    }

    getReelItems() {
        return map(this.reels, (reel) => {
            return reel.getItems();
        });
    }

    getReelsWithoutVirtualRows() {
        // check is the slot has virtual rows
        if (!this.reels[0].hasVirtualRows()) {
            return this.getReelItems();
        }

        return map(this.reels, (reel) => {
            return reel.getItemsWithoutVirtualRows();
        })
    }

    getCenterPosition() {
        const bounds = this.container.getBounds();
        return {
            x: bounds.x + (bounds.width / 2),
            y: bounds.y + (bounds.height / 2),

        }
    }

    // pass in a row and column, and it will return the items around you by row and column.
    getItemsAround(rowAndColumn: RowAndColumn) {
        const row = rowAndColumn.row;
        const column = rowAndColumn.column;
        const items = this.getReelItems();
        const result = [];

        // up
        if (items[column - 1] && items[column - 1][row]) {
            result.push({row: row, column: column - 1});
        }

        // down
        if (items[column + 1] && items[column + 1][row]) {
            result.push({row: row, column: column + 1});
        }

        // left
        if (items[column] && items[column][row - 1]) {
            result.push({row: row - 1, column: column});
        }

        // right
        if (items[column] && items[column][row + 1]) {
            result.push({row: row + 1, column: column});
        }

        return result;
    }

    showBetLine(betLineIndex, speed = 1, disableOtherLine = true) {
        if (disableOtherLine) {
            for (let i = 0; i < this.betLines.length; i++) {
                this.betLinesContainer.children[i].alpha = 0;
            }
        }

        return new Promise((resolve) => {
            TweenMax.to(this.betLinesContainer.getChildAt(betLineIndex), 1 / speed, {
                alpha: 1,
                repeat: 1,
                yoyo: true,
                ease: Power4.easeOut,
                onComplete: () => {

                    // just to make sure the line will always BE dismissed
                    setTimeout(() => {
                        this.dismissBetLine(betLineIndex, speed);
                    }, 1000 / speed);

                    resolve();
                }
            });
        })
    }

    showAllBetLines(speed = 1) {
        let allPromises = [];
        allPromises.push(this.dismissAllBetLines());
        if (state.get('betLine') <= 1) {
            this.shownMaxLine = false;
            allPromises.push(this.showBetLine(0));
        }
        else {
            if (this.shownMaxLine) {
                this.shownMaxLine = false;
            }
            else {
                this.shownMaxLine = true;
                for (let i = 0; i < this.betLines.length; i++) {
                    allPromises.push(this.showBetLine(i, speed, false));
                }
            }
        }

        return Promise.all(allPromises);
    }

    dismissBetLine(betLineIndex, speed = 1) {
        return new Promise((resolve) => {
            TweenMax.to(this.betLinesContainer.getChildAt(betLineIndex), 0.5 / speed, {
                alpha: 0,
                onComplete: resolve,
            });
        });
    }

    dismissAllBetLines() {
        let allPromises = [];
        for (let i = 0; i < this.betLines.length; i++) {
            allPromises.push(this.dismissBetLine(i));
        }

        return Promise.all(allPromises);
    }

    // SHOULD NOT call showMatchedBetLines
    showMatchedBetLine(codes, speed) {

        return Promise.all(map(codes, (code) => {

            const rowAndColumn = utils.getRowAndColumn(code);

            const reel = this.reels[rowAndColumn.column];
            const reelItems = reel.getItems();

            return new Promise((resolve) => {
                setTimeout(() => {

                    // todo: play animation should be promise based
                    reelItems[rowAndColumn.row].playAnimation({
                        animationName: 'state_1',
                        loop: true,
                    });

                    resolve();
                }, 500 / speed);
            })
        }))
    }

    setMasking(mask: PIXI.Sprite | PIXI.Graphics) {
        this.container.mask = mask;
        app.stage.addChild(mask);
    }

    // passing in rows and columns to play reel items animation
    playAnimation(rowsAndColumns, animationName = 'win') {

        // support single row and column.
        if (!Array.isArray(rowsAndColumns)) {
            rowsAndColumns = [rowsAndColumns];
        }

        each(rowsAndColumns, (rowAndColumn) => {
            const reel = this.reels[rowAndColumn.column];
            const reelItems = reel.getItems();
            reelItems[rowAndColumn.row].playWin(animationName);
        })
    }

    addAnimation(rowsAndColumns, animationName = 'idle_1') {

        // support single row and column.
        if (!Array.isArray(rowsAndColumns)) {
            rowsAndColumns = [rowsAndColumns];
        }

        each(rowsAndColumns, (rowAndColumn) => {
            const reel = this.reels[rowAndColumn.column];
            const reelItems = reel.getItems();
            reelItems[rowAndColumn.row].addAnimation(animationName);
        })
    }

    // remove all reel items that containers visible is false
    cleanReels() {
        const reelItems = this.getReelItems();
        each(reelItems, (reel, column) => {
            each(reel, (reelItem, row) => {
                if (!reelItem.container.visible) {
                    this.reels[column].removeReelItem(row);
                }
            })
        });
    }
}
