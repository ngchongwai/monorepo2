import { SpineItem } from './SpineItem';
import { each } from 'lodash';

export class CoverableItem extends SpineItem {
    private coverObject: any;

    constructor(asset: any, symbol: string = null, idleAnimation: string = 'idle_1', coverObject = null) {
        super(asset, symbol, idleAnimation);

        if(coverObject) {
            this.coverObject = new PIXI.extras.AnimatedSprite(coverObject);
            this.coverObject.visible = false;
            this.coverObject.anchor.set(0.5);
            this.coverObject.animationSpeed = 1;
            this.container.addChild(this.coverObject);
        }
    }


    cover(speed = 1) {
        if (!this.coverObject) {
            throw new Error('need Cover object');
        }
        this.coverObject.visible = true;
        this.coverObject.animationSpeed = speed;
        this.coverObject.play();
    }

    dismissCover() {
        if (!this.coverObject) {
            throw new Error('need Cover object');
        }
        this.coverObject.visible = false;
        this.coverObject.stop();
    }

    changeItem(asset: any) {
        super.changeItem(asset);
        each(this.container.children, (child) => {
            if(child.name === 'win glow') {
                this.container.setChildIndex(child, this.container.children.length - 1);
            }
        })
        this.container.swapChildren(this.item, this.coverObject);
    }

}