import * as PIXI from 'pixi.js';
import AnimatedSprite = PIXI.extras.AnimatedSprite;
import Spine = PIXI.spine.Spine;
import { each } from 'lodash';

import { SpineWinGlow, SpriteWinGlow } from '../WinGlowObject';

export abstract class ReelItem {

    /**
     * Notes: The hierarchy of the reel item
     * container is the wrapper for holding background, item and win glow
     * the 0 index of container's children is the background.
     * 1 and 2 are reserved for item or win glow
     */
    container: PIXI.Container;
    item: AnimatedSprite | Spine;
    background: PIXI.extras.AnimatedSprite | Spine;
    winGlowObject = null;
    masking = null;
    symbol: string;

    animationSpeed: number = 1;

    protected constructor(symbol) {
        this.container = new PIXI.Container();
        this.symbol = symbol;
    }

    getBounds() {
        return this.container.getBounds();
    }

    setScale(value) {
        this.item.scale.set(value);
    }

    setAlpha(value) {
        this.item.alpha = value;
    }

    setBackground(backgroundObject, isSpine) {

        if (!isSpine) {
            this.background = new PIXI.extras.AnimatedSprite(backgroundObject);
            this.background.animationSpeed = 1;
            this.background.loop = true;
            this.background.anchor.set(0.5);
        }
        else {
            this.background = new Spine(backgroundObject);
            this.background.state.setAnimation(0, 'idle_1', true);
        }

        this.background.name = 'background';

        // make sure remove previous background
        if (this.container.getChildAt(0).name === 'background') {
            this.container.removeChildAt(0);
        }

        this.container.addChildAt(this.background, 0);
    }

    setWinGlow(winGlowAsset, isSpine, animation = 'state_1') {

        this.winGlowObject = null;
        if (isSpine) {
            this.winGlowObject = new SpineWinGlow(winGlowAsset, animation);
        } else {
            this.winGlowObject = new SpriteWinGlow(winGlowAsset);
        }

        //! get the totem Layer
        let targetIndex = null;
        each(this.container.children, (child, index) => {
            if (child.name === 'item') {
                targetIndex = index;
                return;
            }
        });

        // reminder for developer
        if (targetIndex === null) {
            throw new Error('Reel item is not init yet');
        }

        //! put win glow object behind the totem layer
        this.container.addChildAt(this.winGlowObject.winGlowObject, targetIndex);
    }

    // masking slot item container
    setMasking(mask) {
        this.masking = new PIXI.Sprite(mask);
        this.masking.name = 'masking';
        this.masking.anchor.set(0.5);
        this.container.mask = this.masking;
        this.container.addChildAt(this.masking, 0);
    }

    abstract changeItem(asset: any): void;

    abstract playAnimation(options: any): void;

    abstract addAnimation(options: any): void;

    abstract playWin(options: any): void;

    abstract playLose(): void;

    abstract playIdle(): void;

    abstract playSpin(): void;
}

