import * as PIXI from 'pixi.js';
import Spine = PIXI.spine.Spine;

import { SpineItem } from './SpineItem';

export interface Explodable {
    explosionJson: any;

    explode(explodeAnimation): Promise<any>;
}

export class ExplodableSpineItem extends SpineItem implements Explodable {

    explosionJson: any;

    constructor(asset: any, symbol: string = null, explosionJson) {
        super(asset, symbol);

        this.explosionJson = explosionJson;
    }

    explode(explodingAnimation) {
        if (!explodingAnimation) {
            explodingAnimation = 'win_1'
        }
        const explosionItem = new Spine(this.explosionJson);
        this.container.addChild(explosionItem);
        this.item.alpha = 0;

        // explode background if it exists.
        if (this.background) {
            const background = <Spine> this.background;
            background.state.setAnimation(0, explodingAnimation, false);
            background.state.addListener({
                complete: () => {
                    background.alpha = 0;
                }
            })
        }

        // play explosion animation
        return new Promise((resolve) => {
            explosionItem.state.setAnimation(0, explodingAnimation, false);
            explosionItem.state.addListener({
                complete: () => {
                    this.container.visible = false;
                    resolve();
                }
            });
        });

    }
}