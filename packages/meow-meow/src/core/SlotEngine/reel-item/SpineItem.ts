import { ReelItem } from './ReelItem';
import Spine = PIXI.spine.Spine;

export class SpineItem extends ReelItem {

    private idleAnimationName;

    constructor(asset: any, symbol: string = null, idleAnimation: string = 'idle_1') {
        super(symbol);

        this.item = new Spine(asset);
        this.item.name = 'item';
        this.item.state.setAnimation(0, idleAnimation, true);

        this.container.addChild(this.item);

        this.idleAnimationName = idleAnimation;
    }

    // todo: implementation
    changeItem(asset: any) {
        let temp = this.item;
        this.container.removeChild(this.item);

        this.item = new Spine(asset);

        temp.destroy();
        
        this.container.addChild(this.item);

        // play idle animation when it changed
        this.playAnimation({
            animationName: this.idleAnimationName,
            loop: true,
        })
    }

    playAnimation(options: any) {

        return new Promise((resolve) => {
            options = Object.assign({
                animationName: null,
                loop: null,
            }, options);

            (<Spine> this.item).state.setAnimation(0, options.animationName, options.loop);
            (<Spine> this.item).state.clearListeners();
            (<Spine> this.item).state.addListener({
                complete: () => {
                    resolve(0);
                }
            });
        });
    }

    addAnimation(options: any) {
        (<Spine> this.item).state.addAnimation(0, options, true, 0);
    }

    playWin(animationName = 'win', loop = false) {

        if (Array.isArray(animationName)) {
            let tempRandomAnimation = Math.floor(Math.random() * animationName.length);
            animationName = animationName[tempRandomAnimation];
        }

        let allPromises = [
            this.playAnimation({animationName, loop})
        ];

        let winGlowPromise = null;
        if (this.winGlowObject) {
            winGlowPromise = this.winGlowObject.play();
        }
        allPromises.push(winGlowPromise);

        return Promise.all(allPromises);
    }

    playLose(animationName = 'lose', loop = false) {
        this.playAnimation({animationName, loop});
    }

    playIdle(animationName = 'idle', loop = false) {
        this.playAnimation({animationName, loop});
    }

    playSpin(animationName = 'spin', loop = false) {
        this.playAnimation({animationName, loop});
    }
}