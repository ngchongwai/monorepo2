export interface RowAndColumn {
    row: number;
    column: number;
}
