import { Power2, TweenMax } from 'gsap';
import * as PIXI from 'pixi.js';
import Spine = PIXI.spine.Spine;

export abstract class WinGlowObject {
    winGlowObject;

    abstract play();
}

export class SpineWinGlow extends WinGlowObject {

    private winGlowAnimation : string;

    constructor(winGlowAsset, animationName = 'state_1') {
        super();
        this.winGlowAnimation = animationName;
        this.winGlowObject = new Spine(winGlowAsset);
        this.winGlowObject.name = 'win glow';
        this.winGlowObject.autoUpdate = true;
        this.winGlowObject.state.setAnimation(0, this.winGlowAnimation, false);
        this.winGlowObject.alpha = 0;
    }

    play() {

        return new Promise((resolve) => {
            // todo: implementation
            this.winGlowObject.alpha = 1;
            this.winGlowObject.state.clearListeners();
            this.winGlowObject.state.setAnimation(0, this.winGlowAnimation, false);
            this.winGlowObject.state.addListener({
                complete: (data) => {
                    if (data.animation.name === this.winGlowAnimation) {
                        this.winGlowObject.alpha = 0;
                        resolve();
                    }
                },
            });

        });
    }
}

export class SpriteWinGlow extends WinGlowObject {

    constructor(winGlowAsset) {
        super();
        this.winGlowObject = new PIXI.Sprite(winGlowAsset);
        this.winGlowObject.name = 'win glow';
        this.winGlowObject.anchor.set(0.5);
        this.winGlowObject.alpha = 0;
    }

    play(speed = 1) {
        return new Promise((resolve) => {
            TweenMax.fromTo(this.winGlowObject, 0.3 / speed, {alpha: 0}, {
                alpha: 1,
                repeat: 1,
                yoyo: true,
                ease: Power2.easeOut,
                onComplete: resolve,
            })
        })

    }
}
