import * as crosstab from 'crosstab';
import browserSupport from "./browserSupport";
import { each } from 'lodash';
import environment from 'environment';



let isMobile = browserSupport.isMobile();

// cross tab variable
let tabID = 0;
let tabInfo;

// Single Game Instance
export function singleInstanceChecking() {
    if (!isMobile) {

        tabID = new Date().getTime();

        tabInfo = {id: tabID, gameName: environment.gameName};

        crosstab.on('checking', function (message) {

            if(tabID === message.data.id) {
                return;
            }

            if(message.data.gameName === environment.gameName) {
                let canvas = document.getElementsByTagName('canvas')[0];
                let target = document.getElementsByTagName('body')[0];
                target.removeChild(canvas);
            }
        });

        crosstab.broadcast('checking', tabInfo);
    }
}



export default {
    singleInstanceChecking
}