import * as PIXI from 'pixi.js';
import 'pixi-spine';
import 'gsap';
import { each } from 'lodash';
import environment from 'environment';

import state from './state';

import app, { isMobile, isAndroid } from './app';
import api from './api';
import sound from './sound';
import utils from './utils';
import ui from './ui';
import auto from './auto';
import cheat from './cheat';
import betHistory from './betHistory';
import backing from './backing';
import sync from './sync';

import FreeSpin from './freeSpin'
import FreeBet from './freeBet'
import tutorial from './tutorial';
import session from './session';
import { Power0, Back, TweenMax } from "gsap";
import Big from 'big.js';

let loaded = PIXI.loader.resources;

let panelContainer;

//! Mobile Vars

let betAmountLabel;
let betLineLabel;

//! Panels
let panelSprite;
let closePopUpSprite;
let totalBetButton;
let betContainer;
let betBackground;
let menuContainer;
let spinButtonEffect;
let autoSpinButtonEffect;
let spin100ButtonEffect;
let betMaxSymbol;

//! Labels
let totalBetLabel;
let betMaxLabel;
let betMaxButton;
let turboLabel;

//! Buttons
let spinButton;
let turboButton;
let spin100Button;
let autoSpinButton;
let betAmountIncreaseButton;
let betAmountDecreaseButton;
let betLineDecreaseButton;
let betLineIncreaseButton;
let turboSymbol;

// todo this is new, keep
let settingsButton;
let settingsContainer;
let infoContainer;
let musicButton;
let soundButton;
let exitButton;
let transactionHistoryButton;

// todo this is old, delete once done
let infoMenuButton;
let infoButton;
let tutorialButton;

let spinArea;

//! Text
let betAmountText;
let betLineText;
let totalBetText;

//! Bet Amount and Bet Line index
let betAmountIndex = 0;
let betAmountLength = 0;
let betAmountList;
let betLineIndex = 0;
let betLineLength = 0;
let speedUp = false;

let spinFn;
let spin100Fn;
let isGameRunning;
let spinIsPressed = false;

let autoSpinRunning = false;
let autoSpinCriteria;
let autoSpinState;

let usingBetLines = true;
let fixMultiBetLine = false;

let autoSpinCountdown;
let autoSpinCountdownInt = 0;
let spriteNumberTexture = [];

let spin100YTextOffset;
let spin100YOffset;
let spin100YAddDelay;
let spin100NumberBacking;
let spin100CustomNumberSprite;
let spin100Spine = null;

let winEffectCustomNumberSprite;

let particles;

// Charge Effect
let chargeTargetPosition;
let chargeContainer;

let balanceMask;

let primaryTint;
let panelDisplayContainer;

// win balance animation variable
//! to prevent run more than one time
let canWinAnimLoop = true;
let isExitAutoSpin = false;
let displayWalletAnimation = false;

let autoSpinCountdownScale;

const token = utils.getQueryParam('token');

let freeSpin;
let freeBet;

function init(options) {

    if (!state.get('currency')) {
        state.set('balance', 0);
        state.set('currency', ' ');
    }
    state.set('gotWarning', false);

    //! exit game pop up todo uncomment when comfirmation pop uo needed
    if (environment.environmentName === 'production' || environment.environmentName === 'demo') {
        window.onbeforeunload = function () {
            return exitConfirmation();
        };
    }

    spriteNumberTexture = {
        '0': loaded.tex_num_zero.texture,
        '1': loaded.tex_num_one.texture,
        '2': loaded.tex_num_two.texture,
        '3': loaded.tex_num_three.texture,
        '4': loaded.tex_num_four.texture,
        '5': loaded.tex_num_five.texture,
        '6': loaded.tex_num_six.texture,
        '7': loaded.tex_num_seven.texture,
        '8': loaded.tex_num_eight.texture,
        '9': loaded.tex_num_nine.texture,
        '$': loaded.tex_num_dollar_sign.texture,
        '.': loaded.tex_num_dot.texture,
        ',': loaded.tex_num_comma.texture,
    };

    particles = [
        loaded.particle_a1.texture,
        loaded.particle_a2.texture,
        loaded.particle_a3.texture,
        loaded.particle_a4.texture,
        loaded.particle_a5.texture,
        loaded.particle_a6.texture,
        loaded.particle_a7.texture,
        loaded.particle_a8.texture
    ];

    if (!options.spinFn) {
        throw new Error('spinFn is required');
    }

    spinFn = options.spinFn;
    delete options.spinFn;

    spin100Fn = options.spin100Fn;
    delete options.spin100Fn;

    const newOptions = Object.assign({
        primaryTint: '0x09B6FF',
        secondaryTint: '0xbebebe',
        betAmountOptions: [1, 5, 10, 25, 50, 100, 500],
        betLineOptions: [1, 2, 3],
        spinButtonY: isMobile() ? 1080 : 630,
        spinButtonHitArea: 0,
        autoSpinButtonX: isMobile() ? 145 : 1216,
        autoSpinButtonY: isMobile() ? 1080 : 610,
        turboButtonX: isMobile() ? 170 : 925,
        turboButtonY: isMobile() ? 1180 : 700,
        spin100ButtonX: isMobile() ? 575 : 1216,
        spin100ButtonY: isMobile() ? 1080 : 678,
        spinAreaConfigX: isMobile() ? 360 : 0,
        spinAreaConfigY: isMobile() ? 890 : 0,
        autoSpinCountdownConfigX: isMobile() ? app.getResolutionX() / 2 - 2 : 1097,
        autoSpinCountdownConfigY: isMobile() ? 1083 : 630,
        autoSpinCountdownScale: isMobile() ? 1 : 1,
        spinAreaConfigWidth: isMobile() ? 720 : 0,
        spinAreaConfigHeight: isMobile() ? 460 : 0,
        betLinesUsage: true,
        gotBonusOffset: true,
        spin100YTextOffset: 0,
        spin100YAddDelay: 0,
        spin100TextBacking: false,
        spin100CustomNumberSprites: spriteNumberTexture,
        spin100BackingAlpha: 0.5,
        cheatRadioY: isMobile() ? 920 : app.getResolutionY() - 100,
        cheatRadioX: 60,
        fixMultiBetLine: false,
        spin100YOffset: 0,
        winEffectCustomNumberSprite: spriteNumberTexture,
        chargeTargetPosition: {x: 50, y: 50},
        autoSpinOption: {}
    }, options);

    autoSpinCountdownScale = newOptions.autoSpinCountdownScale;

    spin100CustomNumberSprite = newOptions.spin100CustomNumberSprites;
    winEffectCustomNumberSprite = newOptions.winEffectCustomNumberSprite;

    primaryTint = newOptions.primaryTint;

    betAmountList = state.get('betAmountList') ? state.get('betAmountList') : newOptions.betAmountOptions;

    spin100YTextOffset = newOptions.spin100YTextOffset;
    spin100YOffset = newOptions.spin100YOffset;
    spin100YAddDelay = newOptions.spin100YAddDelay;
    spin100NumberBacking = newOptions.spin100TextBacking;

    fixMultiBetLine = newOptions.fixMultiBetLine;
    usingBetLines = newOptions.betLinesUsage;
    betLineLength = newOptions.betLineOptions.length;
    betAmountLength = betAmountList.length;

    // init charge variables
    chargeTargetPosition = newOptions.chargeTargetPosition;
    chargeContainer = new PIXI.Container();
    app.addComponent(chargeContainer);

    // Set the Bet Amount and Bet Line / Bet Multiplier to to minimum value at start of the game
    state.set('betAmount', betAmountList[0]);
    betLineIndex = usingBetLines ? newOptions.betLineOptions.length - 1 : 0;
    let multiBetLineSetting = newOptions.betLineOptions[betLineLength - 1];
    state.set('betLine', multiBetLineSetting);

    // load core sounds
    sound.addSound('buttonClickSound', loaded.buttonClickSound.sound, {loop: false, volume: 1});
    sound.addSound('buttonClickReleaseSound', loaded.buttonClickReleaseSound.sound, {loop: false, volume: 1});
    sound.addSound('100SpinSound', loaded.spin100Sound.sound, {loop: false, volume: 1});
    sound.addSound('coinSound', loaded.coinSound.sound, {loop: true, volume: 1});
    sound.addSound('coinFlipSound', loaded.coinFlipSound.sound, {loop: true, volume: 1});
    sound.addSound('coinFlipSoundEnd', loaded.coinFlipSoundEnd.sound, {loop: false, volume: 1});

    // Create the text of Bet Amount and text of Bet Line
    betAmountText = ui.createText({
        fontSize: isMobile() ? 46 : 20,
        fontWeight: 'bold'
    }, state.get('betAmount'));
    betLineText = ui.createText({
        fontSize: isMobile() ? 46 : 20,
        fontWeight: 'bold'
    }, state.get('betLine'));

    // A new panel container to store all UI items in landscape mode
    panelContainer = new PIXI.Container();
    panelContainer.name = 'Panel Container';

    // Panel background for top of the UI
    if (isMobile()) {
        panelSprite = new PIXI.Graphics();
        panelSprite.beginFill(0x000000);
        panelSprite.drawRect(0, 1173, app.getResolutionX(), 112);
        panelSprite.endFill();
    } else {
        panelSprite = new PIXI.Sprite(loaded.panel.texture);
        panelSprite.anchor.set(0, 1);
        panelSprite.y = 720;
        panelSprite.alpha = 0.85;
        panelSprite.height = 80;
    }
    panelSprite.name = 'Panel Sprite';
    panelContainer.addChild(panelSprite);

    // An empty sprite that serves to close any opened menu / settings
    closePopUpSprite = new PIXI.Sprite(PIXI.Texture.EMPTY);
    closePopUpSprite.name = 'Pop Up Closer';
    closePopUpSprite.width = isMobile() ? 720 : 1280;
    closePopUpSprite.height = isMobile() ? 1280 : 720;
    closePopUpSprite.interactive = isMobile() ? true : false;
    closePopUpSprite.buttonMode = true;
    closePopUpSprite.on('pointertap', () => {
        if (isMobile()) {
            // Close menu container
            if (menuContainer.y <= 284) {
                spinArea.interactive = true;
                TweenMax.to(menuContainer, 0.5, {y: 1500});
            }

            // Close bet container
            if (betContainer.y <= 920) {
                spinArea.interactive = true;
                TweenMax.to(betContainer, 0.5, {y: 1280});
            }

            // If the game has free spins on its own. We must hide / show the free spin UI that overlaps with panel.
            // Not used for freeSpin & freeBet in core
            panelContainer.emit('alphaFreeSpinNotificationContainer', ({
                'alpha': 1,
                'timeout': 200
            }));
        } else {
            // Close settings container
            if (settingsContainer.y <= 467) {
                TweenMax.to(settingsContainer, 1, {y: 645});
            }

            if (infoContainer.y <= 530) {
                TweenMax.to(infoContainer, 0.5, {y: 680});
            }
            closePopUpSprite.interactive = false;
        }

    });

    let cheatRadioGroup;
    if (environment.allowCheatMode) {
        let options = newOptions.gotBonusOffset ? [
            {label: 'off', value: 'off'},
            {label: 'win', value: 'win'},
            {label: 'bonus', value: 'bonus'},
        ] : [
            {label: 'off', value: 'off'},
            {label: 'win', value: 'win'},
        ];

        //Shows the cheat mode as win if there is no bonus
        if (cheat.getCheatMode() === 'bonus' && !newOptions.gotBonusOffset) {
            cheat.setCheatMode('win');
        }

        // Initialize the radio group to set cheat mode values
        // Refresh the page every time a value is changed
         cheatRadioGroup = ui.createRadioGroup({
            offset: 15,
            onTexture: loaded.radioButtonBackgroundAutoSpinOn.texture,
            offTexture: loaded.radioButtonBackgroundAutoSpinOff.texture,
            options: options,
            fontSize: isMobile() ? 20 : 16
        });
        cheatRadioGroup.x = newOptions.cheatRadioX;
        cheatRadioGroup.y = newOptions.cheatRadioY;
        cheatRadioGroup.on('valueChanged', (data) => {
            data === 'off' ? cheat.deleteCheatMode() : cheat.setCheatMode(data);
            window.location.reload();
        });

        // Hack to show which button is already active
        each(cheatRadioGroup.children, (child) => {
            if ((!cheat.getCheatMode() && child.value === 'off') || (cheat.getCheatMode() === child.value)) {
                child.texture = loaded.radioButtonBackgroundAutoSpinOn.texture;
                child.interactive = false;
                child.buttonMode = false;

                // Breaks the lodash loop
                return false;
            }
        });
    }

    // Balance Mask to mask balance and win to the balance box
    balanceMask = new PIXI.Graphics();
    balanceMask.beginFill(0xffffff);
    if (isMobile()) {
        balanceMask.drawRect(0, 0, app.getResolutionX(), 55);
    } else {
        balanceMask.drawRect(123, 645, 170, 70);
    }
    balanceMask.endFill();
    balanceMask.alpha = 0.35;

    if (isMobile()) {
        let top_bar_background = new PIXI.Sprite(loaded.top_bar_background.texture);
        panelContainer.addChild(top_bar_background);

        spinArea = ui.createButton({
            texture: loaded.spinAreaSlot ? loaded.spinAreaSlot.texture : PIXI.Texture.EMPTY,
            centerAnchor: true,
            onPointerUp: spinButtonClicked,
        });
        spinArea.name = 'Spin Area';
        spinArea.renderable = false;
        spinArea.x = newOptions.spinAreaConfigX;
        spinArea.y = newOptions.spinAreaConfigY;
        spinArea.width = newOptions.spinAreaConfigWidth;
        spinArea.height = newOptions.spinAreaConfigHeight;

        // Menu button that shows more buttons when clicked
        let menuButton = new ui.createButton({
            texture: loaded.menuButton.texture,
            onPointerUp: function () {
                // Opens menu container
                if (menuContainer.y >= 1500) {
                    TweenMax.to(menuContainer, 0.5, {y: 284});
                    spinArea.interactive = false;
                }
                // Closes bet container
                if (betContainer.y <= 920) {
                    TweenMax.to(betContainer, 0.5, {y: 1280});
                }

                panelContainer.emit('alphaFreeSpinNotificationContainer', ({
                    'alpha': 0,
                    'timeout': 50
                }));
            }
        });
        menuButton.x = 24;
        menuButton.y = 1180;
        menuButton.name = 'Menu Button';
        //#region Menu
        // Menu Container stores all menu items inside
        menuContainer = new PIXI.Container();
        menuContainer.y = 1500;
        menuContainer.name = 'menu container';

        let menuBackground = new PIXI.Sprite(loaded.background_menu.texture);
        menuBackground.interactive = true;

        let menuCloseButton = new ui.createButton({
            texture: loaded.button_slidedown.texture,
            onPointerUp: function () {
                if (menuContainer.y >= 0) {
                    TweenMax.to(menuContainer, 0.5, {y: 1500});
                    spinArea.interactive = true;

                    panelContainer.emit('alphaFreeSpinNotificationContainer', ({
                        'alpha': 1,
                        'timeout': 200
                    }));
                }
            }
        });
        menuCloseButton.x = 24;
        menuCloseButton.y = 17;
        menuCloseButton.name = 'menuCloseButton';

        // Menu title
        let menuTitleText = ui.createText({fontSize: 45, fontWeight: 'bold'}, loaded.languageFile.data.menu);
        menuTitleText.name = 'Menu Title Text';
        menuTitleText.x = app.getResolutionX() / 2;
        menuTitleText.y = 66;
        menuTitleText.anchor.set(0.5);

        // Game Rules
        let gameRulesButton = ui.createButton({
            fontSize: 35,
            texture: loaded.button_rectangle.texture,
            text: loaded.languageFile.data.game_rules,
            TextX: 150,
            fontWeight: 'normal',
            textAnchorX: 0,
            textAnchorY: 0.5,
            onPointerUp: function () {
                panelContainer.emit('gameRulesButtonClicked');
            }
        });
        gameRulesButton.name = 'Game Rules Text';
        gameRulesButton.x = 143;
        gameRulesButton.y = 560;

        let gameRulesButtonSymbol = new PIXI.Sprite(loaded.button_gamerules_menu_p.texture);
        gameRulesButtonSymbol.x = 80;
        gameRulesButtonSymbol.y = 30;

        // Paytable
        let paytableButton = ui.createButton({
            fontSize: 35,
            texture: loaded.button_rectangle.texture,
            text: loaded.languageFile.data.paytable,
            TextX: 150,
            fontWeight: 'normal',
            textAnchorX: 0,
            textAnchorY: 0.5,
            onPointerUp: function () {
                panelContainer.emit('infoButtonClicked');
            }
        });
        paytableButton.name = 'Paytable Text';
        paytableButton.x = 143;
        paytableButton.y = 415;

        let paytableButtonSymbol = new PIXI.Sprite(loaded.button_paytable_menu_p.texture);
        paytableButtonSymbol.x = 80;
        paytableButtonSymbol.y = 30;

        //todo enable when fixed transaction history and account
        // Bet History
        let transactionHistoryText = ui.createButton({
            fontSize: 25,
            texture: loaded.button_bet_history_menu.texture,
            text: loaded.languageFile.data.transaction_history,
            fontWeight: 'normal',
            TextY: -30,
            align: 'left',
            onPointerUp: betHistory.show

        });
        transactionHistoryText.name = 'Transaction History Text';
        transactionHistoryText.x = 460;
        transactionHistoryText.y = 262;

        // Music Text
        let musicButton = ui.createToggleButton({
            fontSize: 25,
            textureOn: loaded.button_music_mute_menu.texture,
            textureOff: loaded.button_music_menu.texture,
            text: loaded.languageFile.data.music,
            fontWeight: 'normal',
            TextY: -30,
            onPointerUp: function () {
                panelContainer.emit('musicButtonClicked');
                if (sound.isMusicMuted()) {
                    sound.unmuteMusic();
                }
                else {
                    sound.muteMusic();
                }
            }
        });
        musicButton.name = 'Music Text';
        musicButton.x = 300;
        musicButton.y = 262;

        // Sound
        let soundButton = ui.createToggleButton({
            fontSize: 25,
            textureOn: loaded.button_sound_mute_menu.texture,
            textureOff: loaded.button_sound_menu.texture,
            text: loaded.languageFile.data.sound,
            fontWeight: 'normal',
            TextY: -30,
            onPointerUp: function () {
                panelContainer.emit('soundButtonClicked');

                if (sound.isSoundMuted()) {
                    sound.unmuteSound();
                }
                else {
                    sound.muteSound();
                }
            },
        });
        soundButton.name = 'Sound Text';
        soundButton.x = 141;
        soundButton.y = 262;

        // Tutorial Game Text
        let tutorialButton = ui.createButton({
            fontSize: 35,
            texture: loaded.button_rectangle.texture,
            centerAnchor: true,
            text: loaded.languageFile.data.tutorial,
            TextX: 150,
            fontWeight: 'normal',
            textAnchorX: (0),
            textAnchorY: (0.5),
            onPointerUp: function () {
                tutorial.show();
                TweenMax.to(menuContainer, 0.5, {y: 1500});
            }
        });
        tutorialButton.name = 'Tutorial Game Text';
        tutorialButton.x = 148;
        tutorialButton.y = 705; // 735
        tutorialButton.anchor.set(0);

        let tutorialButtonSymbol = new PIXI.Sprite(loaded.button_tutorial_menu_p.texture);
        tutorialButtonSymbol.x = 80;
        tutorialButtonSymbol.y = 30;

        // Quit Game Text
        let quitGameButton = ui.createButton({
            fontSize: 35,
            texture: loaded.button_rectangle.texture,
            centerAnchor: true,
            text: loaded.languageFile.data.quit_game,
            TextX: 150,
            fontWeight: 'normal',
            textAnchorX: 0,
            textAnchorY: 0.5,
            onPointerUp: exitGame
        });
        quitGameButton.name = 'Quit Game Text';
        quitGameButton.x = 143;
        quitGameButton.y = 850; // 460
        quitGameButton.anchor.set(0);

        let quitGameButtonSymbol = new PIXI.Sprite(loaded.button_quitgame_menu_p.texture);
        quitGameButtonSymbol.x = 80;
        quitGameButtonSymbol.y = 30;
        //#endregion

        // Turbo Button in portrait mode
        turboButton = ui.createToggleButton({
            textureOn: loaded.betMaxButton.texture,
            textureOff: loaded.betMaxButton.texture,
            onPointerUp: onTurboButtonClicked
        });
        turboButton.name = 'Turbo Button';
        turboButton.interactive = false;
        turboButton.x = newOptions.turboButtonX;
        turboButton.y = newOptions.turboButtonY;
        turboSymbol = new PIXI.Sprite(loaded.turboSymbol.texture);
        turboSymbol.x = turboButton.x + (turboButton.width / 4) + 9;
        turboSymbol.y = turboButton.y + (turboButton.height / 4);

        totalBetButton = ui.createButton({
            texture: loaded.buttonMenu.texture,
            onPointerUp: function () {
                // Closes bet container
                if (betContainer.y <= 920) {
                    spinArea.interactive = true;
                    TweenMax.to(betContainer, 0.5, {y: 1280});

                    panelContainer.emit('alphaFreeSpinNotificationContainer', ({
                        'alpha': 1,
                        'timeout': 200
                    }));
                }
                // Opens bet container
                else if (betContainer.y >= 1280) {
                    spinArea.interactive = false;
                    TweenMax.to(betContainer, 0.5, {y: 668});

                    panelContainer.emit('alphaFreeSpinNotificationContainer', ({
                        'alpha': 0,
                        'timeout': 50
                    }));

                    // Tween menu container in if it is out
                    if (menuContainer.y >= 0) {
                        TweenMax.to(menuContainer, 0.5, {y: 1500});
                    }
                }
            }
        });
        totalBetButton.x = 458;
        totalBetButton.y = 1180;
        totalBetButton.name = 'Total Bet Button';
        totalBetLabel = ui.createText({fontSize: 25, fontWeight: 'bold'}, loaded.languageFile.data.total_bet + ' :');
        totalBetLabel.tint = newOptions.primaryTint;
        totalBetLabel.x = 560;
        totalBetLabel.y = 33;
        totalBetLabel.anchor.set(1, 0.5);
        totalBetLabel.name = 'Total Bet Label';
        //#region Bet Option
        // Total Bet Text in portrait mode
        totalBetText = ui.createText({
            fontSize: 25,
            fontWeight: 'bold'
        }, state.get('currency') + ' ' + utils.getTotalBet());
        totalBetText.x = 690;
        totalBetText.y = 33;
        totalBetText.anchor.set(1, 0.5);

        // Bet Container that holds all bet options such as bet lines and amount
        betContainer = new PIXI.Container();
        betContainer.name = 'Bet Container';
        betContainer.y = 1280;

        // Background Sprite in Bet Container in portrait mode
        betBackground = new PIXI.Sprite(loaded.bet_option_background.texture);
        betBackground.interactive = true;

        let betConfirm = ui.createButton({
            texture: loaded.button_bet_confirm.texture,
            text: loaded.languageFile.data.confirmation,
            fontSize: 35,
            onPointerUp: function () {
                // Closes bet container
                if (betContainer.y <= 920) {
                    spinArea.interactive = true;
                    TweenMax.to(betContainer, 0.5, {y: 1280});

                    panelContainer.emit('alphaFreeSpinNotificationContainer', ({
                        'alpha': 1,
                        'timeout': 200
                    }));
                }
            }
        });
        betConfirm.x = app.getResolutionX() / 2 - betConfirm.width / 2;
        betConfirm.y = 453;
        betConfirm.name = 'Total Bet Button';

        let betCloseButton = new ui.createButton({
            texture: loaded.button_slidedown.texture,
            onPointerUp: function () {
                if (menuContainer.y >= 0) {
                    TweenMax.to(betContainer, 0.5, {y: 1280});
                    spinArea.interactive = true;

                    panelContainer.emit('alphaFreeSpinNotificationContainer', ({
                        'alpha': 1,
                        'timeout': 200
                    }));
                }
            }
        });
        betCloseButton.x = 24;
        betCloseButton.y = 17;
        betCloseButton.name = 'betCloseButton';

        let betTitle = ui.createText({fontSize: 50, fontWeight: 'bold'}, loaded.languageFile.data.bet_option);
        betTitle.name = 'Bet Lines Label';
        betTitle.x = 360;
        betTitle.y = 70;

        // Bet lines label in portrait mode
        betLineLabel = ui.createText({
            fontSize: 30,
            fontWeight: 'bold'
        }, usingBetLines ? loaded.languageFile.data.bet_lines : loaded.languageFile.data.bet_multiplier);
        betLineLabel.name = 'Bet Lines Label';
        betLineLabel.tint = newOptions.secondaryTint;
        betLineLabel.x = 190;
        betLineLabel.y = 225;

        betLineText.name = 'Bet Lines Text';
        betLineText.x = 190;
        betLineText.y = 315;
        betLineText.text = state.get('betLine');

        // Bet lines decrease button in portrait mode
        betLineIncreaseButton = ui.createButton({
            texture: loaded.increaseButton.texture,
            onPointerUp: function () {
                betLineIndex = changeItem(betLineIndex + 1, newOptions.betLineOptions.length - 1);
                state.set('betLine', newOptions.betLineOptions[betLineIndex]);
                updateText();
                auto.updateSlider();
                // emit event
                panelContainer.emit('betLineChanged', state.get('betLine'));
            }
        });
        betLineIncreaseButton.name = 'Bet Line Increase Button';
        betLineIncreaseButton.x = 254;
        betLineIncreaseButton.y = 270;

        // Bet lines decrease button in portrait mode
        betLineDecreaseButton = ui.createButton({
            texture: loaded.decreaseButton.texture,
            onPointerUp: function () {
                betLineIndex = changeItem(betLineIndex - 1, newOptions.betLineOptions.length - 1);
                state.set('betLine', newOptions.betLineOptions[betLineIndex]);
                updateText();
                auto.updateSlider();
                // emit event
                panelContainer.emit('betLineChanged', state.get('betLine'));
            }
        });
        betLineDecreaseButton.name = 'Bet Line Decrease Button';
        betLineDecreaseButton.x = 24;
        betLineDecreaseButton.y = 270;

        if (fixMultiBetLine) {
            betLineIncreaseButton.interactive = false;
            betLineIncreaseButton.alpha = 0.5;
            betLineDecreaseButton.interactive = false;
            betLineDecreaseButton.alpha = 0.5;
        }

        // Bet Amount Label in portrait mode
        betAmountLabel = ui.createText({
            fontSize: 30,
            fontWeight: 'bold'
        }, loaded.languageFile.data.bet_amount);
        betAmountLabel.name = 'Bet Amount Label';
        betAmountLabel.tint = newOptions.secondaryTint;
        betAmountLabel.x = 533;
        betAmountLabel.y = 215;

        // Bet Amount decrease button in portrait mode
        betAmountDecreaseButton = ui.createButton({
            texture: loaded.decreaseButton.texture,
            onPointerUp: function () {
                betAmountIndex = changeItem(betAmountIndex - 1, betAmountList.length - 1);
                state.set('betAmount', betAmountList[betAmountIndex]);
                updateText();
                auto.updateSlider();
                // emit event
                panelContainer.emit('betAmountChanged', state.get('betAmount'));
            }
        });
        betAmountDecreaseButton.name = 'Bet Amount Decrease Button';
        betAmountDecreaseButton.x = 374;
        betAmountDecreaseButton.y = 270;

        betAmountIncreaseButton = ui.createButton({
            texture: loaded.increaseButton.texture,
            onPointerUp: function () {
                betAmountIndex = changeItem(betAmountIndex + 1, betAmountList.length - 1);
                state.set('betAmount', betAmountList[betAmountIndex]);
                updateText();
                auto.updateSlider();
                // emit event
                panelContainer.emit('betAmountChanged', state.get('betAmount'));
            }
        });
        betAmountIncreaseButton.name = 'Bet Amount Increase Button';
        betAmountIncreaseButton.x = 604;
        betAmountIncreaseButton.y = 270;
        //#endregion

        // Bet amount text positioning in portrait mode
        betAmountText.name = 'Bet Amount Text';
        betAmountText.x = 533;
        betAmountText.y = 315;
        betAmountText.text = state.get('betAmount');

        // Bet Max Button in portrait mode
        betMaxButton = ui.createButton({
            texture: loaded.betMaxButton.texture,
            onPointerUp: function () {
                if (checkIfBettingMax(newOptions)) {
                    betAmountIndex = 0;
                    if (!fixMultiBetLine) {
                        betLineIndex = 0;
                    }
                }
                else {
                    betAmountIndex = betAmountList.length - 1;
                    if (!fixMultiBetLine) {
                        betLineIndex = newOptions.betLineOptions.length - 1;
                    }
                }
                state.set('betAmount', betAmountList[betAmountIndex]);
                state.set('betLine', newOptions.betLineOptions[betLineIndex]);
                panelContainer.emit('betLineChanged', state.get('betLine'));
                panelContainer.emit('betAmountChanged', state.get('betAmount'));
                panelContainer.emit('betMaxButtonClicked');
                auto.updateSlider();
                updateText();
            }
        });
        betMaxButton.name = 'Bet Max Button';
        betMaxButton.x = 604;
        betMaxButton.y = 1180;

        betMaxSymbol = new PIXI.Sprite(loaded.betMaxSymbol.texture);
        betMaxSymbol.x = betMaxButton.x + (betMaxButton.width / 4);
        betMaxSymbol.y = betMaxButton.y + (betMaxButton.height / 4);

        // Spin Button in portrait mode
        spinButton = ui.createButton({
            texture: loaded.spinButton.texture,
            clickedTexture: loaded.spinDownButton.texture,
            centerAnchor: true,
            onPointerUp: spinButtonClicked,
            hitArea: newOptions.spinButtonHitArea,
        });
        spinButton.name = 'Spin Button';
        spinButton.x = 360;
        spinButton.y = newOptions.spinButtonY;

        // Listener to detect when spin finished so we can play idle animation
        panelContainer.on('spinFinished', () => {
            // The queue delay determines the amount of time
            // the program will wait before playing the next animation
            let queueDelay = (Math.random() * 6000) + 1000;
            let randomIdle = Math.floor(Math.random() * 2);
            let idleAnimation = randomIdle === 0 ? 'idle_1' : 'idle_2';

            setTimeout(() => {
                // Set the state of spin animation and add it to the queue
                spinButtonEffect.state.addAnimation(0, idleAnimation, false, -1);
            }, queueDelay);

            // use queueDelay variable to set new random variable.
            queueDelay = (Math.random() * 9500) + 800;

            setTimeout(() => {
                // Set the state of auto spin animation and add it to the queue
                autoSpinButtonEffect.state.addAnimation(0, idleAnimation, false, -1);
            }, queueDelay);

            queueDelay = (Math.random() * 10000) + 1250;

            setTimeout(() => {
                // Set the state of auto spin animation and add it to the queue
                spin100ButtonEffect.state.addAnimation(0, idleAnimation, false, -1);
            }, queueDelay);
        });

        // Spin Button Effect Spine in portrait mode
        spinButtonEffect = new PIXI.spine.Spine(loaded.buttonEffect_json.spineData);
        spinButtonEffect.state.setAnimation(0, 'idle_1', false);
        spinButtonEffect.autoUpdate = true;
        spinButtonEffect.x = 360;
        spinButtonEffect.y = newOptions.spinButtonY;
        spinButtonEffect.state.addListener({
            complete: () => {
                if (isGameRunning) {
                    return;
                }

                // The queue delay determines the amount of time
                // the program will wait before playing the next animation
                const queueDelay = (Math.random() * 2000) + 1000;

                setTimeout(() => {
                    const randomIdle = Math.floor(Math.random() * 2);
                    const idleAnimation = randomIdle === 0 ? 'idle_1' : 'idle_2';

                    // Set the state of animation and add it to the queue
                    spinButtonEffect.state.addAnimation(0, idleAnimation, false, -1);
                }, queueDelay);
            }
        });

        // Auto Spin Countdown in portrait mode (Appears on the Spin Button if auto spin is active)
        autoSpinCountdown = ui.createText({fontSize: 80}, autoSpinCountdownInt);
        autoSpinCountdown.x = newOptions.autoSpinCountdownConfigX;
        autoSpinCountdown.y = newOptions.autoSpinCountdownConfigY;
        autoSpinCountdown.scale.x = newOptions.autoSpinCountdownScaleX;
        autoSpinCountdown.scale.y = newOptions.autoSpinCountdownScaleY;
        autoSpinCountdown.anchor.set(0.5);
        autoSpinCountdown.renderable = false;

        // Auto Spin button in portrait mode
        autoSpinButton = ui.createButton({
            texture: loaded.autoSpinButton.texture,
            clickedTexture: loaded.autoSpinDownButton.texture,
            centerAnchor: true,
            onPointerUp: () => {

                // turn autoSpinRunning to false to stop the auto spin
                if (autoSpinRunning) {
                    stopAutoSpin();

                    return;
                }
                auto.autoSpinPanelShowHide();
                changeAutoSpinButtonTexture(false);
            },
            onPointerDown: () => {
                if (autoSpinRunning) {
                    autoSpinButton.texture = loaded.autoSpinStopDownButton.texture;
                }

                // Play button effect
                autoSpinButtonEffect.state.setAnimation(0, 'click', false);
            },
        });
        autoSpinButton.name = 'Auto Spin Button';
        autoSpinButton.x = newOptions.autoSpinButtonX;
        autoSpinButton.y = newOptions.autoSpinButtonY;

        // Auto Spin Button Spine in portrait mode
        autoSpinButtonEffect = new PIXI.spine.Spine(loaded.buttonEffect_json.spineData);
        autoSpinButtonEffect.scale.set(0.62);
        autoSpinButtonEffect.state.setAnimation(0, 'idle_1', false);
        autoSpinButtonEffect.autoUpdate = true;
        autoSpinButtonEffect.x = newOptions.autoSpinButtonX;
        autoSpinButtonEffect.y = newOptions.autoSpinButtonY;
        autoSpinButtonEffect.state.addListener({
            complete: (event) => {
                // The queue delay determines the amount of time
                // the program will wait before playing the next animation
                const queueDelay = (Math.random() * 9500) + 5000;

                setTimeout(() => {
                    const randomIdle = Math.floor(Math.random() * 2);
                    const idleAnimation = randomIdle === 0 ? 'idle_2' : 'idle_1';

                    // Set the state of animation and add it to the queue
                    autoSpinButtonEffect.state.addAnimation(0, idleAnimation, false, -1);
                }, queueDelay);
            }
        });

        // Spin 100 Button in portrait mode (Instantly spin 100 times, call spin 100 API)
        spin100Button = ui.createButton({
            texture: loaded.spin100Button.texture,
            clickedTexture: loaded.spin100DownButton.texture,
            centerAnchor: true,
            onPointerUp: () => {
                // terminate if the previous game is still running.
                if (isGameRunning) return;
                if (autoSpinRunning) return;

                let balanceBigValue = new Big(state.get('balance'));
                let newBalanceBigValue = parseFloat(balanceBigValue.minus((utils.getTotalBet() * 100)));

                // terminate if newBalance less than 0;
                if (newBalanceBigValue < 0) {
                    ui.showDialog(loaded.languageFile.data.not_enough_balance, '');
                    return;
                }

                state.set('balance', newBalanceBigValue);
                updateText();

                // emit spinFinished event when game finished
                isGameRunning = true;

                // start backing
                backing.open('Particles Coin Container', newOptions.spin100BackingAlpha);

                let spin100ApiData = {
                    'bet_amount': state.get('betAmount'),
                    'isAutoSpin': false,
                };
                spin100ApiData[usingBetLines ? 'bet_line' : 'multiplier'] = state.get('betLine');

                disablePanelButton();
                disableAutoSpinButton();

                // prevent balance being update while spinning
                sync.disableSync();

                displayWallet();
                spin100Fn(spin100ApiData).then((data) => {
                    start100Spin(data);
                });
            },
        });
        spin100Button.name = 'Spin 100 Button';
        spin100Button.x = newOptions.spin100ButtonX;
        spin100Button.y = newOptions.spin100ButtonY;

        // Spin 100 Button Spine in portrait mode
        spin100ButtonEffect = new PIXI.spine.Spine(loaded.buttonEffect_json.spineData);
        spin100ButtonEffect.scale.set(0.62);
        spin100ButtonEffect.state.setAnimation(0, 'idle_1', false);
        spin100ButtonEffect.autoUpdate = true;
        spin100ButtonEffect.x = newOptions.spin100ButtonX;
        spin100ButtonEffect.y = newOptions.spin100ButtonY;
        spin100ButtonEffect.state.addListener({
            complete: (event) => {
                // The queue delay determines the amount of time
                // the program will wait before playing the next animation
                const queueDelay = (Math.random() * 9000) + 2500;

                setTimeout(() => {
                    const randomIdle = Math.floor(Math.random() * 2);
                    const idleAnimation = randomIdle === 0 ? 'idle_2' : 'idle_1';

                    // Set the state of animation and add it to the queue
                    spin100ButtonEffect.state.addAnimation(0, idleAnimation, false, -1);
                }, queueDelay);
            }
        });

        panelContainer.addChild(closePopUpSprite);
        if (environment.allowCheatMode) {
            panelContainer.addChild(cheatRadioGroup);
        }
        panelContainer.addChild(spinArea);
        panelContainer.addChild(menuButton);
        panelContainer.addChild(menuContainer);

        menuContainer.addChild(menuBackground);
        menuContainer.addChild(menuCloseButton);
        menuContainer.addChild(menuTitleText);
        menuContainer.addChild(gameRulesButton);

        gameRulesButton.addChild(gameRulesButtonSymbol);
        menuContainer.addChild(paytableButton);
        paytableButton.addChild(paytableButtonSymbol);

        menuContainer.addChild(transactionHistoryText);
        menuContainer.addChild(musicButton);
        menuContainer.addChild(soundButton);
        menuContainer.addChild(tutorialButton);

        tutorialButton.addChild(tutorialButtonSymbol);
        menuContainer.addChild(quitGameButton);
        quitGameButton.addChild(quitGameButtonSymbol);

        panelContainer.addChild(turboButton);
        panelContainer.addChild(turboSymbol);
        panelContainer.addChild(totalBetButton);
        panelContainer.addChild(totalBetLabel);
        panelContainer.addChild(totalBetText);
        panelContainer.addChild(betContainer);

        betContainer.addChild(betBackground);
        betContainer.addChild(betConfirm);
        betContainer.addChild(betCloseButton);
        betContainer.addChild(betTitle);
        betContainer.addChild(betLineLabel);
        betContainer.addChild(betLineText);
        betContainer.addChild(betLineIncreaseButton);
        betContainer.addChild(betLineDecreaseButton);
        betContainer.addChild(betAmountLabel);
        betContainer.addChild(betAmountText);
        betContainer.addChild(betAmountDecreaseButton);
        betContainer.addChild(betAmountIncreaseButton);

        panelContainer.addChild(betMaxButton);
        panelContainer.addChild(betMaxSymbol);
        panelContainer.addChild(spinButton);
        panelContainer.addChild(spinButtonEffect);
        panelContainer.addChild(autoSpinCountdown);
        panelContainer.addChild(autoSpinButton);
        panelContainer.addChild(autoSpinButtonEffect);
        panelContainer.addChild(spin100Button);
        panelContainer.addChild(spin100ButtonEffect);

        ui.bringToFront(menuContainer);
        ui.bringToFront(betContainer);
        app.addComponent(panelContainer);
    }
    else {
        //#region Hidden Menu
        // Settings button in landscape mode
        settingsButton = ui.createButton({
            texture: loaded.settingsButton.texture,
            textureHover: loaded.settingsHoverButton.texture,
            clickedTexture: loaded.settingsDownButton.texture,
            onPointerUp: () => {
                // Open settings container
                if (settingsContainer.y >= 645) {
                    TweenMax.to(settingsContainer, 0.5, {y: 467});
                    closePopUpSprite.interactive = true;
                }

                // Close settings container
                else if (settingsContainer.y <= 467) {
                    TweenMax.to(settingsContainer, 0.5, {y: 645});
                    closePopUpSprite.interactive = false;
                }
            }
        });
        settingsButton.name = 'Settings Button';
        settingsButton.x = 51;
        settingsButton.y = 645;

        // Mask for settings container that tweens up and down around the settings button in landscape mode
        let settingsBarMask = new PIXI.Graphics().beginFill(0xFFFFFF).drawRect(51, 402, 66, 242).endFill();
        settingsBarMask.name = 'Settings Bar Mask';

        // Settings container that contains all the required settings (settings bar sprite, sound, music, account, transaction, etc)
        settingsContainer = new PIXI.Container();
        settingsContainer.name = 'Settings Container';
        settingsContainer.x = 51;
        settingsContainer.y = 645; // Fully opened at 415 when included account and transaction history. Fully opened at 530 without account and transaction history
        settingsContainer.mask = settingsBarMask;

        // Settings bar sprite in landscape mode
        let settingsBar = new PIXI.Sprite(loaded.settingsBar.texture);
        settingsBar.interactive = true;
        settingsBar.name = 'Settings Bar';

        // Music button inside Settings Bar in landscape mode
        musicButton = ui.createToggleButton({
            textureOn: loaded.musicMutedButton.texture,
            textureOff: loaded.musicButton.texture,
            textureDownOn: loaded.musicMutedButton.texture,
            textureDownOff: loaded.musicButton.texture,
            textureHover: loaded.musicHoverButton.texture,
            textureHover2: loaded.musicMutedHoverButton.texture,
            centerAnchor: true,
            onPointerUp: function () {
                panelContainer.emit('musicButtonClicked');
                sound.isMusicMuted() ? sound.unmuteMusic() : sound.muteMusic();
            }
        });
        musicButton.name = 'Music Button in Settings Bar';
        musicButton.x = 34;
        musicButton.y = 30;
        musicButton.scale.set(0.80, 0.80);

        // Sound button inside Settings Bar in landscape mode
        soundButton = ui.createToggleButton({
            textureOn: loaded.soundMutedButton.texture,
            textureOff: loaded.soundButton.texture,
            textureDownOn: loaded.soundMutedButton.texture,
            textureDownOff: loaded.soundButton.texture,
            textureHover: loaded.soundHoverButton.texture,
            textureHover2: loaded.soundMutedHoverButton.texture,
            centerAnchor: true,
            onPointerUp: function () {
                panelContainer.emit('soundButtonClicked');
                sound.isSoundMuted() ? sound.unmuteSound() : sound.muteSound();
            }
        });
        soundButton.name = 'Sound Button in Settings Bar';
        soundButton.x = 34;
        soundButton.y = 85;
        soundButton.scale.set(0.80, 0.80);

        //todo enable back the account and the transaction history settings when working on it
        // Account button inside Settings Bar in landscape mode
        exitButton = ui.createButton({
            texture: loaded.exitGameButton.texture,
            textureDown: loaded.exitGameButton.texture,
            textureHover: loaded.exitHoverButton.texture,
            centerAnchor: true,
            onPointerDown: function () {
                exitGame();
            }
        });
        exitButton.name = 'Account Button in Settings Bar';
        exitButton.x = 32;
        exitButton.y = 140;
        exitButton.scale.set(0.80, 0.80);

        let infoBarMask = new PIXI.Graphics().beginFill(0xFFFFFF).drawRect(51, 438, 66, 242).endFill();
        infoBarMask.name = 'Settings Bar Mask';

        infoContainer = new PIXI.Container();
        infoContainer.name = 'Settings Container';
        infoContainer.x = 51;
        infoContainer.y = 680; // Fully opened at 415 when included account and transaction history. Fully opened at 530 without account and transaction history
        infoContainer.mask = infoBarMask;

        let infoBar = new PIXI.Sprite(loaded.settingsBar.texture);
        infoBar.interactive = true;
        infoBar.name = 'Settings Bar';

        // Info button in landscape mode
        infoMenuButton = ui.createButton({
            texture: loaded.infoMenuButton.texture,
            textureHover: loaded.infoMenuHoverButton.texture,
            clickedTexture: loaded.infoMenuDownButton.texture,
            onPointerUp: () => {
                // Open settings container
                if (infoContainer.y >= 680) {
                    TweenMax.to(infoContainer, 0.5, {y: 505});
                    closePopUpSprite.interactive = true;
                }

                // Close settings container
                else if (infoContainer.y <= 570) {
                    TweenMax.to(infoContainer, 0.5, {y: 680});
                    closePopUpSprite.interactive = false;
                }

                if (settingsContainer.y <= 467) {
                    TweenMax.to(settingsContainer, 0.5, {y: 645});
                    closePopUpSprite.interactive = false;
                }
            }
        });
        infoMenuButton.name = 'Info Menu';
        infoMenuButton.x = 51;
        infoMenuButton.y = 682;

        infoButton = ui.createButton({
            texture: loaded.infoButton.texture,
            textureHover: loaded.infoHoverButton.texture,
            centerAnchor: true,
            onPointerUp: () => {
                // Close info container
                if (infoContainer.y <= 530) {
                    TweenMax.to(infoContainer, 0.5, {y: 680});
                    closePopUpSprite.interactive = false;
                }
                panelContainer.emit('infoButtonClicked');
            }
        });
        infoButton.name = 'Info Button';
        infoButton.x = 32;
        infoButton.y = 30;
        infoButton.scale.set(0.80, 0.80);

        // Transaction history button inside Settings Bar in landscape mode
        transactionHistoryButton = ui.createButton({
            texture: loaded.transactionHistoryButton.texture,
            textureDown: loaded.transactionHistoryButton.texture,
            textureHover: loaded.transactionHistoryHoverButton.texture,
            centerAnchor: true,
            onPointerUp: function () {
                if (infoContainer.y <= 530) {
                    TweenMax.to(infoContainer, 0.5, {y: 680});
                    closePopUpSprite.interactive = false;
                }
                transactionHistoryButton.alpha = 1;
                betHistory.show();
            },
            onPointerDown: function () {
                transactionHistoryButton.alpha = 0.5;
            }
        });
        transactionHistoryButton.name = 'Transaction History Button in Settings Bar';
        transactionHistoryButton.x = 32;
        transactionHistoryButton.y = 85;
        transactionHistoryButton.scale.set(0.80, 0.80);

        tutorialButton = ui.createButton({
            texture: loaded.tutorialButton.texture,
            textureHover: loaded.tutorialHoverButton.texture,
            centerAnchor: true,
            onPointerUp: () => {
                // Close info container
                if (infoContainer.y <= 530) {
                    TweenMax.to(infoContainer, 0.5, {y: 680});
                    closePopUpSprite.interactive = false;
                }

                tutorial.show();
            }
        });
        tutorialButton.name = 'Info Button';
        tutorialButton.x = 32;
        tutorialButton.y = 140;
        tutorialButton.scale.set(0.80, 0.80);
        //#endregion

        //#region Panel Sub Menu
        // Bet lines decrease button in landscape mode
        betLineDecreaseButton = ui.createButton({
            texture: loaded.decreaseButton.texture,
            textureHover: loaded.decreaseHoverButton.texture,
            clickedTexture: loaded.decreaseDownButton.texture,
            centerAnchor: true,
            onPointerUp: function () {
                betLineIndex = changeItem(betLineIndex - 1, newOptions.betLineOptions.length - 1);
                state.set('betLine', newOptions.betLineOptions[betLineIndex]);
                updateText();
                auto.updateSlider();

                TweenMax.to(settingsContainer, 1, {y: 645});
                TweenMax.to(infoContainer, 0.5, {y: 680});

                // emit event
                panelContainer.emit('betLineChanged', state.get('betLine'));
            }
        });
        betLineDecreaseButton.name = 'Bet Line Decrease Button';
        betLineDecreaseButton.x = 330;
        betLineDecreaseButton.y = 680;

        // Bet lines box in landscape mode
        let linesBox = new PIXI.Sprite(loaded.linesBox.texture);
        linesBox.name = 'Box for Lines / Levels';
        linesBox.x = 328;
        linesBox.y = 645;

        // Bet lines label in landscape mode
        betLineLabel = ui.createText({
            fontSize: 18,
            fontWeight: 'bold'
        }, usingBetLines ? loaded.languageFile.data.bet_lines.toUpperCase() : loaded.languageFile.data.bet_multiplier.toUpperCase());
        betLineLabel.name = 'Bet Lines Label';
        betLineLabel.anchor.set(0.5, 0);
        betLineLabel.tint = newOptions.secondaryTint;
        betLineLabel.x = 389;
        betLineLabel.y = 660;

        // Bet lines text positioning in landscape mode
        betLineText.name = 'Bet Lines Text';
        betLineText.x = 389;
        betLineText.y = 690;
        betLineText.text = state.get('betLine');

        // Bet lines increase button in landscape mode
        betLineIncreaseButton = ui.createButton({
            texture: loaded.increaseButton.texture,
            textureHover: loaded.increaseHoverButton.texture,
            clickedTexture: loaded.increaseDownButton.texture,
            centerAnchor: true,
            onPointerUp: function () {
                betLineIndex = changeItem(betLineIndex + 1, newOptions.betLineOptions.length - 1);
                state.set('betLine', newOptions.betLineOptions[betLineIndex]);
                updateText();
                auto.updateSlider();

                TweenMax.to(settingsContainer, 1, {y: 645});
                TweenMax.to(infoContainer, 0.5, {y: 680});

                // emit event
                panelContainer.emit('betLineChanged', state.get('betLine'));
            }
        });
        betLineIncreaseButton.name = 'Bet Line Increase Button';
        betLineIncreaseButton.x = 449;
        betLineIncreaseButton.y = 680;

        if (fixMultiBetLine) {
            betLineDecreaseButton.interactive = false;
            betLineDecreaseButton.buttonMode = false;
            betLineDecreaseButton.alpha = 0.5;
            betLineIncreaseButton.interactive = false;
            betLineIncreaseButton.buttonMode = false;
            betLineIncreaseButton.alpha = 0.5;
        }

        // Bet amount decrease button in landscape mode
        betAmountDecreaseButton = ui.createButton({
            texture: loaded.decreaseButton.texture,
            textureHover: loaded.decreaseHoverButton.texture,
            clickedTexture: loaded.decreaseDownButton.texture,
            centerAnchor: true,
            onPointerUp: () => {
                betAmountIndex = changeItem(betAmountIndex - 1, betAmountList.length - 1);
                state.set('betAmount', betAmountList[betAmountIndex]);
                updateText();
                auto.updateSlider();

                TweenMax.to(settingsContainer, 1, {y: 645});
                TweenMax.to(infoContainer, 0.5, {y: 680});

                // emit event
                panelContainer.emit('betAmountChanged', state.get('betAmount'));
            }
        });
        betAmountDecreaseButton.name = 'Bet Amount Decrease Button';
        betAmountDecreaseButton.x = 508;
        betAmountDecreaseButton.y = 680;

        // Bet Amount box in landscape mode
        let betAmountBox = new PIXI.Sprite(loaded.betAmountBox.texture);
        betAmountBox.name = 'Box for Bet Amount';
        betAmountBox.x = 505;
        betAmountBox.y = 645;

        // Bet Amount Label in landscape mode
        betAmountLabel = ui.createText({
            fontSize: 18,
            fontWeight: 'bold'
        }, loaded.languageFile.data.bet_amount.toUpperCase());
        betAmountLabel.name = 'Bet Amount Label';
        betAmountLabel.anchor.set(0.5, 0);
        betAmountLabel.tint = newOptions.secondaryTint;
        betAmountLabel.x = 594;
        betAmountLabel.y = 660;

        // Bet amount text positioning in landscape mode
        betAmountText.name = 'Bet Amount Text';
        betAmountText.x = 594;
        betAmountText.y = 690;
        betAmountText.text = state.get('betAmount');

        // Bet amount increase button in landscape mode
        betAmountIncreaseButton = ui.createButton({
            texture: loaded.increaseButton.texture,
            textureHover: loaded.increaseHoverButton.texture,
            clickedTexture: loaded.increaseDownButton.texture,
            centerAnchor: true,
            onPointerUp: () => {
                betAmountIndex = changeItem(betAmountIndex + 1, betAmountList.length - 1);
                state.set('betAmount', betAmountList[betAmountIndex]);
                updateText();
                auto.updateSlider();

                TweenMax.to(settingsContainer, 1, {y: 645});
                TweenMax.to(infoContainer, 0.5, {y: 680});

                // emit event
                panelContainer.emit('betAmountChanged', state.get('betAmount'));
            }
        });
        betAmountIncreaseButton.name = 'Bet Amount Increase Button';
        betAmountIncreaseButton.x = 681;
        betAmountIncreaseButton.y = 680;

        // Total Bet Label in landscape mode
        totalBetLabel = ui.createText({
            fontSize: 18,
            fontWeight: 'bold'
        }, loaded.languageFile.data.total_bet.toUpperCase());
        totalBetLabel.anchor.set(0.5, 0);
        totalBetLabel.tint = newOptions.primaryTint;
        totalBetLabel.x = 771;
        totalBetLabel.y = 660;
        totalBetLabel.name = 'Total Bet Label';

        // Total Bet Text in landscape mode
        totalBetText = ui.createText({
            fontSize: 20,
            fontWeight: 'bold'
        }, state.get('currency') + ' ' + utils.getTotalBet());
        totalBetText.x = 771;
        totalBetText.y = 690;

        // Bet Max Button in landscape mode
        betMaxButton = ui.createButton({
            texture: loaded.betMaxButton.texture,
            textureHover: loaded.betMaxHoverButton.texture,
            clickedTexture: loaded.betMaxDownButton.texture,
            centerAnchor: true,
            onPointerUp: function () {
                if (checkIfBettingMax(newOptions)) {
                    betAmountIndex = 0;
                    if (!fixMultiBetLine) {
                        betLineIndex = 0;
                    }
                }
                else {
                    betAmountIndex = betAmountList.length - 1;
                    if (!fixMultiBetLine) {
                        betLineIndex = newOptions.betLineOptions.length - 1;
                    }
                }
                state.set('betAmount', betAmountList[betAmountIndex]);
                state.set('betLine', newOptions.betLineOptions[betLineIndex]);
                panelContainer.emit('betLineChanged', state.get('betLine'));
                panelContainer.emit('betAmountChanged', state.get('betAmount'));
                panelContainer.emit('betMaxButtonClicked');
                auto.updateSlider();
                updateText();

                TweenMax.to(settingsContainer, 1, {y: 645});
                TweenMax.to(infoContainer, 0.5, {y: 680});
            },
            onPointerOut: function () {
                checkIfbetMaxNeedTextureChange()
            },
            onPointerDown: function () {
                checkIfbetMaxNeedTextureChange()
            },
            onPointerOver: function () {
                checkIfbetMaxNeedTextureChange()
            }

        });

        // noinspection JSAnnotator
        function checkIfbetMaxNeedTextureChange() {
            if (checkIfBettingMax(newOptions)) {
                betMaxButton.texture = loaded.betMaxDisabledButton.texture;
            }
        }

        betMaxButton.scale.set(0.585, 0.5);
        betMaxButton.name = 'Bet Max Button';
        betMaxButton.x = 925;
        betMaxButton.y = 661;

        // Bet Max Label in landscape mode
        betMaxLabel = ui.createText({
            fontSize: 20,
            fontWeight: 'bold'
        }, loaded.languageFile.data.bet_max_l.toUpperCase());
        betMaxLabel.name = 'Bet Max Label';
        betMaxLabel.x = 925;
        betMaxLabel.y = 661;

        // Turbo Button in landscape mode
        turboButton = ui.createToggleButton({
            textureOn: loaded.turboOnButton.texture,
            textureHover: loaded.turboHoverButton.texture,
            textureDownOn: loaded.turboOnButton.texture,
            textureOff: loaded.turboOffButton.texture,
            textureDownOff: loaded.turboOffButton.texture,
            centerAnchor: true,
            onPointerUp: onTurboButtonClicked
        });
        turboButton.scale.set(0.585, 0.5);
        turboButton.name = 'Turbo Button';
        turboButton.x = newOptions.turboButtonX;
        turboButton.y = newOptions.turboButtonY;
        turboButton.interactive = false;

        // Turbo Label in landscape mode
        turboLabel = ui.createText({
            fontSize: 20,
            fontWeight: 'bold'
        }, loaded.languageFile.data.turbo.toUpperCase());
        turboLabel.name = 'Turbo Label';
        turboLabel.x = 925;
        turboLabel.y = 700;
        //#endregion

        //#region Main Button
        // Spin Button in landscape mode
        spinButton = ui.createButton({
            texture: loaded.spinButton.texture,
            clickedTexture: loaded.spinDownButton.texture,
            centerAnchor: true,
            onPointerUp: spinButtonClicked,
            onPointerOver: () => {
                spinButton.emit('spinButtonHover', true);
                spinButtonEffect.state.setAnimation(0, 'hover', true);
            },
            onPointerOut: () => {
                spinButton.emit('spinButtonHover', false);

                const queueDelay = Math.random() * 8000;
                setTimeout(() => {
                    // Randomize the idle animation
                    const randomIdle = Math.floor(Math.random() * 2);
                    const idleAnimation = randomIdle === 0 ? 'idle_1' : 'idle_2';
                    spinButtonEffect.state.addAnimation(0, idleAnimation, false, 0);
                }, queueDelay);
            },
            hitArea: newOptions.spinButtonHitArea,
        });
        spinButton.name = 'Spin Button';
        spinButton.x = 1098;
        spinButton.y = newOptions.spinButtonY;

        spinButtonEffect = new PIXI.spine.Spine(loaded.buttonEffect_json.spineData);
        spinButtonEffect.state.setAnimation(0, 'idle_1', true);
        spinButtonEffect.autoUpdate = true;
        spinButtonEffect.scale.set(0.65, 0.66);
        spinButtonEffect.x = 1098;
        spinButtonEffect.y = newOptions.spinButtonY;

        // Current state of the spinButton
        let isSpinButtonHover = false;
        spinButton.on('spinButtonHover', (state) => {
            isSpinButtonHover = state;
        });

        // Listener for the spin button effects
        spinButtonEffect.state.addListener({
            complete: (event) => {
                // Returns if the button is currently being hovered on
                if (isSpinButtonHover) {
                    return;
                }

                // The queue delay determines the amount of time the program will wait before playing the next animation
                const queueDelay = (Math.random() * 8000) + 500;

                setTimeout(() => {
                    // Randomize the idle animation
                    const randomIdle = Math.floor(Math.random() * 2);
                    const idleAnimation = randomIdle === 0 ? 'idle_1' : 'idle_2';

                    // Set the state of animation and add it to the queue
                    spinButtonEffect.state.addAnimation(0, idleAnimation, false, 0);
                }, queueDelay);
            }
        });

        // Auto Spin Countdown in landscape mode (Appears on the Spin Button if auto spin is active)
        autoSpinCountdown = ui.createText({fontSize: 60}, autoSpinCountdownInt);
        autoSpinCountdown.x = newOptions.autoSpinCountdownConfigX;
        autoSpinCountdown.y = newOptions.autoSpinCountdownConfigY;
        autoSpinCountdown.scale.set(newOptions.autoSpinCountdownScale);
        autoSpinCountdown.anchor.set(0.5, 0.5);
        autoSpinCountdown.renderable = false;

        // Auto Spin button in landscape mode
        autoSpinButton = ui.createButton({
            texture: loaded.autoSpinButton.texture,
            clickedTexture: loaded.autoSpinDownButton.texture,
            centerAnchor: true,
            onPointerUp: () => {
                // turn autoSpinRunning to false to stop the auto spin
                if (autoSpinRunning) {
                    stopAutoSpin();

                    return;
                }

                TweenMax.to(settingsContainer, 1, {y: 645});
                TweenMax.to(infoContainer, 0.5, {y: 680});

                auto.autoSpinPanelShowHide();
                changeAutoSpinButtonTexture(false);
            },
            onPointerDown: () => {
                if (autoSpinRunning) {
                    autoSpinButton.texture = loaded.autoSpinStopDownButton.texture;
                }

                autoSpinButtonEffect.state.setAnimation(0, 'click', false);
            },
            onPointerOver: () => {
                autoSpinButton.emit('autoSpinButtonHover', true);
                autoSpinButtonEffect.state.setAnimation(0, 'hover', true);
            },
            onPointerOut: () => {
                autoSpinButton.emit('autoSpinButtonHover', false);

                const queueDelay = Math.random() * 2000;
                setTimeout(() => {
                    // Randomize the idle animation
                    const randomIdle = Math.floor(Math.random() * 2);
                    const idleAnimation = randomIdle === 0 ? 'idle_1' : 'idle_2';
                    autoSpinButtonEffect.state.addAnimation(0, idleAnimation, false, 0);
                }, queueDelay);
            },
        });
        autoSpinButton.on('pointerupoutside', function () {
            autoSpinButton.texture = autoSpinRunning ? loaded.autoSpinStopButton.texture : loaded.autoSpinButton.texture;
        });
        autoSpinButton.name = 'Auto Spin Button';
        autoSpinButton.x = newOptions.autoSpinButtonX;
        autoSpinButton.y = newOptions.autoSpinButtonY;

        // Auto Spin Button Spine in landscape mode
        autoSpinButtonEffect = new PIXI.spine.Spine(loaded.buttonEffect_json.spineData);
        autoSpinButtonEffect.scale.set(0.26, 0.28);
        autoSpinButtonEffect.state.setAnimation(0, 'idle_1', false);
        autoSpinButtonEffect.autoUpdate = true;
        autoSpinButtonEffect.x = newOptions.autoSpinButtonX;
        autoSpinButtonEffect.y = newOptions.autoSpinButtonY;

        // Current state of the autoSpinButton
        let isAutoSpinButtonHover = false;
        autoSpinButton.on('spinButtonHover', (state) => {
            isAutoSpinButtonHover = state;
        });

        autoSpinButtonEffect.state.addListener({
            complete: (event) => {
                // Returns if the button is currently being hovered on
                if (isAutoSpinButtonHover) {
                    return;
                }

                const queueDelay = (Math.random() * 6500) + 100;

                setTimeout(() => {
                    const randomIdle = Math.floor(Math.random() * 2);
                    const idleAnimation = randomIdle === 0 ? 'idle_2' : 'idle_1';

                    // Set the state of animation and add it to the queue
                    autoSpinButtonEffect.state.addAnimation(0, idleAnimation, false, -1);
                }, queueDelay);
            }
        });

        // Spin 100 Button in landscape mode (Instantly spin 100 times, call spin 100 API)
        spin100Button = ui.createButton({
            texture: loaded.spin100Button.texture,
            clickedTexture: loaded.spin100DownButton.texture,
            centerAnchor: true,
            onPointerUp: () => {

                // terminate if the previous game is still running.
                if (isGameRunning) return;
                if (autoSpinRunning) return;

                let balanceBigValue = new Big(state.get('balance'));
                let newBalanceBigValue = parseFloat(balanceBigValue.minus((utils.getTotalBet() * 100)));

                // terminate if newBalance less than 0;
                if (newBalanceBigValue < 0) {
                    ui.showDialog(loaded.languageFile.data.not_enough_balance, '');
                    // ui.flash(walletText, '0xff0000', 5);
                    return;
                }

                state.set('balance', newBalanceBigValue);
                updateText();

                // emit spinFinished event when game finished
                isGameRunning = true;

                // start backing
                backing.open('Particles Coin Container', newOptions.spin100BackingAlpha);

                let spin100ApiData = {
                    'bet_amount': state.get('betAmount'),
                    'isAutoSpin': false,
                };
                spin100ApiData[usingBetLines ? 'bet_line' : 'multiplier'] = state.get('betLine');

                disablePanelButton();
                disableAutoSpinButton();

                // prevent balance being update while spinning
                sync.disableSync();

                displayWallet();
                spin100Fn(spin100ApiData).then((data) => {
                    start100Spin(data);
                });
            },
            onPointerDown: () => {
                spin100ButtonEffect.state.setAnimation(0, 'click', false);
            },
            onPointerOver: () => {
                spin100Button.emit('autoSpinButtonHover', true);
                spin100ButtonEffect.state.setAnimation(0, 'hover', true);
            },
            onPointerOut: () => {
                spin100Button.emit('autoSpinButtonHover', false);

                const queueDelay = Math.random() * 2000;
                setTimeout(() => {
                    // Randomize the idle animation
                    const randomIdle = Math.floor(Math.random() * 2);
                    const idleAnimation = randomIdle === 0 ? 'idle_1' : 'idle_2';

                    spin100ButtonEffect.state.addAnimation(0, idleAnimation, false, 0);
                }, queueDelay);
            },
        });
        spin100Button.name = 'Spin 100 Button';
        spin100Button.x = newOptions.spin100ButtonX;
        spin100Button.y = newOptions.spin100ButtonY;

        // Spin 100 Button Spine in landscape mode
        spin100ButtonEffect = new PIXI.spine.Spine(loaded.buttonEffect_json.spineData);
        spin100ButtonEffect.scale.set(0.26, 0.28);
        spin100ButtonEffect.state.setAnimation(0, 'idle_1', false);
        spin100ButtonEffect.autoUpdate = true;
        spin100ButtonEffect.x = newOptions.spin100ButtonX;
        spin100ButtonEffect.y = newOptions.spin100ButtonY;
        spin100ButtonEffect.state.addListener({
            complete: (event) => {
                // The queue delay determines the amount of time
                // the program will wait before playing the next animation
                const queueDelay = (Math.random() * 5000) + 100;

                setTimeout(() => {
                    const randomIdle = Math.floor(Math.random() * 2);
                    const idleAnimation = randomIdle === 0 ? 'idle_2' : 'idle_1';

                    // Set the state of animation and add it to the queue
                    spin100ButtonEffect.state.addAnimation(0, idleAnimation, false, -1);
                }, queueDelay);
            }
        });

        // Current state of the spinButton
        let isSpin100ButtonHover = false;
        spinButton.on('spinButtonHover', (state) => {
            isSpinButtonHover = state;
        });

        // Listener for the spin button effects
        spinButtonEffect.state.addListener({
            complete: (event) => {
                // Returns if the button is currently being hovered on
                if (isSpin100ButtonHover) {
                    return;
                }

                // The queue delay determines the amount of time
                // the program will wait before playing the next animation
                const queueDelay = (Math.random() * 2000) + 1000;

                setTimeout(() => {
                    // Randomize the idle animation
                    const randomIdle = Math.floor(Math.random() * 2);
                    const idleAnimation = randomIdle === 0 ? 'idle_1' : 'idle_2';

                    // Set the state of animation and add it to the queue
                    spinButtonEffect.state.addAnimation(0, idleAnimation, false, 0);
                }, queueDelay);
            }
        });
        //#endregion

        /*
        *   Adding of child into panel container to display, items adding sequence from left to right
        *   =========================================================================================
         */

        if (environment.allowCheatMode) {
            panelContainer.addChild(cheatRadioGroup);
        }

        panelContainer.addChild(settingsBarMask);
        settingsContainer.addChild(settingsBar);
        settingsContainer.addChild(musicButton);
        settingsContainer.addChild(soundButton);
        settingsContainer.addChild(exitButton);
        panelContainer.addChild(infoBarMask);

        infoContainer.addChild(infoBar);
        infoContainer.addChild(infoButton);
        infoContainer.addChild(transactionHistoryButton);
        infoContainer.addChild(tutorialButton);

        panelContainer.addChild(closePopUpSprite);

        // Panel Sprite
        panelContainer.addChild(panelSprite);

        // Settings components
        panelContainer.addChild(settingsContainer);
        panelContainer.addChild(settingsButton);

        // Info Button components
        panelContainer.addChild(infoContainer);
        panelContainer.addChild(infoMenuButton);

        // Balance / Wallet components
        panelContainer.addChild(balanceMask);

        // Bet Lines components
        panelContainer.addChild(linesBox);
        panelContainer.addChild(betLineDecreaseButton);
        panelContainer.addChild(betLineLabel);
        panelContainer.addChild(betLineText);
        panelContainer.addChild(betLineIncreaseButton);

        // Bet Amount components
        panelContainer.addChild(betAmountBox);
        panelContainer.addChild(betAmountDecreaseButton);
        panelContainer.addChild(betAmountLabel);
        panelContainer.addChild(betAmountText);
        panelContainer.addChild(betAmountIncreaseButton);

        // Total Bet Amount components
        // panelContainer.addChild(totalBetBox);
        panelContainer.addChild(totalBetLabel);
        panelContainer.addChild(totalBetText);

        // Bet Max Button component
        panelContainer.addChild(betMaxButton);
        panelContainer.addChild(betMaxLabel);

        // Turbo Button component
        panelContainer.addChild(turboButton);
        panelContainer.addChild(turboLabel);

        // Spin Button component
        panelContainer.addChild(spinButton);
        panelContainer.addChild(spinButtonEffect);
        panelContainer.addChild(autoSpinCountdown);

        // Auto Spin Button component
        panelContainer.addChild(autoSpinButton);
        panelContainer.addChild(autoSpinButtonEffect);

        // Spin 100 Button component
        panelContainer.addChild(spin100Button);
        panelContainer.addChild(spin100ButtonEffect);

        // Finally add panelContainer with all out components to the app itself
        app.addComponent(panelContainer); //todo change to app.addComponent
    }
    
    displayWallet();

    if (isMobile()) {
        // Detection during changing of bet amount
        panelContainer.on('betAmountChanged', () => {
            checkIfBettingMax(newOptions) ? betMaxSymbol.texture = loaded.betMaxSymbolActive.texture : betMaxSymbol.texture = loaded.betMaxSymbol.texture;
        });

        // Detection during changing of bet lines
        panelContainer.on('betLineChanged', () => {
            checkIfBettingMax(newOptions) ? betMaxSymbol.texture = loaded.betMaxSymbolActive.texture : betMaxButton.texture = loaded.betMaxButton.texture;
        });
    }
    else {
        // Detection during changing of bet amount
        panelContainer.on('betAmountChanged', () => {
            checkIfBettingMax(newOptions) ? betMaxButton.texture = loaded.betMaxDisabledButton.texture : betMaxButton.texture = loaded.betMaxButton.texture;
        });

        // Detection during changing of bet lines
        panelContainer.on('betLineChanged', () => {
            checkIfBettingMax(newOptions) ? betMaxButton.texture = loaded.betMaxDisabledButton.texture : betMaxButton.texture = loaded.betMaxButton.texture;
        });
    }

    const autoPanelContainer = auto.init(state.get('betAmount'), newOptions.gotBonusOffset, newOptions.betLinesUsage, newOptions.autoSpinOption);
    if (isMobile()) {
        autoPanelContainer.x = (app.getResolutionX() / 2);
        autoPanelContainer.y = 1790;
    }

    autoPanelContainer.on('autoSpinLabelTapped', () => {
        auto.autoSpinPanelShowHide();
    });

    //! todo clear input of autoSpinUpdated so that can disable autoSpinStart Button
    autoPanelContainer.on('autoSpinUpdated', (data) => {
        if (data.numbered === 0 &&
            data.winningConditions === 0 &&
            data.singleWin === 0 &&
            data.cashIncrease === 0 &&
            data.cashDecrease === 0)

            auto.cannotAutoSpinStartButton();
        else
            auto.allowAutoSpinStartButton();
    });

    autoPanelContainer.on('autoSpinStarted', (data) => {
        if (data.numbered > 0) {
            autoSpinCountdownInt = data.numbered + 1;
            autoSpinCountdown.renderable = true;
            spinButton.texture = loaded.spinDownButton.texture;
        }

        autoSpinRunning = true;

        autoSpinButton.texture = loaded.autoSpinStopButton.texture;

        // set auto spin criteria
        autoSpinCriteria = data;

        // init auto spin state
        autoSpinState = {
            spinCount: 0,
            winCount: 0,
            bonusWinCount: 0,
            totalWin: 0,
            totalBetAmount: 0,
            initialBalance: state.get('balance'),
        };

        // disable panel buttons
        disablePanelButton();

        // prevent balance being sync during auto spin.
        sync.disableSync();

        Promise.resolve().then(autoSpinFn).catch((reason) => {

            // resume balance sync.
            sync.enableSync();

            autoSpinRunning = false;

            // enable panel buttons
            enablePanelButton();
            spinButton.texture = loaded.spinButton.texture;
            // autoSpinCountdown.destroy(); THIS IS FOR LOAD TEXTURE INSTEAD OF TEXT
            autoSpinCountdown.renderable = false;

            console.log(`Auto spin stopped due to: ${reason}`);

            changeAutoSpinButtonTexture(false);
        });

    });

    panelContainer['disableActionButtons'] = () => {
        spinButton.interactive = false;
        turboButton.interactive = false;
        autoSpinButton.interactive = false;
        if (isMobile())
            spinArea.interactive = false;
    };

    panelContainer['enableActionButtons'] = () => {
        spinButton.interactive = true;
        turboButton.interactive = true;
        autoSpinButton.interactive = true;
        if (isMobile())
            spinArea.interactive = true;
    };

    panelContainer.disableActionButtons();

    updateText();
    auto.updateSlider();
    enablePanelButton();
    enableAutoSpinButton();

    ui.bringToFront(panelDisplayContainer);

    //todo : delete
    if (environment.environmentName === 'staging') {
        // let fpsText = ui.createText({
        //     textAnchorX: 0,
        // }, '');
        // fpsText.x = 50;
        // fpsText.y = 50;
        // app.addComponent(fpsText);
        // let AverageFpsText = ui.createText({
        //     textAnchorX: 0,
        // }, '');
        // AverageFpsText.x = 50;
        // AverageFpsText.y = 100;
        // app.addComponent(AverageFpsText);
        //
        // let highestFpsText = ui.createText({
        //     textAnchorX: 0,
        // }, '');
        // highestFpsText.x = 50;
        // highestFpsText.y = 150;
        // app.addComponent(highestFpsText);
        //
        // let lowestFpsText = ui.createText({
        //     textAnchorX: 0,
        // }, '');
        // lowestFpsText.x = 50;
        // lowestFpsText.y = 200;
        // app.addComponent(lowestFpsText);

        let totalFps = 0;
        let counter = 0;
        let fpsTicker = new PIXI.ticker.Ticker();
        let highest = 0;
        let lowest = 60;
        let StartTimer;
        fpsTicker.add(() => {
            let fps = fpsTicker.FPS;
            let latestAverageFPS;
            counter += 1;
            totalFps += fps;
            let curTime = new Date().getTime();

            if ((curTime - StartTimer) > 10000) {
                latestAverageFPS = totalFps / counter;

                if (highest < latestAverageFPS) {
                    highest = latestAverageFPS;
                }

                if (lowest > latestAverageFPS) {
                    lowest = latestAverageFPS;
                }
            }


            if ((curTime - StartTimer) > 3600000) {
                latestAverageFPS = totalFps / counter;

                if (highest < latestAverageFPS) {
                    highest = latestAverageFPS;
                }

                if (lowest > latestAverageFPS) {
                    lowest = latestAverageFPS;
                }

                // fpsText.text = 'FPS: ' + fps.toFixed(2);
                // AverageFpsText.text = 'FPS Average: ' + latestAverageFPS.toFixed(2);
                //
                // lowestFpsText.text = 'Lowest: ' + (lowest).toFixed(2);
                // highestFpsText.text = 'Highest: ' + (highest).toFixed(2);

                console.log('FPS: ' + fps.toFixed(2));
                console.log('FPS Average: ' + latestAverageFPS.toFixed(2));

                console.log('Lowest: ' + (lowest).toFixed(2));
                console.log('Highest: ' + (highest).toFixed(2));

                StartTimer = curTime;
                // totalFps = 0;
                // counter = 0;
            }

        });

        setTimeout(() => {
            StartTimer = new Date().getTime();
            fpsTicker.start();
        }, 5000);
    }

    return panelContainer;
}

function runPreEvents(events = []) {

    if (!Array.isArray(events)) {
        throw new Error('Invalid events list');
    }

    const preEvents = events.concat([initFreeSpinAndFreeBet]);
    utils.serial(preEvents, runPromise);
}

function runPromise(task, previous) {
    return task();
}

function initFreeSpinAndFreeBet() {
    return new Promise((resolve) => {

        setTimeout(() => {
            freeSpin = new FreeSpin();
            panelContainer.updateFreeSpin = () => {
                freeSpin.refreshContainer();
            };

            let infoContainerIndex = utils.getArrayIndexNumByName(app.stage.children, 'Info Container');
            app.stage.addChildAt(freeSpin.getFreeSpinContainer(), infoContainerIndex);

            freeBet = new FreeBet();
            panelContainer.updateFreeBet = () => {
                freeBet.refreshContainer();
            };

            app.stage.addChildAt(freeBet.getFreeBetContainer(), infoContainerIndex);

            freeSpin.checkToAnnounce();
            freeBet.checkToAnnounce();

            resolve();
        }, 200);
    });
}

function beforeSpinApi(isAutoPlay = false) {
    // Available properties for freeSpin and freeBet
    // Used to display announcements and to attach different values to spin API
    let availableFreeSpin = state.get('availableFreeSpin');
    let isFreeSpin = false;
    let freeSpinToken;

    let availableFreeBet = state.get('availableFreeBet');
    let isFreeBet = false;
    let freeBetToken;

    if (availableFreeSpin) {
        isFreeSpin = true;
        freeSpinToken = availableFreeSpin.free_spin_token;
        state.set('betAmount', availableFreeSpin.bet_amount);
        state.set('betLine', availableFreeSpin.multiplier ? availableFreeSpin.multiplier : availableFreeSpin.bet_line);
        updateText();

        freeSpin.overideFreeSpinCounter(availableFreeSpin.free_spins - availableFreeSpin.used_free_spins - 1);
    } else if (availableFreeBet) {
        isFreeBet = true;
        freeBetToken = availableFreeBet.free_bet_token;

        // When there are no more freebet left, it will not disappear automatically
        let freeBetAmount = new Big(availableFreeBet.free_bet_amount);
        let freeBetAmountLeft = parseFloat(freeBetAmount.minus(availableFreeBet.used_free_bet_amount).minus(utils.getTotalBet())).toFixed(2);
        freeBet.updateFreeBetContainer(freeBetAmountLeft);
    }

    // Put all the needed values into an object then we have to decide if the game is
    // using bet lines or using multiplier and subsequently assign it
    let apiData = {
        'bet_amount': parseFloat(state.get('betAmount')).toFixed(2),
        'isAutoPlay': isAutoPlay,
        'isFreeSpin': isFreeSpin,
        'freeSpinToken': freeSpinToken,
        'isFreeBet': isFreeBet,
        'freeBetToken': freeBetToken
    };
    apiData[usingBetLines ? 'bet_line' : 'multiplier'] = state.get('betLine');

    // Stored properties for freeSpin and freeBet
    // Used to update freeSpin and freeBet container with stored values. Works like a tracker
    return new Promise((resolve) => {
        api.spin(
            apiData,
        ).then((data) => {
            state.set('balance', data.balance);

            state.set('availableFreeSpin', data.available_free_spin);
            availableFreeSpin = state.get('availableFreeSpin');

            state.set('availableFreeBet', data.available_free_bet);
            availableFreeBet = state.get('availableFreeBet');

            state.set('pendingReleaseFreeSpin', data.pending_release_free_spin);
            let pendingReleaseFreeSpin = state.get('pendingReleaseFreeSpin');

            state.set('pendingReleaseFreeBet', data.pending_release_free_bet);
            let pendingReleaseFreeBet = state.get('pendingReleaseFreeBet');

            if (pendingReleaseFreeSpin || availableFreeSpin) {

                let isGameRewarded = availableFreeSpin ? availableFreeSpin.is_game_rewarded : pendingReleaseFreeSpin.is_game_rewarded;

                if (!isGameRewarded) {
                    freeSpin.refreshContainer();
                    freeSpin.setVisibility(true);
                }
            } else if ((pendingReleaseFreeBet && pendingReleaseFreeBet.required_turnover > 0) || (availableFreeBet && availableFreeBet.required_turnover > 0)) {
                freeBet.refreshContainer();
                freeBet.setVisibility(true);
            } else {
                freeSpin.setVisibility(false);
                freeBet.setVisibility(false);
            }

            // Detection to see if this free spin is new
            // AND is awarded by admin / system, NOT earned through game spins
            freeSpin.checkToAnnounce();
            freeBet.checkToAnnounce();

            resolve(data);
        });
    });
}

function autoSpinFn() {
    session.renewSession();

    // stop auto spin if flag is off
    if (!autoSpinRunning) {
        spinButton.texture = loaded.spinButton.texture;
        autoSpinCountdown.renderable = false;
        enablePanelButton();

        // resume balance sync.
        sync.enableSync();
        return;
    } else if (autoSpinCriteria.numbered > 0) {
        if (autoSpinCountdownInt > 0) {
            autoSpinCountdownInt--;
            autoSpinCountdown.text = autoSpinCountdownInt;
        }
        spinButton.texture = loaded.spinDownButton.texture;
    }

    // console.log( autoSpinCriteria.numbered === 100 && autoSpinState.spinCount === 0);
    (autoSpinCriteria.numbered === 100 && autoSpinState.spinCount === 0) ? autoSpinCountdown.scale.set(0.6 * autoSpinCountdownScale) : autoSpinCountdown.scale.set((isMobile() ? 0.8 : 0.75) * autoSpinCountdownScale);

    let currentBalance = new Big(state.get('balance'));
    let cashDifferentiation = parseFloat(currentBalance.minus(autoSpinState.initialBalance)).toFixed(2);
    // check stop criteria
    if (autoSpinCriteria.winningConditions === 'anyWin' && autoSpinState.winCount > 0) {
        ui.showDialog(loaded.languageFile.data.autospin_win, '', loaded.languageFile.data.okay, {
            gotSecondButton: true,
            addCloseFunctionTo2Button: true,
            onPointerUp2: auto.callStartAutoSpin
        });
        throw 'win any game';
    } else if (autoSpinCriteria.winningConditions === 'bonusGame' && autoSpinState.isBonus) {
        ui.showDialog(loaded.languageFile.data.autospin_bonus, '', loaded.languageFile.data.okay, {
            gotSecondButton: true,
            addCloseFunctionTo2Button: true,
            onPointerUp2: auto.callStartAutoSpin
        });
        throw 'win bonus game';
    } else if (autoSpinCriteria.singleWin > 0 && autoSpinState.lastPayout > autoSpinCriteria.singleWin) {
        ui.showDialog(utils.replaceString(loaded.languageFile.data.autospin_exceed, `XXX`, `${state.get('currency')} ${utils.showAsCurrency(autoSpinCriteria.singleWin)}`), '', loaded.languageFile.data.okay, {
            gotSecondButton: true,
            addCloseFunctionTo2Button: true,
            onPointerUp2: auto.callStartAutoSpin
        });
        throw `single win exceed ${autoSpinCriteria.singleWin}`;
    } else if (autoSpinCriteria.cashIncrease > 0 && cashDifferentiation >= autoSpinCriteria.cashIncrease) {
        ui.showDialog(utils.replaceString(loaded.languageFile.data.autospin_increase, `XXX`, `${state.get('currency')} ${utils.showAsCurrency(autoSpinCriteria.cashIncrease)}`), '', loaded.languageFile.data.okay, {
            gotSecondButton: true,
            addCloseFunctionTo2Button: true,
            onPointerUp2: auto.callStartAutoSpin
        });
        throw `cash increased`;
    } else if (autoSpinCriteria.cashDecrease > 0 && -(cashDifferentiation) >= autoSpinCriteria.cashDecrease) {
        ui.showDialog(utils.replaceString(loaded.languageFile.data.autospin_decrease, `XXX`, `${state.get('currency')} ${utils.showAsCurrency(autoSpinCriteria.cashDecrease)}`), '', loaded.languageFile.data.okay, {
            gotSecondButton: true,
            addCloseFunctionTo2Button: true,
            onPointerUp2: auto.callStartAutoSpin
        });
        throw `cash decreased`;
    } else if (autoSpinCriteria.numbered > 0 && autoSpinState.spinCount >= autoSpinCriteria.numbered) {
        ui.showDialog(utils.replaceString(loaded.languageFile.data.autospin_number, `XXX`, `${autoSpinCriteria.numbered}`), '', loaded.languageFile.data.okay, {
            gotSecondButton: true,
            addCloseFunctionTo2Button: true,
            onPointerUp2: auto.callStartAutoSpin
        });
        throw 'number of spins';
    }

    displayWallet();
    const newBalance = parseFloat(currentBalance.minus(utils.getTotalBet())).toFixed(2);

    // terminate if newBalance less than 0;
    if (newBalance < 0) {
        // ui.flash(walletText, '0xff0000', 5);
        enablePanelButton();
        ui.showDialog(loaded.languageFile.data.not_enough_balance, '');

        throw 'insufficient balance';
    }

    state.set('balance', newBalance);
    updateText();

    // if the stop criteria not meet, continue the spin
    // note: spinFn should always return a payout of the round.
    autoSpinState.spinCount = autoSpinState.spinCount + 1;
    return spinFn(true).then((checkWinObj) => {

        // checkWinObj can be a number of payout, or an object contain payout, isBonus and bonusValue
        let payout = undefined;
        let isBonus = false;
        let bonusValue = undefined;
        if (Object.prototype.toString.call(checkWinObj) === '[object Object]') {
            if (typeof checkWinObj.payout === 'undefined') throw new Error('checkWinObj does not contain payout');
            payout = checkWinObj.payout;
            isBonus = checkWinObj.isBonus;
            bonusValue = checkWinObj.bonusValue;
        } else {
            payout = checkWinObj;
            isBonus = false;
            bonusValue = undefined;
        }

        payout = payout ? payout : 0;
        // set last payout to check whether single win exceed or not.
        autoSpinState.lastPayout = payout;

        // update total win
        let autoSpinTotalWin = new Big(autoSpinState.totalWin);
        autoSpinState.totalWin = parseFloat(autoSpinTotalWin.plus(payout)).toFixed(2);

        // increase win count if payout more than 0
        autoSpinState.winCount = payout > 0 ? autoSpinState.winCount + 1 : autoSpinState.winCount;

        // is bonus game trigger
        autoSpinState.isBonus = isBonus;
        autoSpinState.bonusValue = bonusValue;

        if (isExitAutoSpin) {
            isExitAutoSpin = false;
            enableAutoSpinButton();
        }
        return autoSpinFn();
    });
}

function changeItem(item, max) {

    if (item > max) {
        item = 0;
    }

    if (item < 0) {
        item = max;
    }

    return item;
}

function changeAutoSpinButtonTexture(autoSpinActive) {
    if (isMobile()) {
        autoSpinButton.texture = loaded.autoSpinButton.texture;
    }
    else {
        autoSpinButton.texture = autoSpinActive ? loaded.autoSpinActive.texture : loaded.autoSpinButton.texture;
    }
}

function checkIfBettingMax(newOptions) {
    let betLineLimit = newOptions.betLineOptions[newOptions.betLineOptions.length - 1];
    let betAmountLimit = betAmountList[betAmountList.length - 1];
    return (state.get('betLine') >= betLineLimit && state.get('betAmount') >= betAmountLimit);
}

//! Used to update display text values (line bet, total bet, wallet). Reminder: Keep the users to date!!
function updateText() {
    setWalletInDisplay(state.get('balance'));
    totalBetText.text = state.get('currency') + ' ' + utils.showAsCurrency(utils.getTotalBet());
    betAmountText.text = state.get('betAmount');
    if (!fixMultiBetLine) {
        betLineText.text = state.get('betLine');
    }

    if (isMobile()) {
        each(panelDisplayContainer.children, (element) => {
            element.children[1].x = element.children[0].x + element.children[0].getBounds().width + 10;
        });

        totalBetLabel.x = totalBetText.x - (totalBetText.getBounds().width) - 5;
    }
}

function spinButtonClicked() {
    displayWalletAnimation = false;
    // terminate if the previous game is still running.
    if (isGameRunning) return;
    if (autoSpinRunning) return;
    displayWallet();
    spinIsPressed = true;

    //reset wallet text color
    // walletText.style.fill = 0xFFFFFF;

    // Play button effect
    spinButtonEffect.state.setAnimation(0, 'click', false);

    // We should not deduct the balance if the user has freeSpins or freeBet that they can use.
    let balanceBigValue = new Big(state.get('balance'));
    let newBalanceBigValue = parseFloat(balanceBigValue.minus(utils.getTotalBet()));
    const newBalance = (state.get('availableFreeSpin') || state.get('availableFreeBet')) ? state.get('balance') : newBalanceBigValue;

    // terminate if newBalance less than 0;
    if (newBalance < 0) {
        // ui.flash(walletText, '0xff0000', 5);
        ui.showDialog(loaded.languageFile.data.not_enough_balance, '');
        return;
    }

    state.set('balance', newBalance);
    updateText();

    // emit spinFinished event when game finished
    isGameRunning = true;

    // prevent sync.js update the text while spinning
    sync.disableSync();

    // disable panel buttons
    disablePanelButton();
    disableAutoSpinButton();

    spinFn().then(() => {
        panelContainer.emit('spinFinished');
        isGameRunning = false;
        enablePanelButton();
        enableAutoSpinButton();
        spinButton.texture = loaded.spinButton.texture;

        // resume sync.js
        sync.enableSync();
    });

    panelContainer.emit('spinButtonClicked');
}

function onTurboButtonClicked() {
    speedUp = !speedUp;
    panelContainer.emit('turboButtonClicked', speedUp);
    if (isMobile()) {
        if (speedUp) {
            turboSymbol.texture = loaded.turboSymbolActive.texture;
        }
        else {
            turboSymbol.texture = loaded.turboSymbol.texture;
        }
    } else {
        TweenMax.to(settingsContainer, 1, {y: 645});
        TweenMax.to(infoContainer, 0.5, {y: 680});
    }
}

function enablePanelButton() {
    spinButton.interactive = true;
    spinButton.texture = loaded.spinButton.texture;

    spin100Button.interactive = true;
    spin100Button.texture = loaded.spin100Button.texture;

    turboButton.interactive = true;
    turboButton.alpha = 1;

    betMaxButton.interactive = true;
    betMaxButton.alpha = 1;


    if (isMobile()) {
        betMaxSymbol.alpha = 1;
        turboSymbol.alpha = 1;
        // menuButton.interactive = true;
        // menuButton.alpha = 1;

        totalBetButton.interactive = true;
        totalBetButton.alpha = 1;

        spinArea.interactive = true;
    }
    else {
        betMaxLabel.alpha = 1;
        // settingsButton.alpha = 1;
        // settingsButton.interactive = true;

        // infoMenuButton.interactive = true;
        // infoMenuButton.buttonMode = true;
        // infoMenuButton.alpha = 1;

        if (!fixMultiBetLine) {
            betLineDecreaseButton.interactive = true;
            betLineDecreaseButton.alpha = 1;
            betLineIncreaseButton.interactive = true;
            betLineIncreaseButton.alpha = 1;
        }
        betLineText.alpha = 1;

        betAmountIncreaseButton.interactive = true;
        betAmountIncreaseButton.alpha = 1;
        betAmountText.alpha = 1;
        betAmountDecreaseButton.interactive = true;
        betAmountDecreaseButton.alpha = 1;


        turboLabel.alpha = 1;
    }
}

function enableAutoSpinButton() {
    autoSpinButton.interactive = true;
    autoSpinButton.texture = loaded.autoSpinButton.texture;
}

function disablePanelButton() {
    spinButton.interactive = false;
    spinButton.texture = loaded.spinDownButton.texture;

    spin100Button.interactive = false;
    spin100Button.texture = loaded.spin100DownButton.texture;

    turboButton.interactive = false;
    turboButton.alpha = 0.5;

    betMaxButton.interactive = false;
    betMaxButton.alpha = 0.5;


    if (isMobile()) {
        betMaxSymbol.alpha = 0.5;
        turboSymbol.alpha = 0.5;
        totalBetButton.interactive = false;
        totalBetButton.alpha = 0.5;
        spinArea.interactive = false;
        if (betContainer.y <= 920) {
            TweenMax.to(betContainer, 0.5, {y: 1280});
        }
    }
    else {
        betMaxLabel.alpha = 0.5;

        betAmountIncreaseButton.interactive = false;
        betAmountDecreaseButton.interactive = false;
        betLineDecreaseButton.interactive = false;
        betLineIncreaseButton.interactive = false;

        betAmountText.alpha = 0.5;
        betLineText.alpha = 0.5;
        betAmountIncreaseButton.alpha = 0.5;
        betAmountDecreaseButton.alpha = 0.5;
        betLineDecreaseButton.alpha = 0.5;
        betLineIncreaseButton.alpha = 0.5;

        // Close settings container
        TweenMax.to(settingsContainer, 1, {y: 645});
        TweenMax.to(infoContainer, 0.5, {y: 680});
        closePopUpSprite.interactive = false;

        turboLabel.alpha = 0.5;
    }
}

function disableAutoSpinButton() {
    autoSpinButton.interactive = false;
    autoSpinButton.texture = loaded.autoSpinDownButton.texture;
}

function displayWallet(data = null) {

    if (panelDisplayContainer) {
        panelDisplayContainer.destroy();
    }

    let loopStep = 0;
    panelDisplayContainer = new PIXI.Container();
    panelDisplayContainer.name = 'Panel Display Container';
    panelDisplayContainer.mask = balanceMask;
    //
    const balanceData = {
        amount: parseFloat(state.get('balance')).toFixed(2),
        text: (isMobile() ? (loaded.languageFile.data.balanceC) : (loaded.languageFile.data.balance)),
    };

    if (data) {
        let dataArray = Array.isArray(data) ? data : [data];

        each(dataArray, (element, index) => {
            createElement(element, isMobile() ? 0 : 660, index);
            ui.flash(panelDisplayContainer.children[index].children[1], '0x00FF00', 5);
        });
        createElement(balanceData, isMobile() ? (0 - (100 * dataArray.length)) : (660 - (100 * dataArray.length)));

    } else {
        createElement(balanceData, isMobile() ? 0 : 660);
    }

    panelContainer.addChild(panelDisplayContainer);

    if (panelDisplayContainer.children.length > 1) {
        displayWalletAnimation = true;
        movePanelDisplay(panelDisplayContainer, loopStep, data);

    }

    function createElement(element, origin, index = null) {
        let elementContainer = new PIXI.Container();
        elementContainer.x = isMobile() ? 0 : 213;
        elementContainer.y = origin - (100 * index);
        panelDisplayContainer.addChild(elementContainer);

        let textLabel = ui.createText({
            fontSize: isMobile() ? 25 : 18,
            fontWeight: 'bold'
        }, element.text);
        textLabel.anchor.x = isMobile() ? 0 : 0.5;
        textLabel.anchor.y = isMobile() ? 0 : 0;
        textLabel.tint = primaryTint;
        textLabel.x = isMobile() ? 24 : 0;
        textLabel.y = isMobile() ? 20 : 0;
        elementContainer.addChild(textLabel);

        // Balance text (wallet) in landscape modes
        let textNumber = ui.createText({
            fill: 'white',
            fontSize: isMobile() ? 25 : 20,
            fontWeight: 'bold'
        }, state.get('currency') + ' ' + (typeof element.amount === 'string' ? element.amount : utils.showAsCurrency(parseFloat(element.amount))));
        textNumber.anchor.x = isMobile() ? 0 : 0.5;
        textNumber.anchor.y = isMobile() ? 0 : 0.5;
        textNumber.x = isMobile() ? 153 : 0;
        textNumber.y = isMobile() ? 20 : 30;
        elementContainer.addChild(textNumber);
    }

    updateText();
}

function movePanelDisplay(panelDisplayContainer, loopStep, data) {
    each(panelDisplayContainer.children, (container, index) => {
        let originLocation = (isMobile() ? 0 : 660) - (100 * index) + (100 * (loopStep));
        if (loopStep === panelDisplayContainer.children.length - 1 && index === 0) {
            originLocation = (isMobile() ? -100 : 560);
        }
        TweenMax.delayedCall((speedUp ? 1 / 3 : 1), () => {
            TweenMax.fromTo(container, speedUp ? 1 / 3 : 1, {
                y: originLocation
            }, {
                y: originLocation + 100,
                onComplete: () => {
                    TweenMax.killTweensOf(container);
                }
            })
        });
    });
    TweenMax.delayedCall(((speedUp ? 1 / 3 : 1)) * 2, () => {
        if (loopStep === panelDisplayContainer.children.length - 1) {
            loopStep = 0;
        } else {
            loopStep++;
        }
        movePanelDisplay(panelDisplayContainer, loopStep, data);
    })
    setTimeout(() => {

    }, (speedUp ? (1 / 3) : 1) * 2000)
}

function setWalletInDisplay(amount) {
    each(panelDisplayContainer.children, (element, index) => {
        if (panelDisplayContainer.children[index].children[0].text === loaded.languageFile.data.balance ||
            panelDisplayContainer.children[index].children[0].text === loaded.languageFile.data.balanceC) {
            element.children[1].text = state.get('currency') + ' ' + utils.showAsCurrency(parseFloat(amount));
        }
    });
}

// todo: need clean up
function displayWinAmountWithSprites(options, speed, amount, coin = null, FrontObjectName = 'Panel Container') {

    let newOptions = Object.assign({
        xPosition: app.getResolutionX() / 2,
        yPosition: app.getResolutionY() / 2,
        tweenToY: -10,
        scaling: isMobile() ? 0.35 : 0.55,
        offset: 153.8,
        needBacking: true,
        backingPositioningY: null,
        accelerationSpeed: 1,
        tweenAlpha: 2,
    }, options);
    // A container to wrap all smaller containers (numbers and particles)
    // Set properties of this container later, once all child have initiated
    let winEffectContainer = new PIXI.Container;

    //return promise
    return new Promise((resolve) => {
        // Smaller number container, child of winEffectContainer todo not longer need this option
        let numberContainer = ui.createSpriteNumbers({
                scaling: newOptions.scaling,
                offset: newOptions.offset
            }, spriteNumberTexture, typeof amount === 'string' ? amount : utils.showAsCurrency(amount), newOptions.needBacking,
            newOptions.backingPositioningY ? newOptions.backingPositioningY : null);

        TweenMax.to(numberContainer, 1 / newOptions.accelerationSpeed, {
            y: newOptions.tweenToY,
            onComplete: function () {
                winEffectContainer.destroy();
                resolve(0);
            }
        });
        setTimeout(() => {
            TweenMax.to(winEffectContainer, newOptions.tweenAlpha / speed, {
                alpha: 0,
            });
        }, (3000 / newOptions.accelerationSpeed) / (2 * speed));

        numberContainer.name = 'Textured Number Container';

        winEffectContainer.x = newOptions.xPosition - numberContainer.getBounds().width / 2;
        winEffectContainer.y = newOptions.yPosition - numberContainer.getBounds().height / 2;

        // Smaller particle container, child of winEffectContainer
        let particleContainer = new PIXI.Container;
        particleContainer.x = numberContainer.getBounds().width / 2;
        particleContainer.y = numberContainer.getBounds().height / 2;
        particleContainer.name = 'Particle Container';
        particleContainer.interactiveChildren = false;
        sound.play('winMoneySound');

        let particleFrequency = amount / state.get('betAmount') > 100 ? 15 : (-0.285 * (amount / state.get('betAmount'))) + 300;
        let particleLife = speed === 1 ? 1000 : (speed * 1000) - 1000;
        let particleInterval = setInterval(() => {
            let randomNegative = (Math.round(Math.random() + 1));
            if (randomNegative === 2) randomNegative = -1;
            let xOffset = randomNegative * (Math.random() * numberContainer.getBounds().width / 2);

            // Initialize new particle for every interval
            let particle = new PIXI.extras.AnimatedSprite(coin ? coin : particles);
            particle.x = xOffset;
            particle.y = numberContainer.y;
            particle.animationSpeed = 0.35;
            particle.anchor.set(0.5);
            particle.play();
            const randScale = (Math.random() * 0.3) + 0.3;
            particle.scale.set(randScale);
            particle.name = 'Particles';

            // Particle movement properties
            let xValues = (Math.random() * 2 - 1) * 6;
            let particleProperties = {
                'xSpeed': xValues,
                'ySpeed': -(Math.random() * 5 + 3),
                'xAcceleration': xValues < 0 ? -0.05 : 0.05,
                'yAcceleration': 0.05,
                'gravity': 0.03,
            };
            particleContainer.addChild(particle);

            // Needs an item with properties to start a tween. Used tween update to update particles
            let dummyContent = {
                'dummyValue': 0,
            };
            TweenMax.to(dummyContent, 3, {
                dummyValue: 10,
                onUpdate: () => {
                    particle.x += particleProperties.xSpeed;
                    particle.y += particleProperties.ySpeed;
                    particle.rotation += 0.15;
                    particleProperties.xSpeed += particleProperties.xAcceleration;
                    particleProperties.ySpeed += particleProperties.yAcceleration;
                    particleProperties.yAcceleration += particleProperties.gravity;
                },
                onComplete: () => {
                    particle.destroy();
                    winEffectContainer.destroy();
                }
            });
        }, particleFrequency);

        // Stop spawning particles 1 second before number container finishes tween
        setTimeout(() => {
            clearInterval(particleInterval)
        }, particleLife);

        winEffectContainer.addChild(particleContainer);
        winEffectContainer.addChild(numberContainer);

        if (FrontObjectName === '') { // front layer
            app.addComponent(winEffectContainer);
        }
        else {
            each(app.stage.children, (child, index) => {
                if (child.name === FrontObjectName) {
                    app.stage.addChildAt(winEffectContainer, index - 1);
                }
            });
        }
    })
}
function start100Spin(data) {
    // terminate if newBalance less than 0;
    if (state.get('balance') < 0) {
        // ui.flash(walletText, '0xff0000', 5);
        ui.showDialog(loaded.languageFile.data.not_enough_balance, '');
        return;
    }

    let infoContainerIndex = utils.getArrayIndexNumByName(app.stage.children, 'Info Container');

    // 100 Spin Spine
    if(spin100Spine === null) {
        spin100Spine = new PIXI.spine.Spine(loaded.spin100Effect_json.spineData);
        spin100Spine.name = '100 Spin Spine';
        spin100Spine.autoUpdate = true;
        spin100Spine.x = isMobile() ? 360 : 640;
        spin100Spine.y = isMobile() ? 635 : 420;
        spin100Spine.y += spin100YOffset;
        spin100Spine.interactiveChildren = false;
        app.stage.addChildAt(spin100Spine, infoContainerIndex - 1);
    }
    spin100Spine.renderable = true;
    spin100Spine.state.setAnimation(0, 'animtion0', false);


    sound.play('100SpinSound');
    const runningNumberContainerOptions = {
        // scaling and offset option available to adjust but currently use default
    };
    let runningNumber = {
        val: 0,
        dummyNumber: 0
    };

    let runningNumberContainer = new PIXI.Container();
    runningNumberContainer.y = app.getResolutionY() / 2;

    TweenMax.to(runningNumber, 2.5 + spin100YAddDelay, {
        val: data.total_win,
        ease: Power4.easeInOut,
        delay: spin100YAddDelay,

        // Updates y value of bonus title and running numbers based on spine.y
        onUpdate: () => {
            if (runningNumberContainer)
                app.stage.removeChild(runningNumberContainer);
            let updatedString = utils.showAsCurrency(runningNumber.val);
            updatedString = updatedString.toString();

            runningNumberContainer = ui.createSpriteNumbers(runningNumberContainerOptions, spin100CustomNumberSprite, updatedString, spin100NumberBacking);
            runningNumberContainer.x = app.getResolutionX() / 2 - 10 - runningNumberContainer.getBounds().width / 2;
            runningNumberContainer.y = isMobile() ? app.getResolutionY() / 2 - 0 : app.getResolutionY() / 2 + 60;
            runningNumberContainer.y += spin100YTextOffset;
            app.stage.addChildAt(runningNumberContainer, infoContainerIndex);
        },

        onComplete: function () {
            setTimeout(() => {
                let panelMove = [
                    {amount: data.total_win, text: loaded.languageFile.data.win},
                ];
                displayWallet(panelMove);
                runningNumberContainer.destroy();
                spin100Spine.renderable = false;
                isGameRunning = false;

                // re-enable sync function
                sync.enableSync();

                if (data.total_win > 0) {
                    state.set('balance', data.balance);
                    // ui.flash(walletText, '0x00FF00', 5);
                    updateText();
                }

                // close black backing
                backing.close();
                // enable back button
                enablePanelButton();
                enableAutoSpinButton();
            }, 2500);
        },
    });

    state.set('availableFreeSpin', data.available_free_spin);
    let availableFreeSpin = state.get('availableFreeSpin');

    state.set('availableFreeBet', data.available_free_bet);
    let availableFreeBet = state.get('availableFreeBet');

    state.set('pendingReleaseFreeSpin', data.pending_release_free_spin);
    let pendingReleaseFreeSpin = state.get('pendingReleaseFreeSpin');

    state.set('pendingReleaseFreeBet', data.pending_release_free_bet);
    let pendingReleaseFreeBet = state.get('pendingReleaseFreeBet');

    if ((pendingReleaseFreeSpin) || (availableFreeSpin)) {
        freeSpin.refreshContainer();
        freeSpin.setVisibility(true);
    } else if ((pendingReleaseFreeBet && pendingReleaseFreeBet.required_turnover > 0) || (availableFreeBet && availableFreeBet.required_turnover > 0)) {
        freeBet.refreshContainer();
        freeBet.setVisibility(true);
    } else {
        freeSpin.setVisibility(false);
        freeBet.setVisibility(false);
    }

    // Detection to see if this free spin is new
    // AND is awarded by admin / system, NOT earned through game spins
    freeSpin.checkToAnnounce();
    freeBet.checkToAnnounce();
}

function exitConfirmation() {
    var msg = loaded.languageFile.data.exit_confirmation;
    return msg;
}

function exitGame() {
    let answer = confirm(loaded.languageFile.data.exit_confirmation);

    if (answer) {
        // note: as now we open the game on new window from gamegeon.com, we can close the window instead of
        // redirect player back to landing page.
        window.parent.postMessage('exitButtonClicked', '*');


        window.close();
    }
}

function getSpriteNumberTexture() {
    return spriteNumberTexture;
}

function getAutoSpinRunning() {
    return autoSpinRunning;
}

function playWinEffect(type, startValue, endValue, frontObjectName = 'Panel Container') {
    return new Promise((resolve) => {
        backing.open('Particles Coin Container');
        let value = {val: startValue};
        let runningNumber;

        let panelContainerIndex = utils.getArrayIndexNumByName(app.stage.children, frontObjectName);
        let winEffectSpine = type === 'mega' ? new PIXI.spine.Spine(loaded.winMega.spineData) : new PIXI.spine.Spine(loaded.winBig.spineData);
        winEffectSpine.autoUpdate = true;
        winEffectSpine.name = 'win effect spine';
        winEffectSpine.x = app.getResolutionX() / 2;
        winEffectSpine.y = app.getResolutionY() / 2;
        winEffectSpine.state.setAnimation(0, 'animtion0', false);
        winEffectSpine.state.addListener({
            complete: () => {
                TweenMax.to(runningNumber, 1, {
                    y: runningNumber.y - 200,
                    onComplete: () => {
                        setTimeout(() => {
                            winEffectSpine.destroy();
                            runningNumber.destroy();
                            backing.close();
                            resolve(0);
                        }, 100);
                    }
                })
            }
        });
        app.stage.addChildAt(winEffectSpine, panelContainerIndex);

        let winEffectIndex = utils.getArrayIndexNumByName(app.stage.children, 'win effect spine');
        TweenMax.to(value, 1.4, {
            val: endValue,
            onUpdate: () => {
                if (runningNumber) {
                    runningNumber.destroy();
                }

                runningNumber = ui.createSpriteNumbers({}, winEffectCustomNumberSprite, utils.showAsCurrency(value.val));
                runningNumber.x = app.getResolutionX() / 2 - runningNumber.getBounds().width / 2;
                runningNumber.y = app.isMobile() ? app.getResolutionY() * 0.6 : app.getResolutionY() * 0.62;
                app.stage.addChildAt(runningNumber, winEffectIndex);
            }
        })
    });
}

function playChargeEffect(spawnPosition = {x: 10, y: 20}, isRotating = false, options) {

    const newOptions = Object.assign({
        turboTime1: 0.35,
        turboTime2: 0.55,
        normalTime1: 0.3,
        normalTime2: 0.7,
        tint: null,
    }, options);

    return new Promise((resolve) => {
        let chargeParticle = new PIXI.Sprite(loaded.charge_particle.texture);
        chargeParticle.x = spawnPosition.x;
        chargeParticle.y = spawnPosition.y;
        chargeParticle.alpha = 0;
        chargeParticle.scale.x = 0.3;
        chargeParticle.scale.y = 0.3;
        if (newOptions.tint) {
            chargeParticle.tint = newOptions.tint;
        }
        chargeContainer.addChild(chargeParticle);

        TweenMax.to(chargeParticle, speedUp ? newOptions.turboTime1 : newOptions.normalTime1, {
            alpha: 1,
            onComplete: () => {
                TweenMax.to(chargeParticle.scale, speedUp ? newOptions.turboTime1 : newOptions.normalTime1, {
                    x: 1, y: 1,
                    onComplete: () => {
                        TweenMax.to(chargeParticle, speedUp ? newOptions.turboTime2 : newOptions.normalTime2, {
                            delay: 0.2,
                            x: chargeTargetPosition.x
                        });
                        TweenMax.to(chargeParticle, speedUp ? newOptions.turboTime2 : newOptions.normalTime2, {
                            delay: 0.2,
                            y: chargeTargetPosition.y,
                            ease: Back.easeOut.config(1.4)
                        });
                        TweenMax.to(chargeParticle, speedUp ? newOptions.turboTime2 : newOptions.normalTime2, {
                            delay: 0.2,
                            alpha: 0,
                            rotation: isRotating ? 4 : 0
                        });
                        TweenMax.to(chargeParticle.scale, speedUp ? newOptions.turboTime2 : newOptions.normalTime2, {
                            delay: 0.4,
                            x: 0.2, y: 0.2,
                            onComplete: () => {
                                resolve(0);
                                setTimeout(() => {
                                    chargeContainer.removeChild(particles);
                                }, 150);
                            }
                        });
                    }
                });
            }
        });
    });
}

function stopAutoSpin() {
    isExitAutoSpin = autoSpinRunning;
    autoSpinRunning = false;
    disableAutoSpinButton();
}

export default {
    init,
    updateText,
    displayWinAmountWithSprites,
    displayWallet,
    getSpriteNumberTexture,
    getAutoSpinRunning,
    beforeSpinApi,
    runPreEvents,
    enablePanelButton,
    enableAutoSpinButton,
    disablePanelButton,
    disableAutoSpinButton,
    playWinEffect,
    playChargeEffect,
    stopAutoSpin,
}
