import environment from 'environment';

import utils from './utils';
import state from './state';
import domUtils from './domUtils';

function init(queryParams = null) {

    domUtils.addStyleString(`
    
    .bet-history {
        position: absolute;
        top: 100%;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        transition: all 0.3s;
    }
    
    .bet-history.show {
        top: 0;
    }
    
    .bet-history .btn-close {
        position: absolute;
        top: 0;
        right: 0;
        font-size: 1.5rem;
        background-color: #2f2f2f;
        outline: 0;
        color: white;
        border: 2px solid white;
        cursor: pointer;
        border-radius: 50%;
        width: 2.5rem;
        height: 2.5rem;
        padding: 5px;
    }
    
    .bet-history .btn-close.hide {
        display: none;
    }
    
    @media (max-width: 600px) {
        .bet-history .btn-close {
            top: 0;
            left: 0;
            font-size: 20px;
            width: 40px;
            height: 40px;
            margin: 10px;
        }
    }
    
    .bet-history iframe {
        width: 100%;
        height: 100%;
        border: 0;
    }
    `);

    const betHistoryDiv = document.createElement('div');
    betHistoryDiv.setAttribute('id', 'bet-history');
    betHistoryDiv.classList.add('bet-history');

    // construct url to transaction history
    const gameName = environment.gameName;
    const token = utils.getQueryParam('token');
    const lang = utils.getQueryParam('lang');
    const isDemo = utils.getQueryParam('isDemo');
    const apiUrl = utils.getQueryParam('apiUrl');
    const currency = state.get('currency');

    let betHistoryURL = getBetHistoryURL();
    let extension = `?game=${gameName}&token=${token}`;
    let URL = `${betHistoryURL}${extension}`;

    if (lang) {
        URL += `&lang=${lang}`;
    }

    if (currency) {
        URL += `&currency=${currency}`;
    }

    if (isDemo) {
        URL += `&isDemo=true`;
    }

    if (queryParams) {
        URL += `&${queryParams}`;
    }

    if (apiUrl) {
        URL += `&apiUrl=${apiUrl}`;
    }

    const iframe = document.createElement('iframe');
    iframe.setAttribute('id', 'bet-history-iframe');
    iframe.setAttribute('src', URL);
    betHistoryDiv.appendChild(iframe);

    const betHistoryCloseButton = document.createElement('button');
    betHistoryCloseButton.setAttribute('id', 'btn-close');
    betHistoryCloseButton.classList.add('btn-close');
    betHistoryCloseButton.innerText = 'X';
    betHistoryCloseButton.addEventListener('click', () => {
        betHistoryDiv.classList.remove('show');

        const betHistoryElement = document.getElementById('bet-history-iframe');
        if (betHistoryElement === null) return;
        const iWindow = betHistoryElement.contentWindow;

        iWindow.postMessage({"for": "closedTransactionDetail"}, betHistoryURL);
    });
    betHistoryDiv.appendChild(betHistoryCloseButton);

    document.body.appendChild(betHistoryDiv);
}

function show() {
    const betHistoryElement = document.getElementById('bet-history');
    betHistoryElement.classList.add('show');
    betHistoryElement.getElementsByTagName('iframe')[0].focus();
}

function hideCloseButton() {
    const closeButton = document.getElementById('btn-close');
    closeButton.classList.add('hide');
}

function showCloseButton() {
    const closeButton = document.getElementById('btn-close');
    closeButton.classList.remove('hide');
}

function getBetHistoryURL(){
    let hostLocation = window.location.host === `localhost:${window.location.port}` ? 'file.staging.majagames.com' : window.location.host;
    let hostProtocol = window.location.host === `localhost:${window.location.port}` ? 'https:' : window.location.protocol;

    let betHistoryUrl = `${hostProtocol}//${hostLocation}/gamegeon-bet-history`;
    return betHistoryUrl;
}

export default {
    init,
    show,
    hideCloseButton,
    showCloseButton,
}
