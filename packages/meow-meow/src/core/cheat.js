function isCheatMode() {
    return (localStorage.cheatMode === 'win' || localStorage.cheatMode === 'bonus');
}

function setCheatMode(cheatMode) {
    localStorage['cheatMode'] = cheatMode;
}

function getCheatMode() {
    return localStorage['cheatMode'];
}

function deleteCheatMode() {
    delete localStorage['cheatMode'];
}

export default {
    isCheatMode,
    setCheatMode,
    getCheatMode,
    deleteCheatMode,
}
