import domUtils from './domUtils';

let preLoaderDiv = null;

function init() {

    domUtils.addStyleString(`
        #pre-loader {
            color: white;
            text-align: center;
        }
    `);

    preLoaderDiv = document.createElement('div');
    preLoaderDiv.setAttribute('id', 'pre-loader');
    // preLoaderDiv.innerText = 'Loading\n加载中';
    document.body.appendChild(preLoaderDiv);
}

function updateText(text) {
    if (preLoaderDiv) {
        preLoaderDiv.innerText = text;
    }
}

function close() {
    document.body.removeChild(preLoaderDiv);
    preLoaderDiv = null;
}

export default {
    init,
    updateText,
    close,
}
