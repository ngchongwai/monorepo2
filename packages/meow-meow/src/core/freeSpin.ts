import * as PIXI from 'pixi.js';
import { TweenMax } from 'gsap';
import { each } from 'lodash';
// @ts-ignore
import MultiStyleText from 'pixi-multistyle-text';

import ui from './ui';
import state from './state';
import { getLanguage, getResolutionX, getResolutionY, isMobile } from './app';
import utils from './utils';

let instance;

export default class FreeSpin {

    protected loaded;
    protected gameName: string;
    protected token: any;
    protected freeSpinContainer: PIXI.Container = new PIXI.Container();
    protected messageContainer: PIXI.Container;
    protected announcementContainer: PIXI.Container;

    constructor() {
        if (instance) {
            return instance;
        }

        instance = this;

        this.loaded = PIXI.loader.resources;

        // Prepares assets required for the floating free spins button
        this.freeSpinContainer.name = 'Free Spin Container';
        this.freeSpinContainer.visible = false;
        this.freeSpinContainer.x = getResolutionX() / 2;
        this.freeSpinContainer.y = getResolutionY() / 2;

        let baseSprite = new PIXI.Sprite(this.loaded.freeMode_base.texture);
        baseSprite.name = 'Base';
        baseSprite.anchor.set(0.5);
        baseSprite.interactive = true;
        baseSprite.buttonMode = true;
        baseSprite.x = isMobile() ? 300 : 580;
        baseSprite.y = isMobile() ? -420 : -270;
        baseSprite.on('pointerdown', () => {
            this.freeSpinContainer.alpha = 0.5;
        });
        baseSprite.on('pointerup', () => {
            this.freeSpinContainer.alpha = 1;
            this.showMessageContainer();
        });
        this.freeSpinContainer.addChild(baseSprite);

        let baseTitle = ui.createText({
            fontSize: 14,
            fontWeight: 'bold',
        }, 'Freespin');
        baseTitle.name = 'Base Title';
        baseTitle.x = baseSprite.x;
        baseTitle.y = baseSprite.y - 50;
        this.freeSpinContainer.addChild(baseTitle);

        let freeSpinCounter = ui.createText({
            fontSize: 46,
            fontWeight: 'bold',
        }, '10');
        freeSpinCounter.name = 'Free Spin Counter';
        freeSpinCounter.x = baseSprite.x;
        freeSpinCounter.y = baseSprite.y + 15;
        this.freeSpinContainer.addChild(freeSpinCounter);

        let fullStarSprite = new PIXI.Sprite(this.loaded.freeMode_chest.texture);
        fullStarSprite.name = 'Full Star';
        fullStarSprite.anchor.set(0.5);
        fullStarSprite.x = baseSprite.x;
        fullStarSprite.y = baseSprite.y + 14;
        this.freeSpinContainer.addChild(fullStarSprite);

        let progressBarBacking = new PIXI.Graphics();
        progressBarBacking.name = 'Progress Bar Backing';
        progressBarBacking.lineStyle(9, 0x555555, 1);
        progressBarBacking.arc(baseSprite.x, baseSprite.y + 15, 35, Math.PI, 4 * Math.PI);
        this.freeSpinContainer.addChild(progressBarBacking);

        let progressBar = new PIXI.Graphics();
        progressBar.name = 'Progress Bar';
        progressBar.lineStyle(9, 0xecd063, 1);
        progressBar.arc(baseSprite.x, baseSprite.y + 15, 35, 2.5 * Math.PI, 1.5 * Math.PI);
        this.freeSpinContainer.addChild(progressBar);

        this.freeSpinContainer['contentUpdate'] = () => {
            this.freeSpinContainer['shouldShow'](true);

            if (this.getAvailableFreeSpin()) {
                freeSpinCounter.text = `${this.getAvailableFreeSpin().free_spins - this.getAvailableFreeSpin().used_free_spins}`;

                freeSpinCounter.visible = true;
                fullStarSprite.visible = false;

                baseTitle.text = `${this.loaded.languageFile.data.free_spin}`;
            } else if (this.getPendingReleaseFreeSpin()) {
                freeSpinCounter.visible = false;
                fullStarSprite.visible = true;

                baseTitle.text = `${this.loaded.languageFile.data.turnover}`;
            }

            let turnoverAmount = this.getAvailableFreeSpin() ? this.getAvailableFreeSpin().used_free_spins - this.getAvailableFreeSpin().free_spins :
                this.getPendingReleaseFreeSpin().current_turnover;
            let requiredTurnoverAmount = this.getAvailableFreeSpin() ? this.getAvailableFreeSpin().free_spins : this.getPendingReleaseFreeSpin().required_turnover;

            let progress = this.calculateTurnoverProgress(turnoverAmount, requiredTurnoverAmount);

            // 1.5 Offset to start from 12o'clock of circle
            // Full circle is 2PI of initial value. Need to progress * 2
            let newAngle = (1.5 + (progress * 2)) * Math.PI;

            progressBar.clear();
            progressBar.lineStyle(9, 0xecd063, 1);
            progressBar.arc(baseSprite.x, baseSprite.y + 15, 35, 1.5 * Math.PI, newAngle);
        };

        this.freeSpinContainer['overideFreeSpinCounter'] = (value) => {
            // Override the free spin text
            freeSpinCounter.text = value;

            // Override the progress bar shape
            let progress = this.calculateTurnoverProgress((value * -1), this.getAvailableFreeSpin().free_spins);

            // 1.5 Offset to start from 12o'clock of circle
            // Full circle is 2PI of initial value. Need to progress * 2
            let newAngle = value === 0 ? 3.5 * Math.PI : (1.5 + (progress * 2)) * Math.PI;

            progressBar.clear();
            progressBar.lineStyle(9, 0xecd063, 1);
            progressBar.arc(baseSprite.x, baseSprite.y + 15, 35, 1.5 * Math.PI, newAngle);
        };

        this.freeSpinContainer['shouldShow'] = (value) => {
            baseSprite.visible = !!(value);
            baseTitle.visible = !!(value);
            freeSpinCounter.visible = !!(value);
            fullStarSprite.visible = !!(value);
            progressBarBacking.visible = !!(value);
            progressBar.visible = !!(value);
        };

        // Filter out to only free spins with same gameName
        let availableFreeSpin = this.getAvailableFreeSpin();
        let pendingReleaseFreeSpin = this.getPendingReleaseFreeSpin();

        this.freeSpinContainer.addChild(this.generateParentContainer());

        if (pendingReleaseFreeSpin && pendingReleaseFreeSpin.required_turnover > 0) {
            this.refreshContainer();
            this.updateMessageContainers();
        }

        if (availableFreeSpin) {
            this.refreshContainer();
            this.updateMessageContainers();
            this.updateMessageController();
        }
    }

    getFreeSpinContainer() {
        return instance.freeSpinContainer;
    }

    getAvailableFreeSpin() {
        return state.get('availableFreeSpin');
    }

    getPendingReleaseFreeSpin() {
        return state.get('pendingReleaseFreeSpin');
    }

    setVisibility(visibility: boolean) {
        this.freeSpinContainer.visible = visibility;
    }

    refreshContainer() {
        this.updateMessageContainers();
        this.freeSpinContainer['contentUpdate']();

        this.updateMessageController();
    }

    overideFreeSpinCounter(value: number) {
        this.freeSpinContainer['overideFreeSpinCounter'](value);
    }

    // Generates a progress text Ex: (5 / 8 spins left)
    generateExpiryDateText(data) {
        if (!data) {
            throw new Error('There is no data passed in');
        }

        let time = this.calculateCountdown();

        const rewardExpiresText = this.loaded.languageFile.data.this_reward_expires_in;
        let daysText = this.loaded.languageFile.data.days;
        let hoursText = this.loaded.languageFile.data.hours;
        let minutesText = this.loaded.languageFile.data.minutes;

        let expiryDateText = new MultiStyleText(`${rewardExpiresText} \n<style1>${time['days']}</style1> ${daysText} <style1>${time['hours']}</style1> ${hoursText} <style1>${time['minutes']}</style1> ${minutesText}`,
            {
                'default': {
                    fontFamily: 'Arial',
                    fontSize: isMobile() ? 24 : 16,
                    fontWeight: 'bold',
                    fill: 0xffffff,
                    align: 'center'
                },
                'style1': {
                    fontFamily: 'Arial',
                    fontSize: isMobile() ? 32 : 24,
                    fill: '0xedd06a',
                    fontWeight: "bold",
                }
            });
        expiryDateText.anchor.set(0.5);

        setInterval(() => {
            let newTime = this.calculateCountdown();
            expiryDateText.text = `${rewardExpiresText} \n<style1>${newTime['days']}</style1> ${daysText} <style1>${newTime['hours']}</style1> ${hoursText} <style1>${newTime['minutes']}</style1> ${minutesText}`;
        }, 2000);


        return expiryDateText;
    }

    // Returns turnover progress over 1
    calculateTurnoverProgress(currentTurnover, requiredTurnover) {
        return currentTurnover / requiredTurnover;
    }

    checkToAnnounce() {
        let availableFreeSpin = this.getAvailableFreeSpin();

        if (availableFreeSpin &&
            availableFreeSpin.used_free_spins === 0 &&   // New if used is 0
            availableFreeSpin.current_turnover === 0 &&  // New if current turnover is 0
            availableFreeSpin.required_turnover > 0 // Awarded if required turnover is more than 0.
        ) {
            TweenMax.to(this.messageContainer.scale, 0.25, {x: 1, y: 1});
            this.showSpecificMessageContainerByName('Message Container 1');
        }
    }

    generateParentContainer() {
        this.messageContainer = new PIXI.Container();
        this.messageContainer.name = 'Message Container';
        this.messageContainer.scale.set(1);
        this.messageContainer.visible = false;

        let background = new PIXI.Graphics().beginFill(0x000000).drawRect(0, 0, getResolutionX(), getResolutionY()).endFill();
        background.name = 'Background Cover';
        background.alpha = 0.8;
        background.interactive = true;
        background.x = -getResolutionX() / 2;
        background.y = -getResolutionY() / 2;
        this.messageContainer.addChild(background);

        this.messageContainer.addChild(this.generateContainer1());
        this.messageContainer.addChild(this.generateContainer2());
        this.messageContainer.addChild(this.generateContainer3());
        this.messageContainer.addChild(this.generateContainer4());
        this.messageContainer.addChild(this.generateContainer5());

        return this.messageContainer;
    }

    generateContainer1() {
        // Prepares all resources needed for the message display of free spins
        let messageContainer1 = new PIXI.Container();
        messageContainer1.name = 'Message Container 1';
        messageContainer1.x = getResolutionX() / 2;

        let messageSprite = new PIXI.Sprite(this.loaded.freeMode_message.texture);
        messageSprite.name = 'Message Background';
        messageSprite.anchor.set(0.5);
        messageSprite.x = -getResolutionX() / 2;
        messageContainer1.addChild(messageSprite);

        let messageTitle = ui.createText({
            align: "center",
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
            fontWeight: "bold",
            fontSize: isMobile() ? 90 : 60
        }, `${this.loaded.languageFile.data.rewards}`);
        messageTitle.name = 'Message Title';
        messageTitle.x = -getResolutionX() / 2;
        messageTitle.y = isMobile() ? -275 : -175;
        messageContainer1.addChild(messageTitle);

        let message2 = ui.createText({
            fontSize: isMobile() ? 28 : 16,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.you_have_received}`);
        message2.name = 'Message 2';
        message2.x = -getResolutionX() / 2;
        message2.y = isMobile() ? -180 : -130;
        messageContainer1.addChild(message2);

        let numberOfFreeSpins = ui.createText({
            fontSize: isMobile() ? 186 : 136,
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
            fontWeight: "bold",
        }, '8');
        numberOfFreeSpins.name = 'Win Amount';
        numberOfFreeSpins.x = -getResolutionX() / 2;
        numberOfFreeSpins.y = -50;
        messageContainer1.addChild(numberOfFreeSpins);

        let message3 = ui.createText({
            fontSize: 28,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.freespin}`);
        message3.name = 'Message 3';
        message3.x = -getResolutionX() / 2;
        message3.y = isMobile() ? 60 : 30;
        messageContainer1.addChild(message3);

        let okButton = ui.createButton({
            texture: this.loaded.okButton.texture,
            clickedTexture: this.loaded.okButton.texture,
            centerAnchor: true,
            onPointerDown: () => {
                this.messageContainer.visible = false;
            },
        });
        okButton.x = -getResolutionX() / 2;
        okButton.y = isMobile() ? 380 : 170;
        messageContainer1.addChild(okButton);

        let expiryDateText = this.generateExpiryDateText({
            days: 10,
            hours: 10,
            minutes: 10,
        });
        expiryDateText.name = 'Expiry Date Message';
        expiryDateText.x = -getResolutionX() / 2;
        expiryDateText.y = isMobile() ? 210 : 100;
        messageContainer1.addChild(expiryDateText);

        messageContainer1['contentUpdate'] = () => {
            if (!this.getAvailableFreeSpin()) {
                return;
            }

            let number = this.getAvailableFreeSpin() ? this.getAvailableFreeSpin().free_spins : this.getPendingReleaseFreeSpin().free_spins;
            numberOfFreeSpins.text = `${number}`;

            expiryDateText.visible = !(this.getAvailableFreeSpin() ? this.getAvailableFreeSpin().is_game_rewarded : this.getPendingReleaseFreeSpin().is_game_rewarded);
        };

        return messageContainer1;
    }

    generateContainer2() {
        // Prepares all resources needed for the message display of free spins
        let messageContainer2 = new PIXI.Container();
        messageContainer2.name = 'Message Container 2';
        messageContainer2.x = getResolutionX() / 2;

        let messageSprite = new PIXI.Sprite(this.loaded.freeMode_message.texture);
        messageSprite.name = 'Message Background';
        messageSprite.anchor.set(0.5);
        messageSprite.x = -getResolutionX() / 2;
        messageSprite.x = -getResolutionX() / 2;
        messageContainer2.addChild(messageSprite);

        let messageTitle = ui.createText({
            fontSize: isMobile() ? 92 : 48,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, `${this.loaded.languageFile.data.free_spin}`);
        messageTitle.name = 'Message Title';
        messageTitle.x = -getResolutionX() / 2;
        messageTitle.y = isMobile() ? -300 : -175;
        messageContainer2.addChild(messageTitle);

        let progressText = new MultiStyleText(`<style1>0</style1> / 0 ${this.loaded.languageFile.data.spins_left}`,
            {
                'default': {
                    fontFamily: 'Arial',
                    fontSize: isMobile() ? 28 : 16,
                    fontWeight: 'bold',
                    fill: 0xffffff,
                },
                'style1': {
                    fontFamily: 'Arial',
                    fontWeight: 'bold',
                    fill: '0xedd06a',
                }
            });
        progressText.anchor.set(0.5);
        progressText.name = 'Progress Test';
        progressText.x = -getResolutionX() / 2;
        progressText.y = isMobile() ? -210 : -125;
        messageContainer2.addChild(progressText);

        let message2 = ui.createText({
            fontSize: isMobile() ? 28 : 16,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.current_freespin_earnings}`);
        message2.name = 'Message 2';
        message2.x = -getResolutionX() / 2;
        message2.y = isMobile() ? -90 : -75;
        messageContainer2.addChild(message2);

        let winAmount = ui.createText({
            fontSize: 108,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, '$38');
        winAmount.name = 'Win Amount';
        winAmount.x = -getResolutionX() / 2;
        winAmount.y = -10;
        messageContainer2.addChild(winAmount);

        let okButton = ui.createButton({
            texture: this.loaded.okButton.texture,
            clickedTexture: this.loaded.okButton.texture,
            centerAnchor: true,
            onPointerDown: () => {
                this.messageContainer.visible = false;
            },
        });
        okButton.x = -getResolutionX() / 2;
        okButton.y = isMobile() ? 380 : 170;
        messageContainer2.addChild(okButton);

        let expiryDateText = this.generateExpiryDateText({
            days: 10,
            hours: 10,
            minutes: 10,
        });
        expiryDateText.name = 'Expiry Date Text';
        expiryDateText.x = -getResolutionX() / 2;
        expiryDateText.y = isMobile() ? 210 : 100;
        messageContainer2.addChild(expiryDateText);

        messageContainer2['contentUpdate'] = () => {
            if (!this.getAvailableFreeSpin()) {
                return;
            }

            let freeSpinsLeft = this.getAvailableFreeSpin().used_free_spins;
            let freeSpinsTotal = this.getAvailableFreeSpin().free_spins;
            progressText.text = `<style1>${freeSpinsLeft}</style1> / ${freeSpinsTotal} ${this.loaded.languageFile.data.spins_left}`;

            let winAmountText = this.getAvailableFreeSpin() ? this.getAvailableFreeSpin().win_amount : this.getPendingReleaseFreeSpin().win_amount;
            winAmount.text = state.get('currency') + utils.showAsCurrency(winAmountText);

            expiryDateText.visible = !(this.getAvailableFreeSpin().is_game_rewarded);
        };

        return messageContainer2;
    }

    generateContainer3() {
        // Prepares all resources needed for the message display of free spins
        let messageContainer3 = new PIXI.Container();
        messageContainer3.name = 'Message Container 3';
        messageContainer3.x = getResolutionX() / 2;

        let messageSprite = new PIXI.Sprite(this.loaded.freeMode_message.texture);
        messageSprite.name = 'Message Background';
        messageSprite.anchor.set(0.5);
        messageSprite.x = -getResolutionX() / 2;
        messageContainer3.addChild(messageSprite);

        let messageTitle = ui.createText({
            fontSize: isMobile() ? 90 : 58,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, `${this.loaded.languageFile.data.unlock}`);
        messageTitle.name = 'Message Title';
        messageTitle.x = -getResolutionX() / 2;
        messageTitle.y = isMobile() ? -290 : -176;
        messageContainer3.addChild(messageTitle);

        let message2 = ui.createText({
            fontSize: isMobile() ? 28 : 16,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.total_freespin_earnings}`);
        message2.name = 'Message 2';
        message2.x = -getResolutionX() / 2;
        message2.y = isMobile() ? -200 : -110;
        messageContainer3.addChild(message2);

        let winAmount = ui.createText({
            fontSize: 108,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, '$58');
        winAmount.name = 'Win Amount';
        winAmount.x = -getResolutionX() / 2;
        winAmount.y = isMobile() ? -110 : -35;
        messageContainer3.addChild(winAmount);

        let tempNum = 88;
        let message3 = new MultiStyleText(`${this.loaded.languageFile.data.complete} <style1>$${tempNum}</style1> ${this.loaded.languageFile.data.turnover_to_unlock_freespin}`,
            {
                'default': {
                    align: 'center',
                    fontSize: isMobile() ? 28 : 16,
                    fill: '0xffffff',
                    fontWeight: 'bold',
                    wordWrap: !!isMobile(),
                    wordWrapWidth: 400,
                },
                'style1': {
                    fontSize: isMobile() ? 32 : 20,
                    fontWeight: 'bold',
                    fill: '0xedd06a',
                }
            });
        message3.name = 'Message 3';
        message3.anchor.set(0.5);
        message3.x = -getResolutionX() / 2;
        message3.y = isMobile() ? -5 : 40;
        messageContainer3.addChild(message3);

        let okButton = ui.createButton({
            texture: this.loaded.okButton.texture,
            clickedTexture: this.loaded.okButton.texture,
            centerAnchor: true,
            onPointerDown: () => {
                this.messageContainer.visible = false;
            },
        });
        okButton.x = -getResolutionX() / 2;
        okButton.y = isMobile() ? 380 : 170;
        messageContainer3.addChild(okButton);

        let expiryDateText = this.generateExpiryDateText({
            days: 10,
            hours: 10,
            minutes: 10,
        });
        expiryDateText.name = 'Expiry Date Text';
        expiryDateText.x = -getResolutionX() / 2;
        expiryDateText.y = isMobile() ? 210 : 100;
        messageContainer3.addChild(expiryDateText);

        messageContainer3['contentUpdate'] = () => {
            if (!this.getPendingReleaseFreeSpin()) {
                return;
            }

            let winAmountText = this.getPendingReleaseFreeSpin().win_amount;
            winAmount.text = state.get('currency') + utils.showAsCurrency(winAmountText);

            let requiredTurnover = state.get('currency') + utils.showAsCurrency(this.getPendingReleaseFreeSpin().required_turnover);
            message3.text = `${this.loaded.languageFile.data.complete} <style1>${requiredTurnover}</style1> ${this.loaded.languageFile.data.turnover_to_unlock_freespin}`;

            expiryDateText.visible = !(this.getPendingReleaseFreeSpin().is_game_rewarded);
        };

        return messageContainer3;
    }

    generateContainer4() {
        // Prepares all resources needed for the message display of free spins
        let messageContainer4 = new PIXI.Container();
        messageContainer4.name = 'Message Container 4';
        messageContainer4.x = getResolutionX() / 2;

        let messageSprite = new PIXI.Sprite(this.loaded.freeMode_message.texture);
        messageSprite.name = 'Message Background';
        messageSprite.anchor.set(0.5);
        messageSprite.x = -getResolutionX() / 2;
        messageContainer4.addChild(messageSprite);

        let messageTitle = ui.createText({
            fontSize: isMobile() ? 90 : 58,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, `${this.loaded.languageFile.data.unlock}`);
        messageTitle.name = 'Message Title';
        messageTitle.x = -getResolutionX() / 2;
        messageTitle.y = isMobile() ? -320 : -176;
        messageContainer4.addChild(messageTitle);

        let progressTest = new MultiStyleText(`<style1>$25 /</style1><style2> $88</style2>\n${this.loaded.languageFile.data.turnover_left}`,
            {
                'default': {
                    fontSize: isMobile() ? 28 : 16,
                    fill: '0xffffff',
                    fontWeight: 'bold',
                    align: 'center',
                },
                'style1': {
                    fontSize: isMobile() ? 46 : 36,
                    fontWeight: 'bold',
                    align: 'center',
                    fill: '0xedd06a',
                },
                'style2': {
                    fontSize: isMobile() ? 46 : 36,
                    fill: '0xffffff',
                    fontWeight: 'bold',
                    align: 'center',
                }
            });
        progressTest.anchor.set(0.5);
        progressTest.name = 'Progress Test';
        progressTest.x = -getResolutionX() / 2;
        progressTest.y = -100;
        messageContainer4.addChild(progressTest);

        let tempNum = 88;
        let message3 = new MultiStyleText(`${this.loaded.languageFile.data.complete} <style1>$${tempNum}</style1> ${this.loaded.languageFile.data.turnover_to_unlock_freespin}`,
            {
                'default': {
                    align: 'center',
                    fontSize: isMobile() ? 28 : 16,
                    fill: '0xffffff',
                    fontWeight: 'bold',
                    wordWrap: !!isMobile(),
                    wordWrapWidth: 400,
                },
                'style1': {
                    fontSize: isMobile() ? 32 : 20,
                    fontWeight: 'bold',
                    fill: '0xedd06a',
                }
            });
        message3.name = 'Message 3';
        message3.anchor.set(0.5);
        message3.x = -getResolutionX() / 2;
        message3.y = -30;
        messageContainer4.addChild(message3);

        let tempWinNum = 20;
        let message4 = new MultiStyleText(`${this.loaded.languageFile.data.your_total_freespin_earnings} <style1>$${tempWinNum}</style1>`,
            {
                'default': {
                    fontSize: isMobile() ? 28 : 16,
                    fill: '0xffffff',
                    fontWeight: 'bold',
                },
                'style1': {
                    fontSize: isMobile() ? 32 : 20,
                    fontWeight: 'bold',
                    fill: '0xedd06a',
                }
            });
        message4.name = 'Message 4';
        message4.anchor.set(0.5);
        message4.x = -getResolutionX() / 2;
        message4.y = 10;
        messageContainer4.addChild(message4);

        let okButton = ui.createButton({
            texture: this.loaded.okButton.texture,
            clickedTexture: this.loaded.okButton.texture,
            centerAnchor: true,
            onPointerDown: () => {
                this.messageContainer.visible = false;
            },
        });
        okButton.x = -getResolutionX() / 2;
        okButton.y = isMobile() ? 380 : 170;
        messageContainer4.addChild(okButton);

        let expiryDateText = this.generateExpiryDateText({
            days: 10,
            hours: 10,
            minutes: 10,
        });
        expiryDateText.name = 'Expiry Date Text';
        expiryDateText.x = -getResolutionX() / 2;
        expiryDateText.y = isMobile() ? 210 : 100;
        messageContainer4.addChild(expiryDateText);

        messageContainer4['contentUpdate'] = () => {
            if (!this.getPendingReleaseFreeSpin()) {
                return;
            }

            let currentTurnover = this.getPendingReleaseFreeSpin().current_turnover;
            let requiredTurnover = this.getPendingReleaseFreeSpin().required_turnover;
            let winAmount = state.get('currency') + utils.showAsCurrency(this.getPendingReleaseFreeSpin().win_amount);
            progressTest.text = `<style1>$${currentTurnover} /</style1><style2> $${requiredTurnover}</style2>\n${this.loaded.languageFile.data.turnover_left}`;
            message3.text = `${this.loaded.languageFile.data.complete} <style1>$${requiredTurnover}</style1> ${this.loaded.languageFile.data.turnover_to_unlock_freespin}`;
            message4.text = `${this.loaded.languageFile.data.your_total_freespin_earnings} <style1>$${winAmount}</style1>`;

            expiryDateText.visible = !(this.getPendingReleaseFreeSpin().is_game_rewarded);
        };

        return messageContainer4;
    }

    generateContainer5() {
        // Prepares all resources needed for the message display of free spins
        let messageContainer5 = new PIXI.Container();
        messageContainer5.name = 'Message Container 5';
        messageContainer5.x = getResolutionX() / 2;

        let messageSprite = new PIXI.extras.AnimatedSprite([
            this.loaded.freeMode_reward_message_0.texture,
            this.loaded.freeMode_reward_message_1.texture,
        ]);
        messageSprite.name = 'Message Background';
        messageSprite.anchor.set(0.5);
        messageSprite.x = -getResolutionX() / 2;
        messageSprite.animationSpeed = 0.025;
        messageSprite.play();
        messageContainer5.addChild(messageSprite);

        let messageTitle = ui.createText({
            fontSize: 56,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, `${this.loaded.languageFile.data.congratulation}!!!`);
        messageTitle.name = 'Message Title';
        messageTitle.x = -getResolutionX() / 2;
        messageTitle.y = isMobile() ? -290 : -160;
        messageContainer5.addChild(messageTitle);

        let message2 = ui.createText({
            fontSize: isMobile() ? 28 : 16,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.you_have_unlocked_freespin}`);
        message2.name = 'Message 2';
        message2.x = -getResolutionX() / 2;
        message2.y = -isMobile() ? -170 : -60;
        messageContainer5.addChild(message2);

        let winAmount = ui.createText({
            fontSize: 128,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, '$58');
        winAmount.name = 'Win Amount';
        winAmount.x = -getResolutionX() / 2;
        winAmount.y = -isMobile() ? -90 : 10;
        messageContainer5.addChild(winAmount);

        let message3 = ui.createText({
            fontSize: -isMobile() ? 28 : 16,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.from_your_freespin}`);
        message3.name = 'Message 3';
        message3.x = -getResolutionX() / 2;
        message3.y = isMobile() ? -5 : 85;
        messageContainer5.addChild(message3);

        let okButton = ui.createButton({
            texture: this.loaded.okButton.texture,
            clickedTexture: this.loaded.okButton.texture,
            centerAnchor: true,
            onPointerDown: () => {
                this.freeSpinContainer.visible = false;
            },
        });
        okButton.x = -getResolutionX() / 2;
        okButton.y = isMobile() ? 380 : 170;
        messageContainer5.addChild(okButton);

        messageContainer5['contentUpdate'] = () => {
            if (!this.getPendingReleaseFreeSpin()) {
                return;
            }

            let winAmountText = this.getPendingReleaseFreeSpin().win_amount;
            winAmount.text = state.get('currency') + utils.showAsCurrency(winAmountText);

            if (winAmount.width > 400) {
                let scale = (400 / winAmount.width) * winAmount.scale.x;

                winAmount.scale.set(scale);
            }
        };

        return messageContainer5;
    }

    updateMessageContainers() {
        each(this.messageContainer.children, (container) => {
            if (container['contentUpdate']) {
                container['contentUpdate']();
            }
        });
    }

    showMessageContainer() {
        each(this.messageContainer.children, (container) => {
            if (container.name !== 'Background Cover') {
                container.visible = false;
            }
        });
        if (this.getAvailableFreeSpin()) {
            this.messageContainer.getChildByName('Message Container 2').visible = true;
        } else if (this.getPendingReleaseFreeSpin()) {
            this.messageContainer.getChildByName('Message Container 4').visible = true;
        }

        this.messageContainer.visible = true;
    }

    showSpecificMessageContainerByName(childName) {
        each(this.messageContainer.children, (container) => {
            if (container.name !== 'Background Cover') {
                container.visible = false;
            }
        });

        this.messageContainer.getChildByName(childName).visible = true;
        this.messageContainer.visible = true;
    }

    updateMessageController() {
        if (this.getAvailableFreeSpin() && this.getAvailableFreeSpin().is_game_rewarded) {
            this.freeSpinContainer.visible = true;
            this.messageContainer.visible = false;

            return;
        } else if (this.getPendingReleaseFreeSpin() && this.getPendingReleaseFreeSpin().is_game_rewarded) {
            if (this.getPendingReleaseFreeSpin().win_amount > 0) {
                this.showSpecificMessageContainerByName('Message Container 5');
                this.freeSpinContainer['shouldShow'](false);
            }

            return;
        }

        if (this.getAvailableFreeSpin() && !this.getAvailableFreeSpin().is_game_rewarded) {
            this.freeSpinContainer.visible = true;
        } else if (this.getPendingReleaseFreeSpin()) {
            let currentTurnover = this.getPendingReleaseFreeSpin().current_turnover;
            let requiredTurnover = this.getPendingReleaseFreeSpin().required_turnover;

            if (currentTurnover === 0 && requiredTurnover > 0) {
                this.showSpecificMessageContainerByName('Message Container 3');
            } else if (currentTurnover >= requiredTurnover && requiredTurnover > 0) {
                this.showSpecificMessageContainerByName('Message Container 5');
            }
        }
    }

    calculateCountdown() {
        if (!this.getAvailableFreeSpin() && !this.getPendingReleaseFreeSpin()) {
            return '';
        }

        let expireUnix = this.getAvailableFreeSpin() ?
            new Date(this.getAvailableFreeSpin().expires.replace(' ', 'T')).getTime() / 1000 :
            new Date(this.getPendingReleaseFreeSpin().expires.replace(' ', 'T')).getTime() / 1000;
        let nowUnix = Date.now() / 1000;
        //Keep this line because it is a useful reference to how to get seconds
        let diffUnix = Math.floor((expireUnix - nowUnix));
        let secondsDifference = diffUnix % 60;
        diffUnix = Math.floor(diffUnix / 60);
        let minutesDifference = diffUnix % 60;
        diffUnix = Math.floor(diffUnix / 60);
        let hoursDifference = diffUnix % 24;
        diffUnix = Math.floor(diffUnix / 24); // This leftover here is now the days remaining in the diffUnix.

        return {
            'days': diffUnix,
            'hours': hoursDifference,
            'minutes': minutesDifference,
            'seconds': secondsDifference,
        };
    }
}
