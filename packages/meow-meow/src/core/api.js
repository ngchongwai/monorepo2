/**
 * @module api
 * @exports api
 */

import axios from 'axios';
import environment from 'environment';

import utils from './utils';
import ui from './ui';
import panel from './panel';
import state from './state';
import cheat from './cheat';

const token = utils.getQueryParam('token');
const isDemo = utils.getQueryParam('isDemo') === 'true';
const AUTH_HEADER = `Bearer ${token}`;

const loaded = PIXI.loader.resources;

/**
 * Send a HTTP GET Request
 * @param {string} url The endpoint to request
 * @returns {string|array|object} Data in the response.
 **/
function get(url) {
    return axios({
        method: 'get',
        url: url,
        headers: {
            Authorization: AUTH_HEADER,
        },
        withCredentials: true
    }).then((res) => {
        return res.data;
    }, errorHandle);
}

/**
 * Send a HTTP POST Request
 * @param {string} url The endpoint to request
 * @param {object} data Optional data that will pass into request's body as JSON format
 * @returns {string|array|object} Data in the response.
 **/
function post(url, data = {}) {

    if (isDemo) {
        data.isDemo = true;
    }

    const apiEndpoint = `${state.get('apiUrl')}${url}`;
    return axios({
        method: 'post',
        url: apiEndpoint,
        data: data,
        headers: {
            Authorization: AUTH_HEADER,
        },
        withCredentials: true
    }).then((res) => {
        return res.data;
    }, errorHandle);
}

/**
 * Send a HTTP POST Request with retry ability
 * @param {string} url The endpoint to request
 * @param {object} data Optional data that will pass into request's body as JSON format
 * @param {number} retried Retries counter
 * @returns {string|array|object} Data in the response.
 **/
function postWithRetry(url, data, retried = 0) {
    return post(url, data).catch((err) => {

        if (retried === 3) {
            panel.stopAutoSpin();
            ui.showDialog(state.get('errorMessage'), loaded.languageFile.data.warningTitle, loaded.languageFile.data.okay, {
                onPointerUp: () => {
                    window.location.reload();
                }
            });
            throw err;
        }

        if (err.response) {
            const statusCodeNoNeedToRecall = [400, 401, 406];
            _.each(statusCodeNoNeedToRecall, (statusCode) => {
                if (err.response.status === statusCode) {
                    throw err;
                }
            });
        }

        return new Promise((resolve) => {
            setTimeout(() => {
                return postWithRetry(url, data, retried + 1).then(resolve);
            }, 3000);
        });

    });
}

function errorHandle(err) {
    const res = err.response;

    if (!res) {
        state.set('errorMessage', loaded.languageFile.data.something_went_wrong);
    }

    const status = res.status;
    const code = res.data.code;
    let msg;

    // show HTTP status and error code
    console.error(status, code);
    panel.stopAutoSpin();

    if (status === 406 && code === 4001) {
        msg = res.data.message;

        // Prompts a 'not enough balance'
        ui.showDialog(loaded.languageFile.data.not_enough_balance, '', loaded.languageFile.data.okay, {
            onPointerUp: () => {
                window.location.reload();
            }
        });
    } else if (status === 406 && code === 4007) {

        ui.showDialog(loaded.languageFile.data.operator_error, '', loaded.languageFile.data.okay, {
            onPointerUp: () => {
                window.location.reload();
            }
        });

    } else if (status === 401) {
        const errorType = res.data.error;

        if (errorType === 'AccountSuspended') {
            msg = loaded.languageFile.data.account_suspended;
        } else if (errorType === 'DuplicatedLogin') {
            msg = loaded.languageFile.data.duplicated_login;
        } else if (errorType === 'Unauthorized') {
            msg = loaded.languageFile.data.unauthorized;
        }

    } else {

        if (loaded.languageFile && loaded.languageFile.data) {
            msg = loaded.languageFile.data.something_went_wrong;
        } else {
            msg = 'something_went_wrong';
        }
    }

    state.set('errorMessage', msg);

    throw err;
}

function authMe() {
    let data = {
        slug: environment.gameName,
    };

    if (utils.getQueryParam('isDemo')) {
        data.isDemo = true;
    }

    return post('/auth/player/me', data).then((data) => {
        return data;
    });
}

function sync() {
    let params = {
        slug: environment.gameName,
    };

    if (utils.getQueryParam('isDemo')) {
        params.isDemo = true;
    }

    return postWithRetry('/auth/player/sync', params).then((data) => {
        return data;
    });
}

function spin(data) {
    if (cheat.isCheatMode()) {
        data.cheatMode = cheat.getCheatMode();
    }

    return postWithRetry(`/play/${environment.gameName}`, data);
}

function spin100(data) {
    return postWithRetry(`/play-100/${environment.gameName}`, data);
}

function submitBonus(token, value) {
    let data = {bonusToken: token, value: value};
    return postWithRetry('/bonus', data);
}

function postChallenge(token) {
    let data = {bonusToken: token};
    return postWithRetry('/challenge', data);
}

export default {
    get,
    post,
    postWithRetry,
    authMe,
    sync,
    spin,
    spin100,
    submitBonus,
    postChallenge
};
