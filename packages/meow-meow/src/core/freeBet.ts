import * as PIXI from 'pixi.js';
import { TweenMax } from 'gsap';
import { each } from 'lodash';
// @ts-ignore
import MultiStyleText from 'pixi-multistyle-text';

import ui from './ui';
import state from './state';
import { getLanguage, getResolutionX, getResolutionY, isMobile } from './app';
import utils from './utils';

let instance;

export default class FreeBet {

    protected loaded;
    protected token: any;
    protected freeBetContainer: PIXI.Container = new PIXI.Container();
    protected messageContainer: PIXI.Container;

    constructor() {
        if (instance) {
            return instance;
        }

        instance = this;

        this.loaded = PIXI.loader.resources;

        // Prepares assets required for the floating free spins button
        this.freeBetContainer.name = 'Free Bet Container';
        this.freeBetContainer.visible = false;
        this.freeBetContainer.x = getResolutionX() / 2;
        this.freeBetContainer.y = getResolutionY() / 2;

        let baseSprite = new PIXI.Sprite(this.loaded.freeMode_base.texture);
        baseSprite.name = 'Base';
        baseSprite.anchor.set(0.5);
        baseSprite.interactive = true;
        baseSprite.buttonMode = true;
        baseSprite.x = isMobile() ? 300 : 580;
        baseSprite.y = isMobile() ? -420 : -270;
        baseSprite.on('pointerdown', () => {
            this.freeBetContainer.alpha = 0.5;
        });
        baseSprite.on('pointerup', () => {
            this.freeBetContainer.alpha = 1;
            this.showMessageContainer();
        });
        this.freeBetContainer.addChild(baseSprite);

        let baseTitle = ui.createText({
            fontSize: 14,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.freebet}`);
        baseTitle.name = 'Base Title';
        baseTitle.x = baseSprite.x;
        baseTitle.y = baseSprite.y - 50;
        this.freeBetContainer.addChild(baseTitle);

        let freeBetIcon = new PIXI.Sprite(this.loaded.freeBetIcon.texture);
        freeBetIcon.anchor.set(0.5);
        freeBetIcon.x = baseSprite.x;
        freeBetIcon.y = baseSprite.y - 2;
        this.freeBetContainer.addChild(freeBetIcon);

        let freeBetCounter = ui.createText({
            fontSize: 26,
            fontWeight: 'bold',
        }, '10');
        freeBetCounter.name = 'Free Spin Counter';
        freeBetCounter.x = baseSprite.x;
        freeBetCounter.y = baseSprite.y + 46.5;
        this.freeBetContainer.addChild(freeBetCounter);

        let fullStarSprite = new PIXI.Sprite(this.loaded.freeMode_chest.texture);
        fullStarSprite.name = 'Full Star';
        fullStarSprite.anchor.set(0.5);
        fullStarSprite.x = baseSprite.x;
        fullStarSprite.y = baseSprite.y + 14;
        this.freeBetContainer.addChild(fullStarSprite);

        let progressBarBacking = new PIXI.Graphics();
        progressBarBacking.name = 'Progress Bar Backing';
        progressBarBacking.lineStyle(9, 0x555555, 1);
        progressBarBacking.arc(baseSprite.x, baseSprite.y + 15, 35, Math.PI, 4 * Math.PI);
        this.freeBetContainer.addChild(progressBarBacking);

        let progressBar = new PIXI.Graphics();
        progressBar.name = 'Progress Bar';
        progressBar.lineStyle(9, 0xecd063, 1);
        progressBar.arc(baseSprite.x, baseSprite.y + 15, 35, 2.5 * Math.PI, 1.5 * Math.PI);
        this.freeBetContainer.addChild(progressBar);

        this.freeBetContainer['contentUpdate'] = (freeBetCounterOveride = null) => {
            if (this.getAvailableFreeBet()) {
                let freeBetCounterText = freeBetCounterOveride ? freeBetCounterOveride : this.getAvailableFreeBet().free_bet_amount - this.getAvailableFreeBet().used_free_bet_amount;
                if (freeBetCounterText <= 0) {
                    freeBetCounterText = 0;
                }

                freeBetCounter.text = utils.showAsCurrency(freeBetCounterText);

                freeBetCounter.visible = true;
                fullStarSprite.visible = false;

                progressBarBacking.visible = false;
                progressBar.visible = false;

                baseTitle.text = `${this.loaded.languageFile.data.freebet}`;
            } else if (this.getPendingReleaseFreeBet()) {
                freeBetIcon.visible = false;
                freeBetCounter.visible = false;
                fullStarSprite.visible = true;

                baseTitle.text = 'Turnover';

                progressBarBacking.visible = true;
                progressBar.visible = true;
            }

            let turnoverAmount = this.getAvailableFreeBet() ? this.getAvailableFreeBet().used_free_bet_amount - this.getAvailableFreeBet().free_bet_amount :
                this.getPendingReleaseFreeBet().current_turnover;
            let requiredTurnoverAmount = this.getAvailableFreeBet() ? this.getAvailableFreeBet().free_bet_amount : this.getPendingReleaseFreeBet().required_turnover;

            let progress = this.calculateTurnoverProgress(turnoverAmount, requiredTurnoverAmount);

            // 1.5 Offset to start from 12o'clock of circle
            // Full circle is 2PI of initial value. Need to progress * 2
            let newAngle = (1.5 + (progress * 2)) * Math.PI;

            if (freeBetCounterOveride && freeBetCounterOveride <= 0) {
                newAngle = (1.5 + 2) * Math.PI;
            }

            progressBar.clear();
            progressBar.lineStyle(9, 0xecd063, 1);
            progressBar.arc(baseSprite.x, baseSprite.y + 15, 35, 1.5 * Math.PI, newAngle);
        };

        // Filter out to only free spins with same gameName
        let availableFreeBet = this.getAvailableFreeBet();
        let pendingReleaseFreeBet = this.getPendingReleaseFreeBet();

        // this.freeBetContainer.addChild(this.messageContainer);
        this.freeBetContainer.addChild(this.generateParentContainer());

        if (pendingReleaseFreeBet && pendingReleaseFreeBet.required_turnover > 0) {
            this.refreshContainer();
            this.updateMessageContainers();
        }

        if (availableFreeBet && availableFreeBet.required_turnover > 0) {
            this.refreshContainer();
            this.updateMessageContainers();
        }
    }

    updateFreeBetContainer(number1) {
        this.freeBetContainer['contentUpdate'](number1);
    }

    getFreeBetContainer() {
        return instance.freeBetContainer;
    }

    getAvailableFreeBet() {
        return state.get('availableFreeBet');
    }

    getPendingReleaseFreeBet() {
        return state.get('pendingReleaseFreeBet');
    }

    setVisibility(visibility: boolean) {
        this.freeBetContainer.visible = visibility;
    }

    refreshContainer() {
        this.updateMessageContainers();
        this.freeBetContainer['contentUpdate']();

        this.updateMessageController();
    }

    // Generates a progress text Ex: (5 / 8 spins left)
    generateExpiryDateText(data) {
        if (!data) {
            throw new Error('There is no data passed in');
        }

        let time = this.calculateCountdown();

        const rewardExpiresText = this.loaded.languageFile.data.this_reward_expires_in;
        let daysText = this.loaded.languageFile.data.days;
        let hoursText = this.loaded.languageFile.data.hours;
        let minutesText = this.loaded.languageFile.data.minutes;

        let expiryDateText = new MultiStyleText(`${rewardExpiresText} \n<style1>${time['days']}</style1> ${daysText} <style1>${time['hours']}</style1> ${hoursText} <style1>${time['minutes']}</style1> ${minutesText}`,
            {
                'default': {
                    fontFamily: 'Arial',
                    fontSize: isMobile() ? 24 : 16,
                    fontWeight: 'bold',
                    fill: 0xffffff,
                    align: 'center'
                },
                'style1': {
                    fontFamily: 'Arial',
                    fontSize: isMobile() ? 32 : 24,
                    fill: '0xedd06a',
                    fontWeight: "bold",
                }
            });
        expiryDateText.anchor.set(0.5);

        setInterval(() => {
            let newTime = this.calculateCountdown();
            expiryDateText.text = `${rewardExpiresText} \n<style1>${newTime['days']}</style1> ${daysText} <style1>${newTime['hours']}</style1> ${hoursText} <style1>${newTime['minutes']}</style1> ${minutesText}`;
        }, 2000);

        return expiryDateText;
    }

    // Returns turnover progress over 1
    calculateTurnoverProgress(currentTurnover, requiredTurnover) {
        return currentTurnover / requiredTurnover;
    }

    checkToAnnounce() {
        let availableFreeBet = this.getAvailableFreeBet();

        if (availableFreeBet &&
            availableFreeBet.used_free_bet_amount === 0 &&   // New if used is 0
            availableFreeBet.current_turnover === 0 &&  // New if current turnover is 0
            availableFreeBet.required_turnover > 0 // Awarded if required turnover is more than 0.
        ) {
            TweenMax.to(this.messageContainer.scale, 0.25, {x: 1, y: 1});
            this.showSpecificMessageContainerByName('Message Container 1');
        }
    }

    generateParentContainer() {
        this.messageContainer = new PIXI.Container();
        this.messageContainer.name = 'Message Container';
        this.messageContainer.scale.set(1);
        this.messageContainer.visible = false;

        let background = new PIXI.Graphics().beginFill(0x000000).drawRect(0, 0, getResolutionX(), getResolutionY()).endFill();
        background.name = 'Background Cover';
        background.alpha = 0.8;
        background.interactive = true;
        background.x = -getResolutionX() / 2;
        background.y = -getResolutionY() / 2;
        this.messageContainer.addChild(background);

        this.messageContainer.addChild(this.generateContainer1());
        this.messageContainer.addChild(this.generateContainer2());
        this.messageContainer.addChild(this.generateContainer3());
        this.messageContainer.addChild(this.generateContainer4());
        this.messageContainer.addChild(this.generateContainer5());

        return this.messageContainer;
    }

    generateContainer1() {
        // Prepares all resources needed for the message display of free bets
        let messageContainer1 = new PIXI.Container();
        messageContainer1.name = 'Message Container 1';
        messageContainer1.x = getResolutionX() / 2;

        let messageSprite = new PIXI.Sprite(this.loaded.freeMode_message.texture);
        messageSprite.name = 'Message Background';
        messageSprite.anchor.set(0.5);
        messageSprite.x = -getResolutionX() / 2;
        messageContainer1.addChild(messageSprite);

        let messageTitle = ui.createText({
            align: "center",
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
            fontWeight: "bold",
            fontSize: isMobile() ? 90 : 60
        }, `${this.loaded.languageFile.data.rewards}`);
        messageTitle.name = 'Message Title';
        messageTitle.x = -getResolutionX() / 2;
        messageTitle.y = isMobile() ? -275 : -175;
        messageContainer1.addChild(messageTitle);

        let message2 = ui.createText({
            fontSize: isMobile() ? 20 : 16,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.you_have_received}`);
        message2.name = 'Message 2';
        message2.x = -getResolutionX() / 2;
        message2.y = isMobile() ? -180 : -115;
        messageContainer1.addChild(message2);

        let amountOfFreeBet = ui.createText({
            fontSize: isMobile() ? 162 : 112,
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
            fontWeight: "bold",
        }, '8');
        amountOfFreeBet.name = 'Win Amount';
        amountOfFreeBet.x = -getResolutionX() / 2;
        amountOfFreeBet.y = -55;
        messageContainer1.addChild(amountOfFreeBet);

        let message3 = ui.createText({
            fontSize: 28,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.freebet}!`);
        message3.name = 'Message 3';
        message3.x = -getResolutionX() / 2;
        message3.y = isMobile() ? 50 : 10;
        messageContainer1.addChild(message3);

        let popupContainer = this.generatePopupContainer();
        popupContainer.x = -getResolutionX() / 2;

        let message4 = ui.createText({
            fontSize: isMobile() ? 24 : 16,
            fontWeight: 'bold',
            fill: 0xffffff,
        }, `${this.loaded.languageFile.data.available_for_multiplier_games}!`/*'It is applicable for multiple games!'*/);
        message4.name = 'Message 4';
        message4.anchor.set(0.50);
        message4.interactive = true;
        message4.buttonMode = true;
        message4.hitArea = new PIXI.Rectangle(10, -6, 119, 16);
        message4.x = -getResolutionX() / 2;
        message4.y = isMobile() ? 75 : 45;
        message4.on('pointertap', () => {
            popupContainer.visible = true;
        });
        messageContainer1.addChild(message4);

        let underlineWidth = (getLanguage() === 'en') ? 119 : 70;
        let underlineXOffset = (getLanguage() === 'en') ? 10 : 5;
        let messageUnderline = new PIXI.Graphics().beginFill(0xffffff).drawRect(0, 0, underlineWidth, 2).endFill();
        messageUnderline.name = 'Message Underline';
        messageUnderline.x = (-getResolutionX() / 2) + underlineXOffset;
        messageUnderline.y = message4.y + 9;
        messageContainer1.addChild(messageUnderline);

        let okButton = ui.createButton({
            texture: this.loaded.okButton.texture,
            clickedTexture: this.loaded.okButton.texture,
            centerAnchor: true,
            onPointerDown: () => {
                this.messageContainer.visible = false;
            },
        });
        okButton.x = -getResolutionX() / 2;
        okButton.y = isMobile() ? 380 : 170;
        messageContainer1.addChild(okButton);

        let expiryDateText = this.generateExpiryDateText({
            days: 10,
            hours: 10,
            minutes: 10,
        });
        expiryDateText.name = 'Message 123123';
        expiryDateText.x = -getResolutionX() / 2;
        expiryDateText.y = isMobile() ? 210 : 100;
        messageContainer1.addChild(expiryDateText);

        // Finally we must add the pop up container because it needs to be infront of all the other text
        messageContainer1.addChild(popupContainer);

        messageContainer1['contentUpdate'] = () => {
            if (!this.getAvailableFreeBet()) {
                return;
            }

            let number = this.getAvailableFreeBet() ? this.getAvailableFreeBet().free_bet_amount : this.getPendingReleaseFreeBet().free_bet_amount;
            amountOfFreeBet.text = `${number}`;
        };

        return messageContainer1;
    }

    generateContainer2() {
        // Prepares all resources needed for the message display of free spins
        let messageContainer2 = new PIXI.Container();
        messageContainer2.name = 'Message Container 2';
        messageContainer2.x = getResolutionX() / 2;

        let messageSprite = new PIXI.Sprite(this.loaded.freeMode_message.texture);
        messageSprite.name = 'Message Background';
        messageSprite.anchor.set(0.5);
        messageSprite.x = -getResolutionX() / 2;
        messageSprite.x = -getResolutionX() / 2;
        messageContainer2.addChild(messageSprite);

        let messageTitle = ui.createText({
            fontSize: isMobile() ? 92 : 48,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, `${this.loaded.languageFile.data.free_bet}!`);
        messageTitle.name = 'Message Title';
        messageTitle.x = -getResolutionX() / 2;
        messageTitle.y = isMobile() ? -300 : -175;
        messageContainer2.addChild(messageTitle);

        let progressText = new MultiStyleText(`<style1>0</style1> / 0 ${this.loaded.languageFile.data.left}`,
            {
                'default': {
                    fontFamily: 'Arial',
                    fontSize: isMobile() ? 24 : 16,
                    fontWeight: 'bold',
                    fill: 0xffffff,
                },
                'style1': {
                    fontFamily: 'Arial',
                    fontWeight: 'bold',
                    fill: '0xedd06a',
                }
            });
        progressText.anchor.set(0.5);
        progressText.name = 'Progress Test';
        progressText.x = -getResolutionX() / 2;
        progressText.y = isMobile() ? -210 : -125;
        messageContainer2.addChild(progressText);

        let message2 = ui.createText({
            fontSize: isMobile() ? 24 : 16,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.current_freebet_earnings}`);
        message2.name = 'Message 2';
        message2.x = -getResolutionX() / 2;
        message2.y = isMobile() ? -90 : -75;
        messageContainer2.addChild(message2);

        let winAmount = ui.createText({
            fontSize: 108,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, '$38');
        winAmount.name = 'Win Amount';
        winAmount.x = -getResolutionX() / 2;
        winAmount.y = -10;
        messageContainer2.addChild(winAmount);

        let okButton = ui.createButton({
            texture: this.loaded.okButton.texture,
            clickedTexture: this.loaded.okButton.texture,
            centerAnchor: true,
            onPointerDown: () => {
                this.messageContainer.visible = false;
            },
        });
        okButton.x = -getResolutionX() / 2;
        okButton.y = isMobile() ? 380 : 170;
        messageContainer2.addChild(okButton);

        let expiryDateText = this.generateExpiryDateText({
            days: 10,
            hours: 10,
            minutes: 10,
        });
        expiryDateText.name = 'Message 123123';
        expiryDateText.x = -getResolutionX() / 2;
        expiryDateText.y = isMobile() ? 210 : 100;
        messageContainer2.addChild(expiryDateText);

        messageContainer2['contentUpdate'] = () => {
            if (!this.getAvailableFreeBet()) {
                return;
            }

            let freeBetLeft = this.getAvailableFreeBet().free_bet_amount - this.getAvailableFreeBet().used_free_bet_amount;
            let freeBetTotal = this.getAvailableFreeBet().free_bet_amount;
            progressText.text = `<style1>${freeBetLeft}</style1> / ${freeBetTotal} ${this.loaded.languageFile.data.left}`;

            let winAmountText = this.getAvailableFreeBet() ? this.getAvailableFreeBet().win_amount : this.getPendingReleaseFreeBet().win_amount;
            winAmount.text = `${state.get('currency')}${winAmountText}`;
        };

        return messageContainer2;
    }

    generateContainer3() {
        // Prepares all resources needed for the message display of free spins
        let messageContainer3 = new PIXI.Container();
        messageContainer3.name = 'Message Container 3';
        messageContainer3.x = getResolutionX() / 2;

        let messageSprite = new PIXI.Sprite(this.loaded.freeMode_message.texture);
        messageSprite.name = 'Message Background';
        messageSprite.anchor.set(0.5);
        messageSprite.x = -getResolutionX() / 2;
        messageContainer3.addChild(messageSprite);

        let messageTitle = ui.createText({
            fontSize: isMobile() ? 90 : 58,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, `${this.loaded.languageFile.data.unlock}`);
        messageTitle.name = 'Message Title';
        messageTitle.x = -getResolutionX() / 2;
        messageTitle.y = isMobile() ? -290 : -176;
        messageContainer3.addChild(messageTitle);

        let message2 = ui.createText({
            fontSize: isMobile() ? 24 : 16,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.total_freebet_earnings}`);
        message2.name = 'Message 2';
        message2.x = -getResolutionX() / 2;
        message2.y = isMobile() ? -200 : -110;
        messageContainer3.addChild(message2);

        let winAmount = ui.createText({
            fontSize: 108,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, '$58');
        winAmount.name = 'Win Amount';
        winAmount.x = -getResolutionX() / 2;
        winAmount.y = isMobile() ? -110 : -35;
        messageContainer3.addChild(winAmount);

        let tempNum = 88;
        let message3 = new MultiStyleText(`${this.loaded.languageFile.data.complete} <style1>$${tempNum}</style1> ${this.loaded.languageFile.data.turnover_to_unlock_freebet}`,
            {
                'default': {
                    align: 'center',
                    fontSize: isMobile() ? 24 : 16,
                    fill: '0xffffff',
                    fontWeight: 'bold',
                    wordWrap: !!isMobile(),
                    wordWrapWidth: 400,
                },
                'style1': {
                    fontSize: isMobile() ? 28 : 20,
                    fontWeight: 'bold',
                    fill: '0xedd06a',
                }
            });
        message3.name = 'Message 3';
        message3.anchor.set(0.5);
        message3.x = -getResolutionX() / 2;
        message3.y = isMobile() ? -5 : 40;
        messageContainer3.addChild(message3);

        let okButton = ui.createButton({
            texture: this.loaded.okButton.texture,
            clickedTexture: this.loaded.okButton.texture,
            centerAnchor: true,
            onPointerDown: () => {
                this.messageContainer.visible = false;
            },
        });
        okButton.x = -getResolutionX() / 2;
        okButton.y = isMobile() ? 380 : 170;
        messageContainer3.addChild(okButton);

        let expiryDateText = this.generateExpiryDateText({
            days: 10,
            hours: 10,
            minutes: 10,
        });
        expiryDateText.name = 'Message 123123';
        expiryDateText.x = -getResolutionX() / 2;
        expiryDateText.y = isMobile() ? 210 : 100;
        messageContainer3.addChild(expiryDateText);

        messageContainer3['contentUpdate'] = () => {
            if (!this.getPendingReleaseFreeBet()) {
                return;
            }

            let winAmountText = this.getPendingReleaseFreeBet().win_amount;
            winAmount.text = `$${winAmountText}`;

            let requiredTurnover = this.getPendingReleaseFreeBet().required_turnover;
            message3.text = `${this.loaded.languageFile.data.complete} <style1>$${requiredTurnover}</style1> ${this.loaded.languageFile.data.turnover_to_unlock_freebet}`;
        };

        return messageContainer3;
    }

    generateContainer4() {
        // Prepares all resources needed for the message display of free spins
        let messageContainer4 = new PIXI.Container();
        messageContainer4.name = 'Message Container 4';
        messageContainer4.x = getResolutionX() / 2;

        let messageSprite = new PIXI.Sprite(this.loaded.freeMode_message.texture);
        messageSprite.name = 'Message Background';
        messageSprite.anchor.set(0.5);
        messageSprite.x = -getResolutionX() / 2;
        messageContainer4.addChild(messageSprite);

        let messageTitle = ui.createText({
            fontSize: isMobile() ? 90 : 58,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, `${this.loaded.languageFile.data.unlock}`);
        messageTitle.name = 'Message Title';
        messageTitle.x = -getResolutionX() / 2;
        messageTitle.y = isMobile() ? -320 : -176;
        messageContainer4.addChild(messageTitle);

        let progressTest = new MultiStyleText(`<style1>$25 /</style1><style2> $88</style2>\n ${this.loaded.languageFile.data.turnover_left}`,
            {
                'default': {
                    fontSize: isMobile() ? 24 : 16,
                    fill: '0xffffff',
                    fontWeight: 'bold',
                    align: 'center',
                },
                'style1': {
                    fontSize: isMobile() ? 46 : 36,
                    fontWeight: 'bold',
                    align: 'center',
                    fill: '0xedd06a',
                },
                'style2': {
                    fontSize: isMobile() ? 46 : 36,
                    fill: '0xffffff',
                    fontWeight: 'bold',
                    align: 'center',
                }
            });
        progressTest.anchor.set(0.5);
        progressTest.name = 'Progress Test';
        progressTest.x = -getResolutionX() / 2;
        progressTest.y = -85;
        messageContainer4.addChild(progressTest);

        let tempNum = 88;
        let message3 = new MultiStyleText(`${this.loaded.languageFile.data.complete} <style1>$${tempNum}</style1> ${this.loaded.languageFile.data.turnover_to_unlock_freebet}`,
            {
                'default': {
                    align: 'center',
                    fontSize: isMobile() ? 24 : 16,
                    fill: '0xffffff',
                    fontWeight: 'bold',
                    wordWrap: !!isMobile(),
                    wordWrapWidth: 400,
                },
                'style1': {
                    fontSize: isMobile() ? 28 : 20,
                    fontWeight: 'bold',
                    fill: '0xedd06a',
                }
            });
        message3.name = 'Message 3';
        message3.anchor.set(0.5);
        message3.x = -getResolutionX() / 2;
        message3.y = -25;
        messageContainer4.addChild(message3);

        let tempWinNum = 20;
        let message4 = new MultiStyleText(`${this.loaded.languageFile.data.your_total_freebet_earnings} <style1>$${tempWinNum}</style1>`,
            {
                'default': {
                    fontSize: isMobile() ? 24 : 16,
                    fill: '0xffffff',
                    fontWeight: 'bold',
                },
                'style1': {
                    fontSize: isMobile() ? 28 : 20,
                    fontWeight: 'bold',
                    fill: '0xedd06a',
                }
            });
        message4.name = 'Message 4';
        message4.anchor.set(0.5);
        message4.x = -getResolutionX() / 2;
        message4.y = 10;
        messageContainer4.addChild(message4);

        let okButton = ui.createButton({
            texture: this.loaded.okButton.texture,
            clickedTexture: this.loaded.okButton.texture,
            centerAnchor: true,
            onPointerDown: () => {
                this.messageContainer.visible = false;
            },
        });
        okButton.x = -getResolutionX() / 2;
        okButton.y = isMobile() ? 380 : 170;
        messageContainer4.addChild(okButton);

        let expiryDateText = this.generateExpiryDateText({
            days: 10,
            hours: 10,
            minutes: 10,
        });
        expiryDateText.name = 'Message 123123';
        expiryDateText.x = -getResolutionX() / 2;
        expiryDateText.y = isMobile() ? 210 : 100;
        messageContainer4.addChild(expiryDateText);

        messageContainer4['contentUpdate'] = () => {
            if (!this.getPendingReleaseFreeBet()) {
                return;
            }

            let currentTurnover = this.getPendingReleaseFreeBet().current_turnover;
            let requiredTurnover = this.getPendingReleaseFreeBet().required_turnover;
            let winAmount = this.getPendingReleaseFreeBet().win_amount;
            progressTest.text = `<style1>$${currentTurnover} /</style1><style2> $${requiredTurnover}</style2>\n ${this.loaded.languageFile.data.turnover_left}`;
            message3.text = `${this.loaded.languageFile.data.complete} <style1>$${requiredTurnover}</style1> ${this.loaded.languageFile.data.turnover_to_unlock_freebet}`;
            message4.text = `${this.loaded.languageFile.data.your_total_freebet_earnings} <style1>$${winAmount}</style1>`;
        };

        return messageContainer4;
    }

    generateContainer5() {
        // Prepares all resources needed for the message display of free spins
        let messageContainer5 = new PIXI.Container();
        messageContainer5.name = 'Message Container 5';
        messageContainer5.x = getResolutionX() / 2;

        let messageSprite = new PIXI.extras.AnimatedSprite([
            this.loaded.freeMode_reward_message_0.texture,
            this.loaded.freeMode_reward_message_1.texture,
        ]);
        messageSprite.name = 'Message Background';
        messageSprite.anchor.set(0.5);
        messageSprite.x = -getResolutionX() / 2;
        messageSprite.animationSpeed = 0.025;
        messageSprite.play();
        messageContainer5.addChild(messageSprite);

        let messageTitle = ui.createText({
            fontSize: 56,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, `${this.loaded.languageFile.data.congratulation}!!!`);
        messageTitle.name = 'Message Title';
        messageTitle.x = -getResolutionX() / 2;
        messageTitle.y = isMobile() ? -290 : -176;
        messageContainer5.addChild(messageTitle);

        let message2 = ui.createText({
            fontSize: isMobile() ? 24 : 16,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.you_have_unlocked_freebet}`);
        message2.name = 'Message 2';
        message2.x = -getResolutionX() / 2;
        message2.y = -isMobile() ? -170 : -70;
        messageContainer5.addChild(message2);

        let winAmount = ui.createText({
            fontSize: 128,
            fontWeight: 'bold',
            fill: [
                "#f4f7dd",
                "#ecc853"
            ],
            fillGradientStops: [
                0.2
            ],
        }, '$58');
        winAmount.name = 'Win Amount';
        winAmount.x = -getResolutionX() / 2;
        winAmount.y = -isMobile() ? -90 : 0;
        messageContainer5.addChild(winAmount);

        let message3 = ui.createText({
            fontSize: -isMobile() ? 24 : 16,
            fontWeight: 'bold',
        }, `${this.loaded.languageFile.data.from_your_freebet}`);
        message3.name = 'Message 3';
        message3.x = -getResolutionX() / 2;
        message3.y = isMobile() ? -5 : 85;
        messageContainer5.addChild(message3);

        let okButton = ui.createButton({
            texture: this.loaded.okButton.texture,
            clickedTexture: this.loaded.okButton.texture,
            centerAnchor: true,
            onPointerDown: () => {
                this.freeBetContainer.visible = false;
            },
        });
        okButton.x = -getResolutionX() / 2;
        okButton.y = isMobile() ? 380 : 170;
        messageContainer5.addChild(okButton);

        messageContainer5['contentUpdate'] = () => {
            if (!this.getPendingReleaseFreeBet()) {
                return;
            }

            let winAmountText = this.getPendingReleaseFreeBet().win_amount;
            winAmount.text = `$${winAmountText}`;
        };

        return messageContainer5;
    }

    generatePopupContainer() {
        let popupContainer = new PIXI.Container();
        popupContainer.name = 'Popup Container';
        popupContainer.visible = false;
        popupContainer.interactive = true;

        let backgroundBox = new PIXI.Graphics().beginFill(0x000000).drawRect(-getResolutionX() / 2, -getResolutionY() / 2, getResolutionX(), getResolutionY()).endFill();
        backgroundBox.name = 'Background Box';
        backgroundBox.interactive = true;
        backgroundBox.alpha = 0.75;
        popupContainer.addChild(backgroundBox);

        let background = new PIXI.Sprite(this.loaded.freeModePopup.texture);
        background.name = 'Background Sprite';
        background.anchor.set(0.5);
        popupContainer.addChild(background);

        let closePopupButton = ui.createButton({
            texture: this.loaded.exitButton.texture,
            clickedTexture: this.loaded.exitButton.texture,
            centerAnchor: true,
            onPointerDown: () => {
                popupContainer.visible = false;
            },
        });
        closePopupButton.name = 'Close Popup Button';
        closePopupButton.scale.set(0.65);
        closePopupButton.anchor.set(0.5);
        closePopupButton.x = background.width / 2;
        closePopupButton.y = -background.height / 2;
        popupContainer.addChild(closePopupButton);

        let popupTitle = ui.createText({
            fontSize: 16,
            fontWeight: 'bold',
        }, 'Applicable Game (s)');
        popupTitle.y = -117.5;
        popupContainer.addChild(popupTitle);

        let scrollableContainer = new PIXI.Container();
        scrollableContainer.name = 'Scrollable Container';
        scrollableContainer.y = -(background.height / 4);
        popupContainer.addChild(scrollableContainer);

        if (!isMobile()) {
            let scrollUpButton = ui.createButton({
                texture: this.loaded.increaseButton.texture,
                textureHover: this.loaded.increaseHoverButton.texture,
                clickedTexture: this.loaded.increaseDownButton.texture,
                centerAnchor: true,
                onPointerDown: () => {
                    scrollUpButton['scrollInterval'] = setInterval(() => {
                        if (scrollableContainer.getBounds().top <= 280) {
                            scrollableContainer.y += 4;
                        }
                    }, 25);
                },
                onPointerUp: () => {
                    scrollUpButton['clearScrollInterval']();
                },
                onPointerOut: () => {
                    scrollUpButton['clearScrollInterval']();
                },
                onPointerUpOutside: () => {
                    scrollUpButton['clearScrollInterval']();
                },
            });
            scrollUpButton.name = 'Scroll Up Container';
            scrollUpButton.scale.set(0.85);
            scrollUpButton.anchor.set(0.5);
            scrollUpButton.x = 135;
            scrollUpButton.y = 0;
            scrollUpButton['clearScrollInterval'] = () => {
                if (scrollUpButton['scrollInterval']) {
                    clearInterval(scrollUpButton['scrollInterval']);
                }
            };
            popupContainer.addChild(scrollUpButton);

            let scrollDownButton = ui.createButton({
                texture: this.loaded.increaseButton.texture,
                textureHover: this.loaded.increaseHoverButton.texture,
                clickedTexture: this.loaded.increaseDownButton.texture,
                centerAnchor: true,
                onPointerDown: () => {
                    scrollDownButton['scrollInterval'] = setInterval(() => {
                        if (scrollableContainer.getBounds().bottom >= 495) {
                            scrollableContainer.y -= 4;
                        }
                    }, 25);
                },
                onPointerUp: () => {
                    scrollDownButton['clearScrollInterval']();
                },
                onPointerOut: () => {
                    scrollDownButton['clearScrollInterval']();
                },
                onPointerUpOutside: () => {
                    scrollDownButton['clearScrollInterval']();
                },
            });
            scrollDownButton.name = 'Scroll Down Container';
            scrollDownButton.scale.set(-0.85);
            scrollDownButton.anchor.set(0.5);
            scrollDownButton.x = 135;
            scrollDownButton.y = 50;
            scrollDownButton['clearScrollInterval'] = () => {
                if (scrollDownButton['scrollInterval']) {
                    clearInterval(scrollDownButton['scrollInterval']);
                }
            };
            popupContainer.addChild(scrollDownButton);
        }

        let maskHeightOffset = 55;
        let textBlockMask = new PIXI.Graphics().beginFill(0xffffff).drawRect(-background.width / 2, -background.height / 2 + maskHeightOffset, background.width, background.height - maskHeightOffset).endFill();
        popupContainer.addChild(textBlockMask);

        let gameNames = !!this.getAvailableFreeBet() ? this.getAvailableFreeBet().game_names.split(', ') : [];
        let gameNamesAsString = '';
        each(gameNames, (text, index) => {
            if (!!index) {
                gameNamesAsString += '\n' + text;
            } else {
                gameNamesAsString += text;
            }
        });

        let textBlock = ui.createText({
            fontSize: 16,
            fontWeight: 'bold',
            lineHeight: 30,
            align: 'left'
        }, gameNamesAsString);
        textBlock.anchor.set(1, 0);
        textBlock.mask = textBlockMask;
        scrollableContainer.addChild(textBlock);

        return popupContainer;
    }

    updateMessageContainers() {
        each(this.messageContainer.children, (container) => {
            if (container['contentUpdate']) {
                container['contentUpdate']();
            }
        });
    }

    showMessageContainer() {
        each(this.messageContainer.children, (container) => {
            if (container.name !== 'Background Cover') {
                container.visible = false;
            }
        });
        if (this.getAvailableFreeBet()) {
            this.messageContainer.getChildByName('Message Container 2').visible = true;
        } else if (this.getPendingReleaseFreeBet()) {
            this.messageContainer.getChildByName('Message Container 4').visible = true;
        }

        this.messageContainer.visible = true;
    }

    showSpecificMessageContainerByName(childName) {
        each(this.messageContainer.children, (container) => {
            if (container.name !== 'Background Cover') {
                container.visible = false;
            }
        });

        this.messageContainer.getChildByName(childName).visible = true;
        this.messageContainer.visible = true;
    }

    updateMessageController() {
        if (this.getAvailableFreeBet()) {
            this.freeBetContainer.visible = true;
        } else if (this.getPendingReleaseFreeBet()) {
            let currentTurnover = this.getPendingReleaseFreeBet().current_turnover;
            let requiredTurnover = this.getPendingReleaseFreeBet().required_turnover;

            if (currentTurnover === 0 && requiredTurnover > 0) {
                this.showSpecificMessageContainerByName('Message Container 3')
            } else if (currentTurnover >= requiredTurnover && requiredTurnover > 0) {
                this.showSpecificMessageContainerByName('Message Container 5');
            }
        }
    }

    calculateCountdown() {
        if (!this.getAvailableFreeBet() && !this.getPendingReleaseFreeBet()) {
            return '';
        }

        let expireUnix = this.getAvailableFreeBet() ?
            new Date(this.getAvailableFreeBet().expires.replace(' ', 'T')).getTime() / 1000 :
            new Date(this.getPendingReleaseFreeBet().expires.replace(' ', 'T')).getTime() / 1000;
        let nowUnix = Date.now() / 1000;
        //Keep this line because it is a useful reference to how to get seconds
        let diffUnix = Math.floor((expireUnix - nowUnix));
        let secondsDifference = diffUnix % 60;
        diffUnix = Math.floor(diffUnix / 60);
        let minutesDifference = diffUnix % 60;
        diffUnix = Math.floor(diffUnix / 60);
        let hoursDifference = diffUnix % 24;
        diffUnix = Math.floor(diffUnix / 24); // This leftover here is now the days remaining in the diffUnix.

        return {
            'days': diffUnix,
            'hours': hoursDifference,
            'minutes': minutesDifference,
            'seconds': secondsDifference,
        };
    }
}
