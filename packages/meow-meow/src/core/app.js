import * as PIXI from 'pixi.js';
import * as PIXISound from 'pixi-sound';
import * as Bowser from 'bowser';
import axios from 'axios';
import { each, map } from 'lodash';
import 'gsap';
import io from 'socket.io-client';
import environment from 'environment';

import api from './api.js';
import loading from './loading';
import utils from './utils';
import state from './state';
import panel from './panel';
import betHistory from './betHistory';
import ui from './ui';
import domUtils from './domUtils';
import sync from './sync';
import browserSupport from './browserSupport';
import singleInstance from './singleInstance';
import session from './session';
import preloader from './preloader';

domUtils.addStyleString(`
html {
    height: 100%;
}

body {
    margin: 0;
    height: 100%;
    width: 100%;
    display: flex;
    overflow: hidden;
    justify-content: center;
    align-items: center;
    position: fixed;
    background: black;
}
`);

preloader.init();

//checking if others tab opened same game
singleInstance.singleInstanceChecking();

domUtils.addScriptString('', `https://www.googletagmanager.com/gtag/js?id=${environment.googleAnalyticCode}`, true);
domUtils.addScriptString(`
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', '${environment.googleAnalyticCode}');
`);

// Does not use Request Animation Frame for tween update
TweenMax.lagSmoothing(0);
TweenMax.ticker.useRAF(false);

let resolutionX = 0;
let resolutionY = 0;

// A variable to decide whether can init the game
let canInit = true;

/**
 * Load query params
 **/
const isDemo = utils.getQueryParam('isDemo') === 'true';
let lang = (utils.getQueryParam('lang') === 'en' || utils.getQueryParam('lang') === 'cn') ? utils.getQueryParam('lang') : 'en';

let loader = PIXI.loader;
let loaded = loader.resources;

// compile high resolution on demo environment
// PIXI.settings.RESOLUTION = environment.environmentName === 'demo' ? 2 : 1;
// todo delete resolution =2
PIXI.settings.RESOLUTION = isMobile() ? 1 : 1.5;

// set url
const apiUrl = `${utils.getQueryParam('apiUrl')}/api`;
const socketUrl = `${utils.getQueryParam('apiUrl')}/socket`;
state.set('apiUrl', apiUrl);
state.set('socketURL', socketUrl);

// Auth Me
if (!utils.getQueryParam('apiUrl')) {
    preloader.updateText('Oops. Something went wrong');
    console.error('no apiUrl');
}
if (!utils.getQueryParam('token')) {
    preloader.updateText('Oops. Something went wrong');
    console.error('no token');
}

if (utils.getQueryParam('token') && utils.getQueryParam('apiUrl')){
    api.authMe().then((data) => {
        data.balance = data.balance ? data.balance : 0;

        state.set('balance', data.balance);

        state.set('token', data.token);

        state.set('bonusLevel', data.bonusLevel);

        state.set('currency', data.currency_symbol ? data.currency_symbol : '$');

        // Hack way to filter out free spins and free bet
        if (data.available_free_spin) {
            state.set('availableFreeSpin', data.available_free_spin);
        }

        // We need to check free bet. We don't need to check free bet if there already is free spin
        if (data.available_free_bet && !state.get('availableFreeSpin')) {
            state.set('availableFreeBet', data.available_free_bet);
        }

        if (data.pending_release_free_spin) {
            state.set('pendingReleaseFreeSpin', data.pending_release_free_spin);
        } else if (data.pending_release_free_bet) {
            state.set('pendingReleaseFreeBet', data.pending_release_free_bet);
        }

        try {
            panel.updateText();
        } catch (e) {
            console.log('panel not init');
        }

        // check is data contain bet amount list
        if (data.bet_amount_list) {
            let isAllNum = true;
            let tempArray = map(data.bet_amount_list, (betAmount) => {
                let temp;

                if (parseInt(betAmount) == betAmount) {
                    temp = parseInt(betAmount);
                }
                else {
                    temp = parseFloat(betAmount).toFixed(2);
                }

                // make sure is numeric
                if (!temp) {
                    console.log('bet amount list need to be numeric (default bet amount list will be use)');
                    isAllNum = false;
                }

                return temp;
            });

            if (isAllNum) {
                state.set('betAmountList', tempArray);
            }
        }

        state.set('playerDetail', data);

        // init bet history container when everything is loaded
        const betHistoryParams = data.operator.bet_history_params;
        betHistory.init(betHistoryParams);

        // update page title
        const title = lang === 'cn' ? data.game_name_ch : data.game_name;
        domUtils.updatePageTitle(title);

        // start synchronization
        sync.start();

        // start session
        session.init();

        state.set('slotData', data.slots);

        state.set('initData', data);

        // start the game loader when authMe is success
        preloader.close();
        document.body.appendChild(app.view);
        loader.load();

        // connect to socket server to start the session
        const token = utils.getQueryParam('token');
        io(state.get('socketURL'), {
            query: {
                token: token,
                slug: environment.gameName,
            }
        });

    }).catch((err) => {
        preloader.updateText('Oops. Something went wrong');
        console.error(err);
    });
}

/**
 * Load core assets
 **/
loader
    .add('languageFile', `./assets/${lang}.json`) // load language file first

    //! Button Effects
    .add('buttonEffect_json', 'assets/core/button_effect.json')

    //! spin100 Effects
    .add('spin100Effect_json', './assets/core/button_100spins_effect.json')

    //! Win Glow
    .add('winGlowback_json', './assets/core/win_glow_back.json')

    //particle
    .add('particle', './assets/core/sprite_bet_line.png')

    // win mega and win bug effect
    .add('winBig', `./assets/core/language/${lang}/Big.json`)
    .add('winMega', `./assets/core/language/${lang}/Mega.json`)

    //! Sound core
    .add('bgMusic', './assets/sounds/music_bg.mp3')
    .add('bgSound', './assets/sounds/sound_bg.mp3')
    .add('bgBonusSound', './assets/sounds/sound_bg_bonus.mp3')
    .add('bgBonusMusic', './assets/sounds/music_bg_bonus.mp3')
    .add('buttonClickSound', './assets/sounds/sound_button_click.mp3')
    .add('buttonClickReleaseSound', './assets/sounds/sound_button_click_release.mp3')
    .add('smallWinSound', './assets/sounds/sound_win_small.mp3')
    .add('bigWinSound', './assets/sounds/sound_win_big.mp3')
    .add('slotSpinningSound', './assets/sounds/sound_slot_spinning.mp3')
    .add('slotSpinningStopSound', './assets/sounds/sound_slot_spinning_stop.mp3')
    .add('bonusTriggerSound', './assets/sounds/sound_win_bonus.mp3')
    .add('moneySound', './assets/sounds/sound_win_money.mp3')
    .add('bonusClickItemSound', './assets/sounds/sound_bonus_click_item.mp3')
    .add('bonusFinishSound', './assets/sounds/sound_bonus_finish.mp3')
    .add('spin100Sound', './assets/sounds/sound_100_spins.mp3')
    .add('coinSound', './assets/sounds/sound_coins.mp3')
    .add('coinFlipSound', './assets/sounds/sound_coins_flip.mp3')
    .add('coinFlipSoundEnd', './assets/sounds/sound_coins_flip_end.mp3')

    // numbers
    .add('tex_num_zero', './assets/core/sprite_number_0.png')
    .add('tex_num_one', './assets/core/sprite_number_1.png')
    .add('tex_num_two', './assets/core/sprite_number_2.png')
    .add('tex_num_three', './assets/core/sprite_number_3.png')
    .add('tex_num_four', './assets/core/sprite_number_4.png')
    .add('tex_num_five', './assets/core/sprite_number_5.png')
    .add('tex_num_six', './assets/core/sprite_number_6.png')
    .add('tex_num_seven', './assets/core/sprite_number_7.png')
    .add('tex_num_eight', './assets/core/sprite_number_8.png')
    .add('tex_num_nine', './assets/core/sprite_number_9.png')
    .add('tex_num_dollar_sign', './assets/core/sprite_number_dollar_sign.png')
    .add('tex_num_dot', './assets/core/sprite_number_dot.png')
    .add('tex_num_comma', './assets/core/sprite_number_comma.png')

    // numbers backing
    .add('numberBackStart', './assets/core/sprite_number_back_1.png')
    .add('numberBackBody', './assets/core/sprite_number_back_2.png')
    .add('numberBackEnd', './assets/core/sprite_number_back_3.png')

    //! Particles
    .add('particle_a1', './assets/particles/a_1.png')
    .add('particle_a2', './assets/particles/a_2.png')
    .add('particle_a3', './assets/particles/a_3.png')
    .add('particle_a4', './assets/particles/a_4.png')
    .add('particle_a5', './assets/particles/a_5.png')
    .add('particle_a6', './assets/particles/a_6.png')
    .add('particle_a7', './assets/particles/a_7.png')
    .add('particle_a8', './assets/particles/a_8.png')
    .add('particles_coin', './assets/particles/particles_coin.json')
    .add('particles_coin2', './assets/particles/particles_coin2.json')
    .add('particles_coin3', './assets/particles/particles_coin3.json')
    .add('particles_confetti_1', './assets/particles/particles_confetti_1.json')
    .add('particles_confetti_2', './assets/particles/particles_confetti_2.json')

    // image asset
    .add('exitButton', './assets/core/button_close.png')

    .add('freeModePopup', './assets/core/freeMode_popup.png')
    .add('freeBetIcon', './assets/core/freeBet_icon.png')

    // charge particles
    .add('charge_particle', './assets/core/charge_particle.png')

if (isMobile()) {
    loader
        .add('button_slidedown', './assets/core/button_slidedown_p.png')
        .add('buttonMenu', './assets/core/button_bet_option_p.png')
        .add('background_menu', './assets/core/background_menu_p.png')
        .add('button_rectangle', './assets/core/button_rectangle_p.png')
        .add('button_sound_menu', './assets/core/button_sound_menu_p.png')
        .add('button_sound_mute_menu', './assets/core/button_sound_mute_menu_p.png')
        .add('button_music_menu', './assets/core/button_music_menu_p.png')
        .add('button_music_mute_menu', './assets/core/button_music_mute_menu_p.png')
        .add('button_bet_history_menu', './assets/core/button_bet_history_menu_p.png')
        .add('button_bet_confirm', './assets/core/button_bet_confirm_p.png')
        .add('bet_option_amount_background', './assets/core/bet_option_amount_background_p.png')
        .add('bet_option_background', './assets/core/bet_option_background_p.png')
        .add('top_bar_background', './assets/core/top_bar_background_p.png')
        // panel

        .add('panel', './assets/core/bottom_black_background.png')

        // Menu Button
        .add('menuButton', './assets/core/button_menu_p.png')

        // Button to increase balance
        .add('addBalanceButton', './assets/core/button_balance_add_p.png')

        // Total bet button to bring up betting menu
        .add('totalBetButton', './assets/core/button_total_bet_p.png')
        .add('slideupButton', './assets/core/button_slideup_p.png')

        // Increase and decrease buttons (Bet Lines and Bet Amount)
        .add('increaseButton', './assets/core/button_increase_p.png')
        .add('decreaseButton', './assets/core/button_decrease_p.png')

        // Bet Max button
        .add('betMaxButton', './assets/core/button_bet_max_p.png')
        .add('betMaxSymbol', './assets/core/button_symbol_betmax_p.png')
        .add('betMaxSymbolActive', './assets/core/button_symbol_betmax_active_p.png')
        .add('betMaxDownButton', './assets/core/button_bet_max_p_down.png')
        .add('betMaxDisabledButton', './assets/core/button_bet_max_p_disabled.png')

        // Spin Button
        .add('spinButton', './assets/core/button_spin_p.png')
        .add('spinDownButton', './assets/core/button_spin_p_down.png')

        // Autospin buttons
        .add('autoSpinButton', './assets/core/button_auto_spin_p.png')
        .add('autoSpinDownButton', './assets/core/button_auto_spin_p_down.png')
        .add('autoSpinStopButton', './assets/core/button_auto_spin_stop_p.png')
        .add('autoSpinStopDownButton', './assets/core/button_auto_spin_stop_p_down.png')

        // Scroll bar
        .add('scrollBar', './assets/core/scroll_bar_p.png')
        .add('scrollBarHighlight', './assets/core/scroll_bar_highlight_p.png')
        .add('scrollButton', './assets/core/scroll_button_p.png')

        // Autospin Panel
        .add('autoSpinPanel', './assets/core/panel_auto_spin_p.png')
        .add('autoSpinPlayButton', './assets/core/button_confirm_p.png')

        // Radio Button
        .add('radioButtonBackgroundAutoSpinOn', './assets/core/button_select_auto_spin_on_p.png')
        .add('radioButtonBackgroundAutoSpinOff', './assets/core/button_select_auto_spin_off_p.png')
        .add('radioButtonBackgroundAnyOn', './assets/core/button_select_any_on_p.png')
        .add('radioButtonBackgroundAnyOff', './assets/core/button_select_any_off_p.png')

        // Autospin Label Box / Mini Box
        .add('autoSpinLabelBox', './assets/core/button_box_check_off_p.png')
        .add('autoSpinLabelMiniBox', './assets/core/button_box_check_on_p.png')

        // Spin 100 Button
        .add('spin100Button', './assets/core/button_100spins_p.png')
        .add('spin100DownButton', './assets/core/button_100spins_p_down.png')

        // Turbo Button
        .add('turboOffButton', './assets/core/button_turbo_p.png')
        .add('turboOnButton', './assets/core/button_turbo_p_on.png')
        .add('turboSymbol', './assets/core/button_symbol_turbo_p.png')
        .add('turboSymbolActive', './assets/core/button_symbol_turbo_active_p.png')

        // Info Panel
        .add('balanceBox', './assets/core/box_balance_l.png')
        .add('paytableBackground', './assets/core/layout_paytable_backing_p.png')
        .add('paytableHeader', './assets/core/layout_paytable_header_p.png')
        .add('paytableLine', './assets/core/layout_paytable_line.png')
        .add('spins100_gameRules', './assets/core/100spins-gamerules-p.png')
        .add('autospin_gameRules', './assets/core/autospin-gamerules-p.png')
        .add('betmax_gameRules', './assets/core/betmax-gamerules-p.png')
        .add('menu_gameRules', './assets/core/menu-gamerules-p.png')
        .add('turbo_gameRules', './assets/core/turbo-gamerules-p.png')

        // tutorial page
        .add('tutorial', `./assets/core/language/${lang}/tutorial.png`)
        .add('tutorial1', `./assets/core/language/${lang}/tutorial1.png`)
        .add('tutorial2', `./assets/core/language/${lang}/tutorial2.png`)
        .add('tutorial3', `./assets/core/language/${lang}/tutorial3.png`)
        .add('tutorial4', `./assets/core/language/${lang}/tutorial4.png`)
        .add('tutorial5', `./assets/core/language/${lang}/tutorial5.png`)
        .add('tutorial6', `./assets/core/language/${lang}/tutorial6.png`)
        .add('tutorial7', `./assets/core/language/${lang}/tutorial7.png`)
        .add('tutorial8', `./assets/core/language/${lang}/tutorial8.png`)
        .add('tutorial9', `./assets/core/language/${lang}/tutorial9.png`)
        .add('tutorial10', `./assets/core/language/${lang}/tutorial10.png`)
        .add('tutorial11', `./assets/core/language/${lang}/tutorial11.png`)
        .add('tutorial12', `./assets/core/language/${lang}/tutorial12.png`)
        .add('tutorial13', `./assets/core/language/${lang}/tutorial13.png`)
        .add('tutorial14', `./assets/core/language/${lang}/tutorial14.png`)
        .add('tutorial15', `./assets/core/language/${lang}/tutorial15.png`)

        // tutorial cursor
        .add('cursor1', `./assets/core/tutorial_cursor_animation_p_1.png`)
        .add('cursor2', `./assets/core/tutorial_cursor_animation_p_2.png`)
        .add('cursor3', `./assets/core/tutorial_cursor_animation_p_3.png`)
        .add('cursor4', `./assets/core/tutorial_cursor_animation_p_4.png`)

        // tutorial skip button
        .add('skipButton', `./assets/core/language/${lang}/skip.png`)

        //game rules
        .add('gameRules', `./assets/core/language/${lang}/game-rules-content-p.png`)

        //! Free Spin
        .add('freeMode_chest', './assets/core/freeMode_chest.png')
        .add('freeMode_base', './assets/core/freeMode_base.png')
        .add('freeMode_message', './assets/core/freeMode_message_p.png')
        .add('freeMode_reward_message_0', './assets/core/freeMode_reward_message_0_p.png')
        .add('freeMode_reward_message_1', './assets/core/freeMode_reward_message_1_p.png')
        .add('okButton', './assets/core/button_ok_p.png')

        //info menu indicator
        .add('button_gamerules_menu_p', './assets/core/button_gamerules_menu_p.png')
        .add('button_paytable_menu_p', './assets/core/button_paytable_menu_p.png')
        .add('button_quitgame_menu_p', './assets/core/button_quitgame_menu_p.png')
        .add('button_tutorial_menu_p', './assets/core/button_tutorial_menu_p.png')
}
else if (!isMobile()) {
    loader
    // panel
        .add('panel', './assets/core/panel_layout_bottom_l.png')

        // Spin Button
        .add('spinButton', './assets/core/button_spin_l.png')
        .add('spinDownButton', './assets/core/button_spin_l_down.png')

        // Autospin Button
        .add('autoSpinButton', './assets/core/button_auto_spin_l.png')
        .add('autoSpinDownButton', './assets/core/button_auto_spin_l_down.png')
        .add('autoSpinStopButton', './assets/core/button_auto_spin_stop_l.png')
        .add('autoSpinStopDownButton', './assets/core/button_auto_spin_stop_l_down.png')

        // Autospin Panel
        .add('autoSpinPanel', './assets/core/panel_auto_spin_l.png')
        .add('autoSpinPlayButton', './assets/core/button_confirm_l.png')

        // Scroll Bar
        .add('scrollBar', './assets/core/scroll_bar_l.png')
        .add('scrollBarHighlight', './assets/core/scroll_bar_highlight_l.png')
        .add('scrollButton', './assets/core/scroll_button_l.png')

        // Radio Buttons
        .add('radioButtonBackgroundAutoSpinOn', './assets/core/button_select_auto_spin_on_l.png')
        .add('radioButtonBackgroundAutoSpinOff', './assets/core/button_select_auto_spin_off_l.png')
        .add('radioButtonBackgroundAnyOn', './assets/core/button_select_any_on_l.png')
        .add('radioButtonBackgroundAnyOff', './assets/core/button_select_any_off_l.png')

        // Autospin Label Box / Mini Box
        .add('autoSpinLabelBox', './assets/core/button_box_check_off_l.png')
        .add('autoSpinLabelMiniBox', './assets/core/button_box_check_on_l.png')

        // Spin 100 Button
        .add('spin100Button', './assets/core/button_100spins_l.png')
        .add('spin100DownButton', './assets/core/button_100spins_l_down.png')

        // Increase Bet Lines / Increase Bet Amount
        .add('increaseButton', './assets/core/button_increase_l.png')
        .add('increaseHoverButton', './assets/core/button_increase_l_hover.png')
        .add('increaseDownButton', './assets/core/button_increase_l_down.png')

        // Decrease Bet Lines / Decrease Bet Amount
        .add('decreaseButton', './assets/core/button_decrease_l.png')
        .add('decreaseHoverButton', './assets/core/button_decrease_l_hover.png')
        .add('decreaseDownButton', './assets/core/button_decrease_l_down.png')

        // Turbo Button
        .add('turboOffButton', './assets/core/button_turbo_l.png')
        .add('turboHoverButton', './assets/core/button_turbo_l_hover.png')
        .add('turboOnButton', './assets/core/button_turbo_l_on.png')

        // Bet Max Button
        .add('betMaxButton', './assets/core/button_bet_max_l.png')
        .add('betMaxHoverButton', './assets/core/button_bet_max_l_hover.png')
        .add('betMaxDownButton', './assets/core/button_bet_max_l_down.png')
        .add('betMaxDisabledButton', './assets/core/button_bet_max_l_disabled.png')

        // Settings Button
        .add('settingsButton', './assets/core/button_settings_l.png')
        .add('settingsHoverButton', './assets/core/button_settings_l_hover.png')
        .add('settingsDownButton', './assets/core/button_settings_l_down.png')

        // Settings Bar and icons inside settings bar
        .add('settingsBar', './assets/core/panel_settings_bar_l.png')
        .add('musicButton', './assets/core/button_music_l.png')
        .add('musicHoverButton', './assets/core/button_music_l_hover.png')
        .add('musicMutedButton', './assets/core/button_music_l_off.png')
        .add('musicMutedHoverButton', './assets/core/button_music_l_off_hover.png')
        .add('soundButton', './assets/core/button_sound_l.png')
        .add('soundHoverButton', './assets/core/button_sound_l_hover.png')
        .add('soundMutedButton', './assets/core/button_sound_l_off.png')
        .add('soundMutedHoverButton', './assets/core/button_sound_l_off_hover.png')
        .add('exitHoverButton', './assets/core/button_exit_l_hover.png')
        .add('exitGameButton', './assets/core/button_exit_l.png')
        .add('transactionHistoryButton', './assets/core/button_transaction_history_l.png')
        .add('transactionHistoryHoverButton', './assets/core/button_transaction_history_l_hover.png')

        // Info Button
        .add('infoMenuButton', './assets/core/button_menu_l.png')
        .add('infoMenuHoverButton', './assets/core/button_menu_l_hover.png')
        .add('infoMenuDownButton', './assets/core/button_menu_l_down.png')
        .add('infoButton', './assets/core/button_info_l.png')
        .add('infoHoverButton', './assets/core/button_info_l_hover.png')
        .add('tutorialButton', './assets/core/button_tutorial_l.png')
        .add('tutorialHoverButton', './assets/core/button_tutorial_l_hover.png')

        // Info Panel
        .add('paytableBorder', './assets/core/panel_paytable_border.png')
        .add('leftRightPaytableButton', './assets/core/button_left_right_paytable.png')
        .add('selectedPaytableButton', './assets/core/button_select_paytable.png')
        .add('selectedPaytableButtonOff', './assets/core/button_select_paytable_off.png')
        .add('selectedPaytableButtonHover', './assets/core/button_select_paytable_hover.png')
        .add('selectPagePaytable', './assets/core/button_select_page_paytable.png')
        .add('selectPagePaytableOff', './assets/core/button_select_page_paytable_off.png')
        .add('leftRightPaytableButtonBg', './assets/core/button_small_off.png')
        .add('leftRightPaytableButtonDown', './assets/core/button_small_down.png')
        .add('leftRightPaytableButtonHover', './assets/core/button_small_hover.png')
        .add('betmax_gameRules', './assets/core/betmax-gamerules-l.png')
        .add('info_gameRules', './assets/core/info-gamerules-l.png')
        .add('setting_gameRules', './assets/core/setting-gamerules-l.png')
        .add('turbo_gameRules', './assets/core/turbo-gamerules-l.png')

        // Background boxes for balance label, levels / lines label, bet amount label and total bet label
        .add('balanceBox', './assets/core/box_balance_l.png')
        .add('linesBox', './assets/core/box_lines_l.png')
        .add('betAmountBox', './assets/core/box_bet_amount_l.png')
        .add('totalBetBox', './assets/core/box_total_bet_l.png')

        // blur background
        .add('blurBackground', './assets/core/blur_background.png')

        //! Free Spin
        .add('freeMode_chest', './assets/core/freeMode_chest.png')
        .add('freeMode_base', './assets/core/freeMode_base.png')
        .add('freeMode_message', './assets/core/freeMode_message_l.png')
        .add('freeMode_reward_message_0', './assets/core/freeMode_reward_message_0_l.png')
        .add('freeMode_reward_message_1', './assets/core/freeMode_reward_message_1_l.png')
        .add('okButton', './assets/core/button_ok_l.png')

        //tutorial
        .add('tutorial', `./assets/core/language/${lang}/landscape_tutorial_l.png`)

        // game rules
        .add('gameRules1', `./assets/core/language/${lang}/game-rules-content1-l.png`)
        .add('gameRules2', `./assets/core/language/${lang}/game-rules-content2-l.png`)
        // .add('gameRules3', `./assets/core/language/${lang}/game-rules-content3-l.png`)
        .add('gameRules4', `./assets/core/language/${lang}/game-rules-content4-l.png`)
        .add('gameRules5', `./assets/core/language/${lang}/game-rules-content5-l.png`)

}

/**
 * Functions definition
 **/
export function isMobile() {
    return browserSupport.isMobile();
}

export function isDemoMode() {
    return isDemo;
}

export function checkCanInit() {
    return canInit;
}

export function changeFalseCanInit() {
    canInit = false;
}

function setResolutionX(x) {
    resolutionX = x;
}

export function getResolutionX() {
    return resolutionX;
}

function setResolutionY(y) {
    resolutionY = y;
}

export function getResolutionY() {
    return resolutionY;
}

export function getLanguage() {
    return lang;
}

if (isMobile()) {
    setResolutionX(720);
    setResolutionY(1280);
} else {
    setResolutionX(1280);
    setResolutionY(720);
}

const app = new PIXI.Application({
    width: getResolutionX(),
    height: getResolutionY(),
    transparent: false,
});

// attach some useful function into app instance.
app.isMobile = isMobile;
app.isDemoMode = isDemoMode;
app.getResolutionX = getResolutionX;
app.getResolutionY = getResolutionY;
app.getLanguage = getLanguage;
app.setResolutionX = setResolutionX;
app.setResolutionY = setResolutionY;

app.getLoader = () => {
    return loader;
};

app.resize = () => {
    let curW = null;
    let curH = null;
    if (isMobile()) {
        setTimeout(() => {
            curH = window.innerHeight; //! (add - to create an offset in height. Ex: curH = window.innerHeight - 50, creates a 50px offset on height)
            curW = curH / 16 * 9;

            app.renderer.view.style.width = `${curW}px`;
        }, 50)
    }
    else if (!isMobile()) {
        curW = window.innerWidth; //! (add - to create an offset in width. Ex: curW = window.innerWidth - 50, creates a 50px offset on width)
        curH = curW / 16 * 9;

        //! Check if height set by the ratio exceeds the window's height.
        if (curH > window.innerHeight) //! If it exceeds, set height to window's height. - for offset
        {
            app.renderer.view.style.height = (window.innerHeight) + "px";
        }
        else if (curH < window.innerHeight) //! Else set to height set by ratio
        {
            app.renderer.view.style.height = curH + "px";
        }
    }
};

const components = {
    items: [],
    buttons: [],
    text: [],
};

app.getComponents = () => {
    return components;
};

app.addComponent = (component) => {
    components.items.push(component);
    app.stage.addChild(component);
};

app.addButton = (component) => {
    components.buttons.push(component);
    app.stage.addChild(component);
};

app.addText = (component) => {
    components.text.push(component);
    app.stage.addChild(component);
};

app.disableAllButtons = () => {
    each(components.buttons, (buttons) => {
        buttons.interactive = false;
    });
};

app.enableAllButtons = () => {
    each(components.buttons, (buttons) => {
        buttons.interactive = true;
    });
};

loader
    .on('start', function () {
        app.resize();
    })
    .on('progress', function () {
        loading.updateProgress();
    })
    .on('complete', function () {
        loading.destroy();
    });

/**
 * App init last check
 **/
// browser support detection
if (!browserSupport.isBrowserSupported()) {
    canInit = false;
}

// temp: disable temporary
// game instance checking

/**
 * Listeners registrations
 **/

// make sure canvas render when window resize
window.onresize = app.resize;

// When the browser becomes offline, show dialog and force player to refresh the browser
window.addEventListener('offline', () => {
    panel.stopAutoSpin();
    ui.showDialog(loaded.languageFile.data.disconnect, '', loaded.languageFile.data.reconnect, {
        onPointerUp: () => {
            window.location.reload();
        }
    });
});

// prevent console being open
const debugKeyEnable = environment.debug;
if (!debugKeyEnable) {

    // prevent right click
    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    }, false);

    // prevent key combination
    document.addEventListener('keydown', function (e) {
        if (
            (e.ctrlKey && e.shiftKey && e.key === 'i') || // 'i' key
            (e.ctrlKey && e.shiftKey && e.key === 'j') || // 'j' key
            (e.key === 's' && (navigator.platform.match('Mac') ? e.metaKey : e.ctrlKey)) || // "S" key + macOS
            (e.ctrlKey && e.key === 'u') || // "U" key
            (event.key === 'F12') // "F12" key
        ) {
            if (e.stopPropagation) {
                e.stopPropagation();
            } else if (window.event) {
                window.event.cancelBubble = true;
            }
            e.preventDefault();
        }

    }, false);
}

// hide and show the bet history big close button
window.addEventListener('message', (event) => {
    if (event.data === 'HIDE_BET_HISTORY_CLOSE_BUTTON') {
        betHistory.hideCloseButton();
    } else if (event.data === 'SHOW_BET_HISTORY_CLOSE_BUTTON') {
        betHistory.showCloseButton();
    }
});

// prevent iOS pinch to zoom
document.addEventListener('touchmove', function (event) {
    if (event.scale !== 1) {
        event.preventDefault();
    }
}, {passive: false});

// prevent iOS double tap to zoom
let lastTouchEnd = 0;
document.addEventListener('touchend', function (event) {
    let now = (new Date()).getTime();
    if (now - lastTouchEnd <= 300) {
        event.preventDefault();
    }
    lastTouchEnd = now;
}, false);

export default app;
