import * as PIXI from 'pixi.js';
import 'pixi-spine';
import 'gsap';
import { each } from 'lodash';

import app, { isMobile } from './app';
import ui from './ui';
import sound from './sound';

const loaded = PIXI.loader.resources;

function init(options) {

    // Assign options to replace default options
    const newOptions = Object.assign({
        gameTitle: 'Unknown game',
        pages: [],
        gameRulesPages: [],
        totems: [],
        debugSpine: null,
        multipleLine: true,
        betLevel: true,
        extraGameRule: [],
        primaryTint: '0x09B6FF',
    }, options);

    // Initialize container to store all paytable assets
    const infoContainer = new PIXI.Container();
    infoContainer.name = 'Info Container';

    let gameRulesContainer = new PIXI.Container();
    gameRulesContainer.name = 'Game Rules Container';

    // Create a black background the covers the whole screen
    let infoBackground = app.isMobile() ? new PIXI.Sprite(loaded.paytableBackground.texture) : new PIXI.Sprite(loaded.blurBackground.texture);
    infoBackground.name = 'Info Background';
    infoBackground.tint = app.isMobile() ? 0xffffff : 0x888888;
    infoBackground.width = app.getResolutionX();
    infoBackground.height = app.getResolutionY();
    infoBackground.interactive = true;
    infoContainer.addChild(infoBackground);

    if (app.isMobile()) {

        let infoHeader = new PIXI.Sprite(loaded.paytableHeader.texture);
        infoHeader.name = 'Info Header';
        infoHeader.width = app.getResolutionX();
        infoHeader.height = 121;
        infoHeader.alpha = 1;
        infoContainer.addChild(infoHeader);

        let gameTitleText = new PIXI.Text(loaded.languageFile.data.paytable, {
            fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
            fill: 'white',
            fontSize: 45,
            fontWeight: 'bold'
        });
        gameTitleText.name = loaded.languageFile.data.paytable;
        gameTitleText.anchor.set(0.5);
        gameTitleText.x = app.getResolutionX() / 2;
        gameTitleText.y = 59;
        infoContainer.addChild(gameTitleText);

        // Create the exit button
        let exitPaytableButton = ui.createButton({
            texture: loaded.exitButton.texture,
            centerAnchor: true,
            onPointerUp: function () {
                exitPaytableButton.alpha = 1;

                //sound
                sound.play('buttonClickReleaseSound');

                // Turn alpha to zero
                infoContainer.visible = false;

                // Remove all interactivity for buttons in paytable
                exitPaytableButton.interactive = false;
                infoBackground.interactive = false;
            },
            onPointerDown: function () {
                sound.play('buttonClickSound');
            }
        });
        exitPaytableButton.x = 24 + exitPaytableButton.width / 2;
        exitPaytableButton.y = 60.5;
        infoContainer.addChild(exitPaytableButton);

        // Element Container Mask
        let elementMask = new PIXI.Graphics().beginFill(0xFFFFFF).drawRect(0, 121, 720, 1159).endFill();
        infoContainer.addChild(elementMask);

        // The very start of the element's Y
        let elementContainer = new PIXI.Container();
        elementContainer.x = app.getResolutionX() / 2;
        elementContainer.mask = elementMask;

        let nextElementY = 150;
        each(newOptions.pages, (element) => {

            let target = null;
            let currentElementOffset = element.spacingOffsetY ? element.spacingOffsetY : 0;

            if (element.type === 'text') {
                const fontSize = element.fontSize ? element.fontSize : 16;

                target = new PIXI.Text(element.content, {
                    fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
                    fontSize: fontSize,
                    align: 'center',
                    fill: 0xffffff,
                    wordWrap: true,
                    wordWrapWidth: 600,
                    breakWords: true,
                    breakWordsWidth: 600,
                    lineHeight: 40,
                });
                target.anchor.set(0.5, 0);
                target.x = 0;
            }

            if (element.type === 'sprite') {
                target = new PIXI.Sprite(element.content);
                target.anchor.set(0.5, 0);

                // use custom offsetX if provided
                target.x = element.textureOffsetX ? element.textureOffsetX : 0;
                if (element.x) {
                    target.x += element.x
                }

                if (element.textureScaleX) target.scale.x = element.textureScaleX;
                if (element.textureScaleY) target.scale.y = element.textureScaleY;
            }

            if (element.type === 'spine') {

                if (!element.spineData) {
                    throw new Error('This type requires a spine data');
                }

                target = new PIXI.spine.Spine(element.spineData);
                target.state.setAnimation(0, element.animationName, true);
                target.autoUpdate = true;

                target.x = 0;
                if (element.x) {
                    target.x += element.x
                }
            }

            if (element.type === 'container') {

                if (!element.container) {
                    throw new Error('This type requires a container');
                }

                target = element.container;

                target.x = 0;
                if (element.x) {
                    target.x += element.x
                }
            }

            if (element.type === 'pageBreak') {
                target = new PIXI.Sprite(loaded.paytableLine.texture);
                target.anchor.set(0.5, 0);
                target.x = 0;
            }

            // use nextElementY to place current element element below previous element
            target.y = nextElementY;

            // update nextElementY to make sure next element will be placed below current element
            nextElementY = target.getBounds().bottom + currentElementOffset + 75;

            // add current element into container
            elementContainer.addChild(target);
        });
        infoContainer.addChild(elementContainer);

        //Simple scrolling screen
        infoBackground.buttonMode = true;
        let initialDistance = 0;
        let isDown = false;

        infoBackground.on('pointerdown', (event) => {
            // Calculates the initial distance between mouse and center point of element Container
            const initialPoint = event.data.getLocalPosition(infoBackground).y;
            initialDistance = initialPoint - elementContainer.y;

            // Start dragging process
            isDown = true;
        });

        infoBackground.on('pointermove', (event) => {
            if (!isDown) {
                return;
            }

            // Update element container with mouse position with respected distance
            if (elementContainer.getBounds().bottom <= app.getResolutionY() - 60) {
                TweenMax.to(elementContainer, 0.5, {
                    y: -elementContainer.height + app.getResolutionY() - 170,
                });
                isDown = false;
                return;
            } else if (elementContainer.getBounds().top >= 170) {
                TweenMax.to(elementContainer, 0.5, {
                    y: 0,
                });
                isDown = false;
                return;
            }
            else {
                let currentPosition = event.data.getLocalPosition(infoBackground).y;
                elementContainer.y = currentPosition - initialDistance;
            }
        });

        infoBackground.on('pointerup', () => {
            // Stop dragging process
            isDown = false;
        });

        // Game Rules Background in portrait mode
        let gameRulesBackground = new PIXI.Sprite(loaded.paytableBackground.texture);
        gameRulesBackground.name = 'Game Rules Background';
        gameRulesBackground.tint = 0xffffff;
        gameRulesBackground.width = app.getResolutionX();
        gameRulesBackground.height = app.getResolutionY();
        gameRulesBackground.interactive = true;
        gameRulesContainer.addChild(gameRulesBackground);

        // Game Rules Header in portrait mode
        let gameRulesHeader = new PIXI.Sprite(loaded.paytableHeader.texture);
        gameRulesHeader.name = 'Game Rules Header';
        gameRulesHeader.width = app.getResolutionX();
        gameRulesHeader.height = 121;
        gameRulesHeader.alpha = 1;
        gameRulesContainer.addChild(gameRulesHeader);

        // Create the exit button
        let exitGameRulesButton = ui.createButton({
            texture: loaded.exitButton.texture,
            centerAnchor: true,
            onPointerUp: function () {
                exitGameRulesButton.alpha = 1;
                exitGameRulesButton.interactive = false;
                sound.play('buttonClickReleaseSound');
                gameRulesContainer.visible = false;
            },
            onPointerDown: function () {
                exitPaytableButton.alpha = 0.5;
                gameRulesContainer.visible = false;
                sound.play('buttonClickSound');
            }
        });
        exitGameRulesButton.x = 24 + exitGameRulesButton.width / 2;
        exitGameRulesButton.y = 60.5;
        gameRulesContainer.addChild(exitGameRulesButton);

        // Game Rules Text in portrait mode
        let gameRuleTitlesText = new PIXI.Text(loaded.languageFile.data.game_rules, {
            fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
            fill: 'white',
            fontSize: isMobile() ? 45 : 48,
            fontWeight: 'bold'
        });
        gameRuleTitlesText.name = loaded.languageFile.data.game_rules;
        gameRuleTitlesText.anchor.set(0.5);
        gameRuleTitlesText.x = app.getResolutionX() / 2;
        gameRuleTitlesText.y = 59;
        gameRulesContainer.addChild(gameRuleTitlesText);

        // // The very start of the Rules's Y
        let rulesContainer = new PIXI.Container();
        rulesContainer.mask = elementMask;

        let nextRulesY = 150;
        each(newOptions.gameRulesPages, (page) => {
            let target = null;
            let currentPageOffset = page.spacingOffsetY ? page.spacingOffsetY : 0;

            if (page.type === 'title') {

                if (!page.contentText) {
                    throw new Error('game rules content missing text');
                }

                const fontSize = page.fontSize ? page.fontSize : 36;
                const fontTint = page.textTint ? page.textTint : 0xffffff;

                target = new PIXI.Text(page.contentText, {
                    fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
                    fontSize: fontSize,
                    align: 'center',
                    fill: fontTint,
                    wordWrap: true,
                    wordWrapWidth: 600,
                    breakWords: true,
                    breakWordsWidth: 600,
                    lineHeight: 40,
                });
                target.anchor.set(0.5, 0);
                target.x = page.textOffsetX ? page.textOffsetX : app.getResolutionX() / 2;
            }
            if (page.type === 'sprite') {
                target = new PIXI.Sprite(page.sprite);
                target.anchor.set(0.5, 0);
                target.x = app.getResolutionX() / 2;

                // use custom offsetX if provided
                target.x += page.textureOffsetX ? page.textureOffsetX : 0;
                target.y += page.textureOffsetY ? page.textureOffsetY : 0;

            }
            else if (page.type === 'content') {
                const fontSize = page.fontSize ? page.fontSize : 27;
                const fontTint = page.textTint ? page.textTint : 0xffffff;
                let preText;

                if (!page.contentText) {
                    throw new Error('game rules content missing text');
                }

                if (!Array.isArray(page.contentText)) {
                    throw new Error('game rules content Text need to be Array');
                }

                if (page.bulletPoint) {
                    preText = [];
                    each(page.contentText, (rulesText) => {
                        preText.push('• '.concat(rulesText));
                    });
                }
                else {
                    preText = page.contentText;
                }

                target = new PIXI.Text(preText.join('\n\n'), {
                    fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
                    fontSize: fontSize,
                    align: 'left',
                    fill: fontTint,
                    wordWrap: true,
                    wordWrapWidth: 600,
                    breakWords: true,
                    breakWordsWidth: 600,
                    lineHeight: 40,
                });

                target.x = page.textOffsetX ? page.textOffsetX : 50;
            }
            else if (page.type === 'pageBreak') {
                target = new PIXI.Sprite(loaded.paytableLine.texture);
                target.anchor.set(0.5, 0);
                target.x = app.getResolutionX() / 2;
            }

            // use nextRulesY to place current element element below previous rules
            target.y = nextRulesY;

            // update nextRulesY to make sure next element will be placed below current rules
            nextRulesY = target.getBounds().bottom + currentPageOffset + 25;

            // add current element into container
            rulesContainer.addChild(target);
        });

        gameRulesBackground.on('pointerdown', (event) => {
            // Calculates the initial distance between mouse and center point of element Container
            const initialPoint = event.data.getLocalPosition(gameRulesBackground).y;
            initialDistance = initialPoint - rulesContainer.y;

            // Start dragging process
            isDown = true;
        });

        gameRulesBackground.on('pointermove', (event) => {
            if (!isDown) {
                return;
            }

            // Update element container with mouse position with respected distance
            if (rulesContainer.getBounds().bottom <= app.getResolutionY() - 60) {
                TweenMax.to(rulesContainer, 0.5, {
                    y: -rulesContainer.height + app.getResolutionY() - 170,
                });
                isDown = false;
                return;
            } else if (rulesContainer.getBounds().top >= 170) {
                TweenMax.to(rulesContainer, 0.5, {
                    y: 0,
                });
                isDown = false;
                return;
            }
            else {
                let currentPosition = event.data.getLocalPosition(gameRulesBackground).y;
                rulesContainer.y = currentPosition - initialDistance;
            }
        });

        gameRulesBackground.on('pointerup', () => {
            // Stop dragging process
            isDown = false;
        });

        rulesContainer.mask = elementMask;
        gameRulesContainer.addChild(rulesContainer);

        app.addComponent(gameRulesContainer);

        // Info Page Helpers
        infoContainer['showGameRules'] = () => {
            exitGameRulesButton.interactive = true;
            gameRulesContainer.visible = true;
        };

        infoContainer['hideGameRules'] = () => {
            exitGameRulesButton.interactive = false;

            gameRulesContainer.visible = false;
        };

        infoContainer['showInfoPage'] = () => {
            infoContainer.visible = true;

            // Opens all interactivity for buttons in paytable
            exitPaytableButton.interactive = true;
            infoBackground.interactive = true;
        };

        infoContainer['hideInfoPage'] = () => {
            // Turn alpha to 0
            infoContainer.visible = false;

            // Closes all interactivity for buttons in paytable
            exitPaytableButton.interactive = false;
            infoBackground.interactive = false;
        };
    }
    else {

        let currentPage = 0; // store current page of info container on desktop
        let currentGameRulesPage = 0;
        let isPaytable = true;

        // Create the paytable border
        let paytableBorder = new PIXI.Sprite(loaded.paytableBorder.texture);
        paytableBorder.anchor.set(0.5);
        paytableBorder.x = app.getResolutionX() / 2;
        paytableBorder.y = app.getResolutionY() / 2;
        paytableBorder.interactive = true;
        infoContainer.addChild(paytableBorder);

        // Create the exit button
        let exitPaytableButton = ui.createButton({
            texture: loaded.selectedPaytableButtonOff.texture,
            clickedTexture: loaded.selectedPaytableButton.texture,
            textureHover: loaded.selectedPaytableButtonHover.texture,
            centerAnchor: true,
            onPointerUp: function () {

                //Sound
                sound.play('buttonClickReleaseSound');

                // hide info container
                infoContainer.hideInfoPage();

            },
            onPointerDown: function () {
                sound.play('buttonClickSound');
            }
        });
        exitPaytableButton.x = 640;
        exitPaytableButton.y = 669;
        infoContainer.addChild(exitPaytableButton);

        let exitText = new PIXI.Text(loaded.languageFile.data.exit, {
            fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
            fontSize: 22,
            fill: 0xffffff,
            fontWeight: 'bold'
        });
        exitText.anchor.set(0.5);
        exitPaytableButton.addChild(exitText);

        // Create Paytable Button
        let paytableButton = new PIXI.Sprite(loaded.selectedPaytableButton.texture);
        paytableButton.name = 'Paytable Button';
        paytableButton.anchor.set(0.5);
        paytableButton.x = 517;
        paytableButton.y = 52;
        paytableButton.interactive = true;
        paytableButton.buttonMode = true;
        paytableButton.on('pointerup', () => {
            sound.play('buttonClickReleaseSound');
            showPayTableOrGameRules('paytable');
        });
        paytableButton.on('pointerdown', () => {
            sound.play('buttonClickSound');
        });
        paytableButton.on('pointerover', () => {
            if (paytableButton.texture === loaded.selectedPaytableButtonOff.texture) {
                paytableButton.texture = loaded.selectedPaytableButtonHover.texture;
            }
        });
        paytableButton.on('pointerout', () => {
            if (paytableButton.texture === loaded.selectedPaytableButtonHover.texture) {
                paytableButton.texture = loaded.selectedPaytableButtonOff.texture;
            }
        });
        infoContainer.addChild(paytableButton);

        // Create Paytable Text
        let paytableText = new PIXI.Text(loaded.languageFile.data.paytable, {
            fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
            fontSize: 22,
            fill: 0xffffff,
            fontWeight: 'bold'
        });
        paytableText.anchor.set(0.5);
        paytableButton.addChild(paytableText);

        // Create Game Rule Button
        let gameRulesButton = new PIXI.Sprite(loaded.selectedPaytableButtonOff.texture);
        gameRulesButton.name = 'Game Rule Button';
        gameRulesButton.anchor.set(0.5);
        gameRulesButton.x = 764;
        gameRulesButton.y = 52;
        gameRulesButton.interactive = true;
        gameRulesButton.buttonMode = true;
        gameRulesButton.on('pointerup', () => {
            sound.play('buttonClickReleaseSound');
            showPayTableOrGameRules('game_rules');
        });
        gameRulesButton.on('pointerdown', () => {
            sound.play('buttonClickSound');
        });
        gameRulesButton.on('pointerover', () => {
            if (gameRulesButton.texture === loaded.selectedPaytableButtonOff.texture) {
                gameRulesButton.texture = loaded.selectedPaytableButtonHover.texture;
            }
        });
        gameRulesButton.on('pointerout', () => {
            if (gameRulesButton.texture === loaded.selectedPaytableButtonHover.texture) {
                gameRulesButton.texture = loaded.selectedPaytableButtonOff.texture;
            }
        });
        infoContainer.addChild(gameRulesButton);

        // Create Paytable Text
        let gameRulesText = new PIXI.Text(loaded.languageFile.data.game_rules, {
            fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
            fontSize: 22,
            fill: 0xffffff,
            fontWeight: 'bold'
        });
        gameRulesText.anchor.set(0.5);
        gameRulesButton.addChild(gameRulesText);

        // Create Left Button
        let leftButton = ui.createButton({
            texture: loaded.leftRightPaytableButtonBg.texture,
            clickedTexture: loaded.leftRightPaytableButtonDown.texture,
            textureHover: loaded.leftRightPaytableButtonHover.texture,
            centerAnchor: true,
            onPointerUp: function () {
                sound.play('buttonClickReleaseSound');
                let pageIndex;

                if (isPaytable) {
                    currentPage--;

                    if (currentPage < 0) {
                        currentPage = newOptions.pages.length - 1;
                    }

                    pageIndex = currentPage;
                }
                else {
                    currentGameRulesPage--;

                    if (currentGameRulesPage < 0) {
                        currentGameRulesPage = newOptions.gameRulesPages.length - 1;
                    }

                    pageIndex = currentGameRulesPage;
                }


                navigateToPage(pageIndex);
            },
            onPointerDown: function () {
                sound.play('buttonClickSound');
            },
        });
        leftButton.name = 'Left Button';
        leftButton.x = 474;
        leftButton.y = 669;
        infoContainer.addChild(leftButton);

        let leftButtonArrow = new PIXI.Sprite(loaded.leftRightPaytableButton.texture);
        leftButtonArrow.anchor.set(0.5);
        leftButton.addChild(leftButtonArrow);

        // Create Right Button
        let rightButton = ui.createButton({
            texture: loaded.leftRightPaytableButtonBg.texture,
            clickedTexture: loaded.leftRightPaytableButtonDown.texture,
            textureHover: loaded.leftRightPaytableButtonHover.texture,
            centerAnchor: true,
            onPointerUp: function () {
                sound.play('buttonClickReleaseSound');

                let pageIndex;

                if (isPaytable) {
                    currentPage++;

                    if (currentPage >= newOptions.pages.length) {
                        currentPage = 0;
                    }

                    pageIndex = currentPage;
                }
                else {
                    currentGameRulesPage++;

                    if (currentGameRulesPage >= newOptions.gameRulesPages.length) {
                        currentGameRulesPage = 0;
                    }

                    pageIndex = currentGameRulesPage;
                }


                navigateToPage(pageIndex);
            },
            onPointerDown: function () {
                sound.play('buttonClickSound');
            },
        });
        rightButton.name = 'Right Button';
        rightButton.x = 806;
        rightButton.y = 669;
        infoContainer.addChild(rightButton);

        let rightButtonArrow = new PIXI.Sprite(loaded.leftRightPaytableButton.texture);
        rightButtonArrow.anchor.set(0.5);
        rightButtonArrow.width = -rightButtonArrow.width;
        rightButton.addChild(rightButtonArrow);

        // hide navigation buttons if there is only 1 page
        if (newOptions.pages.length <= 1) {
            leftButton.alpha = 0.5;
            leftButton.interactive = false;
            rightButton.alpha = 0.5;
            rightButton.interactive = false;
        }

        // Create pages dot at the bottom of screen
        let dotContainer = new PIXI.Container();
        for (let i = 0; i < newOptions.pages.length; i++) {
            let tempTexture = i === 0 ? loaded.selectPagePaytable.texture : loaded.selectPagePaytableOff.texture;

            let tempSprite = new PIXI.Sprite(tempTexture);
            tempSprite.name = `Page Button ${i + 1}`;
            tempSprite.anchor.set(0.5);
            tempSprite.x = i * 30;
            dotContainer.addChild(tempSprite);
        }
        dotContainer.x = app.getResolutionX() / 2 - (newOptions.pages.length * 30) / 2 + 17;
        dotContainer.y = 590;
        if (newOptions.pages.length <= 1) {
            dotContainer.visible = false;
        }
        infoContainer.addChild(dotContainer);

        let gameRulesDotContainer = new PIXI.Container();
        for (let i = 0; i < newOptions.gameRulesPages.length; i++) {
            let tempTexture = i === 0 ? loaded.selectPagePaytable.texture : loaded.selectPagePaytableOff.texture;

            let tempSprite = new PIXI.Sprite(tempTexture);
            tempSprite.name = `Game Rules Page Button ${i + 1}`;
            tempSprite.anchor.set(0.5);
            tempSprite.x = i * 30;
            gameRulesDotContainer.addChild(tempSprite);
        }
        gameRulesDotContainer.x = app.getResolutionX() / 2 - (newOptions.gameRulesPages.length * 30) / 2 + 17;
        gameRulesDotContainer.y = 590;
        gameRulesDotContainer.visible = false;
        infoContainer.addChild(gameRulesDotContainer);

        // Creates a page mask that only shows page inside the mask area
        let pageMask = new PIXI.Graphics();
        pageMask.beginFill(0xffffff, 1);
        pageMask.drawRoundedRect(160, 0, loaded.paytableBorder.texture.width - 18, loaded.paytableBorder.texture.height - 10, 30);
        pageMask.endFill();
        // pageMask.alpha = 0.25;
        infoContainer.addChild(pageMask);

        // Creates page container that stores all the pages
        let pageContainer = new PIXI.Container();
        pageContainer.name = 'Page Container';
        pageContainer.x = app.getResolutionX() / 2;
        pageContainer.y = app.getResolutionY() / 2;
        pageContainer.mask = (app.isMobile()) ? undefined : pageMask;
        infoContainer.addChild(pageContainer);

        // Create each page that was passed in from newOptions.pages
        each(newOptions.pages, (page, index) => {

            // Throws error without page title
            if (!page.title) {
                throw new Error('There must be a text title');
            }

            // Add the page title to the page, required field
            let pageTitle = new PIXI.Text(page.title, {
                fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
                fontSize: 30,
                fill: 0xffffff,
                fontWeight: 'bold'
            });
            pageTitle.name = `${page.title} Title`;
            pageTitle.anchor.set(0.5);
            pageTitle.x = 1280 * index;
            pageTitle.y = -224;
            pageContainer.addChild(pageTitle);

            // Adds an image to the page if text option was passed in
            if (page.pageTexture) {
                let pageImage = new PIXI.Sprite(page.pageTexture);
                pageImage.anchor.set(0.5);
                pageImage.x = 1280 * index;
                pageImage.y = 3;

                if (page.textureOffsetX) pageImage.x += page.textureOffsetX;
                if (page.textureOffsetY) pageImage.y += page.textureOffsetY;
                if (page.textureScaleX) pageImage.scale.x = page.textureScaleX;
                if (page.textureScaleY) pageImage.scale.y = page.textureScaleY;

                pageContainer.addChild(pageImage);
            }

            // Adds an image to the page if spine option was passed in
            if (page.pageSpine) {
                let pageSpine = new PIXI.spine.Spine(page.pageSpine.spineData);
                pageSpine.state.setAnimation(0, page.pageSpine.animationName, false);
                pageSpine.autoUpdate = true;
                pageSpine.x = 1280 * index;
                if (page.y) {
                    pageSpine.y += page.y;
                }
                if (page.textureOffsetX) pageSpine.x += page.textureOffsetX;
                if (page.textureOffsetY) pageSpine.y += page.textureOffsetY;
                if (page.textureScaleX) pageSpine.scale.x = page.textureScaleX;
                if (page.textureScaleY) pageSpine.scale.y = page.textureScaleY;
                pageContainer.addChild(pageSpine);
            }

            // Adds an image to the page if spine option was passed in
            if (page.pageContainer) {
                let container = page.pageContainer;

                container.x = 1280 * index;
                if (page.x) {
                    container.x += page.x;
                }

                container.y = 0;
                if (page.y) {
                    container.y = page.y;
                }

                if (page.containerScaleX) container.scale.x = page.containerScaleX;
                if (page.containerScaleX) container.scale.y = page.containerScaleY;

                pageContainer.addChild(page.pageContainer);
            }

            // Adds text to the page if text option was passed in
            if (page.text) {
                let pageText = new PIXI.Text(page.text, {
                    fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
                    fontSize: 18,
                    align: "center",
                    wordWrap: true,
                    wordWrapWidth: page.wordWrapWidth? page.wordWrapWidth : 600,
                    breakWords: true,
                    breakWordsWidth: 600,
                    fill: 0xffffff,
                    lineHeight: 25,
                });
                pageText.anchor.set(0.5);
                pageText.x = 1280 * index;
                pageText.y = 139;

                if (page.textOffsetX) pageText.X += page.textOffsetX;
                if (page.textOffsetY) pageText.y += page.textOffsetY;

                pageContainer.addChild(pageText);
            }
        });

        // Adjust game rule container that stores the game rules
        gameRulesContainer.x = app.getResolutionX() / 2;
        gameRulesContainer.y = app.getResolutionY() / 2;
        gameRulesContainer.visible = false;
        gameRulesContainer.mask = (app.isMobile()) ? undefined : pageMask;

        each(newOptions.gameRulesPages, (page, index) => {
            // Throws error without page title
            if (!page.title) {
                throw new Error('There must be a text title');
            }

            // Add the page title to the page, required field
            let pageTitle = new PIXI.Text(page.title, {
                fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
                fontSize: 30,
                fill: 0xffffff,
                fontWeight: 'bold'
            });
            pageTitle.name = `${page.title} Title`;
            pageTitle.anchor.set(0.5);
            pageTitle.x = 1280 * index;
            pageTitle.y = -224;
            gameRulesContainer.addChild(pageTitle);

            let contentsContainer = new PIXI.Container();
            contentsContainer.x = 1280 * index;

            // sprite
            if (page.sprite) {
                let sprite = new PIXI.Sprite(page.sprite);
                sprite.anchor.set(0.5);

                if (page.textureOffsetX) sprite.x += page.textureOffsetX;
                if (page.textureOffsetY) sprite.y += page.textureOffsetY;

                contentsContainer.addChild(sprite);
            }

            // content text
            if (page.text) {
                let contentText = new PIXI.Text(page.text, {
                    fontFamily: app.getLanguage() === 'en' ? 'Arial' : 'Microsoft YaHei',
                    fontSize: 18,
                    align: "center",
                    wordWrap: true,
                    wordWrapWidth: 600,
                    fill: 0xffffff,
                    lineHeight: 25,
                });
                contentText.anchor.set(0.5);

                if (page.textOffsetX) contentText.X += page.textOffsetX;
                if (page.textOffsetY) contentText.y += page.textOffsetY;

                contentsContainer.addChild(contentText);
            }

            gameRulesContainer.addChild(contentsContainer);
        });

        // Only add game rule container in landscape mode
        infoContainer.addChild(gameRulesContainer);

        function showPayTableOrGameRules(page = 'paytable') {

            if (page !== 'paytable' && page !== 'game_rules') {
                throw new Error('invalid page');
            }


            pageContainer.visible = false;
            gameRulesContainer.visible = false;

            paytableButton.texture = loaded.selectedPaytableButtonOff.texture;
            gameRulesButton.texture = loaded.selectedPaytableButtonOff.texture;

            if (page === 'paytable') {
                isPaytable = true;

                paytableButton.texture = loaded.selectedPaytableButton.texture;

                pageContainer.visible = true;

                dotContainer.visible = (newOptions.pages.length > 1);
                gameRulesDotContainer.visible = false;

                if (newOptions.pages.length > 1) {
                    dotContainer.visible = true;
                    leftButton.alpha = 1;
                    leftButton.interactive = true;
                    rightButton.alpha = 1;
                    rightButton.interactive = true;
                }
                else {
                    dotContainer.visible = false;
                    leftButton.alpha = 0.5;
                    leftButton.interactive = false;
                    rightButton.alpha = 0.5;
                    rightButton.interactive = false;
                }

            } else if (page === 'game_rules') {
                isPaytable = false;

                gameRulesButton.texture = loaded.selectedPaytableButton.texture;

                gameRulesContainer.visible = true;

                dotContainer.visible = false;

                // navigation
                if (newOptions.gameRulesPages.length > 1) {
                    gameRulesDotContainer.visible = true;
                    leftButton.alpha = 1;
                    leftButton.interactive = true;
                    rightButton.alpha = 1;
                    rightButton.interactive = true;
                }
                else {
                    gameRulesDotContainer.visible = false;
                    leftButton.alpha = 0.5;
                    leftButton.interactive = false;
                    rightButton.alpha = 0.5;
                    rightButton.interactive = false;
                }
            }

        }

        function navigateToPage(page) {
            TweenMax.to(isPaytable ? pageContainer : gameRulesContainer, 0.5, {
                x: (-1280 * page) + 640,
            });

            infoContainer.updatePaginationIndicator();
        }

        // close info page when then background is clicked
        infoBackground.on('click', function () {
            infoContainer.hideInfoPage();
        });

        // Info Page Helpers

        infoContainer['showInfoPage'] = () => {
            infoContainer.visible = true;
        };

        infoContainer['hideInfoPage'] = () => {
            infoContainer.visible = false;

            // Inform game to visible panel
            infoContainer.emit('visiblePanel');
        };

        // Loop through each dot container and set current page
        infoContainer['updatePaginationIndicator'] = () => {
            each(isPaytable ? dotContainer.children : gameRulesDotContainer.children, (child, index) => {
                if (index === (isPaytable ? currentPage : currentGameRulesPage)) {
                    child.texture = loaded.selectPagePaytable.texture;
                }
                else {
                    child.texture = loaded.selectPagePaytableOff.texture;
                }
            });
        }
    }

    app.addComponent(infoContainer);

    return infoContainer;
}

export default {
    init,
}
