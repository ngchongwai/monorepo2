import api from './api';
import state from './state';
import panel from './panel';

let syncTimeout = null;

function start() {
    syncTimeout = setTimeout(() => {

        // stop sync if the flag shouldSync is already turn to false
        if (!state.get('shouldSync')) {
            return start();
        }

        api.sync().then((data) => {

            start();

            state.set('balance', data.balance);

            // prevent api come back after it passed the first check
            if (!state.get('shouldSync')) {
                return;
            }

            try {
                panel.updateText();
            } catch (e) {
                console.log('panel not init');
            }
        })
    }, 5000);
}

function setShouldSync(status = true) {
    state.set('shouldSync', status);
}

function enableSync() {
    setShouldSync(true);
}

function disableSync() {
    setShouldSync(false);
}

function stop() {
    clearTimeout(syncTimeout);
}

// enable sync when init
enableSync();

export default {
    start,
    stop,
    enableSync,
    disableSync,
}
