import * as PIXI from 'pixi.js';
import sound from './sound';
import app, { isMobile } from './app';

// whole particle emitters container
let particlesCoinsContainer;

// individual particle emitter
let bottomCenterEmitterCoin3;
let bottomCenterEmitterCoin2;
let bottomCenterEmitterCoin1;

// confetti emitter
let topCenterConfettiEmitterColor1;
let topCenterConfettiEmitterColor2;

let customCoin2Available;
let customCoin3Available;

function init(options) {
    let newOptions = Object.assign({
        confetti1Color1: 'ffffff', //"439ed7",
        confetti1Color2: 'ffffff', //"002672",
        confetti2Color1: 'ffffff', //"ffe69d",
        confetti2Color2: 'ffffff', //"d6c131",

        coinSpriteList: [
            "a_1.png",
            "a_2.png",
            "a_3.png",
            "a_4.png",
            "a_5.png",
            "a_6.png",
            "a_7.png",
            "a_8.png"
        ],

        customCoin2SpriteList: null,
        customCoin3SpriteList: null,
        coin1FrameRate: 20,
        coin2FrameRate: 20,
        coin3FrameRate: 20,

        confetti1SpriteList : [
            "0_0.png",
            "0_1.png",
            "0_2.png",
            "0_3.png",
            "0_4.png",
            "0_5.png",
            "0_6.png",
            "0_7.png",
            "0_8.png",
            "0_9.png",
            "0_10.png",
            "0_11.png",
        ],

        confetti2SpriteList : [
            "1_0.png",
            "1_1.png",
            "1_2.png",
            "1_3.png",
            "1_4.png",
            "1_5.png",
            "1_6.png",
            "1_7.png",
            "1_8.png",
            "1_9.png",
            "1_10.png",
            "1_11.png",
        ],
        confetti1FrameRate: 6,
        confetti2FrameRate: 6
    }, options);

    //custom coin checking
    customCoin2Available = (!!newOptions.customCoin2SpriteList);
    customCoin3Available = (!!newOptions.customCoin3SpriteList);

    particlesCoinsContainer = new PIXI.Container();
    particlesCoinsContainer.name = 'Particles Coin Container';

    let coinOption = {
        minRot: 240,
        maxRot: 300,
        startSpeed : isMobile() ? 3500 : 2500,
        endSpeed : 100,
        color1 : "ffffff",
        color2 : "ffffff",
        startScale: 0.5,
        endScale: 0.5,
        gravity: isMobile() ? 4100 : 2800
    };

    // bottom Center
    let bottomCenterContainerCoin3 = new PIXI.particles.ParticleContainer();
    bottomCenterContainerCoin3.x = isMobile() ? 320 : 640;
    bottomCenterContainerCoin3.y = isMobile() ? 1380 : 820;
    bottomCenterContainerCoin3.name = 'corner left container bronze';
    particlesCoinsContainer.addChild(bottomCenterContainerCoin3);

    bottomCenterEmitterCoin3 = new PIXI.particles.Emitter(
        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on corner of a bitmap, and not use the root stage Container
        bottomCenterContainerCoin3,

        // The collection of particle images to use
        [
            {
                framerate: newOptions.coin3FrameRate,
                loop: true,
                textures: newOptions.customCoin3SpriteList ? newOptions.customCoin3SpriteList : [
                    "bronze_1.png",
                    "bronze_2.png",
                    "bronze_3.png",
                    "bronze_4.png",
                    "bronze_5.png",
                    "bronze_6.png"
                ]
            }
        ],

        // Emitter configuration, edit this to change the look
        // of the emitter
        initConfig(coinOption)
    );
    bottomCenterEmitterCoin3.particleConstructor = PIXI.particles.AnimatedParticle;

    let bottomCenterContainerCoin2 = new PIXI.particles.ParticleContainer();
    bottomCenterContainerCoin2.x = isMobile() ? 320 : 640;
    bottomCenterContainerCoin2.y = isMobile() ? 1380 : 820;
    bottomCenterContainerCoin2.name = 'corner left container bronze';
    particlesCoinsContainer.addChild(bottomCenterContainerCoin2);

    bottomCenterEmitterCoin2 = new PIXI.particles.Emitter(
        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on corner of a bitmap, and not use the root stage Container
        bottomCenterContainerCoin2,

        // The collection of particle images to use
        [
            {
                framerate: newOptions.coin2FrameRate,
                loop: true,
                textures: newOptions.customCoin2SpriteList ? newOptions.customCoin2SpriteList : [
                    "silver_1.png",
                    "silver_2.png",
                    "silver_3.png",
                    "silver_4.png",
                    "silver_5.png",
                    "silver_6.png"
                ]
            }
        ],

        // Emitter configuration, edit this to change the look
        // of the emitter
        initConfig(coinOption)
    );
    bottomCenterEmitterCoin2.particleConstructor = PIXI.particles.AnimatedParticle;

    let bottomCenterContainerCoin1 = new PIXI.particles.ParticleContainer();
    bottomCenterContainerCoin1.x = isMobile() ? 320 : 640;
    bottomCenterContainerCoin1.y = isMobile() ? 1380 : 820;
    bottomCenterContainerCoin1.name = 'corner left container bronze';
    particlesCoinsContainer.addChild(bottomCenterContainerCoin1);

    bottomCenterEmitterCoin1 = new PIXI.particles.Emitter(
        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on corner of a bitmap, and not use the root stage Container
        bottomCenterContainerCoin1,

        // The collection of particle images to use
        [
            {
                framerate: newOptions.coin1FrameRate,
                loop: true,
                textures: newOptions.coinSpriteList
            }
        ],

        // Emitter configuration, edit this to change the look
        // of the emitter
        initConfig(coinOption)
    );
    bottomCenterEmitterCoin1.particleConstructor = PIXI.particles.AnimatedParticle;


    // Confetti emitter init
    let topCenterContainerColor1 = new PIXI.particles.ParticleContainer();
    topCenterContainerColor1.x = isMobile() ? 320 : 640;
    topCenterContainerColor1.y = -30;
    topCenterContainerColor1.name = 'corner left confetti';
    particlesCoinsContainer.addChild(topCenterContainerColor1);

    topCenterConfettiEmitterColor1 = new PIXI.particles.Emitter(
        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on corner of a bitmap, and not use the root stage Container
        topCenterContainerColor1,

        // The collection of particle images to use
        [
            {
                framerate: newOptions.confetti1FrameRate,
                loop: true,
                textures: newOptions.confetti1SpriteList
            }
        ],

        // Emitter configuration, edit this to change the look
        // of the emitter
        initConfig({
            minRot: 240,
            maxRot: 300,
            startSpeed : 1000,
            endSpeed : 600,
            color1 : newOptions.confetti1Color1,
            color2 : newOptions.confetti1Color2,
            startScale: 0.25,
            endScale: 0.25,
            gravity: 1000,
            lifeTime: isMobile() ? 2.5 : 2
        })
    );
    topCenterConfettiEmitterColor1.particleConstructor = PIXI.particles.AnimatedParticle;


    let topCenterContainerColor2 = new PIXI.particles.ParticleContainer();
    topCenterContainerColor2.x = isMobile() ? 320 : 640;
    topCenterContainerColor2.y = -30;
    topCenterContainerColor2.name = 'corner left confetti';
    particlesCoinsContainer.addChild(topCenterContainerColor2);

    topCenterConfettiEmitterColor2 = new PIXI.particles.Emitter(
        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on corner of a bitmap, and not use the root stage Container
        topCenterContainerColor2,

        // The collection of particle images to use
        [
            {
                framerate: newOptions.confetti2FrameRate,
                loop: true,
                textures: newOptions.confetti2SpriteList
            }
        ],

        // Emitter configuration, edit this to change the look
        // of the emitter
        initConfig({
            minRot: 240,
            maxRot: 300,
            startSpeed : 1000,
            endSpeed : 600,
            color1 : newOptions.confetti2Color1,
            color2 : newOptions.confetti2Color2,
            startScale: 0.25,
            endScale: 0.25,
            gravity: 1000,
            lifeTime: isMobile() ? 2.5 : 2
        })
    );
    topCenterConfettiEmitterColor2.particleConstructor = PIXI.particles.AnimatedParticle;

    // set all particle to be no interactive
    particlesCoinsContainer.interactiveChildren = false;

    //add to stage
    app.addComponent(particlesCoinsContainer);

    // Calculate the current time
    let elapsed = Date.now();

// Update function every frame
    let update = function () {

        // Update the next frame
        requestAnimationFrame(update);

        let now = Date.now();

        // The emitter requires the elapsed
        bottomCenterEmitterCoin3.update((now - elapsed) * 0.001);
        bottomCenterEmitterCoin2.update((now - elapsed) * 0.001);
        bottomCenterEmitterCoin1.update((now - elapsed) * 0.001);

        topCenterConfettiEmitterColor1.update((now - elapsed) * 0.001);
        topCenterConfettiEmitterColor2.update((now - elapsed) * 0.001);

        elapsed = now;
    };

    // scorner emitting at 1st
    bottomCenterEmitterCoin3.emit = false;
    bottomCenterEmitterCoin3.autoUpdate = true;
    bottomCenterEmitterCoin2.emit = false;
    bottomCenterEmitterCoin2.autoUpdate = true;
    bottomCenterEmitterCoin1.emit = false;
    bottomCenterEmitterCoin1.autoUpdate = true;

    topCenterConfettiEmitterColor1.emit = false;
    topCenterConfettiEmitterColor1.autoUpdate = true;
    topCenterConfettiEmitterColor2.emit = false;
    topCenterConfettiEmitterColor2.autoUpdate = true;

    // Start the update
    update();
}

function emitCoins(type = '') {
    sound.play('coinSound');
    sound.play('coinFlipSound');
    switch (type) {
        case 'big':
            // bottomCenterEmitterCoin2.frequency = 0.1;
            // bottomCenterEmitterCoin2.emit = true;
            bottomCenterEmitterCoin1.frequency = 0.05;
            bottomCenterEmitterCoin1.emit = true;
            break;
        case 'extra_big':
            bottomCenterEmitterCoin1.frequency = 0.02;
            bottomCenterEmitterCoin1.emit = true;
            break;
        case 'mega':
            bottomCenterEmitterCoin3.frequency = 0.16;
            bottomCenterEmitterCoin3.emit = customCoin3Available;
            bottomCenterEmitterCoin2.frequency = customCoin3Available ? 0.16 : 0.08;
            bottomCenterEmitterCoin2.emit = customCoin2Available;
            bottomCenterEmitterCoin1.frequency = customCoin2Available ? 0.05 : 0.01;
            bottomCenterEmitterCoin1.emit = true;

            topCenterConfettiEmitterColor1.frequency = 0.01;
            topCenterConfettiEmitterColor1.emit = true;
            topCenterConfettiEmitterColor2.frequency = 0.03;
            topCenterConfettiEmitterColor2.emit = true;

            break;
        default:
            throw new Error('invalid particle coins emit type');
    }
}

function stopAllEmit() {
    sound.stop('coinSound');
    sound.stop('coinFlipSound');
    sound.play('coinFlipSoundEnd');

    bottomCenterEmitterCoin3.emit = false;
    bottomCenterEmitterCoin2.emit = false;
    bottomCenterEmitterCoin1.emit = false;

    topCenterConfettiEmitterColor1.emit = false;
    topCenterConfettiEmitterColor2.emit = false;
}

function initConfig(options) {
    let newOptions = Object.assign({
        minRot: 0,
        maxRot: 0,
        startSpeed : 0,
        endSpeed : 0,
        color1 : "ffffff",
        color2 : "ffffff",
        startScale : 0.7,
        endScale : 1.0,
        gravity: 2000,
        lifeTime: 2
    }, options);
    return {
        scale: {
            list: [
                {
                    value: newOptions.startScale,
                    time: 0
                },
                {
                    value: newOptions.endScale,
                    time: 1
                }
            ],
            isStepped: false,
        },
        color: {
            list: [
                {
                    value: newOptions.color1,
                    time: 0
                },
                {
                    value: newOptions.color2,
                    time: 1
                }
            ],
            isStepped: false
        },
        acceleration: {
            x: 0,
            y: newOptions.gravity
        },
        speed: {
            start: newOptions.startSpeed,
            end: newOptions.endSpeed,
            minimumSpeedMultiplier: 0.5,
            isStepped: false
        },
        startRotation: {
            min: newOptions.minRot,
            max: newOptions.maxRot
        },
        rotationSpeed: {
            min: 0,
            max: 200
        },
        lifetime: {
            min: newOptions.lifeTime,
            max: newOptions.lifeTime
        },
        frequency: 0.1,
        emitterLifetime: -1,
        maxParticles: 200,
        pos: {
            x: 0,
            y: 0
        },
        blendMode: 'normal',
        addAtBack: false,
        spawnType: "point",
    }
}

export default {
    init,
    emitCoins,
    stopAllEmit
}