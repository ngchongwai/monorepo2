import app from './app';
import * as PIXI from 'pixi.js';
import { each } from 'lodash';

let extraInfo;
let backingStatus;
let backingImage;

function init(posX, posY, options = null, backing = false) {
    backingStatus = backing;

    let fontOptions = Object.assign({
        fontSize: 15,
        fill: 0xffffff,
        fontWeight: 'bold',
        stroke: "0xffffff",
        strokeThickness: 0
    }, options);

    if (backingStatus) {
        backingImage = new PIXI.Sprite(PIXI.Texture.WHITE);
        backingImage.tint = 0x000000;
        backingImage.alpha = 0.8;
        backingImage.width = 0;
        backingImage.height = 0;
        app.addComponent(backingImage);
    }

    extraInfo = new PIXI.Text('', fontOptions);
    extraInfo.anchor.set(0, 0.5);
    extraInfo.x = posX;
    extraInfo.y = posY;

    app.addComponent(extraInfo);
}

function updateExtraInfo(codes) {
    if (typeof codes !== "object") {
        throw new Error('Transaction code input need to be an object');
    }

    let temp = '';

    each(codes, (code, index) => {
        if (index !== 'is_pending' && index !== 'triggering_transaction_id') {
            temp = temp.concat(`${index}: ` + code + ' ');
        }
    });

    extraInfo.text = temp;

    if (backingStatus) {
        backingImage.x = extraInfo.x - 5;
        backingImage.y = extraInfo.y - extraInfo.height / 2 - 2.5;
        backingImage.width = extraInfo.width + 10;
        backingImage.height = extraInfo.height + 5;
    }
}

export default {
    init,
    updateExtraInfo,
}