import NoSleep from 'nosleep.js';
import * as PIXI from 'pixi.js';
import { each } from 'lodash';

import app from './app';
import ui from './ui';
import panel from './panel';

let loaded = PIXI.loader.resources;

let sessionDuration = 1000 * 60 * 5;
let noSleep = new NoSleep();
let timer;

function renewSession() {
    noSleep.noSleepVideo.muted = true;
    if (typeof timer !== 'undefined') {
        noSleep.enable();
        timer = clearTimeout(timer);
    }

    timer = setTimeout(timeout, sessionDuration);
}

function timeout() {

    if (panel.getAutoSpinRunning()){
        renewSession();
    }else{
        noSleep.disable();
        if (app.isDemoMode()) {
            ui.showDialog(loaded.languageFile.data.session_end_demo, '', loaded.languageFile.data.play_for_real, {
                onPointerUp: () => {
                    // let realGameURL = window.location.href;
                    // realGameURL = realGameURL.split('&');
                    // each(realGameURL, (text, index) => {
                    //     if (text === 'isDemo=true') {
                    //         realGameURL.splice(index, 1);
                    //     }
                    // });
                    // realGameURL = realGameURL.join('&');
                    //
                    // window.open(realGameURL, '_self');
                    window.location.reload();
                }
            });
        }
        else {
            ui.showDialog(loaded.languageFile.data.session_end, '', loaded.languageFile.data.okay, {
                onPointerUp: () => {
                    window.location.reload();
                }
            });
        }
    }


}

function init() {
    window.addEventListener('touchstart', renewSession, false);
    window.addEventListener('click', renewSession, false);
    window.addEventListener('keypress', renewSession, false);
    renewSession();
}

export default {
    init,
    renewSession,
}
