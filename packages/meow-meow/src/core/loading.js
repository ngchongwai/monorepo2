import {isMobile} from './app';
import app from './app';
import * as PIXI from "pixi.js";

let loader = PIXI.loader;
let loadingContainer = new PIXI.Container();
let loadingSprite;
let loadingMask;
let loadingEmptyBar;
let loadingBar;

function start(options) {

    loadingSprite = new PIXI.Sprite(PIXI.Texture.from('./assets/core/logo.png'));
    loadingSprite.anchor.set(0.5);
    // loadingSprite.scale.set(1.5);
    loadingSprite.x = app.getResolutionX() / 2;
    loadingSprite.y = app.getResolutionY() / 2 - 50;

    // let rectMask = new PIXI.Graphics().beginFill(0xFFFFFF).drawRect(200, 100, 900, 490).endFill();
    loadingMask = new PIXI.Sprite(PIXI.Texture.from('./assets/core/loading_bar_masking.png'));
    loadingMask.anchor.set(0.5);
    loadingMask.scale.set(1.25, 1);
    loadingMask.x = app.getResolutionX() / 2;
    loadingMask.y = app.getResolutionY() / 2 + 50;

    loadingEmptyBar = new PIXI.Sprite(PIXI.Texture.from('./assets/core/loading_bar.png'));
    loadingEmptyBar.anchor.set(0.5);
    loadingEmptyBar.scale.set(1.25, 1);
    loadingEmptyBar.x = app.getResolutionX() / 2;
    loadingEmptyBar.y = app.getResolutionY() / 2 + 50;

    loadingBar = new PIXI.Sprite(PIXI.Texture.from('./assets/core/loading_bar_blue.png'));
    loadingBar.anchor.set(0.5);
    loadingBar.scale.set(1.25, 1);
    loadingBar.x = 0;
    loadingBar.y = app.getResolutionY() / 2 + 50;
    loadingBar.mask = loadingMask;

    loadingContainer.addChild(loadingSprite);
    loadingContainer.addChild(loadingEmptyBar);
    loadingContainer.addChild(loadingBar);
    loadingContainer.addChild(loadingMask);
    app.addComponent(loadingContainer);
}

function updateProgress() {
    loadingBar.x = (app.getResolutionX() / 2 - loadingBar.width) + loadingBar.width * (loader.progress / 100);
}

function destroy(){
    loadingContainer.destroy();
}

export default {
    start,
    updateProgress,
    destroy
}