import * as PIXI from 'pixi.js';
import { each } from 'lodash';

import app, { isMobile } from './app';
import state from './state';
import ui from './ui';
import sound from './sound';
import Big from 'big.js';
import utils from './utils';

const loaded = PIXI.loader.resources;

let autoSpinPanel;
let autoSpinExit;
let autoSpinTitle;
let stopAutoSpinLabel;
let stopAutoSpinLabelBox;
let stopAutoSpinLabelMiniBox;
let stopSingleWinLabel;
let stopSingleWinLabelBox;
let stopSingleWinLabelMiniBox;
let stopCashIncreaseLabel;
let stopCashIncreaseLabelBox;
let stopCashIncreaseLabelMiniBox;
let stopCashDecreaseLabel;
let stopCashDecreaseLabelBox;
let stopCashDecreaseLabelMiniBox;
let exitAutoSpinPanelArea;

let numberOfSpinsLabel;
let numberOfSpinsLabelBox;
let numberOfSpinsLabelMiniBox;

let autoSpinSlider1Placeholder;
let autoSpinSlider2Placeholder;
let autoSpinSlider3Placeholder;

let stopSingleWinLabelToggle;
let stopCashIncreaseLabelToggle;
let stopCashDecreaseLabelToggle;

let autoSpinBackground;

let sliderOptions = [
    0,
    10,
    15,
    20,
    25,
    30,
    35,
    40,
    45,
    50,
    55,
    60,
    65,
    70,
    75,
    80,
    85,
    90,
    95,
    100
];

//! Sliders
let singleWinSlider;
let cashIncreaseSlider;
let cashDecreaseSlider;

//! Start Button
let autoSpinStartButton;

let autoSpinValue = 0;
let autoSpinOption = 'off';
let autoSpinFunction;
let autoSpinPanelOpened = false;
let autoSpinData = {
    numbered: 0,
    winningConditions: 0,
    singleWin: 0,
    cashIncrease: 0,
    cashDecrease: 0,
};
let usingBetline;
let autoSpinState;

//! Containers
let numberOfSpinsButtonGroup = new PIXI.Container();
let stopAutoPlayButtonGroup = new PIXI.Container();
let singleWinContainer = new PIXI.Container();
let cashIncreaseContainer = new PIXI.Container();
let cashDecreaseContainer = new PIXI.Container();
let stopAutoplayOnContainer = new PIXI.Container();


function init(spinFunction, gotBonus, betLinesUsage, autoSpinOption) {
    usingBetline = betLinesUsage;
    autoSpinBackground = new PIXI.Sprite(loaded.balanceBox.texture);
    autoSpinBackground.width = app.getResolutionX();
    autoSpinBackground.height = app.getResolutionY();
    autoSpinBackground.alpha = 0;
    autoSpinBackground.interactive = false;
    autoSpinBackground.on('pointerup', function () {
        autoSpinPanelShowHide();
    });
    app.addComponent(autoSpinBackground);

    autoSpinState = Object.assign({
        singleWinMultiplier: 10,
        singleWinBaseNumber: 0,
    }, autoSpinOption);

    autoSpinFunction = spinFunction;
    let bigTotalBet = new Big(utils.getTotalBet());
    let autoSpinParameter = parseFloat(bigTotalBet.times(10)).toFixed(2);
    state.set('cashIncrease', autoSpinParameter);
    state.set('singleWin', autoSpinParameter);
    state.set('cashDecrease', autoSpinParameter);

    autoSpinPanel = new PIXI.Sprite(loaded.autoSpinPanel.texture);
    autoSpinPanel.name = 'Auto Spin Panel';
    autoSpinPanel.anchor.set(0.5, 0.5);
    autoSpinPanel.interactive = true;
    autoSpinPanel.x = (app.getResolutionX() / 2);
    autoSpinPanel.y = isMobile() ? 780 : (app.getResolutionY() * 2);

    autoSpinTitle = ui.createText({
        fontSize: isMobile() ? 45 : 20,
        fontWeight: isMobile() ? 'bold' : '700'
    }, loaded.languageFile.data.auto_spin);
    autoSpinTitle.anchor.set(1, 1);
    autoSpinTitle.x = isMobile() ? autoSpinTitle.width / 2 : -63;
    autoSpinTitle.y = isMobile() ? -415 : -227;
    autoSpinPanel.addChild(autoSpinTitle);


    //#region Any Win Button & Label
    let stopAutoSpinLabelToggle = false;
    stopAutoSpinLabel = ui.createTextButton({
        fontWeight: '100',
        fontSize: isMobile() ? 26 : 15,
        centerAnchor: false,
        onPointerUp: () => {
            if (autoSpinData.winningConditions === 0) {
                stopAutoPlayButtonGroup.children[0].texture = loaded.radioButtonBackgroundAnyOn.texture;
                stopAutoPlayButtonGroup.children[0].selected = true;
                stopAutoPlayButtonGroup.emit('valueChanged', 'anyWin');
            }
            else if (stopAutoSpinLabelToggle) {
                stopAutoSpinLabelBox.alpha = 1;
                stopAutoSpinLabelMiniBox.alpha = 0;
                each(stopAutoPlayButtonGroup.children, (button) => {
                    button.texture = loaded.radioButtonBackgroundAnyOff.texture;
                    button.selected = false;
                });
                stopAutoSpinLabelToggle = false;
                autoSpinData.winningConditions = 0;
            } else if (!stopAutoSpinLabelToggle) {
                stopAutoSpinLabelBox.alpha = 0;
                stopAutoSpinLabelMiniBox.alpha = 1;
                stopAutoSpinLabelToggle = true;
            }
            sound.play('buttonClickReleaseSound');
            autoSpinPanel.emit('autoSpinUpdated', autoSpinData);
        },
        onPointerDown: () => {
            sound.play('buttonClickSound');
        },
    }, loaded.languageFile.data.stop_auto_play_on);
    stopAutoSpinLabel.anchor.set(0, 0.5);
    stopAutoSpinLabel.x = isMobile() ? -205 : -111;
    stopAutoSpinLabel.y = isMobile() ? -335 : -167;
    if (isMobile())
        stopAutoSpinLabel.hitArea = new PIXI.Rectangle(-70, -25, 280, 50);
    else
        stopAutoSpinLabel.hitArea = new PIXI.Rectangle(-50, -10, 175, 20);

    stopAutoSpinLabelBox = new PIXI.Sprite(loaded.autoSpinLabelBox.texture);
    stopAutoSpinLabelBox.anchor.set(1, 1);
    stopAutoSpinLabelBox.x = isMobile() ? -222 : -123;
    stopAutoSpinLabelBox.y = isMobile() ? -325 : -157;

    stopAutoSpinLabelMiniBox = new PIXI.Sprite(loaded.autoSpinLabelMiniBox.texture);
    stopAutoSpinLabelMiniBox.anchor.set(1, 1);
    stopAutoSpinLabelMiniBox.x = isMobile() ? -222 : -123;
    stopAutoSpinLabelMiniBox.y = isMobile() ? -325 : -157;
    stopAutoSpinLabelMiniBox.alpha = 0;

    stopAutoplayOnContainer.addChild(stopAutoSpinLabel);
    autoSpinPanel.addChild(stopAutoSpinLabelBox);
    autoSpinPanel.addChild(stopAutoSpinLabelMiniBox);

    stopAutoPlayButtonGroup = ui.createRadioGroup({
        offset: isMobile() ? 17 : 21,
        options: gotBonus ? [
            {label: loaded.languageFile.data.any_win, value: 'anyWin'},
            {label: loaded.languageFile.data.winning_bonus, value: 'bonusGame'},
        ] : [
            {label: loaded.languageFile.data.any_win, value: 'anyWin'},
        ],
        fontSize: isMobile() ? 28 : 14,
        onTexture: loaded.radioButtonBackgroundAnyOn.texture,
        offTexture: loaded.radioButtonBackgroundAnyOff.texture,
        sound: sound.get('buttonClickSound'),
        soundRelease: sound.get('buttonClickReleaseSound'),
    });
    // each(stopAutoPlayButtonGroup.children, (button) => {
    //     button.scale.x = isMobile() ? 1.1 : 1.0;
    //     button.scale.y = isMobile() ? 1.1 : 1.0;
    //     button.children[0].scale.x = isMobile() ? 1 : 1.0;
    // });
    stopAutoPlayButtonGroup.x = isMobile() ? -95 : -50;
    stopAutoPlayButtonGroup.y = isMobile() ? -268 : -133;
    stopAutoPlayButtonGroup.on('valueChanged', (data) => {
        if (data === 0) {
            stopAutoSpinLabelBox.alpha = 1;
            stopAutoSpinLabelMiniBox.alpha = 0;
            stopAutoSpinLabelToggle = false;
        } else {
            stopAutoSpinLabelBox.alpha = 0;
            stopAutoSpinLabelMiniBox.alpha = 1;
            stopAutoSpinLabelToggle = true;
        }
        autoSpinData.winningConditions = data;
        autoSpinPanel.emit('autoSpinUpdated', autoSpinData);
    });
    // stopAutoPlayButtonGroup.scale.set(isMobile() ? 1.4 : 1);

    stopAutoplayOnContainer.name = 'Stop Autoplay On Container';
    stopAutoplayOnContainer.alpha = 1;

    stopAutoplayOnContainer.addChild(stopAutoPlayButtonGroup);
    autoSpinPanel.addChild(stopAutoplayOnContainer);
    //#endregion

    //#region Single Win Label, Display & Slider
    stopSingleWinLabelToggle = false;
    stopSingleWinLabel = ui.createTextButton({
        fontWeight: '100',
        fontSize: isMobile() ? 26 : 15,
        centerAnchor: false,
        onPointerUp: () => {
            if (stopSingleWinLabelToggle) {
                singleWinSlider.setToIndex(0);
                stopSingleWinLabelBox.alpha = 1;
                stopSingleWinLabelMiniBox.alpha = 0;
                autoSpinSlider1Placeholder.alpha = 0;
                singleWinSlider.alpha = 0.25;

                stopSingleWinLabelToggle = false;
                autoSpinData.singleWin = 0;
            } else {
                singleWinSlider.setToIndex(0);
                stopSingleWinLabelBox.alpha = 0;
                stopSingleWinLabelMiniBox.alpha = 1;
                autoSpinSlider1Placeholder.alpha = 1;
                singleWinSlider.alpha = 1;

                stopSingleWinLabelToggle = true;
            }
            sound.play('buttonClickReleaseSound');
            autoSpinPanel.emit('autoSpinUpdated', autoSpinData);
            updateText();
        },
        onPointerDown: () => {
            sound.play('buttonClickSound');
        },
    }, loaded.languageFile.data.stop_if_single_win_exceeds);
    stopSingleWinLabel.anchor.set(0, 0.5);
    stopSingleWinLabel.x = isMobile() ? -205 : -111;
    stopSingleWinLabel.y = isMobile() ? -193 : -90;
    if (isMobile())
        stopSingleWinLabel.hitArea = new PIXI.Rectangle(-70, -25, 380, 50);
    else
        stopSingleWinLabel.hitArea = new PIXI.Rectangle(-50, -10, 230, 20);

    stopSingleWinLabelBox = new PIXI.Sprite(loaded.autoSpinLabelBox.texture);
    stopSingleWinLabelBox.anchor.set(1, 1);
    stopSingleWinLabelBox.x = isMobile() ? -222 : -123;
    stopSingleWinLabelBox.y = isMobile() ? -180 : -81;

    stopSingleWinLabelMiniBox = new PIXI.Sprite(loaded.autoSpinLabelMiniBox.texture);
    stopSingleWinLabelMiniBox.anchor.set(1, 1);
    stopSingleWinLabelMiniBox.x = isMobile() ? -222 : -123;
    stopSingleWinLabelMiniBox.y = isMobile() ? -180 : -81;
    stopSingleWinLabelMiniBox.alpha = 0;

    singleWinContainer.addChild(stopSingleWinLabel);
    autoSpinPanel.addChild(stopSingleWinLabelBox);
    autoSpinPanel.addChild(stopSingleWinLabelMiniBox);

    singleWinSlider = ui.createSlider({
        options: sliderOptions,
        sound: sound.get('buttonClickSound'),
        soundRelease: sound.get('buttonClickReleaseSound'),
    });
    singleWinSlider.x = isMobile() ? -200 : -103;
    singleWinSlider.y = isMobile() ? -135 : -58;
    singleWinSlider.alpha = 0.25;

    singleWinSlider.on('valueUpdated', (data) => {
        state.set('singleWin', data);
        updateText();

        singleWinSlider.alpha = 1;
        stopSingleWinLabelBox.alpha = 0;
        stopSingleWinLabelMiniBox.alpha = 1;
        autoSpinSlider1Placeholder.alpha = 1;
        autoSpinData.singleWin = data;
        stopSingleWinLabelToggle = true;
        autoSpinPanel.emit('autoSpinUpdated', autoSpinData);
    });
    autoSpinPanel.addChild(singleWinSlider);
    autoSpinSlider1Placeholder = ui.createText({
        text: "$" + state.get('singleWin'),
        fontSize: isMobile() ? 25 : 15,
    });
    autoSpinSlider1Placeholder.anchor.set(0.5, 0.5);
    autoSpinSlider1Placeholder.x = isMobile() ? 190 : 123;
    autoSpinSlider1Placeholder.y = isMobile() ? -190 : -90;
    autoSpinSlider1Placeholder.alpha = 0;

    autoSpinPanel.addChild(autoSpinSlider1Placeholder);

    singleWinContainer.name = 'Single Win Container';
    singleWinContainer.alpha = 1;

    singleWinContainer.addChild(singleWinSlider);
    singleWinContainer.addChild(autoSpinSlider1Placeholder);
    autoSpinPanel.addChild(singleWinContainer);
    //#endregion

    //#region Cash Increase Label, Display & Slider
    stopCashIncreaseLabelToggle = false;
    stopCashIncreaseLabel = ui.createTextButton({
        fontWeight: '100',
        fontSize: isMobile() ? 26 : 15,
        centerAnchor: false,
        onPointerUp: () => {
            if (stopCashIncreaseLabelToggle) {
                cashIncreaseSlider.setToIndex(0);
                stopCashIncreaseLabelBox.alpha = 1;
                stopCashIncreaseLabelMiniBox.alpha = 0;
                autoSpinSlider2Placeholder.alpha = 0;
                cashIncreaseSlider.alpha = 0.25;

                stopCashIncreaseLabelToggle = false;
                autoSpinData.cashIncrease = 0;
            } else {
                cashIncreaseSlider.setToIndex(0);
                stopCashIncreaseLabelBox.alpha = 0;
                stopCashIncreaseLabelMiniBox.alpha = 1;
                autoSpinSlider2Placeholder.alpha = 1;
                cashIncreaseSlider.alpha = 1;

                stopCashIncreaseLabelToggle = true;
            }
            sound.play('buttonClickReleaseSound');
            autoSpinPanel.emit('autoSpinUpdated', autoSpinData);
            updateText();
        },
        onPointerDown: () => {
            sound.play('buttonClickSound');
        },
    }, loaded.languageFile.data.stop_if_cash_increase_by);
    stopCashIncreaseLabel.anchor.set(0, 0.5);
    stopCashIncreaseLabel.x = isMobile() ? -205 : -112;
    stopCashIncreaseLabel.y = isMobile() ? -70 : -20;
    if (isMobile())
        stopCashIncreaseLabel.hitArea = new PIXI.Rectangle(-70, -25, 380, 50);
    else
        stopCashIncreaseLabel.hitArea = new PIXI.Rectangle(-50, -10, 230, 20);

    stopCashIncreaseLabelBox = new PIXI.Sprite(loaded.autoSpinLabelBox.texture);
    stopCashIncreaseLabelBox.anchor.set(1, 1);
    stopCashIncreaseLabelBox.x = isMobile() ? -222 : -123;
    stopCashIncreaseLabelBox.y = isMobile() ? -58 : -11;

    stopCashIncreaseLabelMiniBox = new PIXI.Sprite(loaded.autoSpinLabelMiniBox.texture);
    stopCashIncreaseLabelMiniBox.anchor.set(1, 1);
    stopCashIncreaseLabelMiniBox.x = isMobile() ? -222 : -123;
    stopCashIncreaseLabelMiniBox.y = isMobile() ? -58 : -11;
    stopCashIncreaseLabelMiniBox.alpha = 0;

    cashIncreaseContainer.addChild(stopCashIncreaseLabel);
    autoSpinPanel.addChild(stopCashIncreaseLabelBox);
    autoSpinPanel.addChild(stopCashIncreaseLabelMiniBox);

    cashIncreaseSlider = ui.createSlider({
        options: sliderOptions,
        sound: sound.get('buttonClickSound'),
        soundRelease: sound.get('buttonClickReleaseSound'),
    });
    cashIncreaseSlider.x = isMobile() ? -200 : -103;
    cashIncreaseSlider.y = isMobile() ? -20 : 12;
    cashIncreaseSlider.alpha = 0.25;

    cashIncreaseSlider.on('valueUpdated', (data) => {
        if (state.get('cashIncrease') > data || state.get('cashIncrease') < data) {
            state.set('cashIncrease', data);
            updateText();
        }
        cashIncreaseSlider.alpha = 1;
        stopCashIncreaseLabelBox.alpha = 0;
        stopCashIncreaseLabelMiniBox.alpha = 1;
        autoSpinSlider2Placeholder.alpha = 1;
        autoSpinData.cashIncrease = data;
        stopCashIncreaseLabelToggle = true;
        autoSpinPanel.emit('autoSpinUpdated', autoSpinData);
    });

    autoSpinSlider2Placeholder = ui.createText({
        text: "$" + state.get('cashIncrease'),
        fontSize: isMobile() ? 25 : 15,
    });
    autoSpinSlider2Placeholder.anchor.set(0.5, 0.5);
    autoSpinSlider2Placeholder.x = isMobile() ? 190 : 123;
    autoSpinSlider2Placeholder.y = isMobile() ? -67 : -20;
    autoSpinSlider2Placeholder.alpha = 0;

    cashIncreaseContainer.name = 'Cash Increase Container';
    cashIncreaseContainer.alpha = 1;

    cashIncreaseContainer.addChild(cashIncreaseSlider);
    cashIncreaseContainer.addChild(autoSpinSlider2Placeholder);
    autoSpinPanel.addChild(cashIncreaseContainer);
    //#endregion

    //#region Cash Decrease Label, Display & Slider
    stopCashDecreaseLabelToggle = false;
    stopCashDecreaseLabel = ui.createTextButton({
        fontWeight: '100',
        fontSize: isMobile() ? 26 : 15,
        centerAnchor: false,
        onPointerUp: () => {
            if (stopCashDecreaseLabelToggle) {
                cashDecreaseSlider.setToIndex(0);
                stopCashDecreaseLabelBox.alpha = 1;
                stopCashDecreaseLabelMiniBox.alpha = 0;
                autoSpinSlider3Placeholder.alpha = 0;
                cashDecreaseSlider.alpha = 0.25;

                stopCashDecreaseLabelToggle = false;
                autoSpinData.cashDecrease = 0;
            } else {
                cashDecreaseSlider.setToIndex(0);
                stopCashDecreaseLabelBox.alpha = 0;
                stopCashDecreaseLabelMiniBox.alpha = 1;
                autoSpinSlider3Placeholder.alpha = 1;
                cashDecreaseSlider.alpha = 1;

                stopCashDecreaseLabelToggle = true;
            }
            sound.play('buttonClickReleaseSound');
            autoSpinPanel.emit('autoSpinUpdated', autoSpinData);
            updateText();
        },
        onPointerDown: () => {
            sound.play('buttonClickSound');
        },
    }, loaded.languageFile.data.stop_if_cash_decrease_by);
    stopCashDecreaseLabel.anchor.set(0, 0.5);
    stopCashDecreaseLabel.x = isMobile() ? -205 : -111;
    stopCashDecreaseLabel.y = isMobile() ? 43 : 49;
    if (isMobile())
        stopCashDecreaseLabel.hitArea = new PIXI.Rectangle(-70, -25, 380, 50);
    else
        stopCashDecreaseLabel.hitArea = new PIXI.Rectangle(-50, -10, 230, 20);

    stopCashDecreaseLabelBox = new PIXI.Sprite(loaded.autoSpinLabelBox.texture);
    stopCashDecreaseLabelBox.anchor.set(1, 1);
    stopCashDecreaseLabelBox.x = isMobile() ? -222 : -123;
    stopCashDecreaseLabelBox.y = isMobile() ? 55 : 59;

    stopCashDecreaseLabelMiniBox = new PIXI.Sprite(loaded.autoSpinLabelMiniBox.texture);
    stopCashDecreaseLabelMiniBox.anchor.set(1, 1);
    stopCashDecreaseLabelMiniBox.x = isMobile() ? -222 : -123;
    stopCashDecreaseLabelMiniBox.y = isMobile() ? 55 : 59;
    stopCashDecreaseLabelMiniBox.alpha = 0;

    cashDecreaseContainer.addChild(stopCashDecreaseLabel);
    autoSpinPanel.addChild(stopCashDecreaseLabelBox);
    autoSpinPanel.addChild(stopCashDecreaseLabelMiniBox);

    cashDecreaseSlider = ui.createSlider({
        options: sliderOptions,
        sound: sound.get('buttonClickSound'),
        soundRelease: sound.get('buttonClickReleaseSound'),
    });
    cashDecreaseSlider.x = isMobile() ? -200 : -103;
    cashDecreaseSlider.y = isMobile() ? 94 : 83;
    cashDecreaseSlider.alpha = 0.25;

    cashDecreaseSlider.on('valueUpdated', (data) => {
        if (state.get('cashDecrease') > data || state.get('cashDecrease') < data) {
            state.set('cashDecrease', data);
            updateText();
        }
        cashDecreaseSlider.alpha = 1;
        stopCashDecreaseLabelBox.alpha = 0;
        stopCashDecreaseLabelMiniBox.alpha = 1;
        autoSpinSlider3Placeholder.alpha = 1;
        autoSpinData.cashDecrease = data;
        stopCashDecreaseLabelToggle = true;
        autoSpinPanel.emit('autoSpinUpdated', autoSpinData);
    });

    autoSpinSlider3Placeholder = ui.createText({
        text: "$" + state.get('cashDecrease'),
        fontSize: isMobile() ? 25 : 15,
    });
    autoSpinSlider3Placeholder.anchor.set(0.5, 0.5);
    autoSpinSlider3Placeholder.x = isMobile() ? 190 : 123;
    autoSpinSlider3Placeholder.y = isMobile() ? 47 : 48;
    autoSpinSlider3Placeholder.alpha = 0;

    cashDecreaseContainer.name = 'Cash Decrease Container';
    cashDecreaseContainer.alpha = 1;

    cashDecreaseContainer.addChild(cashDecreaseSlider);
    cashDecreaseContainer.addChild(autoSpinSlider3Placeholder);
    autoSpinPanel.addChild(cashDecreaseContainer);
    //#endregion

    // //#region Auto Pay Exit
    autoSpinExit = ui.createButton({texture: isMobile() ? loaded.button_slidedown.texture : loaded.exitButton.texture});
    autoSpinExit.anchor.set(1, 1);
    autoSpinExit.scale.set(isMobile() ? 1 : 0.4);
    autoSpinExit.x = isMobile() ? -220 : 159;
    autoSpinExit.y = isMobile() ? -390 : -220;
    autoSpinExit.on('pointerup', function () {
        sound.play('buttonClickReleaseSound');
        autoSpinPanelShowHide();
    });
    autoSpinExit.on('pointerdown', function () {
        sound.play('buttonClickSound');
    });
    autoSpinExit.name = 'autoSpinExit';
    autoSpinPanel.addChild(autoSpinExit);


    exitAutoSpinPanelArea = ui.createButton({
        texture: loaded.spinAreaSlot ? loaded.spinAreaSlot.texture : PIXI.Texture.EMPTY,
        // texture: loaded.spinAreaSlot.texture,
        onPointerUp: () => {
            autoSpinPanelShowHide()
        }
    });
    exitAutoSpinPanelArea.anchor.set(0, 0);
    exitAutoSpinPanelArea.x = 0;
    exitAutoSpinPanelArea.y = app.getResolutionY();
    exitAutoSpinPanelArea.width = app.getResolutionX();
    exitAutoSpinPanelArea.height = app.getResolutionY();
    exitAutoSpinPanelArea.interactive = false;
    exitAutoSpinPanelArea.buttonMode = false;
    exitAutoSpinPanelArea.name = 'Exit Auto Spin Panel Area';

    //#endregion

    //#region Number of Autospin Labels an Radio Group
    let numberOfSpinsLabelToggle = false;
    numberOfSpinsLabel = ui.createTextButton({
        fontWeight: '100',
        fontSize: isMobile() ? 26 : 15,
        centerAnchor: false,
        onPointerUp: () => {
            if (autoSpinData.numbered === 0) {
                numberOfSpinsButtonGroup.children[0].texture = loaded.radioButtonBackgroundAutoSpinOn.texture;
                numberOfSpinsButtonGroup.children[0].selected = true;
                numberOfSpinsButtonGroup.emit('valueChanged', 10);
            }
            else if (numberOfSpinsLabelToggle) {
                numberOfSpinsLabelBox.alpha = 1;
                numberOfSpinsLabelMiniBox.alpha = 0;
                each(numberOfSpinsButtonGroup.children, (button) => {
                    button.texture = loaded.radioButtonBackgroundAutoSpinOff.texture;
                    button.selected = false;
                });
                numberOfSpinsLabelToggle = false;
                autoSpinData.numbered = 0;
            } else if (!numberOfSpinsLabelToggle) {
                numberOfSpinsLabelBox.alpha = 0;
                numberOfSpinsLabelMiniBox.alpha = 1;
                numberOfSpinsLabelToggle = true;
            }
            sound.play('buttonClickReleaseSound');
            autoSpinPanel.emit('autoSpinUpdated', autoSpinData);
        },
        onPointerDown: () => {
            sound.play('buttonClickSound');
        },
    }, loaded.languageFile.data.number_of_spins);
    numberOfSpinsLabel.anchor.set(0, 0.5);
    numberOfSpinsLabel.x = isMobile() ? -205 : -111;
    numberOfSpinsLabel.y = isMobile() ? 163 : 119;
    if (isMobile())
        numberOfSpinsLabel.hitArea = new PIXI.Rectangle(-70, -25, 280, 50);
    else
        numberOfSpinsLabel.hitArea = new PIXI.Rectangle(-50, -10, 175, 20);

    numberOfSpinsLabelBox = new PIXI.Sprite(loaded.autoSpinLabelBox.texture);
    numberOfSpinsLabelBox.anchor.set(1, 1);
    numberOfSpinsLabelBox.x = isMobile() ? -222 : -123;
    numberOfSpinsLabelBox.y = isMobile() ? 174 : 129;

    numberOfSpinsLabelMiniBox = new PIXI.Sprite(loaded.autoSpinLabelMiniBox.texture);
    numberOfSpinsLabelMiniBox.anchor.set(1, 1);
    numberOfSpinsLabelMiniBox.x = isMobile() ? -222 : -123;
    numberOfSpinsLabelMiniBox.y = isMobile() ? 174 : 129;
    numberOfSpinsLabelMiniBox.alpha = 0;

    autoSpinPanel.addChild(numberOfSpinsLabel);
    autoSpinPanel.addChild(numberOfSpinsLabelBox);
    autoSpinPanel.addChild(numberOfSpinsLabelMiniBox);

    numberOfSpinsButtonGroup = ui.createRadioGroup({
        offset: isMobile() ? 17 : 13,
        onTexture: loaded.radioButtonBackgroundAutoSpinOn.texture,
        offTexture: loaded.radioButtonBackgroundAutoSpinOff.texture,
        sound: sound.get('buttonClickSound'),
        soundRelease: sound.get('buttonClickReleaseSound'),
        options: [
            {label: '10', value: 10},
            {label: '30', value: 30},
            {label: '50', value: 50},
            {label: '100', value: 100},
        ],
        fontSize: isMobile() ? 28 : 14,
    });
    numberOfSpinsButtonGroup.x = isMobile() ? -155 : -82;
    numberOfSpinsButtonGroup.y = isMobile() ? 230 : 153;
    numberOfSpinsButtonGroup.on('valueChanged', (data) => {
        if (data === 0) {
            numberOfSpinsLabelBox.alpha = 1;
            numberOfSpinsLabelMiniBox.alpha = 0;
            numberOfSpinsLabelToggle = false;
        } else {
            numberOfSpinsLabelBox.alpha = 0;
            numberOfSpinsLabelMiniBox.alpha = 1;
            numberOfSpinsLabelToggle = true;
        }
        autoSpinData.numbered = data;
        autoSpinPanel.emit('autoSpinUpdated', autoSpinData);
    });
    autoSpinPanel.addChild(numberOfSpinsButtonGroup);
    //#endregion

    autoSpinStartButton = ui.createButton({
        fontSize: isMobile() ? 35 : 16,
        texture: isMobile() ? loaded.button_bet_confirm.texture : loaded.autoSpinPlayButton.texture,
        text: loaded.languageFile.data.confirmation,
        onPointerUp: () => {

            //make sure essential auto spin criteria is provided.
            if (isAutoSpinCriteriaAchieve())
                return;

            sound.play('buttonClickReleaseSound');
            callStartAutoSpin();
            autoSpinPanelShowHide();
        },
        onPointerDown: () => {
            sound.play('buttonClickSound');
            autoSpinStartButton.alpha = 0.5;
        },
    });
    autoSpinStartButton.interactive = false;
    autoSpinStartButton.buttonMode = true;
    autoSpinStartButton.alpha = 0.5;
    autoSpinStartButton.x = -autoSpinStartButton.width / 2;
    autoSpinStartButton.y = isMobile() ? autoSpinPanel.height / 2 - 130 : autoSpinPanel.height / 2 - 42 - autoSpinStartButton.height / 2;

    autoSpinPanel.addChild(autoSpinStartButton);

    app.addComponent(exitAutoSpinPanelArea);
    app.addComponent(autoSpinPanel);

    // updateSlider();
    return autoSpinPanel;
}

function callStartAutoSpin() {
    autoSpinPanel.emit('autoSpinStarted', autoSpinData);
}

function allowAutoSpinStartButton() {
    autoSpinStartButton.interactive = true;
    autoSpinStartButton.buttonMode = true;
    autoSpinStartButton.alpha = 1;
}

function cannotAutoSpinStartButton() {
    autoSpinStartButton.interactive = false;
    autoSpinStartButton.buttonMode = false;
    autoSpinStartButton.alpha = 0.25;
}

function autoSpinPanelShowHide() {
    if (!isMobile()) {
        autoSpinPanel.x = (app.getResolutionX() / 2);
        autoSpinPanel.y = (app.getResolutionY() / 2);
        if (autoSpinPanelOpened) {
            TweenMax.fromTo(autoSpinPanel.scale, 0.5, {x: 1, y: 1}, {x: 0, y: 0});
            TweenMax.fromTo(autoSpinBackground, 0.5, {alpha: 0.8}, {alpha: 0});
            autoSpinBackground.interactive = false;
            exitAutoSpinPanelArea.interactive = false;
            exitAutoSpinPanelArea.buttonMode = false;
        }
        else {
            TweenMax.fromTo(autoSpinPanel.scale, 0.5, {x: 0, y: 0}, {x: 1, y: 1});
            TweenMax.fromTo(autoSpinBackground, 0.5, {alpha: 0}, {alpha: 0.8});
            autoSpinBackground.interactive = true;
            exitAutoSpinPanelArea.interactive = true;
            exitAutoSpinPanelArea.buttonMode = true;
        }
    }
    else {
        if (autoSpinPanelOpened) {
            TweenMax.fromTo(autoSpinPanel, 0.5, {y: 790}, {y: 1780});
            TweenMax.fromTo(autoSpinBackground, 0.2, {alpha: 0.8}, {alpha: 0});
            autoSpinBackground.interactive = false;
            exitAutoSpinPanelArea.interactive = false;
            exitAutoSpinPanelArea.buttonMode = false;
        }
        else {
            TweenMax.fromTo(autoSpinPanel, 0.5, {y: 1780}, {y: 790});
            TweenMax.fromTo(autoSpinBackground, 0.2, {alpha: 0}, {alpha: 0.8});
            autoSpinBackground.interactive = true;
            exitAutoSpinPanelArea.interactive = true;
            exitAutoSpinPanelArea.buttonMode = true;
        }
    }

    autoSpinPanelOpened = !autoSpinPanelOpened;
}

function isAutoSpinCriteriaAchieve() {
    return (autoSpinData.numbered === 0 &&
        autoSpinData.winningConditions === 0 &&
        autoSpinData.singleWin === 0 &&
        autoSpinData.cashIncrease === 0 &&
        autoSpinData.cashDecrease === 0);
}

function updateText() {
    autoSpinSlider1Placeholder.text = (state.get('currency') + ' ' + (state.get('singleWin') / parseInt(state.get('singleWin')) === 1 ? state.get('singleWin') : parseFloat(state.get('singleWin')).toFixed(2)));
    autoSpinSlider2Placeholder.text = (state.get('currency') + ' ' + (state.get('cashIncrease') / parseInt(state.get('cashIncrease')) === 1 ? state.get('cashIncrease') : parseFloat(state.get('cashIncrease')).toFixed(2)));
    autoSpinSlider3Placeholder.text = (state.get('currency') + ' ' + (state.get('cashDecrease') / parseInt(state.get('cashDecrease')) === 1 ? state.get('cashDecrease') : parseFloat(state.get('cashDecrease')).toFixed(2)));
}

function updateSlider() {
    let singleSliderOption = [];
    let increaseSliderOption = [];
    let decreaseSliderOption = [];

    let bigTotalBet = new Big(utils.getTotalBet());

    for (let i = 0; i < sliderOptions.length; i++) {
        const sliderNum = parseFloat(bigTotalBet.times(autoSpinState.singleWinMultiplier).times(i + 1).plus(bigTotalBet.times(autoSpinState.singleWinBaseNumber))); // normal: totalBet * 10 * unit ; For Steampunk case (normal: totalBet * multiplier * unit) + (Initial Value)
        singleSliderOption.push(sliderNum);

        const sliderNum2 = parseFloat(bigTotalBet.times(100).times(i + 1));
        increaseSliderOption.push(sliderNum2);

        const sliderNum3 = parseFloat(bigTotalBet.times(5).times(30).div(10).add(bigTotalBet.times(30).div(100).times(i)));
        decreaseSliderOption.push(sliderNum3);
    }

    singleWinSlider.setRange(singleSliderOption);
    cashIncreaseSlider.setRange(increaseSliderOption);
    cashDecreaseSlider.setRange(decreaseSliderOption);
    singleWinSlider.setToMin();
    cashIncreaseSlider.setToMin();
    cashDecreaseSlider.setToMin();
    state.set('singleWin', singleSliderOption[0]);
    state.set('cashIncrease', increaseSliderOption[0]);
    state.set('cashDecrease', decreaseSliderOption[0]);

    if (stopSingleWinLabelToggle)
        autoSpinData.singleWin = state.get('singleWin');
    if (stopCashIncreaseLabelToggle)
        autoSpinData.cashIncrease = state.get('cashIncrease');
    if (stopCashDecreaseLabelToggle)
        autoSpinData.cashDecrease = state.get('cashDecrease');
    updateText();
}

export default {
    init,
    autoSpinPanelShowHide,
    allowAutoSpinStartButton,
    cannotAutoSpinStartButton,
    updateSlider,
    callStartAutoSpin,
}
