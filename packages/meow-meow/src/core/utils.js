/**
 * @module utils
 */

import { map, each } from 'lodash';
import state from './state';
import Big from 'big.js';

/**
 * Convert a number to string with 2 decimals
 * @returns {string} A number string with 2 decimals
 */
function showAsCurrency(data) {
    let x = new Big(data);
    let y = parseInt(x.times(100));
    let amountAfterFloor = parseFloat((y / 100).toFixed(2));
    return amountAfterFloor.toLocaleString('en',
        {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }
    );
}

/**
 * Get specific query param from URL
 * @param {string} key The name of the query param that you want to get
 * @returns {string} The value
 * */
function getQueryParam(key) {
    const query = window.location.search.substring(1);
    const vars = query.split('&');
    for (let i = 0; i < vars.length; i++) {
        const pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) === key) {
            return decodeURIComponent(pair[1]);
        }
    }
}

/**
 * Create a defer object which contains a promise, resolve and reject
 * @returns {object} Defer object
 **/
function defer() {
    let deferred = {};
    deferred.promise = new Promise(function (resolve, reject) {
        deferred.resolve = resolve;
        deferred.reject = reject;
    });
    return deferred;
}

/**
 * A function to help run promises 1 by 1
 *
 **/
function serial(tasks, fn) {
    return tasks.reduce((promise, task) => promise.then(previous => fn(task, previous)), Promise.resolve(null))
}

/**
 * Shuffle an array with "Fisher–Yates shuffle algorithm"
 * @param {array} array An array that you want to shuffle
 * @returns {array} The array after shuffle
 **/
function shuffleArray(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

/**
 * Pass in a path (array or string) and the slots mapping, it will return
 * @param {array|string} path The layout codes (a1, b2, c3)
 * @param {array} slots Reel items mapping in all reels.
 * @returns {array} An array that contains every global position of the slots item
 **/
function codeToPosition(path, slots) {

    if (typeof path === 'string') {
        path = path.split(', ');
    }

    return map(path, (code) => {
        const rowAndColumn = getRowAndColumn(code);
        return slots[rowAndColumn.column][rowAndColumn.row].container.getGlobalPosition();
    });
}

/**
 * Pass in a code and it will return a RowAndColumn object
 * @description Need to excluded virtual row from container when using this function
 * @param {string} code A layout code
 * @returns {object} An object contains row and column
 **/
function getRowAndColumn(code) {
    let humanLayout = [
        ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7'],
        ['b1', 'b2', 'b3', 'b4', 'b5', 'b6', 'c7'],
        ['c1', 'c2', 'c3', 'c4', 'c5', 'b6', 'c7']
    ];

    let column, row;
    for (let i = 0; i < humanLayout.length; i++) {
        for (let j = 0; j < humanLayout[i].length; j++) {
            if (code === humanLayout[i][j]) {
                column = j;
                row = i;
                break;
            }
        }
    }
    return {row, column};
}

/**
 * Pass in a RowAndColumn object and it will return a layout code.
 * @param {object} rowAndColumn RowAndColum object that you want to know its layout code
 **/
function getCodeByRowAndColumn(rowAndColumn) {
    let humanLayout = [
        ['a1', 'a2', 'a3', 'a4', 'a5'],
        ['b1', 'b2', 'b3', 'b4', 'b5'],
        ['c1', 'c2', 'c3', 'c4', 'c5'],
    ];

    return humanLayout[rowAndColumn.row][rowAndColumn.column];
}

function checkKeyInArray(array, key) {
    let locationOfKey = [];
    for (let row = 0; row < array.length; row++) {
        for (let column = 0; column < array[0].length; column++) {
            if (array[row][column] === key) {
                let detectLocation = {
                    'row': row,
                    'column': column,
                }
                locationOfKey.push(detectLocation);
            }
        }
    }
    return locationOfKey;
}

function responseSlotsToReelSlots(slots) {
    const reelSlots = [];

    for (let i = 0; i < slots[0].length; i++) {
        reelSlots[i] = [];
        for (let j = 0; j < slots.length; j++) {
            reelSlots[i].push(slots[j][i]);
        }
    }

    return reelSlots;
}

function getMatchesPathPassed(matches, totalRow, totalCol) {
    let tempMap = [];

    // init boolean array
    for (let row = 0; row < totalRow; row++) {
        let columns = [];
        for (let col = 0; col < totalCol; col++) {
            let column = false;
            columns.push(column);
        }
        tempMap.push(columns);
    }

    // identify passed path
    each(matches, (match) => {
        let splitPath = match.path.split(', ');
        each(splitPath, (position) => {
            const row = position.charAt(1) - 1;
            const col = alphabetsToIndex(position.charAt(0));
            tempMap[row][col] = true;
        });
    });

    return tempMap;
}

function alphabetsToIndex(code) {
    if (typeof code !== 'string') {
        throw new Error('alphabetsToIndex: parameter need to be string');
    }

    code.replace(/\W/g, ''); // non-alphanumeric filter;

    if (code.length > 1) {
        console.warn('alphabetsToIndex: Only convert first chracter of the string');
    }

    if (code.charAt(0) === code.charAt(0).toLowerCase()) { //lower case condition
        return code.charCodeAt(0) - 97;
    }
    else if (code.charAt(0) === code.charAt(0).toUpperCase()) { //upper case condition
        return code.charCodeAt(0) - 65;
    }
}

function reverseAddonKey(addonArray) {
    let newDataAddon = [];
    each(addonArray, () => {
        newDataAddon.push([]);
    });
    if (addonArray !== undefined) {
        for (let i = 0; i < addonArray[0].length; i++) {
            let keyInALine = [];
            // get the key that required reversing
            for (let j = (addonArray.length - 1); j >= 0; j--) {
                if (addonArray[j][i]) {
                    keyInALine.push(addonArray[j][i]);
                }
            }

            //if key is not full, fill it with null
            addNulltoKey(keyInALine, addonArray);

            // re-arrange key into original slot
            each(keyInALine, (key, index) => {
                newDataAddon[index].push(key)
            });
        }

    }

    return newDataAddon;
}

function addNulltoKey(keyInALine, addonArray) {
    if (keyInALine.length < addonArray.length) {
        keyInALine.push(null);
        addNulltoKey(keyInALine, addonArray)
    }
}

function stringArrayToString(data) {
    let processedString = '';
    each(data, (components) => {
        each(components, (component) => {
            let temp = `${component}`; // to make sure data turn to string;
            processedString = processedString.concat(temp);
        });
    });

    return processedString.replace(/\W/g, ''); // non-alphanumeric filter;;
}

function getTotalBet() {
    let betAmount = new Big(state.get('betAmount'));
    return parseFloat(betAmount.times(state.get('betLine'))).toFixed(2);
}

function getObjectCenterPoint(object) {
    let centerPoint = {
        x: (object.getBounds().left + object.getBounds().right) / 2,
        y: (object.getBounds().top + object.getBounds().bottom) / 2
    };

    return centerPoint;
}

function normalize2DVector(target) {
    let length = Math.sqrt(Math.pow(target.x, 2) + Math.pow(target.x, 2));

    return {x: target.x / length, y: target.y / length};
}

function getArrayIndexNumByName(array, targetName = null) {
    // if (!isArray(array) || !targetName) {
    //     throw new Error('Invalid input');
    // }

    let targetIndex = null;

    each(array, (item, index) => {
        if (item.name === targetName) {
            targetIndex = index;
        }
    });

    if (!targetIndex) {
        throw new Error('cannot find object index');
    }
    else {
        return targetIndex;
    }
}

function showWinTotem(slotReels, matchRowCol, winTotemAmount, longTotem = false) {

    each(slotReels, (reel) => {
        each(reel.reelItems, (item) => {
            item.container.alpha = 0.5;
        })
    });
    each(matchRowCol, (position, index) => {
        if (index < winTotemAmount) {
            slotReels[position.column].reelItems[position.row + 1].container.alpha = 1;
            if (longTotem) {
                // check top
                if (slotReels[position.column].reelItems[position.row + 1].symbol === 'x' && ((slotReels[position.column].reelItems[position.row].symbol === 'x') || position.row === 0)) {
                    slotReels[position.column].reelItems[position.row].container.alpha = 1;
                }
                // check bottom
                if (slotReels[position.column].reelItems[position.row + 1].symbol === 'x' && ((slotReels[position.column].reelItems[position.row + 2].symbol === 'x') || position.row === 2)) {
                    slotReels[position.column].reelItems[position.row + 2].container.alpha = 1;
                }
            }
        }
    })
}

function randomRange(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function replaceString(originalText, textNeedReplace, textToReplace) {
    if (typeof textToReplace != 'string') {
        return 'textToReplace must be string'
    }
    if (typeof textNeedReplace != 'string') {
        return 'textNeedReplace must be string'
    }
    if (typeof originalText != 'string') {
        return 'originalText must be string'
    }

    let newString = originalText.replace(textNeedReplace, textToReplace);
    return newString;
}

function changeObjectLayer(object, frontLayerName, container) {
    if (!object || !frontLayerName || !container) {
        throw new Error('Invalid Input');
    }

    container.removeChild(object);

    let targetIndex = getArrayIndexNumByName(container.children, frontLayerName);

    container.addChildAt(object, targetIndex);
}

function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '0x';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

export default {
    showAsCurrency,
    getQueryParam,
    defer,
    serial,
    shuffleArray,
    codeToPosition,
    getRowAndColumn,
    getCodeByRowAndColumn,
    checkKeyInArray,
    responseSlotsToReelSlots,
    getMatchesPathPassed,
    alphabetsToIndex,
    reverseAddonKey,
    stringArrayToString,
    getTotalBet,
    getObjectCenterPoint,
    normalize2DVector,
    getArrayIndexNumByName,
    showWinTotem,
    randomRange,
    replaceString,
    changeObjectLayer,
    getRandomColor,
}
