import * as PIXI from 'pixi.js';
import {map, each, find, sortBy} from 'lodash';
import Particle from './particle';

export const Spine = PIXI['spine'].Spine;

export default class Emitter {
    protected container: PIXI.Container = new PIXI.Container();
    protected options: any;
    protected particleOptions: any;
    protected spawnPoint: any;
    protected emitTicker = new PIXI.ticker.Ticker();

    constructor(emitterOptions: any, particleOptions: any) {
        if (!emitterOptions) {
            throw new Error('Emitter class requires an emitter option');
        }

        if (!particleOptions) {
            throw new Error('Emitter class requires a particle option');
        }

        // Assign options accordingly to what has been specified by the user
        this.options = Object.assign({
            emitterX: 100,
            emitterY: 150,

            emitFrequency: 5 // todo it shall be the higher the number , the more particles emit per tick ?
        }, emitterOptions);
        this.particleOptions = particleOptions;

        // Create emitter container
        this.container.name = 'Emitter Container';
        this.container.x = 0;
        this.container.y = 0;

        this.spawnPoint = new PIXI.Sprite(PIXI.Texture.EMPTY);
        this.setSpawnPosition({x: this.options.emitterX, y: this.options.emitterY});
        this.container.addChild(this.spawnPoint);
    }

    start() {
        let emitInterval = 0;

        this.emitTicker.add((deltaTime) => {
            emitInterval += deltaTime;

            if (emitInterval > this.options.emitFrequency) {
                emitInterval = 0;

                // We do this because there is an suspected error with PIXI.ticker.Ticker's scope
                // Not declaring the parameter pass into the new particle class will result in the class
                // returning us an empty container
                let particleOptions = this.particleOptions;

                // todo fix this with position passed in by users
                particleOptions.position = {
                    x: this.spawnPoint.x,
                    y: this.spawnPoint.y,
                };

                let particle;
                if (this.particleOptions.animatedSprite) {
                    particle = new Particle({
                        animatedSprite: new PIXI.extras.AnimatedSprite(particleOptions.animatedSprite.textures),
                        animationSpeed: particleOptions.animatedSprite.animationSpeed,
                        gravity: particleOptions.gravity,
                        speed: particleOptions.speed,
                        lifeSpan: particleOptions.lifeSpan,
                        position: particleOptions.position,
                        alpha: particleOptions.alpha,
                        scale: particleOptions.scale,
                    });
                } else if (this.particleOptions.spine) {
                    particle = new Particle({
                        spine: new PIXI.spine.Spine(particleOptions.spine.spineData),
                        animationName: particleOptions.spine.animationName,
                        loop: particleOptions.spine.loop,
                        gravity: particleOptions.gravity,
                        speed: particleOptions.speed,
                        lifeSpan: particleOptions.lifeSpan,
                        position: particleOptions.position,
                        alpha: particleOptions.alpha,
                        scale: particleOptions.scale,
                    });
                }

                particle.update();

                this.container.addChild(particle.getContainer());
            }
        });

        this.emitTicker.start();
    }

    stop() {
        this.emitTicker.stop();
        this.container.destroy();
    }

    getContainer() {
        return this.container;
    }

    setSpawnPosition(positions: any) {
        if (typeof positions.x !== 'number' || !positions.x) {
            return 0;
        }

        this.spawnPoint.x = positions.x;
        this.spawnPoint.y = positions.y;
    }

    getSpawnPosition() {
        let spawnPosition = {
          x: this.spawnPoint.x,
            y: this.spawnPoint.y
        };

        return spawnPosition;
    }
}