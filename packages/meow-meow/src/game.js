import * as PIXI from 'pixi.js';
import 'pixi-spine';
import 'pixi-particles';
import 'gsap';
import { each } from 'lodash';

import app from './core/app';
import api from './core/api';
import state from './core/state';
import utils from './core/utils';
import sound from './core/sound';
import panel from './core/panel';
import info from './core/info';
import tutorial from './core/tutorial';
import particlesCoin from './core/particlesCoin';
import extraInfoService from './core/extraInfoService';
import { Reel } from './core/SlotEngine/reel/Reel';
import { ReelGroup } from './core/SlotEngine/reel-group/ReelGroup';
import Big from 'big.js';

let loaded = PIXI.loader.resources;
let isMobile = app.isMobile();

let reelGroup;
let totalReel = 3;

let animationCharacter;

let uiPanel;
let infoPanel;

const animationArray = [
    'blink',
    'feet_movement',
    'head_tillting',
    'money_come',
    'tail',
    'a_origin_still',
    'win01',
    'win02',
    'win03',
];

let winGlow = [];
let speed = 1;

export function init() {
    let animationWinGlow = [
        loaded.winGlow1.texture,
        loaded.winGlow2.texture,
        loaded.winGlow3.texture,
        loaded.winGlow4.texture,
    ];

    //#region Init Game State
    state.set('betAmount', 1);
    state.set('betLine', 1);
    //#endregion

    //#region Assets Initialize

    //#region Background
    let backgroundSpine = new PIXI.spine.Spine(loaded.bg.spineData);
    backgroundSpine.autoUpdate = true;
    backgroundSpine.x = app.getResolutionX() / 2;
    backgroundSpine.y = app.getResolutionY() / 2;
    backgroundSpine.state.setAnimation(0, 'idle_1', true);
    backgroundSpine.state.addListener({
        complete: function (data) {
            if (data.animation.name === 'win_1') {
                backgroundSpine.state.setAnimation(0, 'idle_1', true);
            }
        }
    });

    let slotFrame = new PIXI.Sprite(loaded.slotFrame.texture);
    slotFrame.x = isMobile ? 15 : 385;
    slotFrame.y = isMobile ? 195 : 105;

    //#endregion

    let slotBackground = [];
    for (let i = 0; i < totalReel; i++) {
        slotBackground[i] = new PIXI.Sprite(loaded.slotBackground.texture);
        slotBackground[i].anchor.set(0.5);
        slotBackground[i].x = isMobile ? 130 + (i * 235) : 495 + (i * 235);
        slotBackground[i].alpha = 1;
        slotBackground[i].y = isMobile ? 430 : 340;

        winGlow[i] = new PIXI.extras.AnimatedSprite(animationWinGlow);
        winGlow[i].anchor.set(0.5);
        winGlow[i].x = isMobile ? 130 + (i * 235) : 495 + (i * 235);
        winGlow[i].alpha = 0;
        winGlow[i].y = isMobile ? 430 : 340;
        winGlow[i].animationSpeed = 0.2;
        winGlow[i].gotoAndPlay(i);
    }

    //#endregion Assets Initialize
    //animation

    animationCharacter = new PIXI.spine.Spine(loaded.characterJson.spineData);
    animationCharacter.autoUpdate = true;
    animationCharacter.scale.set(0.6);

    animationCharacter.x = isMobile ? 370 : 225;
    animationCharacter.y = isMobile ? 935 : 500;

    let logo = new PIXI.Sprite(loaded.logo.texture);
    logo.name = 'Logo';
    logo.x = isMobile ? 195 : 52;
    logo.y = isMobile ? 35 : 10;

    //animation for looping another random animation after one animation is finish.
    animationCharacter.state.addListener({
        complete: function () {
            let tempCharacterAnimation = Math.floor(Math.random() * 5);
            animationCharacter.state.setAnimation(0, animationArray[tempCharacterAnimation], false);
            for (let i = 0; i < 3; i++) {
                winGlow[i].alpha = 0;
            }
        },
    });

    animationCharacter.state.setAnimation(0, animationArray[0], false);

    const totems = {
        'a': loaded.totemA.spineData,
        'b': loaded.totemB.spineData,
        'c': loaded.totemC.spineData,
        'd': loaded.totemD.spineData,
        'e': loaded.totemE.spineData,
        'f': loaded.totemF.spineData,
        'g': loaded.totemG.spineData,
        'h': loaded.totemH.spineData,
    };

    let slots = [];

    for (let i = 0; i < totalReel; i++) {
        let slot = new Reel({
            rows: 3,
            offset: 180,
            x: isMobile ? 125 + (i * 240) : 497 + (i * 238), //663
            y: isMobile ? 620 - 180 : (app.getResolutionY() / 2 - 20) + 185 - 180,
            totemYReset: isMobile ? -app.getResolutionY() / 2 + 200 : 0,
            isSpine: true,
        }, totems);

        slots.push(slot);
    }

    reelGroup = new ReelGroup(slots);

    if (state.get('slotData')) {
        each(reelGroup.reels, (slot, index) => {
            let setSlot = utils.responseSlotsToReelSlots(state.get('slotData'));
            let convertedCombination = [];
            each(setSlot, reel => {
                let convertedReel = [];
                each(reel, (item) => {
                    if (item !== 'z')
                        convertedReel.push(item)
                });
                if (convertedReel.length < 3) {
                    convertedReel.splice(0, 0, 'z');
                }
                convertedCombination.push(convertedReel)
            })

            slot.set(convertedCombination[index])
        });
    }

//#region addComponent
    for (let i = 0; i < 3; i++) {
        app.addComponent(slotBackground[i]);
    }
    app.addComponent(reelGroup.getContainer());
    app.addComponent(backgroundSpine);
    app.addComponent(slotFrame);

    for (let i = 0; i < 3; i++) {
        app.addComponent(winGlow[i]);
    }

    app.addComponent(animationCharacter);
    app.addComponent(logo);

    extraInfoService.init(10, isMobile ? 80 : 10);

//#endregion
    // init particle emitter
    particlesCoin.init();

    uiPanel = panel.init({
        spinFn: gameSpin,
        spin100Fn: gameSpin100,
        reAdjustWallet: 20,
        spinAreaConfigX: isMobile ? 360 : 0,
        spinAreaConfigY: isMobile ? 430 : 0,
        spinAreaConfigWidth: isMobile ? 720 : 0,
        spinAreaConfigHeight: isMobile ? 460 : 0,
        autoSpinCountdownScale: isMobile ? 0.8 : 0.9,
        betLinesUsage: false,
        gotBonusOffset: false,
        primaryTint: '0xe00e13',
        autoSpinPortraitColor: '#cf0e2f',
        gameId: 1,
    });

    uiPanel.on('infoButtonClicked', () => {
        infoPanel.showInfoPage();

        //!invisible panel when info panel on
        if (!isMobile) {
            uiPanel.alpha = 0;
        }
    });

    uiPanel.on('gameRulesButtonClicked', () => {
        if (isMobile) infoPanel.showGameRules();
    });


    uiPanel.on('turboButtonClicked', (speedUp) => {
        speed = speedUp ? 3 : 1;

        for (let i = 0; i < 3; i++) {
            reelGroup.reels[i].setAccelerationSpeed(speed);
        }
    });

    let pages;
    let gameRulePages;
    if (app.isMobile()) {
        pages = [
            {
                type: 'text',
                fontSize: 36,
                content: loaded.languageFile.data.payout,
            },
            {
                type: 'sprite',
                content: loaded.infoPayout.texture,
            },
            {
                type: 'pageBreak'
            },
        ];

        gameRulePages = [
            {
                type: 'sprite',
                sprite: loaded.gameRules.texture
            }
        ]
    }
    else {
        pages = [
            {
                title: loaded.languageFile.data.payout,
                pageTexture: loaded.infoPayout.texture,
                textureOffsetY: 15,
                textureOffsetX: 5,
                text: '',
            },
        ];

        gameRulePages = [
            {
                title: loaded.languageFile.data.game_overview_title,
                sprite: loaded.gameRules1.texture
            },
            {
                title: loaded.languageFile.data.autospin_options_title,
                sprite: loaded.gameRules2.texture
            },
            {
                title: loaded.languageFile.data.game_functions_title,
                sprite: loaded.gameRules4.texture
            },
            {
                title: loaded.languageFile.data.game_functions_title,
                sprite: loaded.gameRules5.texture
            },
        ];
    }

    infoPanel = info.init({
        gameTitle: loaded.gameLanguageFile.data.game_title,
        pages: pages,
        gameRulesPages: gameRulePages,
        multipleLine: false,
        betLevel: true,
        primaryTint: '0xe00e13',
    });
    infoPanel.hideInfoPage();
    if (isMobile) {
        infoPanel.hideGameRules();
    }

    if (!isMobile) {
        infoPanel.on('visiblePanel', () => { //! visible the panel after info panel close
            uiPanel.alpha = 1.0;
        });
    }

    //init tutorial
    let tutorialOptions = {
        cursorRoutine: [
            {'showCursor': false, 'x': 380, 'y': 1080},
            {'showCursor': true, 'x': 380, 'y': 1080},
            {'showCursor': true, 'x': 380, 'y': 500},
            {'showCursor': true, 'x': 600, 'y': 1110},
            {'showCursor': true, 'x': 170, 'y': 1110},
            {'showCursor': true, 'x': 240, 'y': 1230},
            {'showCursor': true, 'x': 530, 'y': 1230},
            {'showCursor': false, 'x': 670, 'y': 1230},
            {'showCursor': true, 'x': 670, 'y': 1230},
            {'showCursor': true, 'x': 90, 'y': 1230},
            {'showCursor': false, 'x': 550, 'y': 680},
            {'showCursor': true, 'x': 550, 'y': 680},
            {'showCursor': true, 'x': 280, 'y': 250},
            {'showCursor': false, 'x': 0, 'y': 0},
            {'showCursor': false, 'x': 0, 'y': 0},
            {'showCursor': false, 'x': 0, 'y': 0},
        ],
    };
    tutorial.init(tutorialOptions);
    let preEventList = [];

    let tutorialToken = state.get('token') + (isMobile ? 'p' : 'l');
    if (!localStorage.getItem(tutorialToken)) {
        preEventList.push(tutorial.show);
    }

    panel.runPreEvents(preEventList);
}

export function initSound() {
    sound.addMusic('bgMusic', loaded.bgMusic.sound, {loop: true, volume: 1});

    sound.addSound('slotSpinningSound', loaded.slotSpinningSound.sound, {loop: true, volume: 1});
    sound.addSound('slotSpinningStopSound', loaded.slotSpinningStopSound.sound, {loop: false, volume: 1});
    sound.addSound('smallWinSound', loaded.smallWinSound.sound, {loop: false, volume: 1});
    sound.addSound('bigWinSound', loaded.bigWinSound.sound, {loop: false, volume: 1});
    sound.addSound('winMoneySound', loaded.moneySound.sound, {loop: false, volume: 1});

    sound.play('bgMusic');
}

function gameSpin(isAutoPlay = false) {
    //! Stopping the totem animation if they have been played because of winning.
    each(reelGroup.reels, (slot, index) => {
        TweenMax.delayedCall((0.25 * index) / speed, () => {
            slot.start();
        });
    });

    setTimeout(() => { sound.play('slotSpinningSound'); }, speed > 1 ? 100 : 350);

    // wait at least 2.5 seconds only stop the slots to prevent API return too fast
    return Promise.all([
        panel.beforeSpinApi(isAutoPlay),
        new Promise((resolve) => {
            setTimeout(resolve, 1000 / speed);
        })
    ]).then(([data]) => {

        let convertedCombination = [];
        each(utils.responseSlotsToReelSlots(data.slots), reel => {
            let convertedReel = [];
            each(reel, (item) => {
                if (item !== 'z')
                    convertedReel.push(item)
            });
            if (convertedReel.length < 3) {
                convertedReel.splice(0, 0, 'z');
            }
            convertedCombination.push(convertedReel)
        });

        const stopPromises = [];

        if (data.extra) {
            extraInfoService.updateExtraInfo(data.extra);
        }
        // stop every reel 1 by 1
        each(reelGroup.reels, (slot, index) => {

            const stopPromise = new Promise((resolve) => {
                setTimeout(() => {
                    if(index === 2) { // to match the timing of spin stop sound and spinning sound
                        setTimeout(() => { sound.stop('slotSpinningSound'); }, speed > 1 ? 350 : 650);
                    }

                    slot.stop(convertedCombination[index]).then(() => {
                        resolve();
                    })
                }, 985 * index / (speed < 2 ? 2.04 : 2.45));
            });

            stopPromises.push(stopPromise);
        });

        // when all reels stopped and stop the spinning sound
        return Promise.all(stopPromises).then(() => {

            return (new Promise((resolve) => {
                checkWin(data).then((payout) => {
                    resolve(payout);
                });
            }));
        });
    });
}

function gameSpin100(apiData) {
    // start spin
    for (let i = 0; i < totalReel; i++) {
        reelGroup.reels[i].start();
    }
    sound.play('slotSpinningSound');

    return api.spin100(apiData).then((data) => {
        let bigTotalWin = new Big(data.total_win);
        let isMegaWin = (parseFloat(bigTotalWin.div(utils.getTotalBet()).div(100)) >= 2);
        particlesCoin.emitCoins(isMegaWin ? 'mega' : 'big');

        let converted100Combination = [];
        each(utils.responseSlotsToReelSlots(data.spins[data.spins.length - 1].slots), reel => {
            let convertedReel = [];
            each(reel, (item) => {
                if (item !== 'z')
                    convertedReel.push(item)
            });
            if (convertedReel.length < 3) {
                convertedReel.splice(0, 0, 'z');
            }
            converted100Combination.push(convertedReel)
        });

        sound.stop('bgMusic');

        if (data.extra) {
            extraInfoService.updateExtraInfo(data.extra);
        }

        setTimeout(() => {
            each((reelGroup.reels), (reel, index) => {
                setTimeout(() => {
                    reel.stop(converted100Combination[index]);
                }, 100 * index);
            });
            particlesCoin.stopAllEmit();
            sound.stop('slotSpinningSound');
            sound.play('bgMusic');
        }, 3000);
        return data;
    });
}

//! Accepts rowID and checks once all 3 rowIDs have been passed into the function. Accepts tail to play win animation if any.
function checkWin(data) {

    const totalWin = data.total_win;

    state.set('balance', data.balance);

    if (totalWin === 0) {
        return new Promise((resolve) => {
            resolve(0);
        })
    }

    // Call win detection to change win text in panel
    let displayData = {
        amount: totalWin,
        text: isMobile ? loaded.languageFile.data.winC : loaded.languageFile.data.win
    };
    panel.displayWallet(displayData);

    return new Promise((resolve) => {
        TweenMax.delayedCall(0.35, function () {
            //set character win animation
            let tempCharacterWinAnimation = 6 + Math.floor(Math.random() * 3);
            animationCharacter.state.setAnimation(0, animationArray[tempCharacterWinAnimation], false);

            for (let i = 0; i < 3; i++) {
                winGlow[i].alpha = 1;
            }

            // update win text
            panel.updateText();

            //play totems win animation
            let nonVirtualSlots = reelGroup.getReelsWithoutVirtualRows();
            // reset all totem to idle animation to prevent overlap win animation
            each(nonVirtualSlots, (slots) => {
                each(slots, (slot, index) => {
                    if (index !== 1) {
                        return;
                    }
                    slot.playIdle('idle_1', false);
                    slot.playIdle('win_1', false);
                });
            });

            let bigTotalWin = new Big(data.total_win)
            let winTypeVariable = parseFloat(bigTotalWin.div(utils.getTotalBet()));

            if (winTypeVariable >= 25) {
                let winType = (winTypeVariable < 50) ? 'big' : 'mega';
                sound.play('bigWinSound');

                panel.playWinEffect(winType, 0, totalWin).then(() => {
                    setTimeout(() => {
                        resolve(totalWin);
                    }, 800 / speed);
                });

                particlesCoin.emitCoins(winType);
                setTimeout(() => {
                    particlesCoin.stopAllEmit();
                }, 1300);
            }
            else {
                let centerPoint = utils.getObjectCenterPoint(reelGroup.getContainer());
                panel.displayWinAmountWithSprites({
                    xPosition: centerPoint.x,
                    yPosition: centerPoint.y - 120,
                    tweenToY: -260,
                    accelerationSpeed: 0.5,
                    tweenAlpha: 2,
                }, 3, totalWin);


                sound.play('smallWinSound');
                sound.play('winMoneySound');

                setTimeout(() => {
                    resolve(totalWin);
                }, 1500 / speed);
            }

        });
    });
}

export default {
    init,
    initSound
}
