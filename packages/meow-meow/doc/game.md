# game.js
This document will write down any notes related with `game.js`.
It provides `init()` and `initSound()`, all other functions are optional and can be designed based on the game requirement.

1. **Assets Initialize** - all components should init here.
1. **Buttons Initialize** - all buttons should init here. make sure all listeners are set.
1. **Slots Initialize** - create slots based on game design.
1. **Text Initialize** - all text which static or dynamic should init here.
1. **App Display Region** - add all above components, buttons, slots and text to stage.
1. **Bonus Initialize** - use `bonus.init()` to init the bonus game.
1. **Init Game State** - set any variable or trigger API to start the game.

Every Initialization should have placement at the end of the section. 
